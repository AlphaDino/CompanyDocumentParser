 

 

 

 

GDF SUEZ CC

Société coopérative 4 responsabilité limitée

 

 

STATUTS

16 décembre 2010

 

 
 

 

GDF SUEZ CC

Société coopérative 4 responsabilité limitée

 

 

 

 

 

 

Siége social : Place du Tréne 1 - 1000 Bruxelles
Numéro d'entreprise : 0442.100.363

Société constituée par acte regu par Maitre Jean-Luc INDEKEU, notaire 4 Bruxelles, le
31 juillet 1990, publié a l'Annexe au Moniteur belge du 19 octobre 1990 (n° 901019-134).

Statuts modifiés par actes des :

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Dates des actes Dates des publications N° des publications
a l'Annexe au Moniteur belge

24.06.1992 05.08.1992 920805-101
04.01.1993 10.02.1993 930210-283
31.07.1996 22.08.1996 960822-266
28.11.1996 25.12.1996 961225-164
02.01.1997 06.02.1997 970206-410
06.03.1997 05.04.1997 970405-182
26.03.1997 23.04.1997 970423-364
25.07.1997 26.08.1997 970826-13
22.04.1999 20.05.1999 990520-35
13.08.1999 04.09.1999 990904-168
14.03.2000 22.04.2000 20000422-469
19.07.2002 02.09.2002 20020902-111487
14.08.2002 09.09.2002 20020909-113148
24.12.2002 03.02.2003 015287
30.09.2004 23.11.2004 04160176
20.12.2007 21.01.2008 08011674
27.02.2009 16.03.2009 09038461
17.07.2009 12.08.2009 09115632
16.12.2010 06.01.2011 11002763

 

 

 

 

 
STATUTS COORDONNES

TITREI
FORME - DENOMINATION - OBJET - SIEGE - DUREE

ARTICLE 1

La société est une société coopérative 4 responsabilité limitée. Elle a pour dénomination

" GDF SUEZ CC".

ARTICLE 2

La société a pour objet exclusif, en Belgique et a l'étranger, la coordination, le développement

et la centralisation, au seul profit de tout ou partie des sociétés faisant partie du méme groupe

que la présente société, des activités suivantes ou de certaines d'entre elles :

centralisation des opérations financiéres, de couverture des risques découlant des
fluctuations des taux de change et des taux d’intérét ;

gestion des assurances et des risques;

relations non commerciales avec les autorités nationales et internationales;

centralisation de travaux administratifs (y compris juridiques, fiscaux et comptables), et
de travaux dans le domaine de |’informatique;

centralisation de campagnes de publicité, analyses de marchés et de techniques de vente,
fourniture et rassemblement d'informations ;

assistance 4 la gestion, notamment en matiéres financiére, de documentation, de
communication, de stratégie, de restructurations, de planification et de développement ;
ainsi que toutes activités ayant un caractére préparatoire ou auxiliaire pour les sociétés du

groupe.

La société peut mener son objet 4 bien, directement ou indirectement, en nom propre ou pour

compte de tiers, en accomplissant en Belgique et a l'étranger toutes opérations mobiliéres,

immobiliéres, commerciales, civiles ou financiéres, en ce compris via la détention d'actions

ou autres parts représentatives de droits sociaux, de nature 4 favoriser son objet ou celui des

sociétés, entreprises ou associations auxquelles elle préte son assistance.
ARTICLE 3

Le siége social est établi dans la région de Bruxelles. Il est actuellement fixé 4 Bruxelles, 1,
Place du Tréne.

Il peut étre transféré en tout autre endroit de cette région par décision du conseil

d'administration.

ARTICLE 4
La société peut établir, par décision du conseil d'administration, des siéges administratifs,
siéges d'opération, siéges d'exploitation, succursales ou agences, tant en Belgique qu'a

l'étranger.

ARTICLE 5

La durée de la société constituée le trente et un juillet mil neuf cent nonante est illimitée.

TITRE I
CAPITAL SOCIAL-OBLIGATIONS

ARTICLE 6

Le capital est représenté par des parts sociales sans valeur nominale, en nombre illimité,
réparties en parts de catégories spécifiques, désignées sous les lettres A, B, C, D, E, F, G et
ainsi de suite, dont les droits respectifs sont définis par les présents statuts.

Les assemblées générales qui seront appelées a délibérer sur les augmentations du capital
indiqueront dans leurs résolutions les parts ainsi émises et les catégories auxquelles elles
appartiendront.

La partie fixe du capital est fixée a un million deux cent cinquante mille euros (EUR
1.250.000).

En exécution de I’article 36 des présents statuts, la société est structurée en divisions
comptables analytiques distinctes, chaque division rassemblant les résultats engendrés par un
courant d’affaires entre la société et une unité opérationnelle (entreprise spécialisée ainsi que,
le cas échéant, les filiales de cette derniére) du groupe GDF SUEZ.

Chaque division est représentée par une catégorie de parts. Sans préjudice de la participation,

si nécessaire, de l’ensemble des parts aux pertes globales éventuelles de la société, chaque
catégorie de parts bénéficie d’une affectation bénéficiaire spécifique 4 la division
correspondante, faisant l’objet des mise en réserve, report 4 nouveau, paiement de
dividendes et participation a l’actif de liquidation définis aux articles 39, 40 et 43. Chaque
part participe en outre, de la fagon et dans la mesure décrites aux articles 39 et 43, au résultat

global et a l’actif global de liquidation de la société, toutes divisions confondues.

ARTICLE 7

Le capital social peut étre augmenté ou réduit, en une ou plusieurs fois, sans modification des
statuts, par l'assemblée générale délibérant aux conditions requises pour les modifications des
statuts, l’assemblée pouvant déléguer cette compétence au conseil d’administration. Le
capital ne peut étre réduit 4 moins de sa partie fixe qu’en suivant la procédure prescrite par le
code des sociétés. Ces opérations pourront porter sur l’ensemble des catégories de parts ou
sur une partie d’entre elles seulement.

Les nouvelles parts 4 souscrire en espéces seront en régle offertes aux associés de la catégorie
de parts au sein de laquelle les nouvelles parts sont créées, proportionnellement 4 la partie du
capital que représentent leurs parts. En cas de création de nouvelles parts au sein de plusieurs
catégories, ce droit de préférence a ainsi vocation a s’appliquer a la ou aux catégorie(s) de
parts déja détenue(s) par chaque associé. L'assemblée générale fixe le délai de l'exercice du
droit de préférence. Elle confére au conseil d'administration tous pouvoirs, aux fins de
réaliser les décisions prises et de fixer les conditions de I'exercice du droit de préférence.
Toutefois, par dérogation 4 ce qui précéde, l'assemblée générale peut, dans l'intérét social,
aux conditions requises pour la modification des statuts, moyennant le respect des conditions
de quorum et de majorité des conventions conclues entre l'ensemble des associés, et, s7il
échet, en appliquant par analogie les dispositions pour la modification des droits respectifs

des catégories d’actions des sociétés anonymes, limiter ou supprimer le droit de préférence.
ARTICLE 8

Les libérations 4 effectuer sur les parts non encore entiérement libérées doivent étre faites aux
lieux et aux dates que le conseil d'administration détermine. Les libérations appelées
s'imputent également sur l'ensemble des parts de la catégorie dont l’associé est titulaire et
appartenant a la catégorie faisant l’objet de l’appel de fonds.

Le conseil d'administration peut autoriser les associés 4 libérer leurs parts par anticipation;

dans ce cas, il détermine les conditions auxquelles les libérations anticipées sont admises.

ARTICLE 9

§ 9.1 Sans préjudice des éventuelles conditions de cessibilité imposées par convention
conclue entre l’ensemble des associés, les parts ne sont cessibles a d’autres
cessionnaires que ceux liés ou associés 4 la S.A. GDF SUEZ au sens du code des
sociétés, et en ce cas moyennant |’agrément préalable du conseil d’administration.

En cas de transfert visé ci-dessus, une condition du transfert sera que, si le bénéficiaire
du transfert cesse d'étre une société liée 4 GDF SUEZ S.A., le bénéficiaire du transfert
retransférera (et le cédant s'engagera 4 ce que le bénéficiaire du transfert retransfére),
avant une telle cessation, les parts concernées au cédant ou, au choix du cédant, 4 une
autre société li¢ée 4 GDF SUEZ S.A.

L'admission de nouveaux associés sera constatée par l'apposition de leur signature,
précédée de la date en regard du nom, sur le registre des parts de la société.

§ 9.2.Un associé ne peut ni démissionner, ni opérer de retrait de parts entiérement libérées ou
non, que moyennant l'accord de I'assemblée générale statuant 4 la majorité des
quatre/cinquiémes (4/Sémes) et moyennant le respect des conditions de quorum et de
majorité des conventions conclues entre l'ensemble des associés. La demande doit avoir
été introduite dans les six premiers mois de l'année sociale.

Un associé ne peut opérer de retrait de versements que moyennant l'accord du conseil
d’administration statuant a la majorité simple et moyennant le respect des conditions de
quorum et de majorité des conventions conclues entre l'ensemble des associés.

En cas de démission ou retrait de parts ou encore en cas d’exclusion d’un associé,
l’associé a droit uniquement a recevoir la valeur de ses parts telle qu'elle résultera des
comptes annuels de l'année sociale pendant laquelle la démission ou le retrait de parts
ou encore l’exclusion de l’associé aura eu lieu et relatifs a la division correspondant a

sa catégorie.
Les opérations visées au présent paragraphe ne peuvent porter que sur la partie variable
du capital.

§ 9.3 La société peut acquérir ses propres parts, entiérement libérées ou non, représentant la
partie variable du capital moyennant une décision de l’assemblée générale statuant a la
majorité des quatre/cinquiémes (4/5émes) et moyennant le respect des conditions de
quorum et de majorité des conventions conclues entre l'ensemble des associés.

Cette acquisition doit se faire au moyen de sommes distribuables au sens de l’article
429 du code des sociétés.

Il peut étre procédé, au moment de |’acquisition, a l’annulation de tout ou partie de ces
parts moyennant réduction correspondante de la partie variable du capital.

Si les parts sont conservées en auto-détention, il est constitué une réserve indisponible
d’un montant égal a la valeur d’inventaire des parts acquises. L’auto-détention ne peut
porter sur plus de vingt pour cent des parts et, si elle persiste pendant plus de six mois,
ce pourcentage doit étre ramené a4 dix pour cent ; passé ce délai, il est procédé a
l’annulation du nombre de parts permettant de ramener |’auto-détention au seuil
autorisé ; cette annulation entraine une réduction 4 due concurrence de la partie variable
du capital et de la réserve indisponible.

Les parts en auto-détention n’ont pas le droit de vote.

Le conseil d’administration décide si elles conservent leur droit au dividende lequel
reste en ce cas attaché jusqu’a leur revente ou leur annulation ou si ce droit est
suspendu jusqu’a leur revente, le bénéfice distribuable étant réparti entre les autres
parts de la fagon prescrite par ailleurs aux présents statuts.

La cession de ces parts est décidée par le conseil d’administration.

ARTICLE 10
Le conseil d'administration peut suspendre I'exercice des droits afférents aux parts faisant
l'objet d'une copropriété, d'un usufruit ou d'un gage, jusqu'a ce qu'une seule personne soit

désignée comme étant, a l'égard de la société, propriétaire de ces parts.

ARTICLE 11
La société ne peut émettre d’obligations ni souscrire 4 un quelconque autre emprunt. La
société pourra toutefois contracter des préts participatifs auprés de ses actionnaires,

subordonnés a toute dette.
A aucun moment l’ensemble des engagements pris par la société, y compris les garanties
accordées par elle ainsi que les engagements conditionnels pris par elle, ne pourra excéder le

montant de son capital social et les emprunts participatifs contractés par la société.

TITRE Il
ADMINISTRATION - DIRECTION - REPRESENTATION - CONTROLE

ARTICLE 12

La société est administrée par un conseil d'administration composé de membres nommés
pour six ans au plus par l'assemblée générale et révocables par elle. Un administrateur au
moins est élu sur présentation de chacune des catégories de parts-

Les administrateurs sont rééligibles.

Les fonctions des administrateurs sortants et non réélus prennent fin immédiatement 4 l'issue
de I'assemblée générale ordinaire.

Le conseil d'administration élit en son sein un président et peut élire un ou plusieurs vice-

présidents.

ARTICLE 13
En cas de vacance d'un ou plusieurs mandats d'administrateur, les membres restants du
conseil d'administration peuvent pourvoir provisoirement au remplacement jusqu'a la

prochaine assemblée générale qui procéde a l'élection définitive.

ARTICLE 14
Les administrateurs ne contractent aucune obligation personnelle relativement aux
engagements de la société. Ils sont responsables 4 l'égard de la société de I'exécution de leur

mandat et des fautes commises dans leur gestion.

ARTICLE 15
Le conseil d'administration se réunit au siége social ou a l'endroit indiqué dans les
convocations, chaque fois que l'intérét de la société l'exige et en tous cas, chaque fois que

deux administrateurs au moins le demandent.
Les réunions du conseil, en ce compris les délibérations et votes, peuvent, sur proposition de
son président, étre tenues via tout moyen de télécommunication, notamment oral ou visuel,
qui permette des débats entre des participants géographiquement éloignés. Dans ce cas, la
réunion est réputée se tenir au siége social et tous les administrateurs participants y étre
présents.

Les réunions sont présidées par le président du conseil ou, en cas d'empéchement de celui-ci,

par le vice-président ou, a leur défaut, par un administrateur désigné par ses collégues.

ARTICLE 16

Le conseil ne peut délibérer et statuer que si la majorité de ses membres est présente ou
représentée. Chaque administrateur peut, par tout moyen de transmission ayant comme
support un imprimé et dont l'auteur soit identifié avec une certitude suffisante, donner 4 l'un
de ses collégues pouvoir de le représenter 4 une séance du conseil et d'y voter en ses lieu et
place. Aucun mandataire ne peut représenter ainsi plus de deux administrateurs.

Tout administrateur peut également, mais seulement dans le cas ott la moitié au moins des
membres du conseil sont présents en personne, exprimer ses avis et formuler ses votes par
tout moyen de transmission ayant comme support un imprimé et dont l'auteur soit identifié
avec une certitude suffisante.

Les décisions sont prises 4 la majorité des voix. Le président n’a pas de voix prépondérante.
Les conditions de quorum et de majorité dans les présents statuts peuvent étre modifiées par

des conventions conclues entre l'ensemble des associés.

ARTICLE 17

Les délibérations du conseil d'administration sont constatées par des procés-verbaux signés
par la majorité des membres présents. Ces procés-verbaux sont consignés ou reliés en un
recueil spécial. Les délégations ainsi que les avis et votes donnés conformément a l'article 16
y sont annexés. Les copies ou extraits, 4 produire en justice ou ailleurs, sont signés par deux

administrateurs.

ARTICLE 18
L'assemblée générale peut allouer aux administrateurs, un émolument fixe ou des jetons de

présence 4 porter 4 charge du compte de résultats.
Le conseil d'administration est autorisé également a accorder aux administrateurs chargés de
fonctions ou missions spéciales, une rémunération particuliére 4 prélever sur les frais

généraux.

ARTICLE 19
Le conseil d'administration a le pouvoir d'accomplir tous les actes nécessaires ou utiles a la
réalisation de l'objet social, 4 l'exception de ceux que la loi ou les statuts réservent a

l'assemblée générale.

ARTICLE 20

Le conseil peut par ailleurs créer en son sein ou en dehors un comité d’audit, dont il
détermine la composition et les compétences.

Le conseil d'administration peut déléguer la gestion journaliére ainsi que la représentation de
la société en ce qui concerne cette gestion 4 une ou plusieurs personnes agissant dans ce cas
deux a deux. Il nomme et révoque les délégués a cette gestion qui sont choisis dans ou hors
son sein, fixe leur rémunération et détermine leurs attributions.

Le conseil d'administration peut confier la direction de l'ensemble, de telle partie ou de telle
branche spéciale des affaires sociales 4 une ou plusieurs personnes.

Il est créé par l'assemblée générale un comité de gestion pour chaque division comptable de
la société (individuellement un "Comité"). Ses membres peuvent, mais ne doivent pas, étre
des administrateurs de la société.

Le conseil d'administration de la société peut déléguer certains pouvoirs 4 chaque Comité, par
le biais d'une procuration spéciale. Le conseil d'administration, ainsi que les délégués 4 la
gestion journaliére dans le cadre de cette gestion, peuvent également conférer des pouvoirs

spéciaux et déterminés 4 une ou plusieurs personnes de leur choix.

ARTICLE 21

La société est représentée dans les actes, y compris ceux ot intervient un fonctionnaire public
ou un officier ministériel, ou en justice en ce compris le conseil d’Etat et les autres
juridictions administratives :

- soit par deux administrateurs agissant conjointement;

- soit, dans les limites de la gestion journaliére, par le délégué a cette gestion s'il n'y en a

qu'un seul, et par deux délégués agissant conjointement s'ils sont plusieurs.
Elle est en outre valablement engagée par des mandataires spéciaux, en ce compris les

Comités, dans les limites de leur mandat.

ARTICLE 22

Le contréle des comptes de la société est confié 4 un commissaire au moins nommé par
l'assemblée générale pour une durée de trois ans, rééligible et révocable par elle.

Si par suite de décés ou pour un autre motif, il n'y a plus de commissaire, le conseil
d'administration doit convoquer immédiatement l'assemblée générale pour pourvoir a cette
vacance.

Les fonctions du ou des commissaires sortants et non réélus prennent fin immédiatement
aprés l'assemblée générale ordinaire.

La mission et les pouvoirs du ou des commissaires sont ceux que leur assigne le code des
sociétés.

L'assemblée générale détermine les émoluments du ou des commissaires correspondant a
leurs prestations de contréle des comptes. Toutefois, le conseil d'administration peut attribuer
aux commissaires des émoluments pour des missions spéciales; il informe la plus prochaine
assemblée générale ordinaire par le rapport de gestion, de l'objet de ces missions et de leur

rémunération.

TITRE IV
ASSEMBLEES GENERALES

ARTICLE 23

L'assemblée générale a les pouvoirs qui sont déterminés par la loi et les présents statuts.

ARTICLE 24
L'assemblée générale se tient au siége social ou 4 l'endroit indiqué dans les avis de
convocation.
L'assemblée se réunit au moins une fois l'an, le dernier mercredi du mois d'avril, 4 quatorze

heures trente ou, si ce jour est un jour férié légal, le premier jour ouvrable suivant.
ARTICLE 25

Le conseil d'administration convoque les assemblées générales tant ordinaires
qu'extraordinaires.

L'assemblée doit étre convoquée 4 la demande d'un ou plusieurs associés justifiant qu'ils

représentent le cinquiéme du capital social.

ARTICLE 26
Pour étre admis a l'assemblée générale, les associés doivent, si le conseil les y invite,
informer six jours au moins avant l'assemblée la société de leur intention d'assister a

l'assemblée ainsi que du nombre de parts pour lequel ils entendent prendre part au vote.

ARTICLE 27

Tout associé peut se faire représenter a l'assemblée générale par un mandataire, associé ou
non.

Le conseil d'administration peut arréter la formule de procuration et exiger que celle-ci soit

déposée 4 I'endroit indiqué par lui et dans le délai qu'il fixe.

ARTICLE 28
Pour étre admis 4 l'assemblée, tout associé ou mandataire doit signer la liste de présence.
Cette liste indique I'identité des associés ainsi que le nombre de titres pour lequel ils prennent

part au vote.

ARTICLE 29

L'assemblée est présidée par le président du conseil d'administration ou, 4 son défaut, par le
vice-président ou, a leur défaut, par un administrateur désigné par ses collégues.

Le président désigne le secrétaire et I'assemblée nomme deux scrutateurs, associés ou non. Ils

forment ensemble le bureau.

ARTICLE 30

Chaque part donne droit 4 une voix.
ARTICLE 31

Sauf les cas prévus par la loi et les présents statuts et, le cas échéant, par convention conclue
entre l’ensemble des associés, les décisions sont prises 4 la simple majorité des voix
valablement exprimées, quel que soit le nombre de titres représentés, sans tenir compte des
abstentions.

Les votes se font par main levée ou par appel nominal, 4 moins que l'assemblée n'en dispose
autrement. Les votes relatifs 4 des personnes se font au scrutin secret sur toute demande d’un
associé.

En cas de nomination, si aucun candidat ne réunit la majorité des voix, il est procédé & un
scrutin de ballottage entre les candidats qui ont obtenu le plus de voix. En cas d'égalité du

nombre de suffrages a ce scrutin, le plus 4gé des candidats est élu.

ARTICLE 32

Lorsque I'assemblée générale est appelée a décider d'une modification aux statuts, elle ne
peut valablement délibérer que si ceux qui assistent 4 l'assemblée représentent la moitié au
moins du capital social. Si cette derniére condition n'est pas remplie, une nouvelle
convocation est nécessaire et la nouvelle assemblée délibére valablement quelle que soit la
portion du capital représentée.

Aucune modification des statuts n'est admise, sauf exception légale, que si elle réunit les
quatre cinquiémes des voix valablement exprimées.

Des conventions conclues entre l'ensemble des associés peuvent imposer des conditions de

quorum et de majorité plus strictes pour la modification des statuts.

ARTICLE 33

L'assemblée générale ne peut délibérer que sur les points figurant 4 son ordre du jour.

ARTICLE 34

Le conseil d'administration a le droit, aprés l'ouverture des débats, de proroger a trois
semaines la décision de l’assemblée générale relative a l’approbation des comptes annuels.
Cette prorogation, notifi¢e avant la cléture de la séance et mentionnée au procés-verbal de

celle-ci, n’annule pas les autres décisions prises. Elle ne peut avoir lieu qu'une fois.
Les associés doivent étre convoqués 4 nouveau pour la date que fixera le conseil
d'administration.

Les formalités remplies pour assister 4 la premiére séance resteront valables pour la seconde.
L'accomplissement de formalités non encore remplies sera admis dans les délais statutaires.

La seconde assemblée générale a le droit d’arréter définitivement les comptes annuels.

ARTICLE 35

Les procés-verbaux des assemblées générales sont signés par les membres du bureau et par
les associés qui le demandent et sont consignés ou reliés dans un recueil spécial tenu au siége
social ou sous une autre forme quelconque remplagant un tel registre.

Les expéditions, copies ou extraits 4 délivrer aux tiers sont signés par deux administrateurs.

TITRE V
INVENTAIRES ET COMPTES ANNUELS - BENEFICE ET REPARTITION

ARTICLE 36

L'exercice social commence le premier janvier et se termine le trente et un décembre de
chaque année.

Le conseil d'administration dresse un inventaire et établit, conformément 4 la loi, les comptes
annuels, qui comprennent le bilan, le compte de résultats et l'annexe. II rédige en outre le
rapport de gestion.

Un mois au moins avant I'assemblée générale, l'inventaire, les comptes annuels et le rapport
de gestion sont remis avec les piéces aux commissaires qui doivent faire leur rapport.

Le conseil d’administration établit en outre une comptabilité analytique, permettant de
distinguer la contribution au résultat de chaque division, telles que celles-ci sont définies 4

l’article 6.

ARTICLE 37
Les comptes annuels, le rapport de gestion et le rapport des commissaires sont adressés aux

associés, en méme temps que la convocation.
ARTICLE 38

Aprés avoir pris connaissance du rapport de gestion et du rapport des commissaires,
l'assemblée générale délibére sur les comptes annuels.

Aprés leur adoption, elle se prononce par un vote spécial sur la décharge des administrateurs

et des commissaires.

ARTICLE 39

Sur le bénéfice net, il est prélevé tout d'abord cinq pour cent au moins pour constituer la
réserve légale ; ce prélévement se répartit en fonction des résultats des divisions. II cesse
d'étre obligatoire lorsque la réserve atteint le dixiéme de la part fixe du capital social.

Il est ensuite prélevé, avant allocation aux réserves ou report, sur le bénéfice de chaque
division, un montant égal 4 1,5 % du poste « dotation en capital social » prorata temporis et
liberationis de cette division, ou, si ce bénéfice divisionnaire ne le permet pas, le montant
inférieur le plus élevé que ce bénéfice autorisera. Le montant cumulé provenant de ces
prélévements est mis a la disposition de l’ensemble des parts, pro rata temporis et
liberationis, soit, - a l’option de chaque catégorie -, sous forme d’un premier dividende égal a
toutes les parts, soit en report 4 nouveau de leur division.

La division qui n’aura pas pu contribuer pleinement au prélévement visé a l’alinea précédent
sera toutefois tenue, dés que ses bénéfices des exercices ultérieurs le permettront mais pas au-
dela du troisiéme exercice suivant, de corriger cette situation par un prélévement de
rattrapage 4 due concurrence.

L’assemblée procéde ensuite, sur proposition du conseil d’administration et, le cas échéant,
des Comités concernés, a l’affectation éventuelle, division par division, aux réserves
facultatives ou au report 4 nouveau.

Le solde du bénéfice disponible au sein de chaque division est ensuite distribué, pro rata

temporis et liberationis, aux parts de la catégorie correspondante.

ARTICLE 40

Le paiement des dividendes se fait aux époques et aux endroits fixés par le conseil
d'administration. La distribution d'un acompte pro rata temporis et liberationis, 4 imputer sur
le dividende qui sera distribué sur les résultats de l'exercice, pourra étre décidée par le conseil
d'administration, méme avant la cléture de l'exercice social. Cette distribution pourra se faire

de fagon différenciée en fonction des catégories de parts.
TITRE VI
DISSOLUTION - LIQUIDATION

ARTICLE 41

En cas de dissolution de la société, l'assemblée générale régle le mode de liquidation et
nomme un ou plusieurs liquidateurs dont elle détermine les pouvoirs et les émoluments. Elle
conserve le pouvoir de modifier les statuts si les besoins de la liquidation le justifient.

La nomination des liquidateurs met fin aux pouvoirs des administrateurs et commissaires.

ARTICLE 42

L'assemblée générale est convoquée, constituée et tenue pendant la liquidation conformément
aux dispositions du titre quatre des présents statuts, les liquidateurs jouissant des mémes
prérogatives que le conseil. Un des liquidateurs la préside; en cas d'absence ou
d'empéchement des liquidateurs, elle élit elle-méme son président.

Les copies ou extraits des procés-verbaux de ses délibérations, 4 produire en justice ou
ailleurs, sont valablement certifiés par les liquidateurs ou l'un d'entre eux, conformément aux

décisions de l'assemblée.

ARTICLE 43

Aprés le paiement des dettes communes 4 toutes les divisions ou la consignation des sommes
nécessaires 4 ce paiement, y compris les frais de liquidation, 5 % de la différence positive
entre, d’une part, l’actif net social aprés déduction de toutes dettes et éventuelles charges
d’imp6t et de liquidation et, d’autre part, le montant du capital effectivement libéré augmenté
des primes d’émission, seront répartis de maniére égale, en espéces ou en nature, entre toutes
les parts.

Le solde des actifs et passifs sera réparti entre les parts de chaque catégorie sur base de I’actif
net de chaque division.

Si les parts ne se trouvent pas libérées toutes dans une égale proportion, les liquidateurs,
avant de procéder a la répartition prévue a l'alinéa qui précéde, doivent tenir compte de cette
diversité de situations et rétablir l'équilibre en mettant toutes les parts sur un pied d'égalité

absolue, soit par des appels de fonds complémentaires 4 charge des parts insuffisamment
libérées, soit par des remboursements préalables, en espéces ou en nature, au profit des parts

libérées dans une proportion supérieure.

TITRE VII
DISPOSITIONS GENERALES

ARTICLE 44

Pour tous les litiges entre la société, ses associés, obligataires, administrateurs, commissaires
et liquidateurs relatifs aux affaires de la société et a l'exécution des présents statuts,
compétence exclusive est attribuée 4 un collége arbitral en nombre impair, composé
conformément au réglement de conciliation et d’arbitrage du Cepani ou, a défaut de celui-ci,
conformément aux régles du code judiciaire; toutefois, la société, si elle est demanderesse,

sera en droit de porter le différend devant tout autre tribunal compétent.

ARTICLE 45

Les associés, obligataires, administrateurs, commissaires et liquidateurs domiciliés a
l'étranger et n'ayant fait aucune élection de domicile en Belgique diiment notifiée a la société,
sont censés avoir élu domicile au siége social ot tous actes peuvent valablement leur étre
signifiés ou notifiés, la société n'ayant pas d'autre obligation que de les tenir a la disposition

du destinataire.

Copie certifiée conforme
Bruxelles, le 16 décembre 2010
