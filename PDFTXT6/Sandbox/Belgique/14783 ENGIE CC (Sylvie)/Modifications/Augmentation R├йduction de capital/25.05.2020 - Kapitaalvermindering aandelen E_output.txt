DocuSign Envelope ID: 6920AE6F-BBA0-4742-82CD-1A25860063D5

ENGIE CC
Coéperatieve vennootschap met beperkte aansprakelijkheid
1000 Brussel, Simon Bolivarlaan 34
RPR Ondernemingsnummer: 0442.100.363

 

Te Brussel, in de zetel van de vennootschap,
WERD GEHOUDEN:
De buitengewone algemene vergadering der vennoten van de codperatieve vennootschap met
beperkte aansprakelijkheid ENGIE CC, gevestigd te 1000 Brussel, Simon Bolivarlaan 34,
ingeschreven in het rechtspersonenregister van Brussel onder nummer 0442.100.363.
Vennootschap opgericht onder de rechtsvorm van een naamloze vennootschap op 31 juli 1990,
bij akte verleden voor Notaris Jean-Luc Indekeu te Brussel, bij uittreksel verschenen in de
bijlagen tot het Belgisch Staatsblad van 19 oktober 1990, onder nummer 901019-135.
De vennootschap werd omgezet in een codperatieve vennootschap met beperkte
aansprakelijkheid bij besluit van de buitengewone algemene vergadering van 24 december
2002. De statuten werden voor het laatst gewijzigd, bij besluit van de buitengewone algemene
vergadering van vennoten van 20 november 2015, bij akte verleden voor Notaris Damien
Hisette te Brussel, bekendgemaakt in de bijlagen tot het Belgisch Staatsblad van 21 december
2015 onder nummer 15177025.
Bureau
De zitting wordt geopend onder het voorzitterschap van Patrick van der Beken.
De voorzitter duidt Michael Delmée aan als secretaris.
De vergadering kiest als stemopnemers:
1) Xavier Aguirre;
2) Thierry van den Hove.
Samenstelling van de vergadering

Aanwezigheidslijst
De buitengewone algemene vergadering wordt samengesteld uit de hierna genoemde

vennoten, die volgens hun verklaringen het aantal aandelen bezitten zoals hierna vermeld:
Aantal aandelen
1. de codperatieve vennootschap met beperkte aansprakelijkheid
ENGIE Energy Management, gevestigd te B-1000 Brussel, Simon Bolivarlaan 34, 1
2. de naamloze vennootschap ELECTRABEL INVEST LUXEMBOURG, gevestigd te

L-1611 Luxemburg, 65 avenue de la Gare, 1
3. de naamloze vennootschap ENGIE INVEST INTERNATIONAL,

gevestigd te L-1611 Luxemburg, 65 avenue de la Gare 19.725.320
TOTAAL: 19.725.322

1/7
DocuSign Envelope ID: 6920AE6F-BBA0-4742-82CD-1A25860063D5

Volmachten
De vennoot ‘sub 1’ wordt vertegenwoordigd door Patrick van der Beken, krachtens een
volmacht hier aangehecht.
De vennoot ‘sub 2’ wordt vertegenwoordigd door Thierry van den Hove, krachtens een
volmacht hier aangehecht.
De vennoot ‘sub 3’ wordt vertegenwoordigd door Xavier Aguirre, krachtens een volmacht
hier aangehecht.

Toelichting van de voorzitter
De voorzitter leidt de vergadering in en licht het volgende toe:

L Het vast gedeelte van het maatschappelijk kapitaal van de vennootschap
bedraagt 1.250.000 EUR. Het ingeschreven kapitaal - vast en veranderlijk gedeelte
samengevoegd - bedraagt 4.931.330.500 EUR, verdeeld in 19.725.322 aandelen zonder
vermelding van nominale waarde.

 

IL. Uit de aanwezigheidslijst blikt dat alle vennoten op de vergadering
vertegenwoordigd zijn. De verzending van geschreven oproepingsbrieven dient aldus niet te
worden aangetoond. De vergadering is bevoegd om rechtsgeldig te beraadslagen en geldige
besluiten te nemen.

Ill. Alle vennoten hebben zich akkoord verklaard dat de vergadering beraadslaagt
over de volgende agenda:

AGENDA

KAPITAALVERMINDERING (Veranderlijk gedeelte)

Op basis van eensluidend advies van de managementcomités en op voorstel van de Raad

van Bestuur,

om

(i) het veranderlijk gedeelte van het kapitaal te verminderen met een bedrag van
1.040.400.000 EUR en dit terug te brengen van 4.930.080.500 EUR naar
3.889.680.500 EUR door de intrekking van 4.161.600 aandelen categorie E, allemaal
volgestort en in handen van ENGIE INVEST INTERNATIONAL. De betaling zal in
natura of in specién kunnen worden uitgevoerd.

(ii) aan de Raad van Bestuur, met mogelijkheid tot delegatie, de toestemming te geven de
PPL aan te passen voor de divisie E (terugbetaling ten belope van 4.161.600.000

EUR).
Vaststelling
De toelichting van de voorzitter wordt door alle aanwezige leden van de vergadering als juist
erkend.

Beraadslagingen - Besluiten
Hierna neemt de vergadering de agenda in behandeling en neemt na beraadslaging de
volgende besluiten:

 

 

 

Op voorstel van de Raad van Bestuur, die de gevolgen ervan op de liquiditeit en solvabiliteit
van de Vennootschap heeft nagekeken, besluit de vergadering om het veranderlijk gedeelte
van het kapitaal te verminderen met een bedrag van 1.040.400.000 EUR en dit terug te
brengen van 4.930.080.500 EUR naar 3.889.680.500 EUR door de intrekking van 4.161.600

2/7
DocuSig n Envelope ID: 6920AE6F-BBA0-4742-82CD-1A25860063D5

aandelen categorie E, allemaal volgestort en in handen van ENGIE INVEST
INTERNATIONAL. De buitengewone algemene vergadering bevestigt dat de betaling
volledig of gedeeltelijk in natura of in specién zal kunnen worden uitgevoerd.

De vergadering beslist om de leden van de Raad van Bestuur, twee aan twee handelend en
met mogelijkheid tot delegatie, te machtigen om de gesloten overeenkomsten met EII (divisie
E) ten belope van 4.161.600.000 EUR aan te passen.

Stemming
Al deze besluiten worden genomen met eenparigheid van stemmen.

VASTSTELLING
Het maatschappelijk kapitaal van de vennootschap bedraagt voortaan 3.890.930.500 EUR
waarvan het vast gedeelte 1.250.000 EUR bedraagt en het veranderlijk gedeelte
3.889.680.500 EUR bedraagt. Het kapitaal wordt vertegenwoordigd door 15.563.722
aandelen, als volgt uitgesplitst:

 

Categorie Aantal aandelen Ing. kapitaal Niet gestort kap. Gestort kapitaal
A ENGIE LI. 9.127.423 2.281.855.750 -441.425.000 1.840.430.750
E.E.M. 1 250 0 250
B ENGIE LI. 4.080.558 1.020.139.500 -2.815.280 1,017.324.220
D ENGIE LI. 513.281 128.320.250 -1.125.000 127.195.250
E ENGIE LI, 1.842.458 460.614.500 -15.706.875 444.907.625
EIL 1 250 0 250

 

 

WAARVAN PROCES-VERBAAL
Opgesteld te Brussel, op voormelde datum en plaats.
Na gedane voorlezing hebben de leden van het bureau deze notulen ondertekend.
(volgt de Franse tekst)

 

3/7
DocuSign Envelope ID: 6920AE6F-BBA0-4742-82CD-1A25860063D5

ENGIE CC
Société coopérative a responsabilité limitée
1000 Bruxelles, Boulevard Simon Bolivar 34
RPM Numéro d’Entreprise : 0442.100.363

REDUCTION DE CAPITAL (partie variable)

 

L'an deux mille vingt, le 25 mai a 10 heures.
A Bruxelles, au si¢ge social
S'EST REUNIE:
L'assemblée générale extraordinaire des associés de la société coopérative a responsabilité
limitée ENGIE CC, établie 4 1000 Bruxelles, Boulevard Simon Bolivar 34, immatriculée au
registre des personnes morales a Bruxelles sous le numéro 0442.100.363.
Société constituée sous la forme d’une société anonyme le 31 juillet 1990, suivant acte regu
par le Notaire Jean-Luc Indekeu 4 Bruxelles, publié par extrait aux annexes du Moniteur
belge du 19 octobre 1990 sous le numéro 901019-134.
La société a été transformée en société coopérative 4 responsabilité limitée par décision de
l'assemblée générale extraordinaire du 24 décembre 2002. Les statuts ont été modifiés en
dernier lieu par décision de l'assemblée générale extraordinaire des associés du 20 novembre
2015, suivant acte regu par le Notaire Damien Hisette 4 Bruxelles ; la publication par extrait
a été faite aux annexes du Moniteur belge du 21 décembre 2015 sous le numéro 15177024.
Bureau
La séance est ouverte sous la présidence de Monsieur Patrick van der Beken.
Le président désigne comme secrétaire Monsieur Michael Delmée.
L'assemblée choisit comme scrutateurs :
1. Xavier Aguirre;
2. Thierry van den Hove.

Composition de I'assemblée
Liste des présences
L'assemblée générale extraordinaire est composée des associés cités ci-dessous, lesquels
déclarent détenir le nombre de parts sociales ci-aprés indiqué :

Nombre de parts sociales

1. la société coopérative a responsabilité limitée ENGIE Energy Management,

établie 4 B-1000 Bruxelles, boulevard Simon Bolivar 34, 1
2. la société anonyme ELECTRABEL INVEST Luxembourg, établie 4

L-1611 Luxembourg, 65 avenue de la Gare 1
3. la société anonyme ENGIE INVEST INTERNATIONAL,

établie 4 L-1611 Luxembourg, 65 avenue de la Gare 19.725.320
SOIT AU TOTAL : 19.725.322

AIT
Docu! Sign Envelope ID: 6920AE6F-BBA0-4742-82CD-1A25860063D5

Procurations
L’associé sous 1 est valablement représenté par Monsieur Patrick van der Beken, en vertu
d’une procuration ci-annexée.
L’associé sous 2 est valablement représenté par Monsieur Thierry van den Hove, en vertu
d’une procuration ci-annexée.
L’associé sous 3 est valablement représenté par Monsieur Xavier Aguirre, en vertu d’une
procuration ci-annexée.

Exposé du président
Le président dirige les débats et expose ce qui suit :

I. La partie fixe du capital social de la société s'éléve a 1.250.000 EUR. Le
capital souscrit - partie fixe et partie variable cumulées - s’établit a 4.931.330.500 EUR,
représenté par 19.725.322 parts sans mention de valeur nominale.

IL. Il résulte de la liste de présence que tous les associés sont représentés a
l'assemblée. Tous les associés étant représentés, il n'a pas lieu de justifier de l’envoi de
convocations écrites. L’assemblée est habilitée 4 délibérer et a statuer valablement.

III. — Tous les associés se sont déclarés d'accord que l'assemblée délibére sur l'ordre
du jour suivant :

ORDRE DU JOUR

REDUCTION DE CAPITAL

Sur avis conforme des comités de management et sur proposition du Conseil d’administration,

(i) réduire la partie variable du capital d’un montant de 1.040.400.000 EUR afin de ramener
celle-ci de 4.930.080.500 EUR a 3.889.680.500 EUR par annulation de 4.161.600 parts
de catégorie E, toutes entiérement libérées et détenues par ENGIE INVEST
INTERNATIONAL. Le paiement pourra s’effectuer en nature ou en espéces.

(ii) octroyer au Conseil d’administration, avec faculté de délégation, |’autorisation d’ adapter
le PPL en ce qui concerne la division E (via un remboursement 4 hauteur de
4.161.600.000 EUR).

Constatation
L'exposé du président est approuvé par tous les membres présents de l'assemblée.

 

Délibérations — Résolutions
L'assemblée aborde son ordre du jour et, aprés délibération, prend les résolutions suivantes :

Sur proposition du Conseil d’administration qui en a vérifié les impacts en terme de liquidité
et solvabilité sur la Société, l’assemblée décide de réduire la partie variable du capital d’un
montant de 1.040.400.000 EUR afin de ramener celle-ci de 4.930.080.500 EUR a
3.889.680.500 EUR par annulation de 4.161.600 parts de catégorie E, toutes entiérement
libérées et détenues par ENGIE INVEST INTERNATIONAL. L’assemblée générale
extraordinaire confirme que le paiement pourra étre effectué en tout ou en partie en nature ou
en espéces.

5/7
DocuSign Envelope ID: 6820AE6F-BBA0-4742-82CD-1A25860063D5

L’assemblée décide d’autoriser les membres du Conseil d’administration, agissant deux 4

deux, avec faculté de délégation, 4 ajuster les conventions conclues avec EII (division E) 4
hauteur de 4.161.600.000 EUR.

Vote
Ces décisions sont prises a l'unanimité des voix.
CONSTATATION
Le capital de ta société s’établit dorénavant 4 3.890.930.500 EUR dont 1.250.000 EUR pour
sa partie fixe et 3.889.680.500 EUR pour sa partie variable. Il est représenté par
15.563.722 parts réparties comme suit :

    

 

Catégorie Détenues par Nbre parts Capital souscrit Capital non libéré Capital libéré
A ENGIE I.1. 9.127.423 2.281.855.750 -441.425.000 1.840.430.750
E.E.M. 1 250 0 250
B ENGIE LI. 4.080.558 1.020.139.500 -2.815.280 1.017.324.220
D ENGIE LI. 513.281 128.320.250 -1.125.000 127.195.250
E ENGIE LI. 1.842.458 460.614.500 -15.706.875 444 907.625

EIL 1 250 0 250

 

 

Cl6ture de la séance
L'ordre du jour étant épuisé, le président déclare la séance levée.
DONT PROCES-VERBAL
Fait et dressé a Bruxelles, date et lieu que dessus.
Lecture faite, les membres du bureau ont signé ce procés-verbal.

 

 

DocuSigned by: DocuSigned by: Docusigned by: DocuSigned by:
DEMEE Micad so) +7 f ACUIRRE Xanier
62170 1BBA40C49D.. — ose etonanoaese BFOBABBF3A4141F,
675C1AA977264AD.. .
M. Delmée P. van der Beken Th. van den Hove X. Aguirre

6/7
DocuSign Envelope ID: 6920AE6F-BBA0-4742-82CD-1A25860063D5

Liste des présences / Aanwezigheidslijst

DocuSigned by: DocuSigned by:
ez
\ | ft
~ ys ![—/ E23CFE42E4BC4E9.
675C1AA977264AD...

ENGIE Energy Management ELECTRABEL INVEST Luxembourg
représentée par / vertegenwoordigd door représentée par / vertegenwoordigd door
Patrick van der Beken Thierry van den Hove

‘DocuSigned by:
AEUVIRRE Koonier

BFOBABBF3A4141F

ENGIE INVEST INTERNATIONAL
représentée par / vertegenwoordigd door
Xavier Aguirre

WT
