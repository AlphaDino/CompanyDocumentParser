ENGIE CC
Codperatieve vennootschap met beperkte aansprakelijicheid
1000 Brussel, Simon Bolivarlaan 34
RPR Ondernemingsnummer: 0442.100.363

KAPITAAL VERMINDERING (VERANDERLIJK GEDEELTE)

Het jaar tweeduizend negentien, op 15 oktober om 10 uur.
Te Brussel, in de zetel van de vennootschap,

WERD GEHOUDEN:
De buitengewone algemene vergadering der vennoten van de codperatieve vennootschap met
beperkte aansprakelijkheid ENGIE CC, gevestigd te 1000 Brussel, Simon Bolivarlaan 34,
ingeschreven in het rechtspersonenregister van Brussel onder nummer 0442.100.363.
Vennootschap opgericht onder de rechtsvorm van een naamloze vennootschap op 31 juli 1990,
bij akte verleden voor Notaris Jean-Luc Indekeu te Brussel, bij uittrekse! verschenen in de
bijlagen tot het Belgisch Staatsblad van 19 oktober 1990, onder nummer 901019-135.
De vennootschap werd omgezet in een codperatieve vennootschap met beperkte
aansprakelijkheid bij besluit van de buitengewone algemene vergadering van 24 december
2002. De statuten werden voor het laatst gewijzigd, bij besluit van de buitengewone algemene
vergadering van vennoten van 20 november 2015, bij akte verleden voor Notaris Damien
Hisette te Brussel, bekendgemaakt in de bijlagen tot het Belgisch Staatsblad van 21 december
2015 onder nummer 15177025.

Bureau
De zitting wordt geopend onder het voorzitterschap van Patrick van der Beka:
De voorzitter duidt Michael Delmée aan als secretaris.
De vergadering kiest als stemopnemers:
1) Xavier Aguirre;
2) Thierry van den Hove.
Samenstelling van de vergadering
Aanwezigheidslijst
De buitengewone algemene vergadering wordt samengesteld uit de hierna genoemde
vennoten, die volgens hun verklaringen het aantal aandelen bezitten zoals hierna vermeld:
Aantal aandelen

 

 

1. de codperatieve vennootschap met beperkte aansprakelijkheid
ENGIE Energy Management, gevestigd te B-1000 Brussel, Simon Bolivarlaan 34, 1
2. de naamloze vennootschap ELECTRABEL INVEST LUXEMBOURG, gevestigd te

L-1611 Luxemburg, 65 avenue de la Gare, 1
3. de naamloze vennootschap ENGIE INVEST INTERNATIONAL,

gevestigd te L-1611 Luxemburg, 65 avenue de la Gare 20.657. 320
TOTAAL : 20.657. 302

1/7
Volmachten

De vennoot ‘sub 1’ wordt vertegenwoordigd door Patrick van der Beken, krachtens een
volmacht hier aangehecht.
De vennoot ‘sub 2’ wordt vertegenwoordigd door Thierry van den Hove, krachtens een
volmacht hier aangehecht.
De vennoot ‘sub 3’ wordt vertegenwoordigd door Xavier Aguirre, krachtens een volmacht
hier aangehecht.

Toelichting van de voorzitter
De voorzitter leidt de vergadering in en licht het volgende toe:

L Het vast gedeelte van het maatschappelijk kapitaal van de vennootschap
bedraagt 1.250.000 EUR. Het ingeschreven kapitaal - vast en veranderlijk gedeelte
samengevoegd - bedraagt 5.164.330.500 EUR, verdeeld in 20.657.322 aandelen zonder
vermelding van nominale waarde.

Il. Uit de aanwezigheidslijst blijkt dat alle vennoten op de vergadering
vertegenwoordigd zijn. De verzending van geschreven oproepingsbrieven dient aldus niet te
worden aangetoond. De vergadering is bevoegd om rechtsgeldig te beraadslagen en geldige
besluiten te nemen.

Il. Alle vennoten hebben zich akkoord verklaard dat de vergadering beraadslaagt
over de volgende agenda:

 

AGENDA
KAPITAALVERMINDERING (Veranderlijk gedeelte)

Op basis van eensluidend advies van de managementcomités en op voorstel van de Raad

van Bestuur,

om

(i) het veranderlijk gedeelte van het kapitaal te verminderen met een bedrag van
233 MEUR en dit terug te brengen van 5.163.080.500 EUR naar 4.930.080.500 EUR
door de intrekking van 932.000 aandelen categorie E, allemaal volgestort en in handen
van ENGIE INVEST INTERNATIONAL. De betaling zal in natura of in specién
kunnen worden uitgevoerd.

(ii) aan de Raad van Bestuur, met mogelijkheid tot delegatie, de toestemming te geven de
PPL aan te passen voor de divisie E (terugbetaling ten belope van 932 MEUR).

Vaststelling
De toelichting van de voorzitter wordt door alle aanwezige leden van de vergadering als juist
erkend.
Beraadslagingen - Besluiten
Hierna neemt de vergadering de agenda in behandeling en neemt na beraadslaging de
volgende besluiten:

KAPITAALVERMINDERING

 

De vergadering besluit om het veranderlijk gedeelte van het kapitaal te verminderen met een
bedrag van 233 MEUR en dit terug te brengen van 5.163.080.500EUR naar
4.930.080.500EUR door de intrekking van 932.000 aandelen categorie E, allemaal volgestort
en in handen van ENGIE INVEST INTERNATIONAL. De buitengewone algemene

 

2/7
vergadering bevestigt dat de betaling volledig of gedeeltelijk in natura of in specién zal
kunnen worden uitgevoerd.

De vergadering beslist om de leden van de Raad van Bestuur, twee aan twee handelend en
met mogelijkheid-tot-delegatie, te machtigen om de gesloten overeenkomsten met EII (divisie
E) ten belope va 9320 ‘UR aan te passen.

Stemmi:
Al deze besluiten worden genomen met eenparigheid van stemmen.

VASTSTELLING
Het maatschappelijk kapitaal van de vennootschap bedraagt voortaan 4.931.330.500 EUR
waarvan het vast gedeelte 1.250.000 EUR bedraagt en het veranderlijk gedeelte
4.930.080.500 EUR bedraagt. Het kapitaal wordt vertegenwoordigd door 19.725.322
aandelen, als volgt uitgesplitst:

    

    

 

  
             
        
 

Categorie Aantal aandelen Ing. kapitaal Niet gestort kap._ Gestort kapitaal
A ENGIE LI. 9.127.423 2.281.855.750 -441.425.000 1.840.430.750
E.E.M. 1 250 0 250
B ENGIE LI. 4.080.558 1.020.139.500 -2.815.280 + 1,107.324.220
D ENGIE LI. 513.281 128.320.250 -1.125.000 127.195.250
E ENGIE LI. 6.004.058 1.501.014.500 -15.706.875 1.485.307.625

      
   
   

   
  

   
  

  
  

EIL 250 250

 

 

AARVAN -" L
Opgesteld te Brussel, op voormelde datum en plaats.
Na gedane voorlezing hebben de leden van het bureau deze notulen ondertekend.
(volgt de Franse tekst)

3/7
ENGIE CC
Société coopérative 4 responsabilité limitée
1000 Bruxelles, Boulevard Simon Bolivar 34
RPM Numéro d’Entreprise : 0442.100.363

REDUCTION DE CAPITAL (partie variable)

 

L'an deux mille dix-neuf, le 15 octobre 4 10 heures.
A Bruxelles, au siége social

S'EST REUNIE:
L'assemblée générale extraordinaire des associés de la société coopérative 4 responsabilité
limitée ENGIE CC, établie 4 1000 Bruxelles, Boulevard Simon Bolivar 34, immatriculée au
registre des personnes morales a Bruxelles sous le numéro 0442.100.363.

 

 

Société constituée sous la forme d’une société anonyme le 31 juillet 1990, suivant acte regu
par le Notaire Jean-Luc Indekeu 4 Bruxelles, publié par extrait aux annexes du Moniteur
belge du 19 octobre 1990 sous le numéro 901019-134.
La société a été transformée en société coopérative 4 responsabilité limitée par décision de
l'assemblée générale extraordinaire du 24 décembre 2002. Les statuts ont été modifiés en
dernier lieu par décision de l'assemblée générale extraordinaire des associés du 20 novembre
2015, suivant acte regu par le Notaire Damien Hisette 4 Bruxelles ; la publication par extrait
a été faite aux annexes du Moniteur belge du 21 décembre 2015 sous le numéro 15177024.
Bureau

La séance est ouverte sous la présidence de Monsieur Patrick van der Beken.
Le président désigne comme secrétaire Monsieur Michael Delmée.
L'assemblée choisit comme scrutateurs :

1. Xavier Aguirre;

2. Thierry van den Hove.

 

Composition de l'assemblée
Liste des présences
L'assemblée générale extraordinaire est composée des associés cités ci-dessous, lesquels
déclarent détenir le nombre de parts sociales ci-aprés indiqué :
Nombre de parts sociales
1. Ia société coopérative 4 responsabilité limitée ENGIE Energy Management,

établie 4 B-1000 Bruxelles, boulevard Simon Bolivar 34, 1
2. la société anonyme ELECTRABEL INVEST Luxembourg, Ctablie a

L-1611 Luxembourg, 65 avenue de la Gare 1
3. la société anonyme ENGIE INVEST INTERNATIONAL,

établie 4 L-1611 Luxembourg, 65 avenue de la Gare 20.657.320
SOIT AU TOTAL : 20.657.322

4/7
Procurations
L’associé sous 1 est valablement représenté par Monsieur Patrick van der Beken, en vertu
d’une procuration ci-annexée.
L’associé sous 2 est valablement représenté par Monsieur Thierry van den Hove, en vertu
d’une procuration ci-annexée.
L’associé sous 3 est valablement représenté par Madame Xavier Aguirre, en vertu d’une
procuration ci-annexée.

Exposé du président
Le président dirige les débats et expose ce qui suit :

L La partie fixe du capital social de la société s'éléve a 1.250.000 EUR. Le
capital souscrit - partie fixe et partie variable cumulées - s’établit 4 5.164.330.500 EUR,
représenté par 20.657.322 parts sans mention de valeur nominale.

IL. Il résulte de la liste de présence que tous les associés sont représentés 4
l'assemblée. Tous les associés étant représentés, il n'a pas lieu de justifier de l’envoi de
convocations écrites. L’assemblée est habilitée 4 délibérer et 4 statuer valablement.

 

III. —_ Tous les associés se sont déclarés d'accord que l'assemblée délibére sur l'ordre
du jour suivant :
ORDRE DU JOUR

REDUCTION DE CAPITAL

Sur avis conforme des comités de management et sur proposition du Conseil d’ administration,

(i) réduire la partie variable du capital d’un montant de 233 MEUR afin de ramener celle-ci
de 5.163.080.500 EUR a 4.930.080.500 par annulation de 932.000 parts de catégorie E,
toutes entiérement libérées et détenues par ENGIE INVEST INTERNATIONAL. Le
paiement pourra s’effectuer en nature ou en espéces.

(ii) octroyer au Conseil d’administration, avec faculté de délégation, I’autorisation d’adapter
le PPL en ce qui conceme la division E (via un remboursement 4 hauteur de 932 MEUR).

Constatation
L'exposé du président est approuvé par tous les membres présents de l'assemblée.

élibérations — Résolutions
L'assemblée aborde son ordre du jour et, aprés délibération, prend les résolutions suivantes :

 

 

REDUCTION DE CAPITAL
aaa eee eee
L’assemblée décide de réduire la partie variable du capital d’un montant de 233 MEUR afin
de ramener celle-ci de 5.163.080.500 EUR a 4.930.080.500 EUR par annulation de 932.000
parts de catégorie E, toutes entiérement libérées et détenues par ENGIE INVEST
INTERNATIONAL. L’assemblée générale extraordinaire confirme que le paiement pourra
étre effectué en tout ou en partie en nature ou en espéces,

L’assemblée décide d’autoriser les membres du Conseil d’administration, agissant deux a

deux, avec faculté de délégation, 4 ajuster les conventions conclues avec EII (division E) &
hauteur de 932 MEUR.

s/7
Vote

Ces décisions sont prises a l'unanimité des voix.

CONSTATATION
Le capital de la société s’établit dorénavant 4 4.931.330.500 EUR dont 1.250.000 EUR pour
sa partie fixe et 4.930.080.500 EUR pour sa partie variable. Il est représenté par
19.725,322 parts réparties comme suit :

       
 

           
 

       
 

        

      
 

  

             
  

Catégorie Détenues par _Nbre parts Capital souscrit Capital non libéré Capital libéré
ENGIE LI. 9.127.423 2.281.855.750 -441.425.000 1.840.430.750
E.E.M. I 250 0 250
B ENGIE LI. 4.080.558 1.020.139.500 -2.815.280 1.107.324.220
D ENGIE LI. 513.281 128.320.250 -1.125.000 127.195.250
E ENGIE LI. 6.004.058 1.501.014.500 -15.706.875 1.485.307.625

   
    

    

1 250 0 250

   

EIL

         

Vote
Cette décision est prise a I'unanimité des voix.

 

Cléture de la séance
L'ordre du jour étant épuisé, le président déclare la séance levée.

DONT PROCES-VERBAL
Fait et dressé 4 Bruxelles, date et lieu que dessus.
Lecture faite, les membres du bureau ont signé ce procés-verbal.

der Beken Th. van den Hove

 

6/7
Liste des présences / Aanwezigheidslijst

» |
y
ENGIE Energy Management ELECTRABEL INVEST Luxembourg
teprésentée par / vertegenwoordigd door représentée par / vertegenwoordigd door
Patrick van der Beken Thierry van den Hove

 

ENGIE INVEST INKERNATIONAL
représentée par / vertegynwoordigd door
Xavier Agui

UT
