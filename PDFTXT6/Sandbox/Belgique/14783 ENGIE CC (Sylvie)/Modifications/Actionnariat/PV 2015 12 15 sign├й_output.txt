ENGIE CC
Coéperatieve vennootschap met beperkte aansprakelijkheid
1000 Brussel, Simon Bolivarlaan 34
RPR Ondernemingsnummer: 0442.100.363
OMZETTINGEN VAN AANDELEN
AFSCHAFFFFING VAN BOEKHOUDKUNDIGE DIVISIES
STATUTAIRE BENOEMINGEN

 

Het jaar tweeduizend en vijftien, op vijftien december om tien uur.
Te Brussel, in de zetel van de vennootschap,
WERD GEHOUDEN:
De buitengewone algemene vergadering der vennoten van de codperatieve vennootschap met
beperkte aansprakelijkheid ENGIE CC, gevestigd te 1000 Brussel, Simon Bolivarlaan 34,
ingeschreven in het rechtspersonenregister van Brussel onder nummer 0442.100.363.
Vennootschap opgericht onder de rechtsvorm van een naamloze vennootschap op 31 juli 1990,
bij akte verleden voor Notaris Jean-Luc Indekeu te Brussel, bij uittreksel verschenen in de
bijlagen tot het Belgisch Staatsblad van 19 oktober 1990, onder nummer 901019-135.
De vennootschap werd omgezet in een codperatieve vennootschap met beperkte
aansprakelijkheid bij besluit van de buitengewone algemene vergadering van 24 december
2002. De statuten werden voor het laatst gewijzigd, bij besluit van de buitengewone algemene
vergadering van vennoten van 20 november 2015, bij akte verleden voor Notaris Damien
Hisette te Brussel, bekendgemaakt in de bijlagen tot het Belgisch Staatsblad van onder
nummer
Bureau
De zitting wordt geopend onder het voorzitterschap van de heer Guido De Clercq.
De voorzitter duidt als secretaris aan de heer Michael Delmée.
De vergadering kiest als stemopnemers:
1) de heer Guido Vanhove;
2) de heer Thierry van den Hove.
Samenstelling van de vergadering
Aanwezigheidslijst

De algemene vergadering wordt samengesteld uit de hiernagenoemde vennoten, die volgens
hun verklaringen het aantal aandelen bezitten zoals hierna vermeld:
Aantal aandelen

1. de codperatieve vennootschap met beperkte aansprakelijkheid

GDF SUEZ E.M.T., gevestigd te B- 1000 Brussel, Simon Bolivarlaan 34, 1
2. de naamloze vennootschap ELECTRABEL, gevestigd te

B- 1000 Brussel, Simon Bolivarlaan 34, 1
3. de naamloze vennootschap GDF SUEZ INVEST INTERNATIONAL,

gevestigd te L-1611 Luxemburg, 65 avenue de la Gare 23.369.320
TEZAMEN : 23.369.322

1/9
Volmachten

De vennoot sub 1 is hier vertegenwoordigd door de heer Guido De Clercq, krachtens een
volmacht hier aangehecht.

De vennoot sub 2 is hier vertegenwoordigd door de heer Thierry van den Hove, krachtens een
volmacht hier aangehecht.

De vennoot sub 3 is hier vertegenwoordigd door de heer Guido Vanhove, krachtens een
volmacht hier aangehecht.

Toelichting van de voorzitter

De voorzitter leidt de vergadering in en zet het volgende uiteen :

I. Het vaste gedeelte van het maatschappelijk kapitaal van de vennootschap
bedraagt 1.250.000 EUR. Het ingeschreven kapitaal - vaste en veranderlijke gedeelte
samengevoegd - bedraagt 5.842.330.500 EUR, verdeeld in 23.369.322 aandelen zonder
vermelding van nominale waarde.

U. Uit de aanwezigheidslijst blijkt dat alle vennoten op de vergadering
vertegenwoordigd zijn. Aangezien alle vennoten vertegenwoordigd zijn, dient de verzending
van geschreven oproepingsbrieven niet te worden aangetoond. De vergadering is aldus
bevoegd om rechtsgeldig te beraadslagen en geldige besluiten te treffen.

Ul. Alle vennoten hebben zich akkoord verklaard dat de vergadering beraadslaagt
over de volgende agenda :

AGENDA
1. OMZETTINGEN VAN AANDELEN
Om (i) 124.468 aandelen van categorie 'F', 1.016.366 aandelen van categorie 'G' en
803.706 aandelen van categorie 'l' om te zetten in evenveel aandelen van categorie 'A', met
dezelfde kenmerken en die dezelfde rechten vertegenwoordigen,

en (ii) er de gevolgen uit te trekken voor de afschaffing van de overeenkomstige
boekhoudkundige divisies F, G en I.

2. STATUTAIRE BENOEMINGEN
Beheercomités: Benoeming van de vertegenwoordigers:
- categorie A : Olivier Homans, Maxime Paulus de Chatelet en Thierry van den Hove;
- categorie B : Arnaud Rotsart en Florence Verhaeghe;
- categorie C : Maxime Paulus de Chatelet en Thierry van den Hove;
- categorie D : Sven De Smet;
- categorie E : Didier Engels;
- categorie J : Anja Mackelberg en Tanguy Stinglhamber.

Vaststelling
De toelichting van de voorzitter wordt door alle aanwezige leden van de vergadering als juist
erkend.

Beraadslagingen - Besluiten
Hierna neemt de vergadering de agenda in behandeling en treft, na beraadslaging de volgende

besluiten:

OMZETTINGEN VAN AANDELEN

Eerste Besluit
De vergadering besluit tot de omzetting van:

2/9
~ 124.468 aandelen van categorie 'F', waarvan 121.668 volledig en 2.800 voor 71,429 % zijn
volgestort,

- 1.016.366 aandelen van categorie 'G', waarvan 12.227 aandelen voor 26 %,
100.000 aandelen voor 90 % en 904.139 aandelen voor 100 % zijn volgestort,

- 803.706 aandelen van categorie 'I', waarvan 2.438 aandelen voor 25 %, 96 aandelen
voor 26 % en 801.172 aandelen voor 100 % zijn volgestort,

in evenveel aandelen van categorie 'A' met dezelfde kenmerken en die dezelfde rechten
vertegenwoordigen, zijnde 1.944.540 aandelen van categorie 'A', als volgt verdeeld:

- 1.826.979 aandelen zijn volledig volgestort,

- 100.000 aandelen zijn volgestort voor 90 %,

- 2.800 aandelen zijn volgestort voor 71,429 %,
- 12.323 aandelen zijn volgestort voor 26 %,

- 2.438 aandelen zijn volgestort voor 25 %.

Zij worden alle gehouden door GDF SUEZ INVEST INTERNATIONAL S.A.
Bijgevolg beslist de algemene vergadering de boekhoudkundige divisies F, G en I -die
overeenkomen met de bovengenoemde categorieén van aandelen- af te schaffen met ingang

vanaf 31 december 2015.

De nieuwe verdeling van het kapitaal van de vennootschap is als volgt:

 

 

 

 

 

 

 

 

Categorie _Gehouden door Aantal aandelen_Ingeschr.kapitaal _ Niet-volgestortkap. Volgestort kapitaal
GDF SUEZ II. 1.949.540 487.385.000 -5.436.880 481.948.120
GDF SUEZ LL. 4.080.558 1.020.139.500 -2.815.280 1.017.324.220
GDF SUEZ LI. 10.827.047 2.706.761.750 -138.788.120 2.567.973.630
GDF SUEZ E.M.T. 1 250 0 250
GDF SUEZ LI. 513.281 128.320.250 -1.125.000 127.195.250
GDF SUEZ LI. 3.771.910 942.977.500 -15.706.875 927.270.625
ELECTRABEL 1 250 0 250
GDF SUEZ LI. 2.226.984 556.746.000 0 556.746.000

STATUTAIRE BENOEMINGEN
Tweede besluit
Beheercomités

Na de aanpassingen over de bovenvermelde categoricén, bevestigt de Vergadering de
volgende samenstelling van de beheercomités:

- categorie A : Olivier Homans, Maxime Paulus de Chatelet en Thierry van den Hove;

- categorie B : Arnaud Rotsart en Florence Verhaeghe;

- categorie C : Maxime Paulus de Chatelet en Thierry van den Hove;

- categorie D : Sven De Smet;

- categorie E : Didier Engels;

- categorie J : Anja Mackelberg en Tanguy Stinglhamber.

3/9
Stemming
Al deze besluiten worden aangenomen met eenparigheid van stemmen.

Sluiting van de zitting
De agenda volledig afgehandeld zijnde, verklaart de voorzitter de zitting geheven.

Opgesteld te Brussel, op voormelde datum en plaats.
Na gedane voorlezing hebben de leden van het bureau deze notulen getekend.
(volgt de Franse tekst)

4/9
ENGIE CC
Société coopérative 4 responsabilité limitée
1000 Bruxelles, Boulevard Simon Bolivar 34
RPM Numéro d’Entreprise : 0442.100.363

SUPPRESSION DE DIVISIONS COMPTABLES
NOMINATIONS STATUTAIRES

L'an deux mille quinze, le quinze décembre a dix heures.
A Bruxelles, au siége social

S'EST REUNIE:
L'assemblée générale extraordinaire des associés de la société coopérative 4 responsabilité
limitée ENGIE CC, établie 4 1000 Bruxelles, Boulevard Simon Bolivar 34, immatriculée au
registre des personnes morales 4 Bruxelles sous le numéro 0442.100.363.
Société constituée sous la forme d’une société anonyme le 31 juillet 1990, suivant acte regu
par le Notaire Jean-Luc Indekeu 4 Bruxelles, publié par extrait aux annexes du Moniteur
belge du 19 octobre 1990 sous le numéro 901019-134.

 

La société a été transformée en société coopérative a responsabilité limitée par décision de
l'assemblée générale extraordinaire du 24 décembre 2002. Les statuts ont été modifiés en
dernier lieu par décision de l'assemblée générale extraordinaire des associés du 20 novembre
2015, suivant acte recu par le Notaire Damien Hisette 4 Bruxelles ; la publication par extrait a
été faite aux annexes du Moniteur belge du sous le numéro
Bureau

La séance est ouverte sous la présidence de Monsieur Guido De Clercq.
Le président désigne comme secrétaire Monsieur Michael Delmée.
L'assemblée choisit comme scrutateurs :

1. Monsieur Guido Vanhove;

2. Monsieur Thierry van den Hove.

Composition de l'assemblée
Liste des présences
L'assemblée générale est composée des associés cités ci-dessous, lesquels déclarent détenir le
nombre de parts sociales ci-aprés indiqué :
Nombre de parts sociales

1. la société coopérative 4 responsabilité limitéee GDF SUEZ E.M.T.,

établie 4 B-1000 Bruxelles, boulevard Simon Bolivar 34, 1
2. la société anonyme ELECTRABEL, établie a

1000 Bruxelles, boulevard Simon Bolivar 34, 1
35 la société anonyme GDF SUEZ INVEST INTERNATIONAL,

établie 4 L-1611 Luxembourg, 65 avenue de la Gare 23.369.320
SOIT AU TOTAL: 23.369.322

5/9
Procurations

L’associé sous | est valablement représenté par Monsieur Guido De Clercq, en vertu d’une
procuration ci-annexée.
L’associé sous 2 est valablement représenté par Monsieur Thierry van den Hove, en vertu
dune procuration ci-annexée.
L’associé sous 3 est valablement représenté par Monsieur Guido Vanhove, en vertu d’une
procuration ci-annexée.

Exposé du président
Le président dirige les débats et expose ce qui suit :

I. La partie fixe du capital social de la société s'éleve 4 1.250.000 EUR. Le
capital souscrit - partie fixe et partie variable cumulées - s’établit 4 5.842.330.500 EUR,
divisé en 23.369.322 parts sans mention de valeur nominale.

IL. Il résulte de la liste de présence que tous les associés sont représentés a
l'assemblée. Tous les associés étant représentés, il n'a pas lieu de justifier de l’envoi de
convocations écrites. L’assemblée est habilitée 4 délibérer et 4 statuer valablement.

Ill. Tous les associés se sont déclarés d'accord que l'assemblée délibére sur l'ordre
du jour suivant :

ORDRE DU JOUR
1. CONVERSIONS DE PARTS
Afin (i) de convertir 124.468 parts de catégories « F », 1.016.366 parts de catégorie « G » et
803.706 parts de catégorie «I» en autant de parts de catégorie « A» ayant les mémes
caractéristiques et représentatives des mémes droits,

et (ii) d’en tirer les conséquences en ce qui concerne la suppression des divisions comptables
F, G et I correspondantes.

2. NOMINATIONS STATUTAIRES
Comités de Management : Désignation des représentants :
-  catégorie A : Olivier Homans, Maxime Paulus de Chatelet et Thierry van den Hove ;
-  eatégorie B : Amaud Rotsart et Florence Verhaeghe ;
-  catégorie C : Maxime Paulus de Chatelet et Thierry van den Hove ;
-  catégorie D : Sven De Smet ;
-  catégorie E : Didier Engels ;
-  catégorie J : Anja Mackelberg et Tanguy Stinglhamber.

Constatation
L'exposé du président est approuvé par tous les membres présents de l'assemblée.
Délibérations — Résolutions
L'assemblée aborde son ordre du jour et, aprés délibération, prend la résolution suivante:

Premiére résolution

L’assemblée décide de convertir :

6/9
- 124.468 parts de catégorie « F » dont 121.668 parts sont entiérement libérées
et 2.800 parts le sont a concurrence de 71,429 %,

- 1.016.366 parts de catégorie «G» dont 12.227 parts sont libérées a
concurrence de 26 %, 100.000 parts 4 concurrence de 90% et 904.139 parts a
concurrence de 100%,

- 803.706 parts de catégorie « I» dont 2.438 parts sont libérées 4 concurrence
de 25%, 96 parts 4 concurrence de 26% et 801.172 parts 4 concurrence de 100%,

en autant de parts de catégories « A » ayant les mémes caractéristiques et représentatives des
mémes droits, 4 savoir 1.944.540 parts de catégorie « A» réparties comme suit :

- 1.826.979 parts sont totalement libérées,

- 100.000 parts sont libérées a concurrence de 90%,

- 2.800 parts sont libérées 4 concurrence de 71,429 %,
- 12.323 parts sont libérées 4 concurrence de 26 %,

- 2.438 parts sont libérées 4 concurrence de 25%.

Elles sont toutes détenues par GDF SUEZ INVEST INTERNATIONAL SA.

L’assemblée générale décide en conséquence la suppression des divisions comptables F, G et
I correspondant aux catégories de parts susvisées, avec effet au 31 décembre 2015.

La nouvelle répartition du capital de la société se présente dés lors comme suit :

 

Catégorie Détenues par Nbre parts Capital souscrit__ Capital non libéré Capital libéré

A GDF SUEZ LI. 1.949.540 487.385.000 -5.436,880 481.948.120
B GDF SUEZ LI. 4.080.558 1.020.139.500 -2.815.280 1.017.324.220
c GDF SUEZ LI. 10.827.047 2.706.761.750 -138.788.120 2.567.973.630

GDF SUEZ E.M.T. 1 250 0 250
D GDF SUEZ LI. 513.281 128.320.250 -1.125.000 127.195.250
E GDF SUEZ LI. 3.771.910 942.977.500 -15.706.875 927.270.625

ELECTRABEL 1 250 0 250
J GDF SUEZ LL. 2.226.984 556.746.000 0 556.746.000

 

NOMINATIONS STATUTAIRES
Deuxiéme résolution

Comités de Management

Suite aux changements dans les catégories précitées, I’assemblée générale confirme la

composition suivante des comités de management :

- catégorie A : Olivier Homans, Maxime Paulus de ChAtelet et Thierry van den Hove ;

- catégorie B : Arnaud Rotsart et Florence Verhaeghe ;

- catégorie C : Maxime Paulus de Chatelet et Thierry van den Hove ;

7 catégorie D : Sven De Smet ;

- catégorie E : Didier Engels ;

- catégorie J : Anja Mackelberg et Tanguy Stinglhamber.

Vote
Toutes ces décisions sont prises a l’unanimité des voix.

19
Cléture de la séance
L'ordre du jour étant épuisé, le président déclare la séance levée.

Fait et dressé 4 Bruxelles, date et lieu que dessus.
Lecture faite, les membres du bureau ont signé ce procés-verbal.

QO. LN EE

lt

Guide VANROVE “T. wom du Hove

 

L. Ne Cvrercy,

file

8/9
Liste des présences / Aanwezigheidslijst

 

plac

    
   

 

GDF SUEZ E.M.T. ELECTRABEL
représentée par / vertegenwoordigd door représentée par / vertegenwoordigd door
Guido De Clercq Thierry van den Hove

G INTERNATIONAL
représentée par / vertegenwoordigd door
Guido Vanhove

9/9
