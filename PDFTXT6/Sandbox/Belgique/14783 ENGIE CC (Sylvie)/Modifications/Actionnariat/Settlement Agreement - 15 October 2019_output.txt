SETTLEMENT AGREEMENT
THIS AGREEMENT IS MADE AND ENTERED INTO
BETWEEN

ELECTRABEL SA/NV, a company incorporated and existing under the laws of Belgium, with
its registered office at Boulevard Simon Bolivar 34, 1000 Brussels, and registered with
RPR/RPM in Brussels under number 0403.170.701 (“Electrabel’”),

ENGIE CC SCRL/CVBA, a company incorporated and existing under the laws of Belgium with
its registered office at Boulevard Simon Bolivar 34, 1000 Brussels, and registered with
RPR/RPM in Brussels under number 0442.100.363 (“Engie CC”),

ENGIE INVEST INTERNATIONAL S.A., a company incorporated and existing under the laws of
Luxembourg, with its registered office at Avenue de la Gare, 65, L-1611 Luxembourg and
registered with RCS in Luxembourg under number B 1.860 (“Engie II”),

Each of them a “Party” and together the “Parties”

WHEREAS

(A)The Parties are affiliated companies that belong to the ENGIE Group. The Parties are
related as follows:
a. Engie II holds 20.657.320 shares, representing 99,9 % of the equity of Engie CC;
Engie II “Division E” holds 100% of the equity of Engie CC “Division E”;
b. Electrabel holds directly 185.622 shares, representing 7,4% of the equity of Engie II;
Electrabel holds 100% of the equity of Engie II “Division E”;

(B)On 1 July 2019 Engie CC granted the following loan to Electrabel: a term loan of
1.165.000.000 € with maturity date on 2 December 2019 (the “Lean”). As of 15 October
2019, the principal amount outstanding thereunder is 1.165.000.000 € and accrued interest
amount to 176,691,67 €;

(C)On 19 July 2011, Engie II and Engie CC entered into a profit participating loan facility and
interest-bearing loan agreement (the “PPL Loan”). As of 15 October 2019, the principal
amount outstanding thereunder with respect to “Division E” is 7.453.494.615,59 €;

(D)On 15 April 2016, Electrabel consented an income sharing loan facility to Engie II “Division
E” (the “ISL Loan”). As of 15 May 2019, the principal amount outstanding thereunder is
7.453.500.000,00 €;

(E) On 29 March 2019, the board of directors of Engie CC decided on the early repayment of
the PPL Loan to Engie II in the amount of 932.000.000 €;

(F) On 4 October 2019, the board of directors of Engie II decided on the early repayment of the
ISL Loan to Electrabel in the amount of 932.000.000 €;
(G) On 2™ December 2019, Electrabel has to pay Engie CC a total amount of 1.165.391.245,83
€ under the Loan;

(H)On 15 October 2019, (i) the extraordinary shareholders meeting of Engie CC decided (i) to
reduce the capital of its “Division E” in the amount of 233.000.000 €, and (il) to confirm
the decision of the board of directors of Engie CC on the early repayment of the PPL Loan
in the amount of 932.000.000 €;

(I) On 15 October 2019, (i) the extraordinary shareholders meeting of Engie II decided (i) to
reduce the capital of its “Division E” in the amount of 233.000.000 €, and (ii) to confirm
the decision of the board of directors of Engie II on the early repayment of the ISL Loan in
the amount of 932.000.000 €;

(J) It is the intention of the Parties today to organize in this agreement the settlement of
payments from and to each other resulting from the capital reductions, early repayments of
the PPL Loan and the ISL Loan and reimbursement of the Loan on its final maturity date
(or as otherwise agreed herein).

NOW, THEREFORE, THE PARTIES HAVE AGREED AS FOLLOWS:

ARTICLE 1 — ENGIE CC CAPITAL REDUCTION AND PPL PREPAYMENT

1.1. On 15 October 2019, Engie CC will pay to Engie II the debt that ensues from (i) the
capital reduction of its “Division E” in the amount of 233.000.000 €, and (ii) the early
repayment of the PPL Loan in the principal amount of 932.000.000 € (whilst interest accrued
thereon shall be paid in accordance with the terms of the PPL Loan on their due date), by the
assignment to Engie II of all the claims, rights, title and interest it holds on Electrabel pursuant
to the Loan and relating to the repayment of its principal amount, in the amount of
1.165.000.000 €.

1.2. On 15 October 2019, Engie CC will assign to Engie II all other claims, rights, title and
interest it holds on Electrabel pursuant to the Loan other than in respect of the repayment of its
principal amount but corresponding to the interest accrued on the Loan as per 15 October 2019
and future interest that will accrue thereafter (if any) for a consideration of 1.165.178.892,64 €,
this consideration being due and payable in cash on the account to be specified by Engie CC on
the same date.

1.3. As from now Electrabel acknowledges these assignments upon their occurrence and
declares that as a consequence, it will become the debtor of Engie II for all amounts and
obligations resulting from the Loan.

1.4. Since (i) the amounts owed by Engie CC to Engie II in respect of the capital reduction
and the early repayment of the PPL Loan equal 1.165.000.00 €, (ii) the amounts owed by Engie
II to Engie CC in respect of the Loan represent a consideration of 1.165.178.892,64 €, and (iii)
the compensations operated under points 1.1. and 1.2. hereinabove amount to 1.165.000.000 €,
Engie II will settle the difference between both amounts (amounting to 178.892,64 €) in cash
to the account as designated by Engie CC.

ARTICLE 2 — ENGIE II CAPITAL REDUCTION AND ISL PREPAYMENT

2.1. | Engie Il and Electrabel agree that the obligation of Engie II to make the payment which
results from the capital reduction of “Division E” of Engie II in the amount of 233.000.000 €
to be paid to Electrabel on 24 December 2019, will be compensated by the obligation of
Electrabel to repay to Engie Il any amounts due and payable on such date under the Loan at
that date.

2.2. On 2™ December 2019, Engie II will partly prepay the ISL Loan to Electrabel, in the
principal amount of 932.000.000 € (corresponding only to the prepayment of principal
outstanding under the ISL Loan whilst interest accrued thereon shall be paid in accordance with
the terms of the ISL Loan on their due date). Engie II and Electrabel agree that the obligation
to make the payment which results from this prepayment of the ISL by Engie II will be
compensated by the obligation to repay any amounts due and payable by Electrabel to Engie IT
on such date under the Loan at that date.

2.2. Since (i) the amount owed by Electrabel to Engie II in respect of the Loan equals
1.165.391.245,83 € and (ii) the compensations operated under points 2.1. and 2.2. hereinabove
amount to 1.165.000.000 €, Electrabel will settle the difference between both amounts
(amounting to 391.245,83 €) in cash to the account as designated by Engie II.

ARTICLE 3 - GOVERNING LAW

This agreement is governed by the laws of Belgium.
Executed in Brussels on 15 October 2019 in 3 original copies, each party acknowledging receipt

of one.

ELECTRABEL SA

Patrick van der BEKEN Philippe VAN TROEYE

Administrateur Administrateur-délégué
ENGIE CC SCRL

eg!
Jean-Marc TURCHINI Thierry van de HOVE
Administrateur Administrateur-délégué

ENGIE INVEST INTERNATIONAL SA

   

ANTELLI Arnaud HANON DE LOUVET
dministrateur Admninistrateur
Executed in Brussels on 15 October 2019 in 3 original copies, each party acknowledging receipt
of one.

ELECTRABEL SA

Patrick van der BEKEN Philippe VAN TROEYE
Administrateur Administrateur-délégué

ENGIE CC SCRL

Thierry van den HOVE
Administrateur-délégué

 

ENGIE INVEST INTERNATIONAL SA

Shepherd SANTELLI Armaud HANON DE LOUVET
Administrateur Administrateur-délégué
