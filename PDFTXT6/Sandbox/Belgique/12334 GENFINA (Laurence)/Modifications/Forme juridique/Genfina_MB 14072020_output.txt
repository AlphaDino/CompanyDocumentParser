Mod PDF 19.01

Copie a publier aux annexes au Moniteur belge
apres dépdt de l'acte au greffe

Réservé
au

Déposé

Moniteur *20332465*
belge

oO
>
oO
a
L
=]
®
£
c
oO
=
5
To
n
@®
x
oO
<
c
¢
'
oO
N
oO
N
~~
x
oO
=
+
~
'
To
&
a
a
2
o
®
ai)
Yn
x
Oo
2
2
®
a
~
oO
£
5
c
®o
Do
&
a

10-07-2020

Greffe

  

 

N° d'entreprise : 0435084986
Nom
(en entier): GENFINA

(en abrégé) :

Forme légale : Société coopérative a responsabilité limitée

Adresse complete du siége Boulevard Simon Bolivar 34
: 1000 Bruxelles

Objet de I'acte : MODIFICATION FORME JURIDIQUE

D’aprés un procés-verbal recu par Maitre Damien HISETTE, notaire a Bruxelles (deuxiéme canton),
associé de « Van Halteren, Notaires Associés », a 1000 Bruxelles, rue de Ligne 13, le 30 juin 2020, il
résulte que :

wl.

PREMIERE RESOLUTION.

Conformément a l’article 39 § 1 de la loi du 23 mars 2019 introduisant le Code des sociétés et des
associations et portant des dispositions diverses, le Code des sociétés et des associations (ci-aprés
le « CSA ») est d’application depuis le 1er janvier 2020 et les statuts doivent étre mis en conformité
avec les dispositions du CSA.

En conséquence, l’assemblée constate que :

. la société adopte la forme légale du Code des sociétés et des associations qui se rapproche
le plus de sa forme actuelle, c’est-a-dire celle de la société a responsabilité limitée (en abrégé SRL);
. constate que la partie libérée de la part fixe du capital et la réserve légale de la société, sont

convertis de plein droit en un compte de capitaux propres statutairement indisponible, en application
de l'article 39, §2, deuxiéme alinéa de la loi du 23 mars introduisant le Code des sociétés et des
associations et portant des dispositions diverses.
En dérogation a ce qui précéde, l’assemblée générale décide de limiter ce compte de capitaux
propres statutairement indisponible a 99.703.850 EUR et de rendre la réserve légale disponible pour
distribution.
weliee
DEUXIEME RESOLUTION.
En conséquence de la résolution qui précéde, l’assemblée générale décide d’adopter un nouveau
texte de statuts, qui sont en conformité avec le Code des sociétés et des associations, sans
modification de l'objet.
L’assemblée générale décide et déclare que le texte des nouveaux statuts est le suivant :
TITRE I.
FORME - DENOMINATION - OBJET - SIEGE_- DUREE
Article 1
La société est une société a responsabilité limitée au sens du Code des Sociétés et des Associations
(« CSA »). Elle a pour dénomination "GENFINA" (ci-aprés, la « Société »).
Article 2
La société a pour objet :
1. la création, l'acquisition et le développement — par voie d'apport, de fusion, d'achat, de
souscription et de cession d'actions, obligations ou autres intéréts ou autrement — de toutes sociétés,
entreprises et associations, en particulier celles exercant leurs activités dans les domaines de
l'6nergie, de l'eau et de la propreté, de la banque, de I'assurance, de I'ingénierie, des
télécommunications, du traitement de l'information, de I'électronique, des communications, des
médias ou des secteurs similaires ou connexes;
2. l'étude, la conception, la recherche et le développement, la fabrication, les services, le négoce, la
_ gestion, I'exploitation, le financement et toutes autres activités, intéressant les domaines susvisés ou
Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
   

Réservé
au
Moniteur

belge

Bijlagen bij het Belgisch Staatsblad - 14/07/2020 - Annexes du Moniteur belge

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

Volet B - suite Mod PDF 19.01
les secteurs similaires ou connexes;

3. l'administration, le financement, l'assistance financiére, technique, administrative ou de gestion,
loctroi de garanties, la réalisation d'opérations financiéres, de trésorerie et de couverture de risques,
en faveur de toutes sociétés, entreprises ou associations.

La société peut réaliser son objet soit directement soit indirectement, en son nom propre ou pour
compte de tiers, en effectuant, tant en Belgique qu’a Iétranger, toutes opérations quelconques
mobiliéres, immobiliéres, commerciales, civiles, financiéres ou industrielles, de nature a favoriser
ledit objet ou celui des sociétés, entreprises ou associations faisant partie du groupe auquel la
société appartient.

Article 3

Le siége de la Société est établi dans la Région de Bruxelles-Capitale.

Il peut 6tre transféré en tout endroit de la Région de Bruxelles-Capitale ou de la région de langue
francaise de Belgique, par simple décision de l’organe d’administration (ci-aprés, le « Conseil d’
administration » ou le « Conseil ») qui a tous pouvoirs aux fins de faire constater authentiquement la
modification statutaire 6ventuelle qui en résulte, sans que cela ne puisse entrainer une modification
de la langue des statuts.

Article 4

La Société peut établir, par décision du conseil d'administration, des siéges administratifs, sieges
d'opération, siéges d'exploitation, succursales ou agences, tant en Belgique qu’a I6tranger.

Article 5

La durée de la Société est illimitée.

TITRE Il.

PATRIMOINE DE LA SOCIETE

Article 6

En rémunération des apports effectués, trois cent quatre-vingt-six mille six cent quatre-vingt-quatre
(386.684) actions ont été émises, intégralement et inconditionnellement souscrites.

Article 7

L’assemblée générale appelée a délibérer sur I’6mission d’actions nouvelles indique notamment
dans ses résolutions leur degré de libération.

Les actions nouvelles a souscrire en numéraire doivent étre offertes par préférence aux actionnaires
existants, proportionnellement au nombre d’actions qu’ils détiennent.

Le droit de souscription préférentielle peut 6tre exercé pendant un délai d’au moins quinze (15) jours
a dater de l’ouverture de la souscription.

L’ouverture de la souscription avec droit de préférence ainsi que son délai d’exercice sont fixés par |’
organe qui procéde a I’émission et sont portés a la connaissance des actionnaires par courrier
électronique, ou, pour les personnes dont elle ne dispose pas d’une adresse électronique, par
courrier ordinaire, a envoyer le méme jour que les communications électroniques. Si ce droit n'a pas
entiérement été exercé, les actions restantes sont offertes conformément aux alinéas précédents par
priorité aux actionnaires ayant déja exercé la totalité de leur droit de préférence. Il sera procédé de
cette maniére, selon les modalités proposées par le conseil d’administration, jusqu’a ce que |’
émission soit entiérement souscrite ou que plus aucun actionnaire ne se prévale de cette faculté.
L’assemblée générale peut décider que des apports se font sans émission d’actions nouvelles sans
que les équilibres entre actionnaires ne soient modifiés, et ce a la majorité simple des voix et par
acte authentique.

Article 8

Toutes les actions futures doivent étre intégralement libérées, sauf décision contraire reprises dans
les conditions d’émission é6tablies par le conseil d’administration.

Les fonds propres ne sont pas distribuables a concurrence de nonante-neuf millions sept cent trois
mille huit cent cinquante euros (EUR 99.703.850) constituant la partie indisponible des capitaux
propres de la Société.

Pour les nouveaux apports, les conditions d’émission détermineront s’ils sont également inscrits sur
ce compte de capitaux propres indisponible. A défaut de stipulation a cet égard dans les conditions d’
émission, ils sont présumés ne pas 6tre inscrits sur ce compte de capitaux propres indisponible.

En cas d’apport sans émission de nouvelles actions, ils sont présumés ne pas étre inscrits sur ce
compte de capitaux propres indisponible.

Article 9

Les libérations a effectuer en numéraire ou par apports en nature sur les actions non encore
entiérement libérées doivent étre faites aux lieux et aux dates que le conseil d’administration
détermine. Les libérations appelées s'imputent de fagon égale sur l'ensemble des actions dont
l'actionnaire est titulaire.

Le conseil d’administration peut autoriser les actionnaires a libérer leurs actions par anticipation;
dans ce cas, il détermine les conditions auxquelles les libérations anticipées sont admises.

Article 10

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
   

Réservé
au
Moniteur

belge

Bijlagen bij het Belgisch Staatsblad - 14/07/2020 - Annexes du Moniteur belge

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

Volet B - suite Mod PDF 19.01
Les actions sont nominatives.

Elles sont inscrites dans le registre des actions nominatives; ce registre contiendra les mentions
requises par le CSA. Les titulaires d’actions peuvent prendre connaissance de ce registre relatif a
leurs titres.

Le conseil d’administration (notamment via un mandataire) veillera aux adaptations requises dans le
registre des actions de la Société. Le registre peut étre tenu de maniére électronique. Le conseil d’
administration (notamment via un mandataire) peut délivrer des certificats attestant de son contenu.
Article 11

Les actions sont librement cessibles entre actionnaires. Elles ne sont cessibles a d'autres
cessionnaires que moyennant l’'agrément préalable du conseil d’administration statuant a la majorité
simple.

L'admission de nouveaux actionnaires sera constatée par l'apposition de leur signature, précédée de
la date en regard du nom, sur le registre des actions de la Société.

Article 12

Pour étre admis comme actionnaire le candidat doit :

a) 6tre agréé par le conseil d’administration statuant a la majorité simple;

b) adhérer sans réserve aux statuts et au reglement d'ordre intérieur si un tel reglement a été
approuvé et, pour ce faire, apposer sa signature personnellement ou par mandataire dans le registre
des actions;

c) souscrire a une (1) action au moins.

Article 13

Un actionnaire ne peut ni démissionner, ni opérer de retrait d’actions, pour tout ou partie des actions
qu'il détient, entiérement libérées ou non, que moyennant l'accord de l'assemblée générale statuant
a la majorité des trois quarts (3/4) des actions émises.

La demande peut 6tre introduite a tout moment de l'année.

En cas de démission, l’actionnaire a droit uniquement a recevoir la valeur de ses actions telle qu'elle
résultera des derniers comptes annuels approuvés tenant compte du montant réellement libéré (non
encore remboursé).

Tout actionnaire peut étre exclu pour juste motif ou s'il cesse de remplir les conditions d'admission
prévues par les présents statuts.

L'exclusion est prononcée par l'assemblée générale.

Elle ne pourra étre prononcée qu'aprés que I'actionnaire dont l'exclusion est demandée aura été
invité a faire connaitre ses observations. La décision d'exclusion doit étre motivée par le conseil d’
administration. II est fait mention de I'exclusion dans le registre des actions.

L'actionnaire exclu a droit au remboursement de la valeur de ses actions telle qu'elle résultera des
derniers comptes annuels approuvés, sous les mémes modalités et réserves que I'actionnaire
démissionnaire tenant compte du montant réellement libéré (non encore remboursé).

Article 14

Moyennant respect des articles pertinents du CSA, la Société peut acquérir ses propres actions,
entiérement libérées, moyennant une décision de l’'assemblée générale statuant a la majorité des
trois quarts (3/4) des actions émises.

Cette offre d’acquisition est faite a tous les actionnaires par classe d’actions sauf si cette décision est
prise a l’'unanimité par une assemblée générale ou l'ensemble des actionnaires étaient présents ou
représentés.

Il peut 6tre procédé, au moment de I’acquisition, a l’'annulation de tout ou partie de ces actions
moyennant adaptation des statuts.

Si les actions sont conservées en auto-détention, il est constitué une réserve indisponible d’un
montant égal a la valeur d’inventaire des actions acquises. Les actions en auto-détention n'ont pas le
droit de vote.

Article 15

Le conseil d’administration peut suspendre I'exercice des droits afférents aux actions faisant l'objet
d'une copropriété, d'un usufruit ou d'un gage, jusqu’a ce qu'une seule personne soit désignée comme
6tant, a I'égard de la Société, propriétaire de ces actions.

TITRE Ill.

ADMINISTRATION - DIRECTION - REPRESENTATION _- CONTROLE.

Article 16

La société est administrée par un conseil d'administration composé de trois (3) membres au
minimum nommés pour six (6) ans au plus par l'assemblée générale des actionnaires et révocables
par elle. Ils forment un organe collégial au sens du CSA.

Les administrateurs sont rééligibles.

Les fonctions des administrateurs sortants et non réélus prennent fin immédiatement a I'issue de
l'assemblée générale ordinaire.

Le conseil d'administration élit en son sein un président et peut élire un vice-président.

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
   

Réservé
au
Moniteur

belge

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

Mod PDF 19.01

Volet B - suite

Article 17

En cas de vacance d'un ou plusieurs mandats d'administrateur, les membres restants du conseil
d'administration peuvent pourvoir provisoirement au remplacement jusqu’a la prochaine assemblée
générale qui procéde a la confirmation de la nomination. A défaut de confirmation, le mandat de |’
administrateur coopté prend fin aprés l’'assemblée générale, sans que cela porte préjudice a la
régularité de la composition du Conseil d’administration jusqu’a cette date.

Article 18

Les administrateurs ne contractent aucune obligation personnelle relativement aux engagements de
la Société. Ils sont responsables a I'égard de la Société de I'exécution de leur mandat et des fautes
commises dans leur gestion.

Article 19

Le conseil d’administration se réunit au siége ou a I'endroit indiqué dans les convocations chaque
fois que I'intérét de la Société I'exige et en tout cas, chaque fois que deux (2) administrateurs au
moins le demandent.

Les convocations sont faites a chacun des administrateurs huit (8) jours avant la réunion, sauf cas
d'urgence, avec communication de I'ordre du jour.

Le conseil d’administration se réunit valablement sans convocation si tous les administrateurs sont
présents ou représentés et ont marqué leur accord sur I'ordre du jour.

Les réunions sont présidées par le président du conseil ou, en cas d'empéchement de celui-ci, par
un administrateur désigné par ses collégues.

Toute communication aux actionnaires, administrateurs et commissaire est suffisamment rencontrée
lorsqu’effectuée par moyen électronique (dont courrier électronique sauf demande spécifique au
conseil d’administration).

Article 20

Le conseil ne peut délibérer et statuer que si la majorité de ses membres est présente ou
représentée.

Chaque administrateur peut, par tout moyen de transmission, donner a I'un de ses collégues pouvoir
de le représenter a une séance du conseil et d'y voter a sa place.

Les réunions, en ce compris les délibérations et votes, peuvent, sur proposition de son président,
étre tenues via tout moyen de télécommunication, notamment oral ou visuel, qui permette des débats
entre des participants géographiquement éloignés. Dans ce cas, la réunion est réputée se tenir au
siége de la Société et tous les administrateurs participants y 6tre présents.

Les décisions sont prises a la majorité des voix. En cas de partage, celle du président de la réunion
est prépondérante. Si, dans une séance du conseil réunissant la majorité requise pour délibérer
valablement, un ou plusieurs administrateurs s‘abstiennent, les résolutions sont valablement prises a
la majorité des autres membres du conseil, présents ou représentés.

Les décisions du conseil d’administration peuvent étre prises par consentement unanime des
administrateurs exprimé par écrit, a l'exception des actes notariés.

Article 21

Les délibérations du conseil d’administration sont constatées par des procés-verbaux signés par le
président et les administrateurs qui le souhaitent. Ces procés-verbaux sont consignés ou reliés en un
recueil spécial sous une forme quelconque, y compris la forme électronique. Les copies ou extraits, a
produire en justice ou ailleurs, sont signés par un ou plusieurs administrateurs ayant le pouvoir de
représentation.

Article 22

L'assemblée générale peut allouer aux administrateurs un émolument fixe ou des jetons de présence
a porter a charge du compte de résultats.

Le conseil d'administration est autorisé également a accorder aux administrateurs chargés de
fonctions ou missions spéciales, une rémunération particuliére a prélever sur les frais généraux.
Article 23

Le conseil d'administration a le pouvoir d'accomplir tous les actes nécessaires ou utiles a la
réalisation de l'objet social, a l'exception de ceux que Ia loi ou les statuts réservent a l'assemblée
générale.

Article 24

Le conseil peut créer en son sein ou en dehors un comité d’audit dont il détermine la composition et
les compétences.

Le conseil d'administration peut déléguer la gestion opérationnelle (en ce compris la gestion
journaliére) ainsi que la représentation de la société en ce qui concerne cette gestion a un ou
plusieurs délégués agissant dans ce cas deux a deux. Le conseil nomme et révoque les délégués a
cette gestion qui sont choisis dans ou hors de son sein, fixe leur rémunération et détermine leurs
attributions.

Le conseil d'administration ainsi que les délégués a la gestion opérationnelle (en ce compris la
gestion journaliére) dans le cadre de cette gestion, peuvent également conférer des pouvoirs

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
   

Réservé
au
Moniteur

belge

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

Mod PDF 19.01

Volet B_ - suite

spéciaux et déterminés a une ou plusieurs personnes de leur choix.

Article 25

La société est représentée dans tous actes, y compris ceux ou intervient un fonctionnaire public ou
un officier ministériel, ou en justice en ce compris le conseil d’Etat et les autres juridictions
administratives :

- soit par deux administrateurs agissant conjointement;

- soit, dans les limites de la gestion opérationnelle (en ce compris la gestion journaliére), par le
délégué a cette gestion s'il n'y en a qu'un seul, et par deux délégués agissant conjointement s'ils sont
plusieurs.

Elle est, en outre, valablement engagée par les mandataires spéciaux agissant dans les limites de
leur mandat.

Article 26

Le contréle des comptes de la société est confié a un commissaire au moins, nommé par
l'assemblée générale pour une durée de trois (3) ans, rééligible et révocable par elle.

Si par suite de décés ou pour un autre motif, il n'y a plus de commissaire, le conseil d'administration
doit convoquer immédiatement l'assemblée générale pour pourvoir a cette vacance.

Les fonctions des commissaires sortants et non réélus prennent fin immédiatement aprés
l'assemblée générale ordinaire.

La mission et les pouvoirs des commissaires sont ceux que leur assigne le CSA.

L'assemblée générale détermine les émoluments des commissaires correspondant a leurs
prestations de contrdle des comptes. Toutefois, le conseil d'administration peut attribuer au
commissaire des émoluments pour des missions spéciales; il informe la plus prochaine assemblée
générale ordinaire par le rapport de gestion, de l'objet de ces missions et de leur rémunération.
TITRE IV.

ASSEMBLEES GENERALES.

Article 27

L'assemblée générale a les pouvoirs qui sont déterminés par la loi et les présents statuts. Toutefois,
une extension statutaire des pouvoirs n’est pas opposable aux tiers, sauf si la Société prouve que le
tiers en avait connaissance ou ne pouvait l'ignorer compte tenu des circonstances, sans que la seule
publication des statuts suffise a constituer cette preuve.

Article 28

L'assemblée générale se tient au siége social ou a I'endroit indiqué dans les avis de convocation.
L’assemblée générale ordinaire a lieu le deuxiéme mardi du mois de juin a quinze heures ou, si ce
jour est un jour férié légal, le premier jour ouvrable suivant, autre qu'un samedi.

Article 29

Le conseil d’administration, et le cas 6chéant le commissaire, convoque les assemblées générales
tant ordinaires qu'extraordinaires.

L'assemblée doit 6tre convoquée lorsque I’intérét social l’exige ou a la demande d'un ou plusieurs
actionnaires justifiant qu'ils représentent un dixiéme du nombre d’actions en circulation sur base du
registre des actions a cette date. Dans ce dernier cas, le ou les actionnaires indiquent au conseil d’
administration les sujets proposés a l’ordre du jour. L’'assemblée générale doit alors étre convoquée
dans un délai de trois (3) semaines de la demande.

Les convocations aux assemblées générales contiennent l’ordre du jour. Elles sont faites par e-mails
envoyés quinze jours au moins — sauf renonciation - avant l'assemblée aux actionnaires, aux
administrateurs et, le cas échéant, aux titulaires d’obligations convertibles nominatives, de droits de
souscription nominatifs ou de certificats nominatifs émis avec la collaboration de la Société et aux
commissaires.

Toute communication aux actionnaires, administrateurs et commissaire est suffisamment rencontrée
lorsqu’effectuée par moyen électronique (dont e-mails) sauf demande spécifique au conseil d’
administration.

Article 30

Pour étre admis a l'assemblée générale, les actionnaires doivent, si le conseil les y invite, six (6)
jours ouvrables avant la tenue de I'assemblée, informer la Société de leur intention d'y assister, ainsi
que du nombre d’actions pour lequel ils entendent prendre part au vote.

Article 31

Tout actionnaire peut se faire représenter a l'assemblée générale par un mandataire, actionnaire ou
non, par le biais d’une procuration écrite pour y voter en ses lieu et place.

Le conseil d’administration peut arréter la formule de procuration et exiger que celle-ci soit déposée a
l'endroit indiqué par lui et dans le délai qu'l fixe.

Une procuration octroyée reste valable pour chaque assemblée générale suivante dans la mesure ou
il y est traité des mémes points de l’ordre du jour, sauf si la Société est informée d’une cession des
actions concernées.

Article 32

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
   

Réservé
au
Moniteur

belge

Bijlagen bij het Belgisch Staatsblad - 14/07/2020 - Annexes du Moniteur belge

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

Mod PDF 19.01

Volet B - suite

L'assemblée est présidée par le président du conseil d’administration ou, a défaut, par un
administrateur désigné par ses collégues.

Le président de l'assemblée choisit le secrétaire. Si le nombre de personnes présentes le permet,
l'assemblée peut choisir deux scrutateurs, actionnaires ou non, sur proposition du président de
l'assemblée. Le président, le secrétaire, et le cas échéant les scrutateurs, forment ensemble le
bureau.

Article 33

Chaque action donne droit a une voix et un droit égal dans la répartition des bénéfices au sein de
chaque classe. Il en va de méme en ce qui concerne le produit de la liquidation de la Société.
Article 34

L'assemblée générale ne peut délibérer que sur les points figurant a son ordre du jour, sauf si toutes
les personnes a convoquer sont présentes ou représentées, et, dans ce dernier cas, si les
procurations le mentionnent expressément.

Sauf les cas prévus par la loi, les décisions sont prises a la simple majorité des voix valablement
exprimées, quel que soit le nombre de titres représentés, sans tenir compte des abstentions.

Les votes se font par main levée ou par appel nominal, a moins que l'assemblée n'en dispose
autrement. Les votes relatifs a des personnes se font au scrutin secret sur toute demande d’un
actionnaire.

En cas de nomination, si aucun candidat ne réunit la majorité des voix, il est procédé a un scrutin de
ballottage entre les candidats qui ont obtenu le plus de voix. En cas d'égalité du nombre de suffrages
ace scrutin, le plus 4gé des candidats est élu.

Article 35

Lorsque l'assemblée générale est appelée a décider d'une modification aux statuts, elle ne peut
valablement délibérer que si ceux qui assistent (ou sont représentés) a l'assemblée représentent la
moitié du nombre total des actions émises. Si cette derniére condition n'est pas remplie, une nouvelle
convocation est nécessaire et la nouvelle assemblée délibére valablement, quelle que soit la portion
représentée.

Une modification des statuts n'est admise, sauf exception légale, que si elle réunit les trois quarts
(3/4) des voix valablement exprimées (sans tenir compte des abstentions).

Article 36

Le conseil d’administration a le droit, aprés ouverture des débats, de proroger, séance tenante, la
décision relative a l'approbation des comptes annuels a trois (3) semaines. Cette prorogation, notifiée
avant la cléture de la séance et mentionnée au procés-verbal, n'annule pas les autres décisions
prises. Elle ne peut avoir lieu qu’une fois.

Les actionnaires doivent 6tre convoqués a nouveau pour la date que fixera le conseil d’
administration.

Les formalités remplies pour assister a la premiére séance, en ce compris le dépét des titres et
procurations, resteront valables pour la seconde. L’accomplissement de formalités non encore
remplies sera admis dans les délais statutaires.

La seconde assemblée générale a le droit d’arréter définitivement les comptes annuels.

Article 37

Les procés-verbaux des assemblées générales sont signés par les membres du bureau et par les
actionnaires qui le demandent et, sauf pour les procés-verbaux authentiques, sont consignés ou
reliés dans un recueil spécial tenu au siége sous une forme quelconque, y compris électronique
sécurisée.

Les expéditions, copies ou extraits a délivrer aux tiers sont signés par un ou plusieurs membres du
conseil d’administration.

Toute communication aux actionnaires, administrateurs et commissaire dans le cadre de toute
séance d’assemblée générale est suffisamment établie par moyen électronique (dont courrier
électronique).

TITRE V

INVENTAIRES ET COMPTES ANNUELS - BENEFICE ET REPARTITION

Article 38

L'exercice social commence le premier janvier et se termine le trente et un décembre de chaque
année.

Le conseil d’administration dresse un inventaire et établit, conformément a Ia loi, les comptes
annuels, qui comprennent le bilan, le compte de résultats et l'annexe. II rédige en outre le rapport de
gestion.

Un mois au moins avant l'assemblée générale, I'inventaire, les comptes annuels et le rapport de
gestion sont remis avec les piéces aux commissaires qui doivent faire leur rapport.

Article 39

Les comptes annuels, le rapport de gestion et le rapport des commissaires sont adressés aux
actionnaires en méme temps que la convocation.

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
Réservé
au
Moniteur

belge

   

Bijlagen bij het Belgisch Staatsblad - 14/07/2020 - Annexes du Moniteur belge

Volet B - suite Mod PDF 19.01
Article 40
Aprés avoir pris connaissance du rapport de gestion et du rapport des commissaires, l'assemblée
générale délibére sur les comptes annuels.

Aprés leur adoption, elle se prononce par un vote spécial sur la décharge des administrateurs et
commissaire(s).

Article 41
L'excédent favorable du compte de résultats constitue le bénéfice net.

Le solde éventuel est mis a la disposition de l'assemblée qui, sur proposition du conseil d’
administration, en détermine I'affectation pro rata temporis et liberationis.

Article 42
Le conseil d’administration peut procéder au paiement de dividendes, et d’acomptes sur dividendes,
dans le cadre des articles pertinents du CSA.

TITRE VI

DISSOLUTION — LIQUIDATION

Article 43

En cas de dissolution de la Société, l'assemblée générale régle le mode de liquidation et nomme un
ou plusieurs liquidateurs dont elle détermine les pouvoirs et les émoluments. Cette nomination se fait
sous réserve de la confirmation ou de I’homologation par le président du tribunal de |’entreprise
compétent.

Elle conserve le pouvoir de modifier les statuts si les besoins de la liquidation le justifient.

Article 44
Aprés le paiement des dettes et charges de la Société, le solde servira d’abord au remboursement
des versements effectués en libération des actions.

Si les actions ne se trouvent pas libérées toutes dans une égale proportion, les liquidateurs, avant de
procéder a la répartition prévue a I'alinéa qui précéde, doivent tenir compte de cette diversité de
situations et rétablir I'6quilibre en mettant toutes les actions sur un pied d'égalité absolue, soit par
des appels de fonds complémentaires a charge des actions insuffisamment libérées, soit par des
remboursements préalables, en espéces ou en nature, au profit des actions libérées dans une
proportion supérieure.

Le solde des actifs et passifs sera réparti entre les actions, par quotités égales.

TITRE VII

DISPOSITIONS GENERALES

Article 45

Pour tous litiges entre la Société et ses actionnaires, obligataires, administrateurs, commissaire(s) et
liquidateurs relatifs aux affaires de la Société et a l'exécution des présents statuts, compétence
exclusive est attribuée a un collége arbitral en nombre impair, composé conformément au réglement
de conciliation et d’arbitrage du CEPANI ou, a défaut de celui-ci, conformément aux régles du code
judiciaire; toutefois, la Société, si elle est demanderesse, sera en droit de porter le différend devant
tout autre tribunal compétent.

Article 46

Les actionnaires, obligataires, commissaire(s) et liquidateurs domiciliés a I'étranger et n'ayant fait
aucune élection de domicile en Belgique dament notifiée a la Société, sont censés avoir élu domicile
au siége ou tous actes peuvent valablement leur étre signifiés ou notifiés, la Société n'ayant pas
d’autre obligation que de les tenir a la disposition du destinataire.

wel.

TROISIEME RESOLUTION.

L’assemblée décide de donner mandat, pour une durée indéterminée :

* a tout administrateurs, a Michaél Delmée, Mesdames Danielle Timperman, Sylvie Deconinck,
Brit Colman et Claire de Meester, agissant seules et avec faculté de subdéléguer, afin d’inscrire et/ou
de modifier/supprimer l'inscription et toutes les formalités nécessaires a effectuer auprés du greffe du
tribunal de l’entreprise compétent, tribunaux, la Banque Carrefour des Entreprises, les guichets d’
entreprises, toutes instances administratives (dont l’administration TVA, l’administration des impdts
directs, les fonds d’assurance sociales, etc), tous registres (dont UBO) ;

* a tout employé de I’étude des notaires « VAN HALTEREN, Notaires associés », dont les
bureaux sont situés a 1000 Bruxelles, rue de Ligne 13, avec faculté de subdéléguer, aux fins de (i)
signer les formulaires de publication d’un extrait du procésverbal de toute décision d’ assemblée
générale et de faire le nécessaire pour le dépdét au greffe du Tribunal de l’entreprise compétent ainsi
que pour sa publication au Moniteur belge; (ii) gérer le registre électronique de la Société et mettre
tout certificat utile en la matiére.
wel.

QUATRIEME RESOLUTION.

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
Réservé VoletB  - suite Mod PDF 19.01
au

Moniteur L'assemblée décide de conférer tous pouvoirs, avec faculté de subdéléguer, au conseil d’

administration pour l’exécution des résolutions qui précédent.

wel

' | Pour extrait analytique conforme.

Déposé en méme temps : expédition et procurations.

(signé) Samuel WYNANT, notaire associé a Bruxelles.

 

Bijlagen bij het Belgisch Staatsblad - 14/07/2020 - Annexes du Moniteur belge

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
