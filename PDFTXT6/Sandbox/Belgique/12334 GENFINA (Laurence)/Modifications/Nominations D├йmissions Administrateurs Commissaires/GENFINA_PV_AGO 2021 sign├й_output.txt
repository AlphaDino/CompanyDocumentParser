GENFINA
SRL

Siége social : Boulevard Simon Bolivar 34 - 1000 Bruxelles
RPM Bruxelles n° 0435.084.986

PROCES-VERBAL DE L'ASSEMBLEE GENERALE ORDINAIRE
DES ASSOCIES TENUE LE 8 JUIN 2021

Ouverture de la séance

La séance est ouverte 4 15.00 heures, au siége social, sous la présidence du Comte
DAVIGNON.

Constitution du bureau
Le Président désigne comme secrétaire Michael DELMEE.

L'assemblée désigne comme scrutateurs Thierry van den HOVE et Xavier AGUIRRE
qui acceptent.

Ces personnes prennent place au bureau.

Dépét des piéces
Les piéces suivantes sont déposées sur le bureau :
— le rapport de gestion ainsi que le rapport du commissaire ;

- le bilan, le compte de résultats ainsi que l'annexe arrétés au 31 décembre 2020;
- les procurations des associés représentés.

Constitution de l'assemblée

Cette liste des présences constate que les 3 associés sont représentés. Ils représentent
ensemble 386.674 actions formant la totalité du capital.

Les administrateurs et commissaire ont renoncé aux formalités de convocation.
Toutes les formalités légales et statutaires ayant été accomplies, l'assemblée se reconnait

réguliérement convoquée, valablement constituée et apte a délibérer sur son ordre du
jour.

vy
L
t
Ordre du jour

1. Rapport de gestion du Conseil d’administration et rapport du commissaire sur
Vexercice cléturé le 31 décembre 2020.

2. Approbation des comptes annuels arrétés au 31 décembre 2020.

3. Décharge aux administrateurs et au commissaire.

4. Nominations statutaires.

Rapport de gestion et rapport du commissaire

Le Président rappelle que chaque associé a pu prendre connaissance du rapport de
gestion présenté par le conseil d'administration et du rapport du commissaire. II Propose
a l'assemblée de dispenser le bureau de la lecture de ces rapports.

Cette proposition est acceptée a l'unanimité.

Comptes annuels

Le bilan, dont le total s'éléve 4 518.485.146,35 €, le compte de résultats et l'annexe
arrétés au 31 décembre 2020, ainsi que |’affectation ci-dessous sont soumis au vote de
l'assemblée.

Bénéfice (perte) de l’exercice a affecter : -35.959.535,59 €
Bénéfice reporté de l’exercice précédent : 16.128.707,04 €
Bénéfice (perte) a reporter : -19.830.828,55 €

Le bilan, le compte de résultats et l'annexe, arrétés au 31 décembre 2020, ainsi que
l’affectation sont approuvés 4 l'unanimité des voix.

Décharge aux administrateurs et au commissaire

Le Président met aux voix la décharge 4 donner aux administrateurs pour leur gestion et
au commissaire pour sa mission de contréle durant I’exercice cléturé 31 décembre 2020.

L'assemblée approuve la décharge aux administrateurs et au commissaire 4 l'unanimité
des voix.
9. Nominations Statutaires
Conseil d’administration

Les mandats d’administrateur du Comte Etienne Davignon, de Xavier Aguirre, Patrick
van der Beken, Thierry van den Hove, Sergio Val viennent 4 échéance 4 I'issue de la
présente assemblée.

Sur proposition du Conseil, l’assemblée approuve a l’unanimité des voix: le
renouvellement des mandats d’administrateur :

(i) du Comte Davignon pour un terme d’un an; 1’assemblée acte la désignation, par le
Conseil, du Comte Davignon en qualité de Président et d’administrateur-délégué pour la
durée de son mandat d’administrateur (échéance a |’ AGO 2022).

(ii) de Xavier Aguirre, Patrick van der Beken, Thierry van den Hove et Sergio Val pour
un mandat de 3 ans (échéance a l’ AGO 2024).

Les mandats d’administrateur ne sont pas rémunérés.

10. Signature du procés-verbal

L'ordre du jour étant épuisé, le Président invite le bureau et les associés qui le souhaitent
a signer le présent procés-verbal. .

La séance est levée 4 15.50 heures.

CLV

Le Président
Comte E. Davignon

iB Nn. |

Le Secrétaire Les Scrutateurs
M. Delmée X. Aguirr Th. van den Hove

   
Liste des présences

4
SOPRANOR S.A.S ENGIE S.A.
représentée par Th: van den Hove représentée par le Comte E. Davignon

en vertu d’une procuration sous seing privé en vertu d’une procuration sous seing privé

 
