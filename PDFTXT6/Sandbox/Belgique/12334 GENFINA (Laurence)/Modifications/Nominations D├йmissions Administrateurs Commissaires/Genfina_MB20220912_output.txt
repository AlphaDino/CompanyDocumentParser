Réservé
au LJ
Moniteur

belge

 

Bijlagen bij het Belgisch Staatsblad - 12/09/2022 - Annexes du Moniteur belge

Mod PDF 19.01

Copie a publier aux annexes au Moniteur belge
apres dépdt de l'acte au greffe

Déposé
*22357018*

08-09-2022

Greffe

 

 

N° d'entreprise : 0435084986
Nom
(en entier): GENFINA

(en abrégé) :

Forme légale : Société a responsabilité limitée

Adresse complete du siége Boulevard Simon Bolivar 34
: 1000 Bruxelles

Objet de l'acte : DIVERS

ll résulte du procés-verbal de l’'assemblée générale du 14 juin 2022 que l’assemblée a pris les
décisions suivantes :

wl.

9. Nomination statutaire

Conseil d'administration

Le mandat d'administrateur du Comte Davignon vient a échéance a l’issue de la présente
assemblée.

Sur proposition du Conseil, l'assemblée approuve a I'unanimité des voix le renouvellement de
mandat d'administrateur du Comte Davignon pour un terme d'un an.

L'assemblée acte la désignation, par le Conseil, du Comte Davignon en qualité de Président et
d'administrateur-délégué pour la durée de son mandat d'administrateur (6échéance a I'AGO 2023).
wl.

Pour extrait conforme.

(signé) Damien HISETTE

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
