DocuSign Envelope ID: B598ECF2-33DE-43AA-B128-FOBC188E7A54

GENFINA

SCRL

Siege social : Boulevard Simon Bolivar 34 - 1000 Bruxelles
RPM Bruxelles n° 0435.084.986

PROCES-VERBAL DE L'ASSEMBLEE GENERALE ORDINAIRE
DES ASSOCIES TENUE LE 9 JUIN 2020

 

 

1. Ouverture de la séance
La séance est ouverte 4 15.00 heures, au siége social, sous la présidence du Comte
DAVIGNON.

2. Constitution du bureau
Le Président désigne comme secrétaire Michael DELMEE.

L'assemblée désigne comme scrutateurs Thierry van den HOVE et Xavier AGUIRRE
qui acceptent.

Ces personnes prennent place au bureau.

3. Dépdt des piéces
Les piéces suivantes sont déposées sur le bureau :
— le rapport de gestion ainsi que le rapport du commissaire ;
- le bilan, le compte de résultats ainsi que l'annexe arrétés au 31 décembre 2019;
— les procurations des associés représentés.

4. Constitution de l'assemblée

Cette liste des présences constate que les 3 associés sont représentés. Ils représentent
ensemble 386.674 actions formant la totalité du capital.

Les administrateurs et commissaire ont renoncé aux formalités de convocation.
Toutes les formalités légales et statutaires ayant été accomplies, l'assemblée se reconnait

réguligrement convoquée, valablement constituée et apte 4 délibérer sur son ordre du
jour.
DocuSign Envelope ID: B598ECF2-33DE-43AA-B128-FOBC188E7A54

5.

Ordre du jour

1. Rapport de gestion du Conseil d’administration et rapport du commissaire sur
l’exercice cloturé le 31 décembre 2019.

2. | Approbation des comptes annuels arrétés au 31 décembre 2019.

3. Décharge aux administrateurs et au commissaire.

4. Nominations statutaires.

Rapport de gestion et rapport du commissaire

 

Le Président rappelle que chaque associé a pu prendre connaissance du rapport de
gestion présenté par le conseil d'administration et du rapport du commissaire. Il propose
a l'assemblée de dispenser le bureau de la lecture de ces rapports.

Cette proposition est acceptée a l'unanimité.

Comptes annuels

Le bilan, dont le total s'éléve 4 554.902.698 €, le compte de résultats et l'annexe arrétés
au 31 décembre 2019, ainsi que l’affectation ci-dessous sont soumis au vote de
l'assemblée.

Bénéfice (perte) de l’exercice a affecter : -10.478.509 €
Bénéfice reporté de l’exercice précédent : 26.607.216 €
Prélévement sur les capitaux propres : -€
Bénéfice a reporter : 16.128.707€

Le bilan, le compte de résultats et I'annexe, arrétés au 31 décembre 2019, ainsi que
l’affectation sont approuvés a l'unanimité des voix.

Décharge aux administrateurs et au commissaire

 

Le Président met aux voix la décharge 4 donner aux administrateurs pour leur gestion et
au commissaire pour sa mission de contréle durant l’exercice cloturé 31 décembre 2019.

L'assemblée approuve la décharge aux administrateurs et au commissaire a l'unanimité
des voix.
DocuSign Envelope ID: B598ECF2-33DE-43AA-B128-FOBC188E7A54

10.

Nominations Statutaires
Conseil d’administration

Le mandat d’administrateur du Comte Davignon vient a échéance 4 l'issue de la
présente assemblée.

L’assemblée approuve également a l’unanimité des voix le renouvellement du mandat
d’administrateur du Comte Davignon pour un terme d’un an et acte la désignation par le
Conseil du Comte Davignon en qualité de Président et d’administrateur-délégué pour la
durée de son mandat d’administrateur (échéance a |’ AGO 2021).

Commissaire

Le mandat de commissaire de la S.C. s.fid. SCRL Deloitte Réviseurs d’ entreprises vient
a échéance a l’issue cette assemblée.

Sur proposition du Conseil, l’assemblée approuve, a l’unanimité des voix, le
renouvellement du mandat de commissaire de la S.C. s.f.d. SCRL Deloitte Réviseurs
d’entreprises, représentée par Laurent Boxus, pour un nouveau terme de trois ans

échéant a l’issue de l’assemblée annuelle de 2023.

Le Conseil propose a I'assemblée de fixer les émoluments du commissaire 4 un montant
de 2.925 € par an.

L'assemblée approuve cette proposition a l'unanimité des voix.

Signature du procés-verbal

L'ordre du jour étant épuisé, le Président invite le bureau et les associés qui le souhaitent
a signer le présent procés-verbal.

La séance est levée a 15.50 heures.

[ peicne étiwune
Le President

Comte E. Davignon

DocuSigned by: DocuSigned by:

DEMEE Midna ACUIRRE Xanivr
6217D1BBA40C49D. BFOBABBF3A4141F. / ~-
Le Secrétaire Les Scrutateurs '

DocuSigned by:

M. Delmée X. Aguirre Th. vai der tove
DocuSign Envelope ID: B598ECF2-33DE-43AA-B128-FOBC188E7A54

Liste des présences
DocuSigned by: DocuSigned by:
MEUIRRE Xawier [ peicne Chiu
BFOBABBF3A4141F. B066733798B14E2.
SOPRANOR S.A.S ENGIE S.A.
représentée par X. Aguirre représentée par le Comte E. Davignon
en vertu d’une procuration sous seing privé en vertu d’une procuration sous seing privé

Docusigned by:
E23CFE42E4BC4E9.
ELECTRABEL S.A.

représentée par Th. van den Hove
en vertu d’une procuration sous seing privé
