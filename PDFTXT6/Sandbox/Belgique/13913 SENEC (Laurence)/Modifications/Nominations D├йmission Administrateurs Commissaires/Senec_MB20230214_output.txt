Réservé
au LJ
Moniteur

belge

 

Bijlagen bij het Belgisch Staatsblad - 14/02/2023 - Annexes du Moniteur belge

Mod PDF 19.01

Copie a publier aux annexes au Moniteur belge
apres dépdt de l'acte au greffe

Déposé
*23313772*

10-02-2023

Greffe

 

 

N° d'entreprise : 0422672748
Nom
(en entier): SENEC

(en abrégé) :

Forme légale : Société anonyme

Adresse complete du siege Chaussée de Ruisbroek 85
: 1190 Forest

Objet de I'acte : DEMISSIONS, NOMINATIONS

ll résulte du procés-verbal des résolutions écrites de l'actionnaire unique du 13 décembre 2022 que :
wl.

1. Démission des administrateurs

L'actionnaire unique prend connaissance de la démission des administrateurs suivants :

- Mme. Isabella Naméche, domiciliée a Dréve de la Chataigneraie 1, 1490 Faux (Court-St.-Etienne),
Belgique, avec effet a partir du 6 décembre 2022.

- Mme. Bénédicte Lefévre, domiciliée a du Moulin d'Havré 14, 7021 Havré, Belgique, avec effet a
partir du 6 décembre 2022.

2. Procuration pour les formalités

L’actionnaire unique décide de donner procuration, avec faculté de délégation, a chaque employé de
l'étude des notaires « Van Halteren », ayant son siége a 1000 Bruxelles, rue de Ligne 13, a qui elle
donne pleins et entier pouvoirs pour agir en son nom et pour la représenter, qualitate qua, auprés du
registre des personnes morales, des guichets d’entreprises et l’office TVA compétent a l’effet d’y
effectuer toutes les démarches nécessaires pour effectuer toute publication dans le Moniteur Belge,
et en conséquence, remplir, signer et déposer tous formulaires, y compris les formulaires de
publication, faire toutes déclarations et en général, faire tout ce qui serait nécessaire ou utile a |’
exécution du présent mandat, en promettre au besoin ratification.

wl.

Pour extrait conforme.

(signé) Samuel WYNANT

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
