ad -

- 16/07/2018 - Annexes di

“Moniteur belge ~

lu

bi

aats

h Ste

het Belgis

t
i

 

  

Bijlagen

Cc.

Mi

ny

 

      
  
  
 

'
'

a
18110 E

‘Mod Word 15.4

Copie a publier aux annexes au Moniteur beige
aprés dépét de l'acte au greffe

te

  

    

   
   

éposé/Requle
Me
02 JUL, 208,

au wroffe da weit de cammerca
franesphoned te Bruxelles

5A7* LGlg

    
     

N° @entreprise : 0422.672.748
Dénomination

(enentier) : SENEC-SENERCOM-CONFORT MULTI SERVICES
(en abrége): SENEC

Forme juridique : société anonyme

: Adresse compléte du siége : Chaussée de Ruisbroek 85 - 1190 Bruxelles

Objet de l’acte: Réélection et Démssion administrateur / Réélection commissaire-réviseur
Réduction nombre d'administrateurs

Décision de Assemblée générale ordinaire des actionnaires du 28 juin 2018 :

Le mandat d’administrateur de Monsieur Chris SAP arrive & échéance a l'issue de Assemblée générale
ordinaire de 2018. L.’Assemblée renouvelle son mandat pour une période de trois ans, soit jusqu’a l'issue de
(Assemblée générale ordinaire de 2021 statuant sur les comptes de l’exercice 2020.

L’Assemblée prend acte de la démission de Monsieur Xavier SINECHAL de son mandat d'administrateur et -
président te 13 mars 2018. L’Assembiée tient a le remercier vivement pour le travail accompli.

L’Assemblée décide de réduire le nombre d'administrateurs de quatre a trois.
Le mandat de commissaire-réviseur de la société DELOITTE arrive & échéance a l'issue de I'Assemblée

générale ordinaire de 2018. L'Assemblée renouvelle son mandat pour une période de trois ans, soit jusqu’a
lrissue de YAssemblée générale ordinaire de 2021 statuant sur tes comptes de l’exercice 2020.

Pour extrait conforme
SENEC-SENERCOM-CONFORT MULTI SERVICES S.A.

$. de PIERPONT, Administrateur
Ph. LAURENT, Administrateur

tionner sur la derniére page du VoletB: Aurecto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

 

 

. ayant pouvoir de représenter ia personne morale 4 l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type « Mention »).

 

 

 

        
 
