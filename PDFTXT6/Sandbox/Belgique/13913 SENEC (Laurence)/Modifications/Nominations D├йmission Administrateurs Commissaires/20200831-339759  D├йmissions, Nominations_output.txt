Mod PDF 19.01

Copie a publier aux annexes au Moniteur belge
apres dépdt de l'acte au greffe

Réservé
au LJ

Déposé

Moniteur *20339759*
belge

o
>
o
Q
.
)
@
#
c
3
=
>
s
wn
o
x<
©
Cc
Cc
<x
1
o
q
So
a
N
o
S
g
=
oO
'
3
&
a
n
2
o
©
£
n
x=
o
2
>
o
a
~
o
x=
5
c
o
D
&
=

27-08-2020

Greffe

  

 

N° d'entreprise : 0422672748
Nom
(en entier): SENEC

(en abrégé) :

Forme légale : Société anonyme

Adresse complete du siege Chaussée de Ruisbroek 85
: 1190 Forest

Objet de I'acte : DEMISSIONS, NOMINATIONS

Du procés-verbal de l’assemblée générale ordinaire du 25 juin 2020 il résulte que :

wl.

Le mandat d’administrateur de M. Philippe LAURENT arrive a échéance a l’issue de la présente
assemblée générale et il n’en sollicite pas le renouvellement.

L’assemblée prend acte de la démission de M. Stanislas de PIERPONT de son mandat d’
administrateur avec effet a la date de la présente assemblée générale.

L’assemblée décide a l’unanimité des voix de nommer administrateur pour une durée de quatre ans :

* Mme Bénédicte LEFEVRE, domiciliée rue du Moulin d’Havré 14 a 7021 Havré ;

+ M. Jan DE SMET, domicilié J. van Gijsellaan 44 a 1780 Wemmel.
Leur mandat arrivera a échéance a l’issue de l’'assemblée générale ordinaire de 2024 statuant sur
les comptes de l’exercice 2023 et sera exercé a titre gratuit.
L’assemblée décide également a l’unanimité d’augmenter le nombre des administrateurs de trois a
quatre et de nommer administrateur M. Jan FEIJEN, domicilié Transvaalstraat 11 a 2600 Berchem,
pour une durée de quatre ans. Son mandat arrivera a échéance a l’issue de l’assemblée générale
ordinaire de 2024 statuant sur les comptes de l’exercice 2023 et sera exercé a titre gratuit.
wl.
L’assemblée donne pouvoir, avec faculté de délégation, a tout employé de |’étude des notaires «
VAN HALTEREN, Notaires associés », dont les bureaux sont situés a 1000 Bruxelles, rue de Ligne
13, aux fins de signer les formulaires de publication d’un extrait du procés-verbal de la présente
assemblée générale et de faire le nécessaire pour le dépdét au greffe du Tribunal de l’entreprise
compétent ainsi que pour sa publication au Moniteur belge.

Pour extrait conforme
(signé) Damien HISETTE, notaire associé a Bruxelles

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
