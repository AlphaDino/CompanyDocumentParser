138

Sociétés commerciales et agricoles — Annexe au Moniteur belye du 30 janvier 1997
Handels- en landbouwvennootschappen — Bijlage tot het Belgisch Staatsblad van 30 januari 1997

a

meerdere lasthebbers, een gedeelte van zijn
bevoegdheid delegeren, waarbij hij de omvang
en de duur van bepaalt.

Hij vertegenwoordigt de vennootschap, bij
alle akten, hieronder begrepen de akten voor
de rechtshandelingen.

Hij mag alle handelingen met het cog op de
sluiting van de vereffening vervullen.

De vergadering besluit dat het mandaat van
de vereffenaar onbezoldigd is.
vV..Sluiting van de vereffening.

De Vergadering spreekt de sluiting van de
vereffening uit en stelt vast dat de
vennootschap “RESTINVEST", definitief
opgehouden heeft te bestaan.

De Vergadering beslist daarenboven dat de
boeken en bescheiden van de vennootschap
neergelegd en gedurende vijf jaar bewaard
zullen worden te Elsene, Macaulaan, 5.

Voor gelijkvormig analytisch uittreksel :
(Get.) Fernand Jacquet,

notaris.
~*

. Tegelijk hiermee neergelegd : een uitgifte van het proces-verbaal
en het verslag van de accountant.
Neergclegd, 16 januari 1997.
3 5562 BIW 21% 1 168 6 730

(11150)

 

N. 970130 — 268
3IOTIM

Naamloze Vennootschap

Montenegrostraat 138-144, 1190 Brussel

Brussel nr. 592.004

BE 425885824
~

RAAD VAN BESTUUR : VERHOGING VAN HET
AANTAL BESTUURDERS - BENOEMING.

De buitengewone algemene vergadering
van aandeelhouders van 10 oktober 1996
heeft beslist, met ,éénparigheid van
stemmen, het aantal bestuurders van §$
tet 6 te brengen en, om het nieuw
mandaat te bezetten, de Heer Frédéric
de SCHREVEL, licenciaat in rechten en
notariaat, 1500 Halle,
Steenweg op Lennik 452 gekozen ; zijn
mandaat zal vervallen na afloop van de

wonende te

jaarlijkse algemene vergadering van
2002 en zal ten kosteloze titel
uitgecefend-worden. A
(Get.) F. Simoens, (Get) J. Reymann,
bestuurder-directeur-generaal, voorzilier-afgevaardigd bestiurder
Neergelegd, 16 januari 1997.

1 1834 BIW 21% 359 2243

N. 970130 — 269

SENEC

Société Anonyme
Rue du Monténégro 152, 1190 Bruxelles
Bruxelles, n° 441.353

BE 422672748

CONSEIL D'ADMINISTRATION AUGMENTATION
DU NOMBRE DES ADMINISTRATEURS, NOMINA-
TIONS.

L'assemblée générale extraordinaire des

actionnaires du 30 septembre 1996 a, a

L'unanimité, décidé

~ de porter le nombre des administra-
teurs de 4 4 6 et appelé pour occuper
les nouveaux mandats M. Frédéric de
SCHREVEL, licencié en droit et
notariat, demeurant 4 1500 Halle,
Steenweg op Lennik 452, et M.
Christian PONCELET, ingénieur indus-
triel, demeurant 4 7840 Bassily, rue
de Grammont 65 ;

- décidé que les mandats de MM. de

SCHREVEL. et PONCELET expireront 4

l’issue de l’assemblée générale ordi-

naire de 2002 et seront /exercés a

titreygratuit. ,

(Signé) P. Vilain,

(Signé) J. Reymann,

 

administrateur. président.
Déposé, 16 janvier 1997.
1 1854 TV.A. 21% 389 2243
(11156)
N. 970130 — 270
COENRAETS EQUIPMENT
Société anonyme
Allée verte, 54
1000 Bruxelles
Bruxelles 369.283
BE 412.019.178
Transfert du siége social
Le Conseil d'administration réuni le 18 novembre 1996 a
décidé de transférer ce jour le si¢ge social 4 Schaerbeck
(1030 Bruxelles), Boulevard Général Wahis. 16 D.
(Signé) Guy Coenraets,
administrateur délégue,
Dépose, 16 janvier 1997,
1 1Sd4 EMAL 2144 389 2243

(11128)
