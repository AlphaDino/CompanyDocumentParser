 

 

Bijlagen bij het Belgisch Staatsblad - 10/12/2021 - Annexes du Moniteur belge

  

Mod DOC 19.01

Copie a publier aux annexes au Moniteur belge
aprés dépét de l'acte au greffe

 

02 02. 28

eucrete du triteapal do Tenirasrice

N° d’entreprise : 0422 672 748
Nom
enentie): SENEC
{en abrégé) :

Forme légale : Société anonyme
Adresse complete du sige: Forest (1190 Bruxelles), chaussée de Ruisbroek 85

Objet de P'acte ; AUGMENTATION DE CAPITAL

D’aprés un acte regu par Maitre Damien HISETTE, notaire a Bruxelles (deuxiéme canton), associé de « Van
Halteren, Notaires Associés », 4 1000 Bruxelles, rue de Ligne 13, le 16 novembre 2021, il résulte que :

(..)

PREMIERE RESOLUTION.

Conformément a article 7 :179 paragraphe 3 du Code des sociétés et des associations, l’assemblée,
représentant l'ensemble des actionnaires, décide de renoncer aux rapports prévus par ce méme article.

DEUXIEME RESOLUTION.

L'assemblée décide d’augmenter le capital 4 concurrence de 1.310.000 EUR pour le porter de 997.000 EUR
& 2.307.000 EUR sans émission de nouvelles actions et par augmentation du pair comptable des actions
existantes.

Le montant de 1.310.000 EUR sera intégralement souscrit et intégralement libéré en espéces lors de la
souscription.

(.)

TROISIEME RESOLUTION.

Comme conséquence de la résolution qui précéde, l'assemblée décide de modifier l'article 6 des statuts de
la société comme suit :

« Le capital est fixé 4 deux millions trois cent sept mille euro (2.307.000,00 €) et est représenté par soixante
et un mille trois cent septante (61.370) actions sans mention de valeur nominale. »

QUATRIEME RESOLUTION.

L'assemblée décide de conférer tous pouvoirs, avec faculté de subdéléguer

- au conseil ‘administration pour l'exécution des résolutions qui précédent ;

~ 4 Mesdames Stéphanie Ernaelsteen, Myriam Tebarint et Monsieur Martin Pierreux, agissant séparément,
pour I'établissement du texte coordonné des statuts. -

(.)

Pour extrait analytique conforme.

Déposé en méme temps : expédition, procuration et attestation bancaire.

(signé) Damien HISETTE, notaire associé a Bruxelles.

Mentionner sur Ja derniére page du VoletB: Au recto: Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale & l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type « Mention »),

 
