Voor-

behouden |_

aan het
Belgisch
Staatsblad

 

Bijlagen bij het Belgisch Staatsblad - 11/06/2021 - Annexes du Moniteur belge

Mod PDF 19.01

In de bijlagen bij het Belgisch Staatsblad bekend te maken kopie
na neerlegging van de akte ter griffie

Neergelegd
*21335834*

09-06-2021

Griffie

 

 

Ondernemingsnr : 0628836449

Naam
(voluit): WIND4FLANDERS
(verkort): W4F

Rechtsvorm : Naamloze vennootschap

Volledig adres v.d. zetel Simon Bolivarlaan 34
: 1000 Brussel

Onderwerp akte : ONTSLAGEN, BENOEMINGEN

Het blijkt uit het proces-verbaal van de eenparige schriftelijke besluiten van de aandeelhouders van
18 mei 2021 dat de aandeelhouders de volgende beslissingen hebben genomen:

(...)

5. Mandaten

Raad van bestuur

De vergadering beslist de heer Filip Meuleman (...) definitief als bestuurder te benoemen. Het
mandaat zal verstrijken bij afloop van de gewone algemene vergadering van 2025.

De vergadering beslist de bestuursopdrachten van de heren Rik Soens, Tom Bollaert, Jurgen
Callaerts, Henk Kindt, Lionel Matthysen, Nico Priem, Harold Vanheel, Christian Viaene, Luc Van
Hove en mevrouw Muriel Desplanque, Valérie Duchésne, Ann Naessens en Ann Panis te
hernieuwen. Deze mandaten zullen verstrijken bij afloop van de gewone algemene vergadering van
2025.

Commissaris

De vergadering beslist het mandaat van Deloitte Bedrijfsrevisoren, vertegenwoordigd door de heer
Dirk Cleymans, te hernieuwen voor een periode van drie jaar. Het mandaat zal verstrijken bij afloop
van de gewone algemene vergadering van 2024.

(...)

Voor eensluitend uittreksel

(getekend) Damien HISETTE

Op de laatste biz. van Luik B vermelden : Voorkant : Naam en hoedanigheid van de instrumenterende notaris, hetzij van de perso(o)n

bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen
Achterkant : Naam en handtekening (dit geldt niet voor akten van het type "Mededelingen").
