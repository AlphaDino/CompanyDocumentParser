CONVENTION DE VENTE DE PART

ENTRE
La société coopérative a responsabilité limitée de droit belge
GDF SUEZ ENERGY MANAGEMENT TRADING ayant son
si¢ge social 4 1000 Bruxelles, Boulevard Simon Bolivar 34 et
inscrite au registre des personnes morales de Bruxelles sous le
numéro 0831.958.211, représentée par Messieurs Dirk Beeuwsaert
et Patrick van der Beken ;

Ci-aprés dénommée "GDF SUEZ E.M.T.", le vendeur;

ET
La société anonyme de droit belge TELFIN, ayant son siége social
4 1000 Bruxelles, Boulevard Simon Bolivar 34 et inscrite au
Registre de Commerce et des Sociétés du Luxembourg sous le
numéro 0427.428.025, représentée par Messieurs Guido Vanhove
et Thierry van den Hove ;

Ci-aprés dénommeée "TELFIN", l’acheteur;

EN PRESENCE DE

La société coopérative 4 responsabilité limitée de droit belge
GDF SUEZ SHARED SERVICES CENTER, ayant son siége social
4 1000 Bruxelles, Boulevard Simon Bolivar 34 et inscrite au registre
des personnes morales de Bruxelles sous le numéro 0505.619.626,
représentée par Messieurs Guido De Clercq et Guy Ruelle ;

ci-aprés dénommée « GDF SUEZ SSC »

IL EST PREALABLEMENT EXPOSE QUE :

GDF SUEZ E.M.T. est titulaire d'une (1) part du capital de la société coopérative a
responsabilité limitée de droit belge GDF SUEZ SSC ;

TELFIN souhaite acquérir et GDF SUEZ E.M.T. souhaite céder une part de GDF
SUEZ SSC.

ENSUITE DE QUOI, LES PARTIES ONT CONVENU CE QUI SUIT :

Article 1 — Objet

GDF SUEZ E.M.T. vend ce jour 4 TELFIN, qui accepte, une (1) part de GDF SUEZ
SSC (« La Part ») tout dividende payable ultérieurement attaché.
Article 2 — Garanties

GDF SUEZ E.M.T. est seule propriétaire de La Part et a tous pouvoirs pour conclure
la présente convention et transférer valablement la propriété de La Part 4 TELFIN.

La Part est valablement émise, intégralement libérée et quitte et libre de tout gage,
sdreté, usufruit, option ou autre droit quelconque au profit de tiers.

Article 3 — Prix de vente et paiement du prix
Le prix global de la vente de La Part est fixé 4 100 EUR.

Il est pale au compte bancaire BE12 3100 0001 0392 (BIC : BBRUBEBB) de
GDF SUEZ E.M.T. auprés de ING Bank Brussels, dés que possible aprés signature de
la présente convention.

Article 4 ~— Transfert de propriété

La propriété de La Part est transférée 4 TELFIN 4 la signature de la présente
convention.

Le vendeur et l’acheteur donnent mandat 4 MM. M. Delmée, E. Boogaerts,
Mmes C. de Meester, I. Gohy et D. Timperman, agissant seul(e)s ou conjointement,
pour prendre toutes démarches requises a cette fin, signer le registre des actionnaires
de la société et y acter le transfert de La Part a l’acheteur.

Article 5 — Litiges
La présente convention est régie par le droit belge.
Tout litige qui surviendrait a l'occasion de la conclusion ou concernant la validité,

l'interprétation ou l'exécution de la présente convention sera de la compétence
exclusive des cours et tribunaux de Bruxelles.
3

Fait 4 Bruxelles, le_9 ochobre. 2015 en trois exemplaires originaux, chacune
des parties reconnaissant avoir recu le sien.

Pour le vendeur, GDF SUEZ ENERGY MANAGEMENT TRADING SCRL :

bk

Patrick van der Beken k Beeuwsaert
Administrateur Administrateur-délégué
Pour I’acheteur, TELFIN SA :

      

/ a
Thierry van den Hove uido Vanhove
Administrateur, Administrateur

Pour GDF SUHZ SSC SCRL :

ste

y Guido De Clercq
Administrateur-délégué Président

 
