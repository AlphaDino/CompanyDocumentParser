DocuSign Envelope ID: D7A02E67-6F72-4750-8EFF-97DCA5780E71

ENGIE Global Business Support

CoGperatieve Vennootschap

Zetel van de Vennootschap : Simon Bolivarlaan 34 - 1000 Brussel
RPR Brussel - ondernemingsnummer 0505.619.626

NOTULEN VAN DE ALGEMENE VERGADERING VAN AANDEELHOUDERS
VAN 16 JUNI 2020

 

1. Opening van de vergadering

De vergadering wordt geopend om 14.30 uur te Brussel, Simon Bolivarlaan 34, onder
het voorzitterschap van Patrick van der Beken, Voorzitter.

2. Samenstelling van het bureau

De Voorzitter wijst Michael Delmée als secretaris aan.

De vergadering kiest als stemopnemers Jean-Louis Samson en Danielle Timperman,
die aanvaarden.

Deze personen nemen zitting in het bureau.

3.  Neerlegging van de stukken
Worden neergelegd op het bureau :
— het jaarverslag alsook het verslag van de commissaris betreffende het boekjaar
- de balans, de resultatenrekening alsook de toelichting, vastgelegd op 31 december

2019;
— de volmachten van vertegenwoordigde aandeelhouders.

4. Samenstelling van de vergadering

Uit de aanwezigheidslijst blijkt dat de vijf aandeelhouders vertegenwoordigd zijn. Zij
vertegenwoordigen samen 37 000 aandelen, hetzij het geheel van het kapitaal.

De bestuurders en commissaris hebben verzaakt aan de oproepingsformaliteiten.
Daar alle wettelijke en statutaire formaliteiten zijn vervuld, erkent de vergadering dat

zij op regelmatige wijze bijeengeroepen en geldig samengesteld is en dat zij bevoegd
is om te beraadslagen over haar agenda die de onderstaande punten omvat:

1/7
DocuSign Envelope ID: D7A02E67-6F72-4750-8EFF-97DCA5780E71

5. Agenda
1. Jaarverslag en verslag van de commissaris.
2. Goedkeuring van de jaarrekening per 31 december 2019.
3. Kwijting aan de bestuurders en aan de commissaris.
4. Statutaire benoemingen.

6.  Jaarverslag en verslag van de commissaris

 

De Voorzitter brengt in herinnering dat elke vennoot kennis heeft kunnen nemen van
het door de Raad van bestuur voorgelegde jaarverslag en van het verslag van de
commissaris.

7. Jaarrekening

De balans, waarvan het totaal 49,1 MEUR bedraagt, de resultatenrekening en de
toelichting, afgesloten per 31december 2019, alsook de  onderstaande
resultaatsbestemming worden ter stemming aan de vergadering voorgelegd.

Het maatschappelijk boekjaar 2019 wordt afgesloten met een resultaat van 0 EUR.

Dit vloeit, enerzijds, voort uit een facturatie van bedrijfskosten voor een bedrag van
48,0 MEUR en een belastinglast van 0,5 MEUR.

De balans, de resultatenrekening en de toelichting, afgesloten per 31 december 2019,
alsook de resultaatsbestemming worden goedgekeurd met eenparigheid van stemmen.

8.  Kwijting aan de bestuurders en aan de commissaris

 

De Voorzitter brengt de kwijting ter stemming, te verlenen aan de bestuurders voor
hun bestuur en aan de commissaris voor diens controleopdracht tijdens het boekjaar
dat per 31 december 2019 is afgesloten.

De vergadering keurt de kwijting aan de bestuurders en aan de commissaris goed met
eenparigheid van stemmen.

2/7
DocuSign Envelope ID: D7A02E67-6F72-4750-8EFF-97DCA5780E71

10.

11.

Statutaire benoemingen

Raad van bestuur

De vergadering stelt vast dat de opdracht van alle bestuurders na afloop van huidige
vergadering verstrijken en beslist de opdracht van Patrick van der Beken, Jean-Louis
Samson, Dominique Bourgeon, Wathelet Lepour, Christine Marchal, Dirk Raemaekers
en Mireille Van Staeyen te hernieuwen voor een termijn van 4 jaar (tot GAV 2024).
Op voorstel van de Raad, beslist de vergadering om Xavier Allard, gevestigd te 7000
Bergen (Mons), Avenue Jean d’Avesnes 24, als bestuurder te benoemen voor een
termijn van 4 jaar (AGO 2024) ;

De vergadering neemt nota van de benoeming door de Raad van Patrick van der Beken

als Voorzitter en van Jean-Louis Samson als Gedelegeerd bestuurder met ingang na
afloop van deze vergadering.

Mededeling

Bij toepassing van het koninklijk besluit van 27 november 1973 werd de economische
en financiéle informatie aan de ondernemingsraden verstrekt.

Ondertekening van de notulen

De Voorzitter verzoekt de leden van het bureau en de aandeelhouders die dit wensen
om de notulen te ondertekenen.

De zitting wordt opgeheven om 15.30 uur.

(Franse tekst volgt)

3/7
DocuSign Envelope ID: D7A02E67-6F72-4750-8EFF-97DCA5780E71

ENGIE Global Business Support
Société Coopérative

Siége social : Boulevard Simon Bolivar 34 - 1000 Bruxelles
RPM Bruxelles - numéro d’entreprise 0505.619.626

PROCES-VERBAL DE L’ASSEMBLEE GENERALE
DES ACTIONNAIRES DU 16_JUIN 2020

 

 

1. Ouverture de la séance

La séance est ouverte 4 14h30, 4 Bruxelles, Boulevard Simon Bolivar 34, sous la
présidence de Patrick van der Beken, Président.

2. Composition du bureau

Le Président désigne comme secrétaire Michael Delmée.

L’assemblée désigne comme scrutateurs Jean-Louis Samson et Danielle Timperman
qui acceptent.

Ces personnes prennent place au bureau.

3. Dépédt des piéces
Les piéces suivantes sont déposées sur le bureau :
- le rapport de gestion ainsi que le rapport du commissaire concernant l’exercice
2019;
- le bilan, le compte de résultats ainsi que I'annexe arrétés au 31 décembre 2019 ;
- les procurations des actionnaires représentés.

4. Constitution de l'assemblée

Cette liste des présences constate que les cinq actionnaires sont représentés. Ils
représentent ensemble les 37.000 actions représentatives du capital social.

Les administrateurs et commissaire ont renoncé aux formalités de convocation.
Toutes les formalités légales et statutaires ayant été accomplies, l'assemblée se

reconnait réguliérement convoquée, valablement constituée et apte a délibérer sur son
ordre du jour qui comporte :

4/7
DocuSign Envelope ID: D7A02E67-6F72-4750-8EFF-97DCA5780E71

5. Ordre du jour

1. Rapport de gestion et rapport du commissaire.
2. Approbation des comptes annuels au 31 décembre 2019.
3. Décharge aux administrateurs et au commissaire.
4. Nominations statutaires.
6. Rapport de gestion et rapport du commissaire

 

Le Président rappelle que chaque actionnaire a pu prendre connaissance du rapport de
gestion présenté par le conseil d'administration et du rapport du commissaire.

7. Comptes annuels
Le bilan, dont le total s'éléve 4 49,1 MEUR, le compte de résultats et l'annexe arrétés
au 31 décembre 2019 ainsi que l'affectation suivante sont soumis au vote de
l'assemblée.

L'exercice social 2019 se cloture par un résultat de 0 EUR.

Il résulte, d’une part, de la facturation des cotits d’exploitation pour 48,0 MEUR et de
la charge d’imp6t pour 0,5 MEUR.

Le bilan, le compte de résultats et l'annexe, arrétés au 31 décembre 2019 ainsi que
l'affectation sont approuvés 4 I'unanimité des voix.

8.  Décharge aux administrateurs et au commissaire

 

Le Président met aux voix la décharge 4 donner aux administrateurs pour leur gestion
et au commissaire pour sa mission de contréle durant l'exercice social qui a été cloturé
le 31 décembre 2019.

L'assemblée approuve la décharge aux administrateurs et au commissaire a l'unanimité
des voix.

9. Nominations statutaires
Conseil d’administration

L’assemblée générale constate que l’ensemble des mandats d’administrateur arrive 4
échéance et décide de procéder au renouvellement des mandats de Patrick van der
Beken, Jean-Louis Samson, Dominique Bourgeon, Wathelet Lepour, Christine
Marchal, Dirk Raemaekers et Mireille Van Staeyen et ce pour un durée de 4 ans
(échéance AGO 2024). Sur proposition du Conseil, l’assemblée générale décide de
nommer Xavier Allard, domicilié Avenue Jean d’Avesnes 24 4 7000 Mons, en qualité
d’administrateur pour une durée de 4 ans (échéance AGO 2024).

5/7
DocuSign Envelope ID: D7A02E67-6F72-4750-8EFF-97DCA5780E71

L’assemblée prend acte de la désignation par le Conseil de Patrick van der Beken en
qualité de Président et de Jean-Louis Samson en qualité d’Administrateur délégué
(CEO) avec effet 4 l’issue de la présente séance.

10. Communication

En application de I'arrété royal du 27 novembre 1973, les informations économiques et
financiéres ont été fournies au conseil d'entreprise.

11. Signature du procés-verbal

Le Président invite le bureau et les actionnaires qui le souhaitent 4 signer le présent
procés-verbal.

La séance est levée a 15h30.

DocuSigned by: Cole by:
DocuSigned by:

DEMEE Midael Cid i (SAMSON JEM-BULS

: 74913BE31ABB4,
De Secretaris De Voorzitter sist Stemopnemers
Le Secrétaire Le Président Les Scrutateurs

 

6/7
DocuSign Envelope ID: D7A02E67-6F72-4750-8EFF-97DCA5780E71

AANWEZIGHEIDSLIJST / LISTE DES PRESENCES

DocuSigned by:

SAMS AW JEMM-(BUIS
TECFIN'N-VVS.A.

vertegenwoordigd door/ représentée par J.L. Samson

=
ELECTRABELNVYS.A.

vertegenwoordigd door/ représentée par D. Timperman

~DocuSigned by:
fi ane
ENGIECCEVEASERL

vertegenwoordigd door/ représentée par D. Timperman

 

DocuSigned by:

DEMEE Mia

6217D1BBA40C49D.

FABRICOM N.V./S.A.
vertegenwoordigd door/ représentée par M. Delmée

DocuSigned by:

DEIMEE Mica

6217D1BBA40C49D.

COFELY SERVICES N.V./S.A.
vertegenwoordigd door/ représentée par M. Delmée

7/7
Certificat de réalisation

Identifiant d’enveloppe: D7A02E676F7247508EFF97DCA5780E71
Objet: Veuillez signer avec DocuSign : PV AGO 2020 bil draft.pdf
Enveloppe source:

Nombre de pages du document: 7 Signatures: 9
Nombre de pages du certificat: 5 Paraphe: 0
Signature dirigée: Activé

Horodatage de l’enveloppe: Activé

Fuseau horaire: (UTC+01:00) Bruxelles, Copenhague, Madrid, Paris

Suivi du dossier
Etat: Original
16/06/2020 15:39:24

Titulaire: TIMPERMAN Danielle
danielle.timperman@engie.com

Evénements de signataire Signature
DELMEE Michaél pees
michael.delmee@engie.com DEMEE Midad,

Responsable Service Sociétés BXL eet7pIBSMoc4sD

ENGIE GBS
Niveau de sécurité: E-mail, Authentification de
compte (aucune)

Sélection d'une signature : Style présélectionné
En utilisant l'adresse IP: 165.225.88.96
Détails du fournisseur de signature:

Type de signature: DS Electronic

Divulgation relative aux Signatures et aux Dossiers électroniques:
Accepté: 24/04/2020 13:43:15
ID: bS5c68e4c-e3b6-4641 -b49f-4b1b2deed675

VAN DER BEKEN Patrick DocuSigned by:
patrick.vanderbeken@engie.com eaASyh
Bestuurder 675C1AAG772640D.

Niveau de sécurité: E-mail, Authentification de

compte (aucune) Sélection d’une signature : Image de signature

chargée
En utilisant l’'adresse IP: 165.225.76.136

Détails du fournisseur de signature:
Type de signature: DS Electronic

Divulgation relative aux Signatures et aux Dossiers électroniques:
Accepté: 23/04/2020 19:24:37
ID: 5d85c843-127d-4edd-9d72-ec3c2d72fdae

DocuSigned by:

SAMSON JEMM(PUIS

629CF498F 7 C4AE.

SAMSON JEAN-LOUIS
jean-louis.samson@engie.com

Directeur Immobilier et Logistique

ENGIE SA

Niveau de sécurité: E-mail, Authentification de
compte (aucune)

Sélection d'une signature : Style présélectionné
En utilisant l’'adresse IP: 165.225.88.145
Détails du fournisseur de signature:

Type de signature: DS Electronic

Divulgation relative aux Signatures et aux Dossiers électroniques:
Accepté: 16/06/2020 16:00:14
ID: 72cc6b8d-cd5a-4994-a81 0-aef498917353

DocuSign,

Etat: Complétée

Emetteur de l’enveloppe:

TIMPERMAN Danielle

ENGIE NewCorp Direction Trésorerie, Risques et
Assurances 1, Place Samuel de Champlain

Paris La Défense CEDEX, France 92930
danielle.timperman@engie.com

Adresse IP: 83.137.244.9

Lieu: DocuSign

Horodatage

Envoyée: 16/06/2020 15:40:54
Consultée: 16/06/2020 15:42:32
Signée: 16/06/2020 15:43:33
Signature sous forme libre

Envoyée: 16/06/2020 15:43:35
Consultée: 16/06/2020 15:44:30
Signée: 16/06/2020 15:45:51
Signature sous forme libre

Envoyée: 16/06/2020 15:45:53
Renvoyée: 16/06/2020 15:58:58
Consultée: 16/06/2020 16:00:14
Signée: 16/06/2020 16:01:39
Signature sous forme libre
Evénements de signataire
TIMPERMAN Danielle
danielle.timperman@engie.com
Assistante de Direction
ENGIE GBS
Niveau de sécurité: E-mail, Authentification de
compte (aucune)
Détails du fournisseur de signature:

Type de signature: DS Electronic

Signature
Docusigned by:
ae
(F 5
74919BE31ABBAA6.
Sélection d’une signature : Image de signature

chargée
En utilisant l'adresse IP: 165.225.77.49

Divulgation relative aux Signatures et aux Dossiers électroniques:

Accepté: 16/06/2020 16:05:17

ID: 9c2ee35c-c1 34-4f40-a3f2-f0d90d79a594

Evénements de signataire en personne Signature

Evénements de livraison a I’éditeur

Evénements de livraison a l’agent

Etat

Etat

Evénements de livraison intermédiaire Etat

Evénements de livraison certifiée
Evénements de copie carbone
Evénements de témoins
Evénements notariaux

Récapitulatif des événements de
l'enveloppe

Enveloppe envoyée
Remise certifiée
Signature complétée
Complétée

Evénements de paiement

Etat

Etat
Signature
Signature
Etat
Haché/crypté
Sécurité vérifiée
Sécurité vérifiée
Sécurité vérifiée

Etat

Divulgation relative aux Signatures et aux Dossiers électroniques

Horodatage

Envoyée: 16/06/2020 16:01:42
Consultée: 16/06/2020 16:05:17
Signée: 16/06/2020 16:07:27
Signature sous forme libre

Horodatage
Horodatage
Horodatage
Horodatage
Horodatage
Horodatage
Horodatage
Horodatage

Horodatages

16/06/2020 16:01:42
16/06/2020 16:05:17
16/06/2020 16:07:27
16/06/2020 16:07:27

Horodatages
Divulgation relative aux Signatures et aux Dossiers électroniques créée le: 23/04/2020 18:34:54
Parties convenues: DELMEE Michaél, VAN DER BEKEN Patrick, SAMSON JEAN-LOUIS, TIMPERMAN Danielle

ELECTRONIC RECORD AND SIGNATURE DISCLOSURE

From time to time, ENGIE Corporate Trésorerie (we, us or Company) may be required by law to
provide to you certain written notices or disclosures. Described below are the terms and
conditions for providing to you such notices and disclosures electronically through the DocuSign
system. Please read the information below carefully and thoroughly, and if you can access this
information electronically to your satisfaction and agree to this Electronic Record and Signature
Disclosure (ERSD), please confirm your agreement by selecting the check-box next to ‘I agree to
use electronic records and signatures’ before clicking ‘CONTINUE’ within the DocuSign
system.

Getting paper copies

At any time, you may request from us a paper copy of any record provided or made available
electronically to you by us. You will have the ability to download and print documents we send
to you through the DocuSign system during and immediately after the signing session and, if you
elect to create a DocuSign account, you may access the documents for a limited period of time
(usually 30 days) after such documents are first sent to you. After such time, if you wish for us to
send you paper copies of any such documents from our office to you, you will be charged a
$0.00 per-page fee. You may request delivery of such paper copies from us by following the
procedure described below.

Withdrawing your consent

If you decide to receive notices and disclosures from us electronically, you may at any time
change your mind and tell us that thereafter you want to receive required notices and disclosures
only in paper format. How you must inform us of your decision to receive future notices and
disclosure in paper format and withdraw your consent to receive notices and disclosures
electronically is described below.

Consequences of changing your mind

If you elect to receive required notices and disclosures only in paper format, it will slow the
speed at which we can complete certain steps in transactions with you and delivering services to
you because we will need first to send the required notices or disclosures to you in paper format,
and then wait until we receive back from you your acknowledgment of your receipt of such
paper notices or disclosures. Further, you will no longer be able to use the DocuSign system to
receive required notices and consents electronically from us or to sign electronically documents
from us.

All notices and disclosures will be sent to you electronically
Unless you tell us otherwise in accordance with the procedures described herein, we will provide
electronically to you through the DocuSign system all required notices, disclosures,
authorizations, acknowledgements, and other documents that are required to be provided or made
available to you during the course of our relationship with you. To reduce the chance of you
inadvertently not receiving any notice or disclosure, we prefer to provide all of the required
notices and disclosures to you by the same method and to the same address that you have given
us. Thus, you can receive all the disclosures and notices electronically or in paper format through
the paper mail delivery system. If you do not agree with this process, please let us know as
described below. Please also see the paragraph immediately above that describes the
consequences of your electing not to receive delivery of the notices and disclosures
electronically from us.

How to contact ENGIE Corporate Trésorerie:

You may contact us to let us know of your changes as to how we may contact you electronically,
to request paper copies of certain information from us, and to withdraw your prior consent to
receive notices and disclosures electronically as follows:

To advise ENGIE Corporate Trésorerie of your new email address

To let us know of a change in your email address where we should send notices and disclosures
electronically to you, you must send an email message to us at and in the body of such request
you must state: your previous email address, your new email address.

If you created a DocuSign account, you may update it with your new email address through your
account preferences.

To request paper copies from ENGIE Corporate Trésorerie

To request delivery from us of paper copies of the notices and disclosures previously provided
by us to you electronically, you must send us an email to and in the body of such request you
must state your email address, full name, mailing address, and telephone number.

To withdraw your consent with ENGIE Corporate Trésorerie

To inform us that you no longer wish to receive future notices and disclosures in electronic
format you may:

i. decline to sign a document from within your signing session, and on the subsequent page,
select the check-box indicating you wish to withdraw your consent, or you may;
ii. send us an email to and in the body of such request you must state your email, full name,
mailing address, and telephone number. . .

Required hardware and software

The minimum system requirements for using the DocuSign system may change over time. The
current system requirements are found here: https://support.docusign.com/guides/signer-guide-

signing-system-requirements.

 

Acknowledging your access and consent to receive and sign documents electronically

To confirm to us that you can access this information electronically, which will be similar to
other electronic notices and disclosures that we will provide to you, please confirm that you have
read this ERSD, and (1) that you are able to print on paper or electronically save this ERSD for
your future reference and access; or (ii) that you are able to email this ERSD to an email address
where you will be able to print on paper or save it for your future reference and access. Further,
if you consent to receiving notices and disclosures exclusively in electronic format as described
herein, then select the check-box next to ‘I agree to use electronic records and signatures’ before
clicking ‘CONTINUE’ within the DocuSign system.

By selecting the check-box next to ‘I agree to use electronic records and signatures’, you confirm
that:

e You can access and read this Electronic Record and Signature Disclosure; and

e You can print on paper this Electronic Record and Signature Disclosure, or save or send
this Electronic Record and Disclosure to a location where you can print it, for future
reference and access; and

e Until or unless you notify ENGIE Corporate Trésorerie as described above, you consent
to receive exclusively through electronic means all notices, disclosures, authorizations,
acknowledgements, and other documents that are required to be provided or made
available to you by ENGIE Corporate Trésorerie during the course of your relationship
with ENGIE Corporate Trésorerie.
