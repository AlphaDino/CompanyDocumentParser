 

 

 

 

 

  
 
 
    

« GDF SUEZ ENERGY SERVICES INTER
en abrégé « GDF SUEZ ES.

Société Anonyme

a Uccle (1180 Bruxelles), rue Gatti

BCE 0403.108.14 Wi
fo
QRDOM “

Société constituée suivant acte du \. y Jean-Maurice DE DONCKER, 4

 
 

STATUTS CO!

Bruxelles, du 6 novembre 1946, publié a l'Annexe au Moniteur belge du 21 novembre
suivant, sous le numéro 20.963.

%

Dont les statuts ont été modifiés suivant :

procés-verbal, dressé par le notaire Jean-Maurice DE DONCKER, 4 Bruxelles, le 18
juillet 1950, publié a l'Annexe au Moniteur belge du 9 aodit suivant, sous le numéro
19.121.

procés-verbal, dressé par le notaire Jean-Maurice DE DONCKER, a Bruxelles, le 28
juin 1951, publié a l'Annexe au Moniteur belge du 18 juillet suivant, sous le numéro
17.029,

procés-verbal, dressé par le notaire Jean-Maurice DE DONCKER, a Bruxelles, le 11
mai 1956, publié a I'Annexe au Moniteur belge des 28-29 mai suivant, sous le numéro
13.821.

procés-verbal, dressé par le notaire Jean-Maurice DE DONCKER, 4 Bruxelles, le 2
février 1958, publié a l'Annexe au Moniteur belge du 20 mars suivant, sous le numéro
4.769.

%S procés-verbal, dressé par le notaire André SCHEYVEN, 4 Bruxelles, le 4 mai 1971,

publié a !'Annexe au Moniteur belge du 28 mai suivant, sous le numéro 1.516-2.

% procés-verbal, dressé par le notaire André SCHEYVEN, 4 Bruxelles, le 4 mai 1976,

é&

6 € & E

publié a l'Annexe au Moniteur belge du 22 mai suivant, sous le numéro 1.631-2.
procés-verbal, dressé par le notaire André SCHEYVEN, a Bruxelles, le 28 novembre
1978, publié a4 l'Annexe au Moniteur belge du 23 décembre suivant, sous le numéro
2.546-32

procés-verbal, dressé par le notaire André SCHEYVEN, 4 Bruxelles, le 13 décembre
1978, publié a l'Annexe au Moniteur belge du 9 janvier 1979, sous le numéro 5-19.
procés-verbal, dressé par le notaire Jean-Luc INDEKEU, 4 Bruxelles, le 17 décembre
1982, publié a l'Annexe au Moniteur belge du 7 janvier 1983, sous le numéro 102-7.
procés-verbal, dressé par le notaire Jean-Luc INDEKEU, 4 Bruxelles, le 3 juin 1983,
publié a l'Annexe au Moniteur belge du 24 juin suivant, sous le numéro 1.625-20.
procés-verbal, dressé par le notaire Jean-Luc INDEKEU, 4 Bruxelles, le 30 janvier
1987, publié a l'Annexe au Moniteur belge du 27 février suivant, sous le numéro
870227-130.

procés-verbal, dressé par le notaire Jean-Luc INDEKEU, 4 Bruxelles, le 15 décembre
1987, publié 4 I'Annexe au Moniteur belge du 20 janvier 1988, sous le numéro
880120-160.

procés-verbal, dressé par le notaire Edwin VAN LAETHEM, a Ixelles, le 24 décembre
1992, publié 4 l'Annexe au Moniteur belge du 3 février 1993, sous le numéro
930203-47.

acte dressé par le notaire Edwin VAN LAETHEM, a Ikxelles, le 22 décembre 1994,
publié a l'Annexe au Moniteur belge du 25 janvier 1995, sous le numéro 950125-88.
procés-verbal dressé par le notaire Pierre-Paul vander BORGHT, a Schaerbeek, le 15
juillet 1996, publié a l'Annexe au Moniteur belge du 8 aotit 1996, sous les numéros
960808-242 et 243.

procés-verbal dressé par le notaire Pierre-Paul vander BORGHT, a Schaerbeek, le 21
février 1997, publié a l'Annexe au Moniteur belge du 14 mars 1997, sous les numéros
970314-290 et 291.

procés-verbal dressé par le notaire Pierre-Paul vander BORGHT, a Schaerbeek, le 5
décembre 1997, publié a l'Annexe au Moniteur belge du 20 décembre 1997, sous les
numéros 971220-289 et 290.

procés-verbal dressé par le notaire Pierre-Paul vander BORGHT, a Schaerbeek, le 22
décembre 1999, publié a l'Annexe au Moniteur belge du 7 janvier 2000, sous les
numéros 20000107-159 et 160.

Procés-verbal dressé par le notaire Pierre-Paul vander BORGHT, a Schaerbeek, le 19
septembre 2002, publié 4 l’ Annexe au Moniteur belge du 1 octobre suivant, sous les
numeéros 0121222 et 0121223.

Procés-verbal dressé par le notaire Pierre-Paul vander BORGHT, a Schaerbeek, le 29
décembre 2003, publié 4 l’Annexe au Moniteur belge du 9 février suivant, sous le
numéro 20040209-0020533 et du 19 février suivant sous le numéro 20040219-
0027710.

Procés-verbal dressé par le notaire Pierre-Paul vander BORGHT, a Schaerbeek, le 26
septembre 2007, publié a 1’Annexe au Moniteur belge du 22 octobre suivant, sous le
numéro 20071022-0153263.

Procés-verbaux dressés par le notaire Pierre-Paul vander BORGHT, 4 Schaerbeek, le
30 mars 2009, publiés 4 l’Annexe au Moniteur belge du 20 avril suivant, sous les
numéros 0056706 et 0056708.

Procés-verbal dressé par le notaire Pierre-Paul vander BORGHT, a Schaerbeek, le 22
avril 2010, en cours de publication 4 annexe au Moniteur belge.
~~

TITRE I. - FORME - DENOMINATION - OBJET - SIEGE- DUREE

Article 1
La société est une société anonyme. Elle a pour dénomination « GDF SUEZ
ENERGY SERVICES INTERNATIONAL », en abrégé « GDF SUEZ E.S.L. ».

Article 2.

La société a pour objet principal de s'intéresser par voie de cession, apport,
souscription, achat ou échange de titres, participation, commandite, prét ou de toute autre
maniére, dans toutes sociétés ou entreprises créées ou 4 créer dans le domaine des
installations techniques et des services aux collectivités.

La société a également pour objet la prise de participations en actions ou autres
instruments financiers, en Belgique et dans tous pays étrangers :

(a) dans toutes sociétés dont l’ objet est similaire ou connexe au sien et

(b) dans toutes sociétés 4 objet financier ou analogue dont l’activité est utile au

développement tant de la société et de ses filiales que de toutes autres sociétés
du groupe dont fait partie la société.

Elle peut réaliser son objet soit directement soit indirectement et effectuer toutes
opérations de nature a favoriser son développement et toutes opérations financiéres,
industrielles, commerciales et immobiliéres nécessaires.

Elle peut enfin construire, acquérir, prendre et donner 4 bail ou gérer toutes usines,
installations, exploitations, magasins ou dépéts, demander, acheter, céder et exploiter tous
brevets ou licences, construire, acheter, vendre, prendre et donner en location tout matériel
ou appareil, et faire toutes opérations de consignation, de représentation, de courtage ou de
commission relatives ou connexes a son objet.

Article 3.

Le siége social est établi dans l'agglomération bruxelloise. Il est actuellement fixé
4 Uccle, rue Gatti de Gamond, 254.

Il peut étre transféré en tout autre endroit de l'agglomération bruxelloise par
décision du conseil d'administration.

Article 4.

La société peut établir, par décision du conseil d'administration, des siéges
administratifs, sigges d'opération, siéges d'exploitation, succursales ou agences, tant en
Belgique qu'a I'étranger.

Article 5.

La durée de la société constituée le six novembre mil neuf cent quarante-six est
illimitée. Elle pourra étre dissoute 4 tout moment par décision de I'assemblée générale
statuant dans les formes prescrites pour les modifications aux statuts.

TITRE II. : CAPITAL SOCIAL

Article 6.

Le capital social est fixé 4 un milliard trois cent quatre-vingts millions neuf cent
vingt-trois mille six cent cinquante euros (1.380.923.650 €).

Il est représenté par cent trente-deux mille trois cent trente (132.330) actions sans
désignation de valeur nominale, représentant chacune un/cent trente-deux mille trois cent
trentiéme (132.330°") de l’avoir social.
Article 7.

Le capital social peut étre augmenté ou réduit, en une ou plusieurs fois, par
l'assemblée générale délibérant aux conditions requises pour les modifications des statuts et
conformément aux dispositions du code des sociétés.

Les nouvelles parts sociales 4 souscrire en espéces sont offertes par préférence aux
actionnaires, proportionnellement 4 la partie du capital que représentent leurs parts
sociales.

L'assemblée générale fixe le délai de I'exercice du droit de préférence.

Elle confére au conseil d'administration tous pouvoirs, aux fins de réaliser les
décisions prises et de fixer les conditions de l'exercice du droit de préférence.

Toutefois, par dérogation a ce qui précéde, l’assemblée générale peut, dans l'intérét
social, aux conditions requises pour la modification des statuts et dans le respect des
dispositions légales, limiter ou supprimer le droit de souscription préférentielle.

Le conseil d'administration a toujours la faculté de passer, dans le respect des
dispositions légales, aux clauses et conditions qu'il avisera, toutes conventions destinées a
assurer la souscription de tout ou partie des nouvelles parts sociales a émettre.

Article 7 bis,
Tous transferts d'actions sont soumis aux régles suivantes:

1. Sont entiérement libres et seront régularisées immédiatement, les cessions d'actions ou

de droits permettant la souscription d'actions ainsi que les transferts résultant d'une

fusion ou d'une absorption entre les sociétés actionnaires et les sociétés affiliées du

méme groupe. Les autres actionnaires ne pourront s'y opposer que pour de justes motifs.

Deux sociétés sont considérées comme affili¢es d'un méme groupe si l'une d'elles détient

le contréle direct ou indirect de l'autre société (société mére et filiale) ou si toutes deux

sont directement ou indirectement contrélées par une méme société (sociétés soeurs).

Si un transfert d'actifs dans le cadre d'une opération de fusion, d'absorption, de

restructuration de sociétés a pour conséquence de faire sortir des actions «GDF SUEZ

ENERGY SERVICES INTERNATIONAL» de la société propriétaire de celles-ci ou du

groupe auquel elles appartenaient, il y a lieu de traiter cette situation comme s'il s'agissait

d'une cession d’actions hors groupe.

. Les transferts et cessions d'actions ou de droits permettant la souscription d'actions hors

groupe ne peuvent avoir lieu sans que les actions n'aient été, au préalable, offertes en

vente aux autres actionnaires.

a) A cet effet, l'actionnaire cédant ou son représentant doit aviser les autres actionnaires
de son intention de céder tout ou partie de ses actions par lettre recommandée avec
copie pour le conseil d'administration, en précisant:

- je nombre d'actions dont il désire se séparer;

- les conditions de la cession proposée.
Le droit de préemption des actionnaires concerne l'ensemble des actions dont la
cession est proposée et s'exerce aux conditions mentionnées, chacun pouvant se porter
acquéreur au prorata de sa participation existante.

b) Les autres actionnaires disposeront d'un délai de trente jours calendrier pour exercer
leur droit de préemption et en aviser l'actionnaire cédant par lettre recommandée avec
copie au conseil d'administration. Au cas ov ils n'auraient pas exercé intégralement
leur droit de préemption dans les trente jours ainsi définis, le cédant en avisera le
conseil d'administration et les autres actionnaires par lettre recommandée.

Ces derniers disposeront alors d'un délai de vingt jours calendrier pour faire connaitre
au conseil d'administration et au cédant, par lettre recommandée, le nombre d’actions
supplémentaires qu'ils souhaiteraient acquérir.

Au cas ot la demande excéderait I'offre, le conseil d'administration diment avisé par
le cédant, opérera la réduction nécessaire dans un délai de dix jours
bd

proportionnellement aux participations respectives de chacun dans le capital de la
société.

Au cas ot la demande serait inférieure 4 l'offre, !'actionnaire cédant pourra alors pour
les actions restantes, rechercher un acquéreur tiers.

c) Toutefois, tout actionnaire qui estimerait ne pas pouvoir souscrire les actions a vendre
en raison du prix demandé pour celles-ci, pourra exiger qu'avant toute cession a un
tiers, la valeur intrinséque des actions soit déterminée par une société internationale
d'audit choisie de commun accord.

L'évaluation sera basée sur des données comptables et d'autres données objectives, 4
Vexclusion de facteurs spéculatifs ou de convenances qui peuvent expliquer le
montant d'offres circonstancielles.

Tous les actionnaires auront alors la possibilité de se porter acquéreur, au prorata de
leurs participations, a la valeur ainsi déterminée.

La notification par l'actionnaire demandeur de sa demande d'évaluation d'actions
devra étre faite par lettre recommandée 4 l'actionnaire cédant avec copie au conseil
d'administration et aux autres actionnaires dans les vingt jours calendrier qui suivent
la premiére notification de l'actionnaire cédant.

d) Dans le cas of tout ou partie des actions mises en vente par I'actionnaire cédant
n'auraient pas été acquises par les autres actionnaires et/ou !'actionnaire cédant aurait
trouvé un acquéreur tiers, il devra, avant toute cession effective, en aviser les autres
actionnaires par lettre recommandée avec copie au conseil d'administration précisant:

- l'identité du tiers de bonne foi auquel il se propose de céder les actions;

- les conditions de la cession proposée.
Les autres actionnaires auront un délai de quinze jours calendrier pour faire a
l'actionnaire cédant, par lettre recommandée avec copie au conseil d'administration et
aux autres actionnaires, une offre d'achat que celui-ci devra accepter si les conditions
en sont au moins égales a celles précisées dans la notification.
En I'absence de telles offres et passé ce délai de quinze jours, I'actionnaire cédant sera
libre de réaliser la cession avec le tiers, aux conditions indiquées dans la notification.
S'il y a plusieurs offres, le conseil d'administration diment avisé par le cédant,
opérera la réduction nécessaire dans un délai de dix jours, comme stipulé ci-avant.
De méme, les autres actionnaires ont la faculté soit de contester le prix conformément
au paragraphe c) ci-avant si cette faculté n'a pas encore été utilisée précédemment,
soit de faire valoir les résultats de cette contestation, s'ils ont au contraire déja utilisé
cette faculté.

e) Pour tous les avis et notifications adressés en vertu du présent article sous pli
recommandé, le cachet de la poste fera foi de la date d'envoi.

3. Le conseil d'administration veille 4 ce que l'exercice du droit de préemption selon les
modalités décrites ci-dessus ne puisse aboutir a ce que !'incessibilité des actions excéde
la période prévue par l'article 510 du Code des sociétés.

Article 8.

Les libérations 4 effectuer en numéraire ou par apports en nature sur les parts
sociales non encore entiérement libérées doivent étre faites aux lieux et aux dates que le
conseil d'administration détermine. Les libérations appelées s'imputent également sur
l'ensemble des parts sociales dont l'actionnaire est titulaire.

Les libérations appelées et non effectuées huit jours aprés celui de leur exigibilité
portent intérét, calculé par jour de retard 4 compter de ]'échéance, au taux légal, sans qu'une
mise en demeure soit requise.

Le conseil d'administration peut, en outre, aprés une mise en demeure notifiée par
lettre recommandée restée sans résultat pendant un mois, prononcer la déchéance de
l'actionnaire et, dans le respect de I'égalité des actionnaires, vendre les parts sociales sur
lesquelles les libérations appelées n'ont pas été opérées. A compter de Ja mise en demeure,
les droits liés aux parts sociales non libérées sont suspendus.

Le produit net de la vente s'impute au profit de la société, sur ce qui lui est df, en
principal et intéréts, par l'actionnaire défaillant, sans préjudice du droit de la société de lui
réclamer le restant di, ainsi que tous dommages et intéréts éventuels.

Les parts sociales ne pourront étre valablement cédées qu'aprés leur entiére
libération, sauf accord écrit du conseil d'administration.

Article 9.

Le conseil d'administration peut autoriser les actionnaires 4 libérer leurs parts
sociales par anticipation ; dans ce cas, il détermine les conditions auxquelles les libérations
anticipées sont admises.

Article 10.

Les parts sociales sont nominatives jusqu'a leur entiére libération. Aprés leur
libération, elles restent nominatives. I] est tenu au siége social un registre des actions
nominatives dont tout actionnaire peut prendre connaissance.

Article 11,

Le conseil d'administration peut suspendre l'exercice des droits afférents aux parts
sociales faisant l'objet d'une copropriété, d'un usufruit ou d'un gage, jusqu'a ce qu'une seule
personne soit désignée comme étant, a l'égard de la société, propriétaire de ces parts
sociales.

Article 12.

La société peut émettre des obligations, hypothécaires ou autres, par décision du
conseil d'administration qui en déterminera le type et fixera le taux des intéréts, le mode et
l'époque des remboursements, ainsi que toutes autres conditions d'émission.

Elle peut émettre des obligations convertibles en parts sociales, ou assorties d'un
droit de souscription, aux conditions prévues par les articles 489 et suivants du code des
sociétés, par décision de l'assemblée générale, agissant conformément aux dispositions
relatives aux modifications des statuts.

En cas d'émission d'obligations convertibles ou avec droit de souscription, les
actionnaires bénéficient d'un droit de souscription préférentielle proportionnellement a la
partie du capital que représentent leurs parts sociales; l'exercice du droit de souscription
préférentielle est organisé conformément aux dispositions de la loi.

Liassemblée générale des actionnaires peut, dans I'intérét social, limiter ou
supprimer ce droit de souscription préférentielle en respectant les conditions prévues par la
loi.

Les dispositions de l'article onze sont applicables aux obligations émises par la
société.

TITRE ITIl._: ADMINISTRATION - DIRECTION - REPRESENTATION -
CONTROLE

Article 13

La société est administrée par un conseil d'administration composé de trois
membres au moins, nommés pour six ans au plus par l'assemblée générale des actionnaires
et révocables par elle.

Les administrateurs sont rééligibles.

Toutefois, lorsque la société est constituée par deux fondateurs ou que, a une
assemblée générale des actionnaires de la société, il est constaté que celle-ci n’a pas plus de
deux actionnaires, la composition du conseil d’administration peut étre limitée 4 deux
membres jusqu’a l’assemblée générale ordinaire suivant la constatation par toute voie de
droit de l’existence de plus de deux actionnaires.

Les fonctions des administrateurs sortants et non réélus prennent fin
immédiatement 4 l'issue de I'assemblée générale ordinaire.

Le conseil d'administration peut élire en son sein un président et un vice-président.

L'assemblée peut, sur proposition du conseil d'administration, conférer 4 d'anciens
membres de celui-ci le titre honorifique de leurs fonctions.

Article 14,

En cas de vacance d'un ou plusieurs mandats d'administrateur, les membres
restants du conseil d'administration peuvent pourvoir provisoirement au remplacement
jusqu'a la prochaine assemblée générale qui procéde 4 l'élection définitive.

Tout administrateur, désigné dans les conditions ci-dessus, n'est nommé que pour
le temps nécessaire a I'achévement du mandat de I'administrateur qu'il remplace.

Article 15,

Les administrateurs ne contractent aucune obligation personnelle relativement aux
engagements de la société. Ils sont responsables a !'égard de la société de I'exécution de
leur mandat et des fautes commises dans leur gestion, notamment du dépassement des
pouvoirs tels qu'ils résultent des présents statuts ou de décisions de l'assemblée générale.

Article 16.

Le conseil d'administration se réunit au siége social ou a l'endroit indiqué dans les
convocations chaque fois que l'intérét de la société l'exige et en tout cas, chaque fois qu’un
administrateur au moins le demande.

Les réunions sont présidées par le président du conseil ou, en cas d'empéchement
de celui-ci, par le vice-président ou, 4 leur défaut, par un administrateur désigné par ses
collégues.

Les administrateurs honoraires peuvent étre invités a assister aux réunions, mais
avec voix consultative seulement.

Article 17.

Le conseil ne peut délibérer et statuer que si la majorité de ses membres est
présente ou représentée.

Chaque administrateur peut, méme par simple lettre, télégramme, télex, télécopie
ou tout autre moyen de transmission ayant comme support un imprimé, donner a l'un de ses
collégues pouvoir de le représenter 4 une séance du conseil et d'y voter en ses lieu et place.
Aucun mandataire ne peut représenter ainsi plus d'un administrateur.

Les décisions sont prises 4 la majorité des voix. En cas de partage, celle du
président de la réunion est prépondérante. Si, dans une séance du conseil réunissant la
majorité requise pour délibérer valablement, un ou plusieurs administrateurs s'abstiennent,
les résolutions sont valablement prises 4 la majorité des autres membres du conseil,
présents ou représentés.

Dans les cas exceptionnels diment justifiés par l'urgence et l'intérét social, les
décisions du conseil d'administration peuvent étre prises par consentement unanime des
administrateurs exprimé par écrit. Il ne pourra cependant pas étre recouru a cette procédure
pour l'arrét des comptes annuels.

Article 18.

Les délibérations du conseil d'administration sont constatées par des
procés-verbaux signés par la majorité des membres présents. Ces procés-verbaux sont
consignés ou reliés en un recueil spécial. Les délégations ainsi que les avis et votes
donnés, conformément a l'article dix-sept, y sont annexés. Les copies ou extraits, a
produire en justice ou ailleurs, sont signés par deux administrateurs.

Article 19.

L'assemblée générale peut allouer aux administrateurs un émolument fixe ou des
jetons de présence a porter 4 charge du compte de résultats.

Le conseil d'administration est autorisé également 4 accorder aux administrateurs
chargés de fonctions ou missions spéciales, une rémunération particuliére a prélever sur les
frais généraux.

Article 20.

Le conseil d'administration a le pouvoir d'accomplir tous les actes nécessaires ou
utiles A la réalisation de l'objet social, 4 l'exception de ceux que la loi ou les statuts
réservent a l'assemblée générale.

Article 21.

Le conseil d'administration peut constituer un comité exécutif dont les membres
sont choisis dans ou hors de son sein. II détermine les pouvoirs de ce comité exécutif, en
régle le fonctionnement et fixe la rémunération de ses membres, 4 imputer sur les frais
généraux.

Le conseil d'administration peut déléguer la gestion journaliére ainsi que la
représentation de la société en ce qui concerne cette gestion a une ou plusieurs personnes
agissant dans ce cas deux a deux. II nomme et révoque les délégués 4 cette gestion qui sont
choisis dans ou hors de son sein, fixe leur rémunération et détermine leurs attributions.

Le consei! d'administration peut confier la direction de l'ensemble, de telle partie
ou de telle branche spéciale des affaires sociales a une ou plusieurs personnes.

Le conseil d'administration ainsi que les délégués a la gestion journaliére, dans le
cadre de cette gestion, peuvent également conférer des pouvoirs spéciaux et déterminés a
une ou plusieurs personnes de leur choix.

Article 22.

La société est représentée dans les actes, y compris ceux ov intervient un
fonctionnaire public ou un officier ministérie! et en justice :

- soit par deux administrateurs agissant conjointement ;

- soit, dans les limites de la gestion journaliére, par le délégué a cette gestion s'il
n'y en a qu'un seul, et par deux délégués agissant conjointement s'ils sont plusieurs.

Elle est en outre valablement engagée par des mandataires spéciaux dans les
limites de leur mandat.

Article 23.

Le contréle des comptes de la société est confié 4 un commissaire au moins,
nommeé par l'assemblée générale pour une durée de trois ans, rééligible et révocable par
elle.

Si par suite de décés ou pour un autre motif, il n'y a plus de commissaire, le conseil
d'administration doit convoquer immédiatement l'assemblée générale pour pourvoir a cette
vacance,

Les fonctions des commissaires sortants et non réélus prennent fin immédiatement
aprés l'assembi¢e générale ordinaire.

La mission et les pouvoirs des commissaires sont ceux que leur assigne le code des
sociétés.

L'assemblée générale détermine les émoluments des commissaires correspondant a
leurs prestations de contréle des comptes. Toutefois, le conseil d'administration peut
attribuer aux commissaires des émoluments pour des missions spéciales ; il en informe la
plus prochaine assemblée générale ordinaire par le rapport de gestion, de l'objet de ces
 

missions et de Jeur rémunération.

Article 24.

L'assemblée générale peut en outre choisir un commissaire suppléant pour une
durée égale a la durée du mandat du commissaire effectif selon les modalités prévues pour
ce dernier. En cas de décés, ou dés que le conseil d'administration constate que le
commissaire effectif est dans l'impossibilité de remplir son mandat, le commissaire
suppléant entre en fonction pour la durée de l'empéchement. Si cet empéchement est
définitif, le commissaire suppléant achéve le mandat du commissaire effectif.

TITRE IV. : ASSEMBLEES GENERALES

Article 25.
L'assemblée générale a les pouvoirs qui sont déterminés par la loi et les présents
statuts.

Article 26.

L'assemblée générale se tient au siége social ou a I'endroit indiqué dans les avis de
convocation.

L'assemblée se réunit au moins une fois I'an, le premier mardi du mois de mai a dix
heures trente ou, si ce jour est un jour férié légal ou s'il est chOmé eu égard aux régles en
usage dans !a société, le premier jour ouvrable suivant, autre qu'un samedi.

Article 27.

Le conseil d’administration convoque les assembiées générales tant ordinaires
qu'extraordinaires.

L'assemblée doit étre convoquée 4 la demande d'un ou plusieurs actionnaires
justifiant qu'ils possédent le cinquiéme du capital social.

Article 28.
Les convocations pour toute assemblée sont faites conformément aux dispositions
légales.

Article 29.

Pour étre admis a I'assemblée générale, tout propriétaire d’actions doit informer la
société six jours au moins avant l’assemblée de son intention d’assister a l'assemblée ainsi
que du nombre d’actions pour lesquelles il entend prendre part au vote.

Article 30.

Tout propriétaire de parts sociales peut se faire représenter 4 l'assemblée générale
par un mandataire, actionnaire ou non.

Le conseil d'administration peut arréter Ja formule de procuration et exiger que
celle-ci soit déposée a !'endroit indiqué par lui et dans le délai qu'il fixe.

Tout actionnaire est autorisé a voter par correspondance, au moyen d'un formulaire
dans lequel doivent étre mentionnés son nom, I'ordre du jour, le sens du vote ou de
l'abstention sur chacun des points de celui-ci, et le délai de validité du mandat. Ce
formulaire sera diiment signé, la ou les signatures étant précédées de la mention “bon pour
pouvoir". Il ne sera tenu compte que des formulaires qui auront été recus par la société a
son siége social, trois jours francs avant la réunion de l'assemblée générale.

Article 31.
Pour étre admis a l'assembl¢ée, tout actionnaire ou mandataire doit signer la liste de
présence. Cette liste indique l'identité des actionnaires et de Jeurs mandataires, ainsi que le
nombre de titres pour leque!l ils prennent part au vote.

Article 32.

L'assemblée est présidée par le président du conseil d'administration ou, 4 son
défaut, par le vice-président ou, a leur défaut, par un administrateur désigné par ses
collégues et, en cas de carence des administrateurs, par un actionnaire ou son représentant,
désigné par l'assemblée.

Le président désigne le secrétaire et l'assemblée nomme deux scrutateurs,
actionnaires ou non. Ils forment ensemble le bureau.

Article 33.
Chaque part sociale donne droit 4 une voix.

Article 34.

Sauf les cas prévus par la loi, Jes décisions sont prises a la simple majorité des voix
valablement exprimées, quel que soit le nombre de titres représentés, sans tenir compte des
abstentions.

Les votes se font par main levée ou par appel nominal, 4 moins que l'assemblée
n'en dispose autrement.

En cas de nomination, si aucun candidat ne réunit la majorité des voix, il est
procédé a un scrutin de ballottage entre les candidats qui ont obtenu le plus de voix. En cas
d'égalité du nombre de suffrages a ce scrutin, le plus 4gé des candidats est élu.

Article 35.

Lorsque I'assemblée générale est appelée a décider d'une modification aux statuts,
elle ne peut valablement délibérer que si ceux qui assistent a l'assemblée représentent la
moitié au moins du capital social. Si cette derniére condition n'est pas remplie, une
nouvelle convocation est nécessaire et la nouvelle assemblée délibére valablement, quelle
que soit la portion du capital représentée.

Aucune modification des statuts n'est admise, sauf exception légale, que si elle
réunit les trois quarts des voix valablement exprimées.

Lorsque la délibération est soumise par Ja loi 4 des conditions plus strictes,
"assemblée n'est valablement constituée et ne peut statuer que dans les conditions de
présence et de majorité requises par le code des sociétés,

Article 36.

L'assemblée générale ne peut délibérer et prendre des décisions que sur les points
figurant a son ordre du jour.

L’assemblée générale ne peut statuer sur des points qui ne sont pas inscrits a l’ordre
du jour que dans Phypothése ot tous les actionnaires sont présents ou représentés.

Article 37,

Quels que soient les points a l'ordre du jour, le conseil d'administration a le droit,
aprés l'ouverture des débats, de proroger, a trois semaines au plus, toute assemblée générale
tant ordinaire qu'extraordinaire.

Cette prorogation, notifiée avant la cléture de la séance et mentionnée au
procés-verbal de celle-ci, n’annule que les décisions prises concernant les comptes annuels,
sauf si l’assemblée générale en décide autrement. Elle ne peut avoir lieu qu'une fois.

Les actionnaires doivent étre convoqués 4 nouveau pour la date que fixera le
conseil d'administration, avec le méme ordre du jour.

Les formalités remplies pour assister 4 la premiére séance, en ce compris le dépét
des procurations, resteront valables pour la seconde. De nouveaux dépéts seront admis
dans les délais statutaires.
La seconde assemblée générale statue définitivement sur les points a J'ordre du
jour.

Article 38.

Les procés-verbaux des assemblées générales sont signés par les membres du
bureau et par les actionnaires qui le demandent et, sauf pour les procés-verbaux
authentiques, sont consignés ou reliés dans un recuei! spécial tenu au siége social.Les
expéditions, copies ou extraits 4 délivrer aux tiers sont signés par deux administrateurs.

TITRE V,_: INVENTAIRES ET COMPTES ANNUELS - BENEFICE ET
REPARTITION

Article 39.

L'exercice social commence le premier janvier et se termine le trente et un
décembre de chaque année.

Le conseil d'administration dresse un inventaire et établit, conformément 4 la loi,
les comptes annuels, qui comprennent le bilan, le compte de résultats et I'annexe. II rédige
en outre le rapport de gestion.

Un mois au moins avant l'assemblée générale, l'inventaire, les comptes annuels et
le rapport de gestion sont remis avec les piéces aux commissaires qui doivent faire leur
rapport.

Article 40.

Quinze jours avant l'assemblée générale ordinaire, les actionnaires peuvent prendre
connaissance au siége social :

- des comptes annuels;

- de la liste des fonds publics, des actions, parts sociales, obligations et autres titres
de sociétés qui composent le portefeuille,

- de la liste des actionnaires qui n'ont pas entiérement libéré leurs parts sociales
avec l'indication du nombre de leurs parts sociales et celle de leur domicile;

- du rapport de gestion et du rapport des commissaires.

Les comptes annuels, le rapport de gestion et le rapport des commissaires sont
adressés aux actionnaires en nom, en méme temps que la convocation. Tout actionnaire a le
droit d'obtenir gratuitement un exemplaire de ces piéces, sur justification de son titre,
quinze jours avant l'assemblée.

Article 41,

Aprés avoir pris connaissance du rapport de gestion et du rapport des
commissaires, l'assemblée générale délibére sur les comptes annuels.

Aprés leur adoption, elle se prononce par un vote spécial sur la décharge des
administrateurs et des commissaires.

Article 42.
Les comptes annuels, le rapport de gestion, le rapport des commissaires ainsi que
les autres documents prévus par la loi font l'objet de mesures de publicité légale.

Article 43,

L'excédent favorable du compte de résultats constitue le bénéfice net.

1. Sur ce bénéfice, i] est prélevé cing pour cent au moins pour constituer la réserve
légale, ce prélévement cessant d'étre obligatoire lorsque la réserve atteint le dixiéme du
capital social.
2. Le solde se répartit comme suit :

a. un premier montant déterminé par l'assemblée générale ordinaire - limité
toutefois 4 cinq pour cent de ce solde - au conseil d'administration. Les
membres du conseil d'administration se répartiront leurs tanti¢mes suivant
leurs conventions particuliéres, faculté leur étant laissée de faire participer les
administrateurs honoraires 4 cette répartition;

b. le solde éventuel, uniformément entre toutes les parts sociales.

3. Toutefois, sous réserve de ce qui est dit sous le 1. du présent article, l'assemblée
générale peut, sur proposition du conseil d'administration, affecter tout ou partie du
bénéfice a la création et a l'alimentation de réserves ou bien le reporter 4 nouveau
totalement ou partiellement.

Article 44.

Le paiement des dividendes se fait aux époques et aux endroits fixés par le conseil
d'administration. La distribution d'un acompte, a imputer sur le dividende qui sera
distribué sur les résultats de l'exercice, pourra étre décidée par le conseil d'administration,
méme avant la cléture de l'exercice social.

TITRE VI. : DISSOLUTION- LIOUIDATION

Article 45.

En cas de dissolution de la société, l'assemblée générale régle le mode de
liquidation et nomme un ou plusieurs liquidateurs dont elle détermine les pouvoirs et les
émoluments. Elle conserve le pouvoir de modifier les statuts si les besoins de la liquidation
le justifient.

La nomination des liquidateurs met fin aux pouvoirs des administrateurs.

Article 46.

L'assemblée générale est convoquée, constituée et tenue pendant la liquidation
conformément aux dispositions du titre quatre des présents statuts, les liquidateurs jouissant
des mémes prérogatives que le conseil. Un des liquidateurs la préside; en cas d'absence ou
d'empéchement des liquidateurs, elle élit elle-méme son président.

Les copies ou extraits des procés-verbaux de ses délibérations, 4 produire en justice
ou ailleurs, sont valablement certifiés par les liquidateurs ou l'un d'entre eux,
conformément aux décisions de l'assemblée.

Article 47.

A moins que l'assemblée n’ait réglé autrement le mode de liquidation a la majorité
requise pour modifier les statuts, le produit de Ja liquidation, aprés le paiement des dettes
ou la consignation des sommes nécessaires a cet effet, y compris les frais de liquidation, est
réparti, en espéces ou en titres, entre toutes les parts sociales.

Si le produit net ne permet pas de rembourser toutes les parts sociales, les
liquidateurs remboursent par priorité les parts sociales libérées dans une proportion
supérieure jusqu'a ce qu'elles soient sur un pied d'égalité avec les parts sociales libérées
dans une moindre proportion ou procédent a des appels de fonds complémentaires a charge
de ces derniéres.

TITRE VI. : DISPOSITIONS GENERALES

Article 48.
Pour tous litiges entre la société, ses actionnaires, obligataires, administrateurs,
commissaires et liquidateurs relatifs aux affaires de la société et a l'exécution des présents
statuts, compétence exclusive est attribuée aux tribunaux du siége social; toutefois, la
société, si elle est demanderesse, sera en droit de porter le différend devant tout autre
tribunal compétent.

Article 49.

Les actionnaires et obligataires domiciliés a I'étranger et n'ayant fait aucune
élection de domicile en Belgique diiment notifiée a la société, sont censés avoir élu
domicile au siége social ot tous actes peuvent valablement leur étre signifiés ou notifiés, la
société n'ayant pas d'autre obligation que de les tenir a la disposition du destinataire.

Article 50 - Déontologie.

Les membres des organes de la société, son personnel et ses mandataires spéciaux

-  respectent, a l’intérieur et a l’extérieur de la société, Ja plus grande discrétion et
les régles strictes de l’éthique en matiére d’indépendance de jugement et de comportement ;

- s’abstiennent de communiquer a des tiers ou d’exploiter pour leur propre compte
ou le compte d’autrui toutes informations relatives 4 la société et a leurs activités
professionnelles en son sein ;

- s’efforcent de prévenir les conflits entre leur intérét personnel et celui de la
société ainsi qu’entre l’intérét de la société et celui d’autres entreprises auxquelles ils sont
liés par une fonction ou un mandat et, en cas de survenance de pareil conflit, agissent
suivant les principes d’impartialité et d’objectivité de la déontologie des affaires.

Les organes de la société veillent, par les moyens appropriés, a la mise en ceuvre et
au respect des dispositions qui précédent.

Article 51 - Marchés publics

Chaque fois qu’elle est amenée a soumissionner ou 4 participer 4 un marché public
de travaux, de fournitures, de services ou de concession, la société fait abstraction des liens
qu’elle a vis-a-vis des société

a) ot elle détient une participation ou
b) qui détiennent une participation dans son propre capital ou
c) dans lesquelles une société sous b) détient une participation,

que cette participation puisse ou non étre réputée comme constitutive d’influence dominante
pour I’application des législations belge, européenne ou étrangéres en matiére de marchés
publics.

En conséquence, pour ce qui concerne la justification par la société de son degré
d’autonomie par rapport aux entreprises qui lui sont liées, la présente disposition statutaire
arréte Jes principes suivants, avec valeur de présomption a l’égard des tiers :

(i) pour tout marché public auquel la société ou ses filiales participe(nt), la
détention - fit-elle majoritaire - par les actionnaires du capital de la société ou des voix
attachées aux actions, ou leur droit de désigner des membres - fiit-ce leur majorité - de son
conseil d’administration ou de tout organe éventuel de direction et de surveillance de la
société ou de ses filiales, n’emportent ni pour la société ou ses filiales, ni pour une autre
personne physique ou morale qui leur soit liée et intervenant dans ce marché public, le
bénéfice d’un quelconque avantage injustifié de nature a fausser les conditions normales de
la concurrence, et demeurent sans effet sur ce marché ;

(ii) lexercice par une personne physique ou morale d’un mandat ou d’une
mission a la fois dans la société et dans une autre personne morale intervenant 4 un marché
public auque! intervient la société ou une de ses filiales, ne constitue ni en droit ni en fait un
quelconque pouvoir de direction ou de gestion, directement ou par personne interposée.
La société justifie, A l’occasion de tout marché public, de son autonomie ou celle
d’une filiale ainsi que de l’absence d’effet d’une éventuelle influence dominante par rapport
a tout autre intervenant 4 ce marché, en produisant la présente disposition par extrait des
statuts diment signé conformément a I’ article 22 ci-dessus.

Article 52

Les parties entendent se conformer entigrement au Code des sociétés. En
conséquence, les dispositions de ce Code auxquelles il ne serait pas licitement dérogé, sont
réputées inscrites dans le présent acte et les clauses contraires aux dispositions impératives
de ce Code sont censées non écrites.

POUR COORDINATION CONFORME

 

 

 

 

 

 
