Déposd / Regu le

ae if ie
= = % _ Neenlas van Ruendlonek, Notre
\ Square Vergole 41 1030 Bruxelles

 
 
   

in tripunal Ge commMeRH? suEZ ENERGY SERVICES INTERNATIONAL
ne de Bruxelles en abrégé GDF SUEZ E.S.1.
Société anonyme
Boulevard Sim6n Bolivar, 34-36 a 1000 Bruxelles
Tribunal du Commerce de Bruxelles
Registre des Personnes Morales 0403.108.145

francop

Rép. 2015/354

 

MODIFICATION DES STATUTS

 

L'AN DEUX MIL QUINZE.
Le dix-sept novembre.
A 1030 Bruxelles, en |’ Etude.

Par devant Nous, Maitre Nicolas van RAEMDONCK. notaire & la résidence de
Schaerbeek.

S'est réunie l'assemblée générale extraordinaire des actionnaires de la société anonyme
«GDF SUEZ ENERGY SERVICES INTERNATIONAL », en abrégé « GDF SUEZ
E.S.I, », ayant son siége 4 1000 Bruxelles, boulevard Simon Bolivar, 34-36.

Société constituée suivant acte regu par Maitre Jean-Maurice De Doncker, notaire a
Bruxelles, le six novembre mil neuf cent quarante-six, publié 4 l'annexe au Moniteur belge du
vingt et un novembre suivant, sous le numéro 20.963.

Dont les statuts ont été modifiés 4 diverses reprises et pour la derniére fois suivant
procés-verbal dressé par le notaire Nicolas van Raemdonck, a Schaerbeck, soussigné, le 28 juin
2011, publié aux Annexes du Moniteur belge du 27 juillet suivant, sous le numéro 0115103.

Le siége social a été transféré par décision du Conseil d°administration du 15 avril 2014.

Inscrite auprés de la Banque Carrefour des Entreprises sous le numéro 0403.108.145.

Bureau
La séance est ouverte a 10 heures 30 minutes sous la présidence de Madame LAMBERT
Nadia, numéro national 65.1 1.12-226.80, domiciliée 4 1630 Linkebeek, chaussée d° Alsemberg.
89A.
Le Président décide de ne pas nommer de secrétaire.

Vu le nombre restreint de personnes présentes, le Président décide de ne pas nommer de
scrutateur.

Le bureau est ainsi constitué.

Composition de l'assemblée
Sont représentés les actionnaires dont les nom, prénoms, domicile ou dénomination et
siége social, ainsi que le nombre d'actions de chacun d'eux, sont repris dans la liste de présence
ci-dessous:
1. La société anonyme de droit frangais GDF SUEZ ENERGIE SERVICES,
immatriculée sous le numéro 552 046 955 au RCS de Nanterre, ayant son siége social situé 4

PREMIER ROLE
[fi
Puteaux (Hauts-de-Seine - France), | Place des Degrés.
Propriétaire de cent trente-deux mille trois cent vingt-neuf actions 132.329
2. La société par actions simplifiée de droit francais COFELY FINANCE &
INVESTISSEMENT, immatriculée sous le numéro 308 647 213 au RCS de Nanterre, ayant son
siége social situé 4 Puteaux, 1 Place des Degrés.
Propriétaire d'une action l
Total : cent trente-deux mille trois cent trente actions 132.330
Soit la totalité des actions représentatives de 'intégralité du capital social.

Procurations
Les actionnaires, précités, sont ici représentés par Madame LAMBERT Nadia,
prénommeée, suivant deux procurations sous seing privé datées du 12 novembre 2015, qui
resteront annexées au présent acte.

EXPOSE DU PRESIDENT
Le Président de l'assemblée expose et requiert le notaire soussigné d'acter que:

i La présente assemblée générale extraordinaire a pour ordre du jour :

1. Confirmation de la décision du transfert du siége social vers 1000 Bruxelles,
boulevard Simén Bolivar, 34-36.

2. Modification de la dénomination sociale en « ENGIE ENERGY SERVICES
INTERNATIONAL » et en abrégé « ENGIE E.S.L. ».

3. Modification des articles 1, 3 et 7bis des statuts pour les mettre en concordance
avec les décisions prises sur les points qui précédent a l’ordre du jour.

4, Pouvoirs a conférer :

- pour l'exécution des résolutions prises sur les objets qui précédent ;

- pour la coordination des statuts -tant en frangais qu’en néerlandais- et le dépat des
textes coordonnés conformément aux dispositions légales ;

- pour I’ accomplissement de toutes les formalités auprés du guichet d’ entreprises, de la
Banque Carrefour des Entreprises, de administration de la TVA et pour toutes autres
formalités éventuelles qui découlent des décisions prises par cette assemblée générale
extraordinaire.

Il. Il existe actuellement cent trente-deux mille trois cent trente actions.

Il résulte de la liste de présence ci-dessus que toutes les actions sont représentées.

Le Président déclare et I'assemblée reconnait que la présente société n'a émis ni titre
sans droit de vote, ni titre non représentatif du capital, ni obligation. ni droit de souscription sous
quelque forme que ce soit.

Apreés avoir regu lecture de l'article 533 du code des sociétés, les actionnaires, précités.
déclarent renoncer aux convocations.

A la demande du notaire soussigné, les comparants déclarent également que le
commissaire de la société (étant la société civile sous forme de société coopérative a
responsabilité limitée DELOITTE Réviseurs d’ Entreprises, établie Pegasus Park, Berkenlaan 8b
4 1831 Diegem, représentée par Monsieur Daniel KROES), ainsi que les administrateurs de la
société, ont renoncé aux convocations et a étre présents a la présente assemblée.

Dés lors, les actionnaires consentent 4 délibérer sur les points a l'ordre du jour.

L'assemblée peut donc délibérer et statuer valablement sur l'ordre du jour sans qu’il
doive étre justifié de l'accomplissement des formalités relatives aux convocations.
NII. Pour assister a |'assemblée les actionnaires se sont conformés aux statuts.

IV. Pour étre admises, les propositions reprises 4 l’ordre du jour doivent réunir les
majorités suivantes, 4 chaque fois des voix pour lesquelles il est pris part au vote :

- les propositions sub 1, 2 et 3; une majorité de trois/quarts ;

- la proposition sub 4 ; une simple majorité.

Vv. Chaque action donne droit 4 une voix, sous réserve des limitations statutaires
éventuelles et des cas de suspension prévus par la loi.

Vi. La société ne fait pas ou n'a pas fait appel public 4 I'épargne.

VIL Depuis la demniére assemblée, il n'y a pas eu de décisions ou d'opérations visées
par l'article 523 du code des sociétés (intéréts opposés).

CONSTATATION DE LA VALIDITE DE L-ASSEMBLEE
Lexposé du Président. aprés vérification. est reconnu exact par l'assemblée. celle-ci se
reconnait valablement constituée et apte 4 délibérer sur les différents points figurant a l'ordre du
jour.

DELIBERATION
L'assemblée aborde l'ordre du jour et, aprés avoir délibéré, prend, résolution par
résolution et chaque fois a 'unanimité, les résolutions suivantes:

Premiére résolution
L'assemblée générale décide de confirmer la décision prise par le Conseil
d'administration en date du 15 avril 2014 de transférer le si¢ge social de la société 4 1000
Bruxelles, boulevard Sim6n Bolivar, 34-36 (avec effet au 1° septembre 2014).

Deuxiéme résolution
L’assemblée générale décide de modifier la dénomination sociale actuelle et d’ adopter a
compter de ce jour, la dénomination suivante: «ENGIE ENERGY SERVICES
INTERNATIONAL » et en abrégé « ENGIE E.S.L. ».

Troisiéme résolution

L’assemblée générale décide de modifier les articles suivants des statuts, comme suit :

- la deuxiéme phrase de l'article |:

‘Elle a pour dénomination « ENGIE ENERGY SERVICES INTERNATIONAL », en
abrégé « ENGIE E.S.L. ».

- la deuxiéme phrase de l'article 3 :

‘Ul est actuellement fixé a 1000 Bruxelles, boulevard Simon Bolivar, 34-36.’

- la demiére phrase du point I de l'article 7bis :

‘Si un transfert d'actifs dans le cadre d'une opération de fusion, d'absorption, de
restructuration de sociétés a pour conséquence de faire sortir des actions «ENGIE ENERGY
SERVICES INTERNATIONAL» de la société propriétaire de celles-ci ou du groupe auquel elles
appartenaient, il y a lieu de traiter cette situation comme s'il s'agissait d'une cession d'actions
hors groupe.’

,
/

DEUXIEME ROLE

:
Quatriéme résolution
Pouvoirs

L'assemblée confére tous pouvoirs

- au conseil d'administration pour l'exécution des décisions prises sur les objets qui
précédent ;

- au notaire soussigné pour la coordination des statuts -tant en frangais qu’en
néerlandais- et le dép6t des textes coordonnés conformément aux dispositions légales ;

- i Madame LAMBERT Nadia, prénommée, avec faculté de substitution, afin de signer
et déposer toute déclaration et pour l’accomplissement de toutes les formalités auprés du guichet
d’ entreprises, de la Banque Carrefour des Entreprises, de administration de la TVA et pour
toutes autres formalités éventuelles qui découlent des décisions prises par cette assemblée
générale extraordinaire.

Dispositions finales

1. Les comparants déclarent que le notaire soussigné les a informé des droits,
obligations et charges résultant des actes juridiques qu’ils ont posés aux termes du présent
acte et qu’il les a conseillés de maniére impartiale.

2. Les comparants reconnaissent avoir recu un projet du présent acte depuis cing jours
ouvrables.

Cldture de séance
Plus rien n'étant A l'ordre du jour, la séance est levée 4 10 heures 55 minutes.

Droit d’écriture
Le droit d’écriture s*éléve 4 nonante-cing euros (€ 95,00) et est payé sur déclaration par
Maitre Nicolas van Raemdonck, notaire 4 Schaerbeek, soussigné.

DONT PROCES-VERBAL.

Dressé date et lieu que dessus.

Et lecture intégrale et commentée faite, les comparants, présents ou représentés, en leurs
qualités susmentionnées, ont signé avec Nous, Notaire.
(Suivent les signatures)
PROCURATION

La société anonyme de droit francais GDF SUEZ ENERGIE SERVICES, immatriculée sous le numéro
552 046 955 au RCS de Nanterre, ayant son siége social situé 4 Puteaux (92). | place des Degrés.

Propriétaire de 132.329 actions de la société ci-aprés nommeée.

Ici représentée. conformément a ses statuts, par son Administrateur - Directeur Général :

- M. Jéréme TOLOT.

Constitue pour mandataire spécial:
Madame Nadia LAMBERT, domiciliée 4 1630 Linkebeek (Belgique), chaussée d° Alsemberg 89A.

A qui elle confére tous pouvoirs aux fins de :

la représenter a l'assemblée générale extraordinaire des actionnaires de la société anonyme GDF
SUEZ ENERGY SERVICES INTERNATIONAL, ayant son siége social 4 1000 Bruxelles, boulevard
Sim6n Bolivar 34-36. qui se tiendra en "étude du notaire Nicolas van Raemdonck a Schaerbeek / 1030
Bruxelles, square Vergote. 41, le 17 novembre 2015, 4 10.30 heures, avec l'ordre du jour suivant:

1, Confirmation de la décision du transfert du siége social vers 1000 Bruxelles. boulevard
Simon Bolivar, 34-36.

2 Modification de la dénomination sociale en « ENGIE ENERGY SERVICES
INTERNATIONAL » et en abrégé « ENGIE E.S.1. ».

3 Modification des articles 1, 3 et 7bis des statuts pour les mettre en concordance avec les
décisions prises sur les points qui précédent al ordre du jour.

4 Pouvoirs a conférer :
- pour [exécution des résolutions prises sur les objets qui précédent :
- pour la coordination des statuts -tant en frangais qu'en néerlandais- et le dépat des textes
coordonnés conformément aux dispositions légales :
- pour Vaccomplissement de toutes les formalités auprés du guichet d ‘entreprises. de la
Banque Carrefour des Entreprises, de ladministration de la TVA et pour toutes autres
formalités éventuelles qui découlent des décisions prises par cette assemblée générale
extraordinaire.

Le mandataire peut :

- Assister a toute assemblée qui aurait le méme ordre du jour: prendre part 4 toutes délibérations. y
émettre tous votes sur toutes propositions figurant a l'ordre du jour ci-dessus ainsi que sur toutes
celles que l'assemblée déciderait d'y porter:

Aux effets ci-dessus, passer et signer tous actes, piéces. procés-verbaux, registres, élire domicile.
substituer et, en général, faire le nécessaire, méme ce qui n'est pas expressément prévu aux

     

présentes.
'
Fait a Puteaux. le 12 novembre 2015. '
un wey
Jerome J) LOT |
Administrateur irecteur Général.

/

/

TROISIEME ROLE

Ip

£
PROCURATION

La société par actions simplifiée de droit frangats COPELY FINANCE & INVESTISSEMENT.
immatriculée sous le numéro 308 647 213 au RCS de Nanterre, ayant son siége social situé 4 Puteaux (92).
| place des Degrés.

Propriétaire de | action de la société ci-aprés nommée.
Ici représentée, conformément a ses statuts. par son Président :
- M, Jean-Pierre NEGRE,

Constitue pour mandataire spécial:
Madame Nadia LAMBERT. domiciliée a 1630 Linkebeek (Belgique), chaussée d’ Alsemberg 89A.

A qui elle confére tous pouvoirs aux fins de :

la représenter a l'assembiée générale extraordinaire des actionnaires de la société anonyme GDF

SUEZ ENERGY SERVICES INTERNATIONAL, ayant son siége social 4 1000 Bruxelles, boulevard
Simon Bolivar 34-36, qui se tiendra en l'étude du notaire Nicolas van Raemdonck a Schaerbeek / 1030
Bruxelles, square Vergote, 41, le 17 novembre 2015. 4 10.30 heures, avec l'ordre du jour suivant:

/

Confirmation de la décision du transfert du siége social vers 1000 Bruxelles, boulevard
Simon Bolivar, 34-36.

Modification de la dénomination sociale en « ENGIE ENERGY SERVICES
INTERNATIONAL » et en abrégé « ENGIE E.S.1. »

Modification des articles 1, 3 et 7his des statuts pour les mettre en concordance avec les
décisions prises sur les points qui précédent a Vordre du jour.

Pouvoirs a conférer :

- pour lexécution des résolutions prises sur les objets qui précédent ;

- pour la coordination des statuts -tant en francais qu’en néerlanduis- et le dépot des textes
coordonnés conformément aux dispositions légales :

- pour laccomplissement de toutes les formalités auprés du guichet d entreprises, de la
Banque Carrefour des Entreprises, de ladministration de la TVA et pour toutes autres

formalités éventuelles qui découlent des décisions prises par cette assemblée générale
extraordinaire

Le mandataire peut :

Assister a toute assemblée qui aurait le méme ordre du jour; prendre part a toutes délibérations. y

émettre tous votes sur toutes propositions figurant a l'ordre du jour ci-dessus ainsi que sur toutes
celles que l'assemblée déciderait d'y porter:

Aux effets ci-dessus, passer et signer tous actes. piéces, procés-verbaux, registres, élire domicile.

substituer et, en général. faire le nécessaire, méme ce qui n'est pas expressément prévu aux
présentes.

Fait 4 Puteaux. le 12 novembre 2015. fis tty ‘ LA

4

oy

Jean-Pierre NEGRE, | ~
Président.
POUR EXPEDITION CONFORME

 

QUATRIEME ET DERNIER ROLE
Pour l'acte avec n° de répertoire 20150354, passé le 17 novembre 2015
FORMALITES DE L'ENREGISTREMENT

Enregistré 4 rdles, 0 renvois,

au Bureau de Il'Enregistrement Actes Authentiques Bruxelles 5 le 27 novembre
2015

Réference 5 Volume 0 Folio 0 Case 23216.

Droits percus: cinquante euros (€ 50,00).

Le receveur

ANNEXE

Enregistré 2 rdles, 0 renvois,

au Bureau de l'Enregistrement Actes Authentiques Bruxelles 5 le 27 novembre
2015

Réference 6 Volume 0 Folio 100 Case 8363.

Droits pergus: cent euros (€ 100,00).

Le receveur
