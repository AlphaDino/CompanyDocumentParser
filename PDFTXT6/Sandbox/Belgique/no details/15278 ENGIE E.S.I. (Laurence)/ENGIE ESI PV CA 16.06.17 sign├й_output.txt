CNGiIC

PROCES-VERBAL DU CONSEIL
D'ADMINISTRATION
16 juin 2017

Sont présents : - Patrick van der BEKEN, Président ;
- Sven DE SMET, Administrateur délégué ;
- Etienne JACOLIN, Administrateur ;
- Thierry van den HOVE, Administrateur ;

Est excusé : - Didier ENGELS, Administrateur ;
Assistent également: - Yves DELPORTE qui assure les fonctions de Secrétaire du Conseil
- Valérie SIRJACOBS
+
+ +

La séance est ouverte a 10.00 h sous la présidence de Patrick van der Beken. II constate que la majorité
des administrateurs sont présents et que le conseil est donc valablement constitué afin de pouvoir
délibérer et décider sur les points inscrits a l’ordre du jour.

 

Le procés-verbal de la séance du conseil du 18 octobre 2016 sera soumis a la signature des
administrateurs lors du prochain conseil.

2. Examen et arrété des comptes au 31 décembre 2016

Le Conseil procéde a I’examen des documents remis présentant I’activité de la société au cours de
l'exercice écoulé et les comptes clos le 31 décembre 2016.

En préambule, le Président rappelle qu’au cours de l’exercice 2016, ENGIE E.S.I. a poursuivi la
gestion de ses participations actives en Europe et hors Europe tant dans les métiers de l‘ingénierie,
de linstallation et la maintenance technique, les services et |’énergie.

a. Comptes statutaires

Le total des rubriques du bilan s'éléve, a l'actif et au passif, 4 2.398.830 KE et le total du compte
de résultats, en charges et en produits, a 146.331 KE.

b. Affectation du résultat
L'exercice se cléture par une perte de 78.805 K€. Compte tenu du report bénéficiaire de l’exercice
précédent d’un montant de 172.112 K€, le résultat a affecter s‘éleve 4 93.307 K€ que le conseil
propose de reporter.

Cette proposition d’affectation est approuvée a l’unanimité par les administrateurs.

Boulevard Simon Bolivariaan 34 - 1000 Brussels - BELGIUM

BTW/TVA BE 0403, 108.145 =
RPR/RPM Bruxelies 5

===
ENGIF ENERGY SERVICES INTERNATIONAL sa/nv G U5
CNGi

c. Autres informations financiéres
Les principaux mouvements intervenus cette année dans notre portefeuille concernent :

e Une augmentation de capital de notre filiale ENGIE Holdings Inc de 427 ME dans le cadre
du financement partiel de acquisition aux USA de la société GCN ainsi que de l’acquisition
intragroupe d’Engie North America reprenant les activités Retail, LNG et Generation ;

e Une augmentation de capital d’Engie Holding Wien de 17 ME en vue d’acquérir Engie
Energy GmbH (opération intragroupe) ;

e — L’acquisition d’Engie Polska a 1 euro suivie d’‘une augmentation de capital pour un montant
de 115 M€ en vue de restructurer la situation financiére de la société et de soutenir
durablement le développement des énergies renouvelables ;

e Au Pays-Bas, une augmentation de capital de 9,1 M€ de Fabricom Nederland BV pour le
financement de !‘augmentation de capital de sa filiale Fabricom AS en Norvége ;

e L’acquisition d’Inéo Polynésie pour 1,2 ME ;

e Une augmentation de capital de 26,7 ME de Cofely Espajia.

d. Situation fiscale
i. Contrdle fiscal en matiére de prix de transfert
Divers sujets ont été discutés avec l’administration fiscale, notamment la
rémunération des garanties, des comptes courants et des préts long termes.

ii. Situation des déficits
Un tableau reprenant les latences fiscales pour les exercices d‘imposition de
2010 a 2017 fait partie du dossier remis aux membres du conseil.

b

3. Rapport de gestion
Le Conseil examine le projet de rapport de gestion et arréte les termes de ce dernier qui sera
communiqué a l’'assemblée générale ordinaire.
4. 4 m néral
Conformément aux dispositions statutaires, I'assemblée générale ordinaire aurait di avoir lieu
le mardi 2 mai 2017 a 10.30 heures. Il a été décidé de la reporter au vendredi 23 juin 2017 a
10.30 heures.
Aucun mandat n’arrive a échéance lors de l’assemblée générale approuvant les comptes.
Le Président informe le conseil de la démission de Yanick BIGAUD de son mandat
d‘administrateur avec effet au 17 janvier 2017 et de la démission de Sven DE SMET de ses
mandats d’administrateur délégué et d’administrateur de la société avec effet au 30 juin 2017.
Yanick Bigaud et Sven De Smet sont chaleureusement remerciés pour le travail accompli dans
le cadre de I’exercice de leur mandat.
Le conseil propose de fixer ordre du jour de l’'assemblée comme suit :
1. Rapport de gestion du conseil d’administration et rapport du commissaire.
2. Examen et approbation des comptes annuels au 31 décembre 2016.
Affectation du résultat.
3. Décharge aux administrateurs et au commissaire.
4. Conseil d’administration: démissions - nomination - réduction du nombre
d’administrateurs.
ENGIE ENERGY SERVICES INTERNATIONAL sa/nv - PV 16.06.2017 - ¢

J’
i memneeieeert

CNGi

5.

7.

ENGIE ENERGY SERVICES INTERNATIONAL sa/nv - PV 16.06.2017 -

Cc

Opérati
a) immobilier - cession @ la Commune de I’immeuble a Uccle rue Gatti de Gamond -
évolution du litige — transaction

Une transaction consistant au versement par ENGIE E.S.I. de 150.000 € a la Commune
d’Uccle a été signée le 5 mai 2017 mettant fin au litige relatif aux travaux de
désamiantage de Iimmeuble situé rue Gatti de Gamond.

b) recapitalisation de Fabricom Nederland BV
Pour remplacer l’encours de dette que Fabricom Nederland BV a contracté avec ENGIE
CC (79 ME), ETM (145 M€) et ENGIE Services Nederland NV (23 ME), il est demandé a
ENGIE E.S.I. d‘injecter des capitaux propres pour un montant total de 250 ME a
Fabricom Nederland BV qui a des besoins de financement a long terme en tant que
sous-holding.

c) augmentation du capital de Fabricom SA a hauteur de 30 ME

Le résultat net en 2016 de FABRICOM SA a été lourdement impacté par les activités
internationales et en particulier les activités « Oil, Gaz & Power » suite & de grands
projets difficiles. Ces activités ont été restructurées fin 2016 et leurs perspectives
restaurées. La perte statutaire de l’exercice s‘éleéve a 30 ME. Cette perte a pour
conséquence une dégradation de la solvabilité et de la liquidité de maniére significative
et a un niveau en dessous des exigences de marché. Pour restaurer rapidement son
ratio de solvabilité, Fabricom SA sollicite sa maison-mére pour une augmentation de
capital équivalant a la perte exceptionnelle de 30 ME.

Modification de l’obj ial — changement des le 28 jui 17

Suite aux acquisitions récentes, ENGIE E.S.I. entend élargir ses activités qui se situent
actuellement essentiellement dans le domaine des installations techniques et de services aux
collectivités en vue d’y inclure toute activité liée directement ou indirectement a la production,
la transformation et la distribution de toutes formes d’énergie.

Il est donc décidé de modifier les statuts de la société afin d’ajouter certains paragraphes a
larticle 2 relatif a l'objet social, d’en profiter pour opérer un toilettage de I’ensemble du texte
et pour modifier l'article 26 relatif a la date de l’assemblée générale ordinaire qui se tiendra
dorénavant le deuxiéme mardi du mois de juin.

Conformément aux dispositions légales applicables, le conseil rédige et signe le rapport spécial

auquel est joint un état résumant la situation active et passive de la société arrété au 31 mars
2017, sur lequel il sera demandé a notre commissaire de faire un rapport.

actuslisationd —

Le Conseil décide de reporter l’actualisation des pouvoirs de signature aprés les délibérations
de l’'assemblée générale.

ass
wy

3/5
—

CNGiC

8. Mise a jour de la Charte Ethique Engie

Documents de référence du Groupe ENGIE en matiére d’éthique et de compliance, la Charte
éthique (remise en séance) et le Guide pratique de |’éthique d’ENGIE ont été mis a jour en
novembre 2016.

Cette nouvelle édition tient compte des évolutions importantes du cadre Iégal et réglementaire
en matiére d’éthique des affaires depuis la 1ére édition de la Charte éthique et du Guide
pratique de I’éthique en 2009 et leur mise a jour en 2012, notamment en matiére de lutte contre
la corruption et le trafic d’influence (par exemple FCPA aux Etats-Unis, UK Bribery Act, loi Sapin
TI en France).

La Charte éthique d’ENGIE présente les principes éthiques fondamentaux du Groupe et fixe le
cadre général dans lequel doit s'inscrire le comportement professionnel de tous les
collaborateurs du Groupe. Le Guide pratique de |’éthique quant a lui détaille les modalités
d’application des principes éthiques et donne des exemples de mises en situation, afin d’aider
dans la pratique de I’éthique au quotidien.

Tous les collaborateurs du Groupe s‘engagent a respecter les valeurs et engagements éthiques
d’ENGIE, dont les 4 principes éthiques fondamentaux sont :

1. Agir en conformité avec les lois et les réglementations ;

2. Se comporter avec honnéteté et promouvoir une culture d’intégrité ;

3. Faire preuve de loyauté ;

4. Respecter les autres.
*

Les administrateurs de ENGIE E.S.I. ont pris connaissance de la nouvelle Charte Ethique
d'ENGIE, que la société fait sienne, et veillent 4 tenir compte tant de sa lettre que de son esprit
dans l’accomplissement de leur mission d’administrateur.

Cette politique d’éthique a vocation a s’appliquer a toutes les relations de Ientreprise avec ses
parties prenantes.

9. Divers
Cooptation
Le conseil décide de nommer provisoirement (coopter) a la fonction d’administrateur Philippe
LEKANE, domicilié 4 5080 La Bruyére, rue du Surtia 8, en remplacement de Sven DE SMET,

avec effet au 1er juillet 2017. Son mandat devra étre confirmé par I’'assemblée générale prévue
le 23 juin 2017.

Opérations en cours

Mongolie : projet Sainshand

Acquisition, par elle-méme ou par l'une de ses sociétés affiliées, de 40% dans Sainshand Salkhin
Park LLC ; une JV développant un parc éolien de 55 MW en Mongolie. Les autres partenaires
du projet seront Ferrostaal (18,42%), IFU Danemark (40%) et un partenaire local (1,58%).

La décision du CDE (annexe 1) et la note sur |investissement soumis au Commitment
Committee (annexe Lbis) sont jointes au présent procés-verbal.

ENGIE ENERGY SERVICES INTERNATIONAL sa/nv - PV 16.06.2017 - % 4/s

fr)
OT

CNGiIC

Brésil : INEO LATAM

Augmentation de capital de COFELY do BRASIL souscrite par Engie Brasil Participagdes Ltda qui
aura pour effet de diluer la participation de ENGIE ESI (aujourd’hui actionnaire 4 hauteur de
99 %) et de « sortir » ENGIE ES (détenteur d’une unique action). Cette augmentation permettra
a COFELY de financer l’acquisition d’‘INEO Latam et cette opération d'une facon plus globale
permettra terme de réorganiser les activités « services » au Brésil.

Une procuration afin de faire représenter ENGIE E.S.I. par des avocats brésiliens dans toutes
les démarches liées 4 cette opération a été signée le 17 janvier 2017 ; elle est jointe au présent
procés-verba! (annexe 2), accompagné du mémo (annexe 2bis).

Canada « projet CAMH (Center of Addiction & Mental Health) a Toronto

ENGIE £.S.I. a donné son accord pour |’émission d’une garantie type « PCG » visant les

engagements d'une filiale locale indirecte (ENGIE Services Inc.) dans le cadre de ce projet ; ce
document signé le 27 février 2017 est joint au présent procés-verbal (annexe 3).

Plus aucun point efigurant a l’ordre du jour, la séance est levée a 12 heures.

     
 

 
 

/ ? /, =
Thierry van dén HOVE Etienne JA Sven DE SMET Patrick vah der BEKEN
Administrateur Admini Administrateur Délégué Président

Annexes : 5

ENGIE ENERGY SERVICES INTERNATIONAL sa/nv - PV 16.06.2017 -

5/5
