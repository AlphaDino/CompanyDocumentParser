Réservé
au
Moniteur

belge

 

Bijlagen bij het Belgisch Staatsblad - 30/10/2023 - Annexes du Moniteur belge

Mod PDF 19.01

Copie a publier aux annexes au Moniteur belge
aprés dépét de l'acte au greffe

Fe Déposé
*23417969* ae 26-10-2023

ep
Greffe

 

 

N° d'entreprise : 0403108145
Nom
(en entier): ENGIE ENERGY SERVICES INTERNATIONAL
(en abrégé): ENGIE E.S.1.

Forme légale : Société anonyme

Adresse complete du siége Boulevard Simon Bolivar 36
: 1000 Bruxelles

Objet de I'acte : CAPITAL, ACTIONS, STATUTS (TRADUCTION, COORDINATION,
AUTRES MODIFICATIONS)

D’aprés un procés-verbal recu par Maitre Damien HISETTE, notaire a Bruxelles (deuxiéme canton),
associé de « Van Halteren, Notaires Associés », 4 1000 Bruxelles, rue de Ligne 13, le 23 octobre
2023, il résulte que :

(...)

PREMIERE RESOLUTION.

Conformément a l’article 7 :179 paragraphe 3 du Code des sociétés et associations, l’assemblée,
représentant l'ensemble des actionnaires, décide de renoncer aux rapports prévus par ce méme
article.

DEUXIEME RESOLUTION.

L’assemblée décide d’augmenter le capital a concurrence de 1.364.915.450 EUR pour le porter de
1.668.923.650 EUR a 3.033.839.100 EUR par l’émission de 108.225 nouvelles actions ordinaires,
identiques a celles existantes et jouissant des mémes droits et avantages a partir de la répartition
afférente a l'exercice social commencé le 1er janvier 2023 et a compter de leur émission.

Ces actions nouvelles seront souscrites pour le prix global de 2.200.000.000 EUR, dont une prime d’
émission de 835.084.550 EUR, et intégralement libérées en espéces lors de la souscription.
SOUSCRIPTION - LIBERATION.

Ensuite la société anonyme de droit francais ENGIE, préqualifiée et représentée comme il a été
exposé, aprés avoir entendu lecture de tout ce qui précéde, déclare par l'organe de son représentant

a) avoir une parfaite connaissance des statuts de la société et en connaitre parfaitement la situation
financiére;
b) et souscrire les 108.225 actions nouvelles dont I’émission vient d'étre décidée.

Ensuite la souscriptrice libere entierement sa souscription par un versement en espéces, de sorte
que la somme de 2.200.000.000 EUR se trouve, dés a présent, a la libre disposition de la société a
un compte spécial ouvert a cet effet au nom de la société auprés de ING Belgique SA.

Une attestation de ce dépét restera ci-annexée.

CONSTATATION DE LA REALISATION EFFECTIVE DE L’AUGMENTATION DE CAPITAL.
L'assemblée constate et requiert le notaire instrumentant d'acter que l’augmentation de capital est
effectivement réalisée et que le capital est ainsi porté a 3.033.839.100 EUR, représenté par 240.555
actions, sans désignation de valeur nominale.

TROISIEME RESOLUTION.

L'assemblée décide que le montant de la prime d'émission, soit 835.084.550 EUR, représentant la
différence entre la valeur conventionnelle de l’apport et le montant de l’augmentation de capital, sera
conformément a l'article 184 CIR 92, porté et maintenu a un compte distinct :"Primes d'émission"
dans les capitaux propres au passif du bilan.

QUATRIEME RESOLUTION.

En application de l'article 39, §1, alinéa 1 et 3 de la loi du 23 mars 2019 introduisant le Code des
sociétés et des associations et portant des dispositions diverses (1), l’assemblée générale décide d’

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
Réservé
au
Moniteur

belge

   

Bijlagen bij het Belgisch Staatsblad - 30/10/2023 - Annexes du Moniteur belge

Volet B - suite Mod PDF 19.01
adapter les statuts aux dispositions du Code des sociétés et des associations.
CINQUIEME RESOLUTION.

En conséquence de ce qui précéde, l’assemblée décide d’adopter un nouveau texte des statuts
conforme au Code des sociétés et des associations.

ll est ici précisé que dans le libellé de l’objet dans le texte néerlandais des statuts, le mot « doel » est
chaque fois remplacé par le mot « voorwerp » et dans le texte néerlandais et dans le texte frangais, |’
adjectif « social » ou « sociale » est chaque fois supprimé, afin d’utiliser la terminologie du Code des
sociétés et des associations. Aucun rapport au sens de l'article 7 :154 du Code des sociétés et des
associations n’est donc requis.

L’assemblée décide donc d’adopter le texte suivant en tant que nouveau texte des statuts, en
remplacement de l’ancien texte :

TITRE |

FORME DENOMINATION OBJET - SIEGE DUREE

Article 1

La société est une société anonyme. Elle a pour dénomination « ENGIE ENERGY SERVICES
INTERNATIONAL », en abrégé « ENGIE E.S.1. ».

Il peut 6tre fait usage isolément de la dénomination compléte ou abrégée.

Article 2

La société a pour objet principal :

* de s'intéresser par voie de cession, apport, souscription, achat ou échange de titres,
participation, commandite, prét ou de toute autre maniére, dans toutes sociétés ou entreprises
créées ou a créer dans le domaine des installations techniques et des services aux collectivités ;

* la production, le transport, la transformation et la distribution de toutes formes d'énergie et de
sources d'énergie, notamment I'électricité et le gaz ;

* la commercialisation d'énergie électrique, la commercialisation de gaz, la fourniture de tous
produits et services liés directement ou indirectement a I'6énergie, au confort, a la sécurité, a la
performance énergétique, a I'habitat, aux infrastructures, aux communications ainsi que la fourniture
de services ou conseils et études relatifs a ces mémes activités, de méme que toutes les activités
connexes ;

* la production, le transport, la transformation et la distribution par tous les moyens d'informations
et de signaux ;

* la fourniture de produits et services dans le cadre des prestations collectives d'utilité publique ;

* toutes activités d'ingénierie de conception et de réalisation, tant de services que de travaux ;

* toutes opérations financiéres de quelque nature qu’elles soient, liées au financement des
opérations du groupe dont fait partie la société ;

+ loctroi de tout type de sdreté, personnelle et réelle, afin de garantir ses propres engagements,
ainsi que des obligations de tiers (en ce compris celles de sociétés liées).

La société a également pour objet la prise de participations en actions ou autres instruments
financiers (par voie de cession, apport, souscription, achat ou échange de titres, participation,
commandite, prét ou de toute autre maniére), en Belgique et dans tous autres pays :

1. dans toutes sociétés dont l'objet est similaire ou connexe au sien, et

2. dans toutes sociétés a objet financier ou analogue dont I'activité est utile au développement
tant de la société et de ses filiales que de toutes autres sociétés du groupe dont fait partie la société.
Elle peut réaliser son objet soit directement soit indirectement et effectuer toutes opérations de
nature a favoriser son développement et toutes opérations financiéres, industrielles, commerciales et
immobiliéres nécessaires.
Elle peut enfin construire, acquérir, prendre et donner a bail ou gérer toutes usines, installations,
exploitations, magasins ou dépéts, demander, acheter, céder et exploiter tous brevets ou licences,
construire, acheter, vendre, prendre et donner en location tout matériel ou appareil, et faire toutes
opérations de consignation, de représentation, de courtage ou de commission relatives ou connexes
a son objet.
La société peut conclure des accords de coopération avec des sociétés, exer¢ant des activités
similaires ou connexes, belges ou étrangéres, constituer des sociétés pour l'exploitation des
entreprises qu'elle aurait acquises, établies ou étudiées, et leur faire cession ou apport, sous une
forme quelconque, de tout ou partie de l'avoir social.
D'une maniére générale, la société peut, tant en Belgique qu’a I'étranger, se livrer a toutes activités
et opérations immobiliéres ou mobiliéres, financiéres, industrielles ou commerciales se rattachant
directement ou indirectement, en tout ou en partie, a son objet ou qui seraient de nature a en faciliter
la réalisation.
Article 3
Le siége est établi dans la Région de Bruxelles-Capitale. II peut étre transféré en tout endroit en

Mentionner sur la derniére page du VoletB: —_Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
Réservé
au
Moniteur

belge

   

Bijlagen bij het Belgisch Staatsblad - 30/10/2023 - Annexes du Moniteur belge

Mod PDF 19.01

Volet B_ - suite

Belgique, par simple décision de l’organe d’administration qui a tous pouvoirs aux fins de faire
constater authentiquement la modification statutaire 6ventuelle qui en résulte, sans que cela ne
puisse entrainer une modification de la langue des statuts.

Article 4

La société peut établir, par décision du conseil d'administration, des sieges administratifs, sieges
d'opération, siéges d'exploitation, succursales ou agences, tant en Belgique qu’a I6tranger.

Article 5

La durée de la société est illimitée.

TITRE II

CAPITAL - OBLIGATIONS

Article 6

Le capital est fixé a trois milliards trente-trois millions huit cent trente-neuf mille cent euros
(3.033.839. 100 EUR).

Il est représenté par deux cent quarante mille cinq cent cinquante-cing (240.555) actions sans
désignation de valeur nominale, représentant chacune un/deux cent quarante mille cing cent
cinquante-cinquiéme (240.555 eme) de I'avoir social.

Article 7.

Le capital peut 6tre augmenté ou réduit, en une ou plusieurs fois, par l'assemblée générale
délibérant aux conditions requises pour les modifications des statuts et conformément aux
dispositions du Code des sociétés et des associations.

Les nouvelles actions a souscrire en espéces sont offertes par préférence aux actionnaires,
proportionnellement a la partie du capital que représentent leurs actions.

L'assemblée générale fixe le délai de I'exercice du droit de préférence.

Elle confére au conseil d'administration tous pouvoirs, aux fins de réaliser les décisions prises et de
fixer les conditions de l'exercice du droit de préférence.

Toutefois, par dérogation a ce qui précéde, l'assemblée générale peut, dans I'intérét social, aux
conditions requises pour la modification des statuts et dans le respect des dispositions légales,
limiter ou supprimer le droit de souscription préférentielle.

Le conseil d'administration a toujours la faculté de passer, dans le respect des dispositions légales,
aux clauses et conditions qu'il avisera, toutes conventions destinées a assurer la souscription de tout
ou partie des nouvelles actions a émettre.

Article 8

Les libérations a effectuer en numéraire ou par apports en nature sur les actions non encore
entiérement libérées doivent étre faites aux lieux et aux dates que le conseil d'administration
détermine. Les libérations appelées s'imputent également sur l'ensemble des actions dont
l'actionnaire est titulaire.

Les libérations appelées et non effectuées huit jours aprés celui de leur exigibilité portent intérét,
calculé par jour de retard a compter de I'6chéance, au taux légal, sans qu'une mise en demeure soit
requise.

Le conseil d'administration peut, en outre, aprés une mise en demeure notifiée par lettre
recommandée restée sans résultat pendant un mois, prononcer la déchéance de I'actionnaire et,
dans le respect de I'égalité des actionnaires, vendre les actions sur lesquelles les libérations
appelées n'ont pas été opérées. A compter de la mise en demeure, les droits liés aux actions non
libérées sont suspendus.

Le produit net de la vente s'impute au profit de la société, sur ce qui lui est du, en principal et intéréts,
par I'actionnaire défaillant, sans préjudice du droit de la société de lui réclamer le restant dd, ainsi
que tous dommages et intéréts éventuels.

Les actions ne pourront étre valablement cédées qu'aprés leur entiére libération, sauf accord écrit du
conseil d'administration.

Article 9

Le conseil d'administration peut autoriser les actionnaires a libérer leurs actions par anticipation ;
dans ce cas, il détermine les conditions auxquelles les libérations anticipées sont admises. Le
conseil d’administration peut délivrer des certificats attestant de son contenu.

Article 10

Les actions sont nominatives jusqu’a leur entiére libération. Aprés leur libération, elles restent
nominatives. Il est tenu au siége un registre des actions nominatives dont tout actionnaire peut
prendre connaissance. Le registre peut étre tenu de maniére électronique.

Article 11

Le conseil d'administration peut suspendre I'exercice des droits afférents aux actions faisant l'objet
d'une copropriété, d'un usufruit ou d'un gage, jusqu’a ce qu'une seule personne soit désignée comme
é6tant, a l'égard de la société, propriétaire de ces actions.

Article 12

La société peut émettre des obligations, hypothécaires ou autres, par décision du conseil

Mentionner sur la derniére page du VoletB: —_Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
   

Réservé
au
Moniteur

belge

Bijlagen bij het Belgisch Staatsblad - 30/10/2023 - Annexes du Moniteur belge

Mentionner sur la derniére page du VoletB: —_Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

Mod PDF 19.01

Volet B_ - suite

d'administration, qui en déterminera le type et fixera le taux des intéréts, le mode et I'6époque des
remboursements, ainsi que toutes autres conditions d'émission.

Elle peut émettre des obligations convertibles en actions, ou assorties d'un droit de souscription, aux
conditions prévues par les articles 7 :65 et suivants du Code des sociétés et des associations, par
décision de l'assemblée générale, agissant conformément aux dispositions relatives aux
modifications des statuts.

En cas d'émission d'obligations convertibles ou avec droit de souscription, les actionnaires
bénéficient d'un droit de souscription préférentielle proportionnellement a la partie du capital que
représentent leurs actions ; l'exercice du droit de souscription préférentielle est organisé
conformément aux dispositions de la loi.

L'assemblée générale des actionnaires peut, dans I'intérét social, limiter ou supprimer ce droit de
souscription préférentielle en respectant les conditions prévues par la loi.

Les dispositions de l'article onze des statuts sont applicables aux obligations émises par la société.
TITRE III

ADMINISTRATION - DIRECTION - REPRESENTATION — CONTROLE

Article 13

La société est administrée par un conseil d'administration composé de trois membres au moins,
nommés pour quatre ans au plus par l'assemblée générale des actionnaires et révocables par elle.
Les administrateurs sont rééligibles.

Toutefois, tant que la société compte moins de trois actionnaires, le conseil d'administration peut étre
constitué de deux administrateurs.

Les fonctions des administrateurs sortants et non réélus prennent fin immédiatement a I'issue de
l'assemblée générale ordinaire.

Le conseil d'administration élit en son sein un président.

Article 14

En cas de vacance d'un ou plusieurs mandats d'administrateur, les membres restants du conseil
d'administration peuvent pourvoir provisoirement au remplacement jusqu’a la prochaine assemblée
générale qui confirme le mandat de l’administrateur coopté.

Tout administrateur, désigné dans les conditions ci-dessus, n'est nommé que pour le temps
nécessaire a l'achévement du mandat de I'administrateur qu'il remplace.

Article 15

Les administrateurs ne contractent aucune obligation personnelle relativement aux engagements de
la société. lls sont responsables a I'égard de la société de I'exécution de leur mandat et des fautes
commises dans leur gestion, notamment du dépassement des pouvoirs tels qu'ils résultent des
présents statuts ou de décisions de l'assemblée générale.

Article 16

Le conseil d'administration se réunit au siége ou a I'endroit indiqué dans les convocations chaque
fois que I'intérét de la société I'exige et en tout cas, chaque fois qu'un administrateur au moins le
demande.

Les convocations sont faites par simple lettre ou par mail envoyé, sauf urgence, au moins cing jours
francs avant la réunion et contenant I'ordre du jour.

Les réunions sont présidées par le président du conseil ou, en cas d'empéchement de celui-ci, par
un administrateur désigné par ses collégues.

Le conseil d'administration se réunit valablement sans convocation si tous les administrateurs sont
présents ou représentés et ont marqué leur accord sur I'ordre du jour.

Article 17

Le conseil ne peut délibérer et statuer que si la majorité de ses membres est présente ou
représentée.

Chaque administrateur peut, par tout moyen de transmission écrit dont l'auteur est identifié avec une
certitude suffisante, donner a l'un de ses collegues pouvoir de le représenter a une séance du
conseil et d'y voter a sa place. Un administrateur ne peut toutefois représenter plus de deux autres
membres du conseil.

Les réunions, en ce compris les délibérations et votes, peuvent, sur proposition de son président,
étre tenues via tout moyen de télécommunication, notamment oral ou visuel, qui permette des débats
entre des participants géographiquement éloignés. Dans ce cas, la réunion est réputée se tenir au
siége et tous les administrateurs participants y étre présents.

Les décisions sont prises a la majorité des voix. En cas de partage, celle du président de la réunion
est prépondérante. Si, dans une séance du conseil réunissant la majorité requise pour délibérer
valablement, un ou plusieurs administrateurs s‘abstiennent, les résolutions sont valablement prises a
la majorité des autres membres du conseil, présents ou représentés.

Les décisions du conseil d'administration peuvent étre prises par consentement unanime des
administrateurs exprimé par écrit, a l'exception des décisions qui requiérent un acte notarié.

Article 18

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
   

Réservé
au
Moniteur

belge

Mentionner sur la derniére page du VoletB: —_Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

Volet B - suite Mod PDF 19.01
Les délibérations du conseil d'administration sont constatées par des procés-verbaux signés par le
président et les administrateurs qui le demandent.

Ces procés-verbaux sont consignés ou reliés en un recueil spécial sous une forme quelconque, y
compris la forme électronique. Les copies ou extraits, a produire en justice ou pouvant étre utiles a
toutes fins, sont signés par deux administrateurs et font preuve des décisions prises.

Article 19

L'assemblée générale peut allouer aux administrateurs un émolument fixe ou des jetons de présence
a porter a charge du compte de résultats.

Le conseil d'administration est autorisé également a accorder aux administrateurs chargés de
fonctions ou missions spéciales, une rémunération particuliére a prélever sur les frais généraux.
Article 20

Le conseil d'administration a le pouvoir d'accomplir tous les actes nécessaires ou utiles a la
réalisation de l'objet, a l'exception de ceux que la loi réserve a l'assemblée générale.

Article 21

Le conseil d'administration peut déléguer la gestion opérationnelle (en ce compris la gestion
journaliére) ainsi que la représentation de la société en ce qui concerne cette gestion a une ou
plusieurs personnes agissant dans ce cas deux a deux. Il nomme et révoque les délégués a cette
gestion qui sont choisis dans ou hors de son sein, fixe leur rémunération et détermine leurs
attributions.

Le conseil d'administration peut confier la direction de l'ensemble, de telle partie ou de telle branche
spéciale des affaires sociales a une ou plusieurs personnes.

Le conseil d'administration ainsi que les délégués a la gestion opérationnelle (en ce compris la
gestion journaliére), dans le cadre de cette gestion, peuvent également conférer des pouvoirs
spéciaux et déterminés a une ou plusieurs personnes de leur choix.

Article 22

La société est représentée dans tous les actes, y compris ceux ou intervient un fonctionnaire public
ou un officier ministériel et en justice :

* soit par deux administrateurs agissant conjointement ;

* soit, dans les limites de la gestion opérationnelle ou journaliére, par le délégué a cette gestion
s'il n'y en a qu'un seul, et par deux délégués agissant conjointement s'ils sont plusieurs.
Elle est en outre valablement engagée par des mandataires spéciaux dans les limites de leur
mandat.
Article 23
Le contréle des comptes de la société est confié a un commissaire, nommé par l'assemblée générale
pour une durée de trois ans, parmi les réviseurs d'entreprises, inscrits au registre public des
réviseurs d'entreprises ou les cabinets d'audit enregistrés, rééligible et révocable par elle.
Si par suite de décés ou pour un autre motif, il n'y a plus de commissaire, le conseil d'administration
doit convoquer immédiatement l'assemblée générale pour pourvoir a cette vacance.
Les fonctions du commissaire sortant et non réélu prennent fin immédiatement aprés l'assemblée
générale ordinaire.
La mission et les pouvoirs du commissaire sont ceux que lui assigne le Code des sociétés et des
associations.
L'assemblée générale détermine les émoluments du commissaire correspondant a ses prestations
de contréle des comptes. Toutefois, le conseil d'administration peut attribuer au commissaire des
émoluments pour des missions spéciales ; il en informe la plus prochaine assemblée générale
ordinaire par le rapport de gestion, de l'objet de ces missions et de leur rémunération.
TITRE IV
ASSEMBLEES GENERALES
Article 24
L'assemblée générale a les pouvoirs qui sont déterminés par la loi et les présents statuts.
Article 25
L'assemblée générale se tient au siége ou a I'endroit indiqué dans les avis de convocation.
L'assemblée générale ordinaire se réunit le deuxiéme mardi du mois de juin a dix heures trente ou, si
ce jour est un jour férié légal ou s'il est chmé eu égard aux régles en usage dans la société, le
premier jour ouvrable suivant.
Article 26
Le conseil d'administration convoque les assemblées générales tant ordinaires qu'extraordinaires.
L'assemblée doit 6tre convoquée a la demande d'un ou plusieurs actionnaires justifiant qu'ils
possédent le dixiéme du capital.
Article 27
Les convocations contiennent I'ordre du jour et sont faites conformément aux dispositions légales en
la matiére.

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
   

Réservé
au
Moniteur

belge

Mentionner sur la derniére page du VoletB: —_Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

Mod PDF 19.01

Volet B_ - suite

Article 28

Le conseil d'administration décide des conditions et des modalités d'admission a l'assemblée
générale.

Article 29

Tout propriétaire d'actions peut se faire représenter a l'assemblée générale par un mandataire,
actionnaire ou non, qui sera porteur d'un pouvoir spécial qui pourra étre donné sous forme de simple
lettre ou de télécopie.

Le conseil d'administration peut arréter la formule de procuration et exiger que celle-ci soit déposée a
l'endroit indiqué par lui et dans le délai qu'l fixe.

Tout actionnaire est autorisé a voter par correspondance, au moyen d'un formulaire dans lequel
doivent étre mentionnés notamment : son nom, I'ordre du jour, le sens du vote ou de I'abstention sur
chacun des points de celui-ci, et le délai de validité du mandat. Ce formulaire sera diment signé, la
ou les signatures étant précédées de la mention "bon pour pouvoir". Il ne sera tenu compte que des
formulaires qui auront été recus par la société a son siége, trois jours francs avant la réunion de
l'assemblée générale.

Article 30

Pour étre admis a l'assemblée, tout actionnaire ou mandataire doit signer la liste de présence qui
pourra 6tre attachée au procés-verbal de ladite séance. Cette liste indique I'identité des actionnaires
et de leurs mandataires, ainsi que le nombre de titres pour lequel ils prennent part au vote.

Article 31

L'assemblée est présidée par le président du conseil d'administration ou, a son défaut, par un
administrateur désigné par ses collégues et, en cas de carence des administrateurs, par un
actionnaire ou son représentant, désigné par l'assemblée.

Le président désigne le secrétaire et, si le nombre d'actionnaires présents a l'assemblée le
nécessite, l'assemblée nomme un scrutateur, actionnaire ou non. Ils forment ensemble le bureau.
Article 32

Chaque action donne droit a une voix.

Article 33

Sauf les cas prévus par la loi, les décisions sont prises a la simple majorité des voix valablement
exprimées, quel que soit le nombre de titres représentés, sans tenir compte des abstentions.

Article 34

Lorsque l'assemblée générale est appelée a décider d'une modification aux statuts, elle ne peut
valablement délibérer que si ceux qui assistent a l'assemblée représentent la moitié au moins du
capital. Si cette derniére condition n'est pas remplie, une nouvelle convocation est nécessaire et la
nouvelle assemblée délibére valablement, quelle que soit la portion du capital représentée.

Aucune modification des statuts n'est admise, sauf exception légale, que si elle réunit les trois quarts
des voix valablement exprimées.

Lorsque la délibération est soumise par la loi a des conditions plus strictes, l'assemblée n'est
valablement constituée et ne peut statuer que dans les conditions de présence et de majorité
requises par le Code des sociétés et des associations.

Article 35

L'assemblée générale ne peut délibérer et prendre des décisions que sur les points figurant 4a son
ordre du jour.

L’assemblée générale ne peut statuer sur des points qui ne sont pas inscrits a l’ordre du jour que
dans I’hypothése ou tous les actionnaires sont présents ou représentés

Article 36

Quels que soient les points a I'ordre du jour, le conseil d'administration a le droit, aprés l'ouverture
des débats, de proroger, a trois semaines au plus, toute assemblée générale tant ordinaire
qu'extraordinaire.

Cette prorogation, notifiée avant la cloture de la séance et mentionnée au procés-verbal de celle-ci,
n’annule que les décisions prises concernant les comptes annuels, sauf si l'assemblée générale en
décide autrement. Elle ne peut avoir lieu qu'une fois.

Les actionnaires doivent étre convoqués a nouveau pour la date que fixera le conseil
d'administration, avec le méme ordre du jour.

Les formalités remplies pour assister a la premiére séance, en ce compris le dépdt des procurations,
resteront valables pour la seconde.

La seconde assemblée générale statue définitivement sur les points a I'ordre du jour.

Article 37

Les procés-verbaux des assemblées générales sont signés par les membres du bureau et par les
actionnaires qui le demandent et, sauf pour les procés-verbaux authentiques, sont consignés ou
reliés dans un recueil spécial tenu au siége, sous une forme quelconque, y compris électronique. Les
expéditions, copies ou extraits a délivrer aux tiers sont signés par deux administrateurs.

TITRE V

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
Réservé
au
Moniteur

belge

   

Bijlagen bij het Belgisch Staatsblad - 30/10/2023 - Annexes du Moniteur belge

Mod PDF 19.01

Volet B - suite

INVENTAIRES ET COMPTES ANNUELS — BENEFICE ET REPARTITION

Article 38

L'exercice social commence le premier janvier et se termine le trente et un décembre de chaque
année.

Le conseil d'administration dresse un inventaire et établit, conformément a la loi, les comptes
annuels, qui comprennent le bilan, le compte de résultats et l'annexe. II rédige en outre le rapport de
gestion.

Un mois au moins avant l'assemblée générale, I'inventaire, les comptes annuels et le rapport de
gestion sont remis avec les piéces au commissaire qui doit faire son rapport.

Article 39

Quinze jours avant l'assemblée générale ordinaire, les actionnaires peuvent prendre connaissance
au siége :

- des comptes annuels ;

- dela liste des fonds publics des actions, obligations et autres titres de sociétés qui composent le
portefeuille ;

- dela liste des actionnaires qui n'ont pas entiérement libéré leurs actions avec I'indication du
nombre de leurs actions et celle de leur domicile ;

- du rapport de gestion et du rapport du commissaire.

Les comptes annuels, le rapport de gestion et le rapport du commissaire sont adressés aux
actionnaires en nom, en méme temps que la convocation. Tout actionnaire a le droit d'obtenir
gratuitement un exemplaire de ces piéces, sur justification de son titre, quinze jours avant
l'assemblée.

Article 40

Apreés avoir pris connaissance du rapport de gestion et du rapport du commissaire, l'assemblée
générale délibére sur les comptes annuels.

Aprés leur adoption, elle se prononce par un vote spécial sur la décharge des administrateurs et du
commissaire.

Article 41

Les comptes annuels, le rapport de gestion, le rapport du commissaire ainsi que les autres
documents prévus par la loi font l'objet de mesures de publicité légale.

Article 42

L'excédent favorable du compte de résultats constitue le bénéfice net.

Sur ce bénéfice, il est prélevé :

a) cing pour cent au moins pour constituer la réserve légale, ce prélevement cessant d'étre
obligatoire lorsque la réserve atteint le dixiéme du capital.

b) les sommes que l’assemblée générale, sur proposition du conseil d’administration, déciderait a’
affecter soit a un report a nouveau, soit a la création ou a la dotation de réserves

Le solde éventuel est mis a la disposition de l’'assemblée qui, sur proposition du conseil d’
administration, en détermine I’affectation..

Article 43

Le paiement des dividendes se fait aux 6poques et aux endroits fixés par le conseil d'administration.
Le conseil d'administration pourra, sous sa propre responsabilité et dans le respect des dispositions
légales en la matiére, décider le paiement d'acomptes sur dividende et fixer la date de leur paiement.
TITRE VI

DISSOLUTION - LIQUIDATION

Article 44

En cas de dissolution de la société, l'assemblée générale régle le mode de liquidation et nomme un
ou plusieurs liquidateurs dont elle détermine les pouvoirs et les émoluments. Elle conserve le pouvoir
de modifier les statuts si les besoins de la liquidation le justifient.

La nomination des liquidateurs met fin aux pouvoirs des administrateurs et dans les cas prévus par la
loi, doit 6tre soumise au président du tribunal de I’entreprise pour confirmation.

Article 45

Les liquidateurs disposeront des pouvoirs les plus étendus conférés par les articles 2 :87et suivants
du Code des sociétés et des associations.

L'assemblée déterminera, le cas échéant, les émoluments des liquidateurs.

Chaque année, le(s) liquidateur(s) soumettra(ont) a l'assemblée générale les résultats de la
liquidation avec I'indication des causes qui ont empéché celle-ci d'étre terminée.

L'assemblée se réunira sur convocation et sous la présidence du liquidateur ou de I'un d'eux,
conformément aux dispositions des présents statuts.

Article 46

A moins que l'assemblée n‘ait réglé autrement le mode de liquidation a la majorité requise pour
modifier les statuts, le produit de la liquidation, aprés le paiement des dettes ou la consignation des
sommes nécessaires a cet effet, y compris les frais de liquidation, est réparti, en espéces ou en

Mentionner sur la derniére page du VoletB: —_Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
Réservé
au
Moniteur

belge

   

Bijlagen bij het Belgisch Staatsblad - 30/10/2023 - Annexes du Moniteur belge

Volet B - suite Mod PDF 19.01
titres, entre toutes les actions.
Si le produit net ne permet pas de rembourser toutes les actions, les liquidateurs remboursent par
priorité les actions libérées dans une proportion supérieure jusqu’a ce qu’elles soient sur un pied
d'égalité avec les actions libérées dans une moindre proportion ou procédent a des appels de fonds
complémentaires a charge de ces derniéres.

TITRE VII

DISPOSITIONS GENERALES

Article 47

Pour tous litiges entre la société, ses actionnaires, obligataires, administrateurs, commissaire et
liquidateurs relatifs aux affaires de la société et a l'exécution des présents statuts, compétence
exclusive est attribuée aux tribunaux du siége; toutefois, la société, si elle est demanderesse, sera
en droit de porter le différend devant tout autre tribunal compétent.

Article 48

Les actionnaires et obligataires domiciliés a I'étranger et n'ayant fait aucune élection de domicile en
Belgique diment notifiée a la société, sont censés avoir élu domicile au siége ou tous actes peuvent
valablement leur étre signifiés ou notifiés, la société n'ayant pas d'autre obligation que de les tenir a
la disposition du destinataire.

SIXIEME RESOLUTION.

L’assemblée confirme que l’adresse du siége est maintenu dans la Région de Bruxelles-Capitale, a
Bruxelles (1000 Bruxelles), boulevard Simon Bolivar 36.

SEPTIEME RESOLUTION.

L'assemblée décide de conférer tous pouvoirs, avec faculté de subdéléguer au conseil
d'administration pour l'exécution des résolutions qui précédent.

(...)

Pour extrait analytique conforme.

Déposé en méme temps : expédition et procuration, statuts coordonnés.

(signé) Damien HISETTE, notaire associé a Bruxelles

Mentionner sur la derniére page du VoletB: —_Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
