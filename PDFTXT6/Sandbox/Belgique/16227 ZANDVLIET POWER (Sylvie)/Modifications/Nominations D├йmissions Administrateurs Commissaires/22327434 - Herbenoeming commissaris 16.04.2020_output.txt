Mod PDF 19.01

In de bijlagen bij het Belgisch Staatsblad bekend te maken kopie
na neerlegging van de akte ter griffie

Voor-
behouden |_

Neergelegd

aan het *22327434*
Belgisch
Staatsblad

22-04-2022

Griffie

  

 

\ Ondernemingsnr : 0477543470
Naam
(voluit) : ZANDVLIET POWER

(verkort) :

Rechtsvorm : Naamloze vennootschap

Volledig adres v.d. zetel Scheldelaan 600
: 2040 Antwerpen

Onderwerp akte : DIVERSEN

Het blijkt uit het proces-verbaal van de gewone algemene vergadering van 16 april 2020 dat de
algemene vergadering de volgende beslissingen heeft genomen:

wl.

Mandaten

De opdracht als Commissaris van Deloitte Bedrijfsrevisoren neemt een afloop na de huidige gewone
algemene vergadering. Voorstel om Deloitte Bedrijfsrevisoren, vertegenwoordigd door D. Cleymans,
te benoemen voor een periode van drie jaar.

De algemene vergadering beslist met eenparigheid van stemmen het mandaat als Commissaris van
Deloitte Bedrijfsrevisoren, vertegenwoordigd door de heer D. Cleymans, te hernieuwen voor een
nieuwe periode van drie jaar. De opdracht zal dan verstrijken bij afloop van de gewone algemene
vergadering van 2023.

wl.

Voor eensluitend uittreksel

(getekend) Samuel WYNANT

Bijlagen bij het Belgisch Staatsblad - 26/04/2022 - Annexes du Moniteur belge

Op de laatste biz. van Luik B vermelden : Voorkant : Naam en hoedanigheid van de instrumenterende notaris, hetzij van de perso(o)n
bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen
Achterkant : Naam en handtekening (dit geldt niet voor akten van het type "Mededelingen").
