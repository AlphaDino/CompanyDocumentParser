 

 
   
  
   
 

Rés

Mor
be

&b
of
o
oO
3
g
3
3
g
:
:
oy
S
S
a
~~
S
ai
c
t
z
4
i]
8
nv
a
oO
G
=
wo
ma
_-
3
a
=
‘5
8
2
&
>
a

nc

 

Copie qui sera publiée aux annexes du Moniteur belge
aprés dépét de I'acte au greffe

    
 

*06066450*

   

Dénomination: SUEZ-TRACTEBEL
Forme juridique: Société anonyme

Siage Place du Tréne 1 4 1000 Bruxelles
N° d'entreprise 0403.203.264

_Objet de l'acte: FUSION PAR ABSORPTION - AUGMENTATION DE CAPITAL -
MODIFICATIONS DES STATUTS

Extrait du procés-verbal de 'assemblée générale extraordinaire des actionnaires dressé par les Notaires
Jean-Philippe Lagae et Jean-Luc Indekeu, tous deux Notaires résidant a Bruxelles, le 21 février 2006
1 Lassembiée a décide de fusionner la société anonyme "SUEZ-TRACTEBEL" et la société,

‘anonyme “BELEGGINGSMAATSCHAPPIJ VOOR NOORD-BELGIE”, en abrégé et ci-aprés:

dénommée "NOBEMA", dont le siége social est établi 4 1000 Bruxelles, Place du Tréne, 1, (Numéro:
d'entreprise 0419.240.730 (RPM Bruxelles)) par absorption de cette derniére et transfert 4 "SUEZ-.
TRACTEBEL" de I'intégralité du patrimoine actif et passif de "NOBEMA" par suite de sa dissolution
sans liquidation, sur base d'une situation comptable arrétée au trente septembre deux mille cing et
ce aux conditions et modalités reprises dans te projet de fusion Sur le plan du droit comptable, du:
droit des sociétés et du droit commercial, elle est réalisée a la date du 21 février 2006,
conformément a l'article 701 du code des sociétés

2 Suite a la fusion, !'assemblée a constaté et décidé l'augmentation du capital social a concurrence de
trois millons nonante-neuf mille neuf cent six euros soixante-huit cents (EUR 3.099.906,68) pour le
porter d'un milliard huit cent somante et un millions cent vingt-cing mille cent quatre-vingt-trois euros
(EUR 1.8614 125 183,00) & un milliard huit cent soixante-quatre millions deux cent vingt-cing mille
quatre-vingt-neuf euros soixante-huit cents (EUR 1 864.225 089,68) par la création de trente mille cent
septante-huit (30 178) actions

3. L'assembiée a décidé de modifier les statuts en remplacant le premier alinéa de l'article cing des
statuts par le texte suivant :

"Le capital souscrit de la société est fixé a un milliard hurt cent soixante-quatre millions deux cent vingt-
cing mille quatre-vingt-neuf euros soixante-huit cents (EUR 1 864.225.089,68) et représenté par cent
dix millions six cent trente-quatre mille sept cents (110.634 700) actions, sans mention de valeur
nominale {I est intégralement libéré.”

POUR EXTRAIT CONFORME

Signé: Jean-Philippe Lagae, Notaire

Déposées en méme temps’ une expédition, 2 procurations, une coordination des statuts

sur la derniére page du VoleLB* Aurecto Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso Nom et signature
