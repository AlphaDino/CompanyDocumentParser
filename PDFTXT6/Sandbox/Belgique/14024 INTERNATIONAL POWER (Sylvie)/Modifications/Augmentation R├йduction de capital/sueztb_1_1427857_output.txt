  
  

Moo 2.0.

WEGii=)) Copie qui sera publiée aux annexes du Moniteur belge

AC

*09037105*

   
  
 
     
 
   
  

 

 

 

Réservé
au
Moniteur
belge

 

N° d'entreprise : 403203264
anomination

fenentie); SUEZ-TRACTEBEL

Forme juridique : Société anonyme

 

Siége: 1000 Bruxeltes, Place du Tréne 1
: Objet de l'acte: CESSION DE BRANCHE D’ACTIVITE

D'aprés un acte recu par Maitre Damien HISETTE, notaire associé 4 Bruxelles, le 27 février 20039, il résulte!

1. La société anonyme SUEZ-TRACTEBEL, ayant san siége a 1000 Bruxelles, Place du Tréne, 1, inscrite!
' : au registre des personnes morales et auprés des services de la taxe sur la valeur ajoutée sous le numéro BE:
! ; 0403. 203.264.
‘ Dénommeée ci-aprés société cédante ou cédant. :
2. La société anonyme TRACTEBEL ENGINEERING, ayant son siége a Woluwe-Saint-Lambert (1200:
| ; Bruxelles), avenue Ariane, 7, inscrite au registre des personnes morales et auprés des services de la taxe sur lai
i : valeur ajoutée sous le numéro BE 0412.639,681.
i aah.
‘ Dénammée ci-aprés société cessionnaire ou cessionnaire.
' web
it OBJET DE LA CESSION :
: : L’objet de fa cession est Ja branche d’activité ingénierte de SUEZ-TRACTEBEL, comprenant sa division «,
: ‘ TRACTEBEL ENGINEERING » et des filiales d'ingénierie basées en Belgique et a I'étranger détenues,
! actuellement par SUE7-TRACTEBEL, aux seutes exceptions : (i) des engagements de pensions et de-
ii | prépensions (ou équivalents) existants envers les agents avec statut CPNAE et les agents avec statut assimilé:
! | a celui du secteur gaz/électricité qui ont pris leur retraite ou sont en prépension a la date de passation de Tacte.
it ‘ guthentique de cession de la branche d’activité, de l'ensemble des droits et obligations y retatifs, (ii) de la «
: | Parent Company Guarantee » octroyée par SUEZ-TRACTEBEL le 29 mars 2005 dans le cadre du projet «&
: | Reganosa » et (ii) de la concession immobiliére située 4 Kinshasa (Congo), avenue Kananga, Ngaliema!
! (quartier Binza Pigeon), 30b. :

 

Par extrait analytique conforme.
Déposé en méme temps : expédition, procuration et état actif-passif
(signé) Damien HISETTE, notaire associé a Bruxelles

Bijlagen bij het Belgisch Staatsblad - 12/03/2009 - Annexes du Moniteur belge

 

Mentionnar sur la demi ‘ere page du Voit

 

urecto : Norm et qualité du nO" aire instromentant ou de la persanne ou des personnes
ayant pouvoir de représenter la personne morale a l'égard des fiers
Au verso : Nam et signature

  

 
     
   
   
        
 

Voor-
behouden
aan het
Belgisch
Staatsblad

©
a
oO
2
.
>
®
#
c
°
=
a
3
wn
®
x
o
c
c
x
'
a
3
o
q
~~
3
9S
g
Aq
_
'
oa
o
2
o
L
o
o
£
wn
x=
Oo
D
>
o
a
~
o
=
2
Cc
©
a
&
oO

 

Mod 2.1

TTL i=} |} = In de bijlagen bij het Belgisch Staatsblad bekend te maken kopie

na neerlegging ter griffie van de akte

I oh 09 208

*09037106*

, Griffie
i ,

Ondernemingsnr: 403203264 :
Benaming !
(vow): SUEZ-TRACTEBEL :

Rechtsverm : Naamloze vennootschap

 

 

 

 

 

 

 

Zetel: 1000 Brussel, Troonptein 1
i Onderwerp akte : OVERDRACHT VAN BEDRIJFSTAK

Uit een akte verleden voor Meester Damien HISETTE, geassocieerd notaris te Brussel, ap 27 februari 2009,
blijkt het dat:

hos

1. De naamtoze vennootschap SUEZ-TRACTEBEL, met zetel te 1000 Brussel, Troonplein, t, Ingeschreven’
in het Rechtspersonenregister en bij de diensten van de belasting over de toegevoegde waarde onder nummer;
BE 0403.203.264.

web.

Hierna genoemd : "de overdragende vennootschap” of “de verkoper”.

2. De naamloze vennootschap TRACTEBEL ENGINEERING, met zetel te Sint-Lambrechts-Woluwe (1200,
russel), Arianelaan, 7, Ingeschreven in het Rechtspersonenregister en bij de diensten van de belasting over de.
lpegevoegde waarde onder nummer BE 0412.639.684 :

wobee :

Hierna genoaemd : "de overnemende vennoctschap” of “de koper’.

woh

VOORWERP VAN DE OVERDRACHT ‘

Het voorwerp van de averdracht is de bedrijfstak engineering van SUEZ-TRACTEBEL, waarvan haar’
activiteit ‘TRACTEBEL ENGINEERING” en dochterondememingen (engineeringactiviteiten} gevestigd in Belgié:
f in het buitenland -en die vandaag in het bezit zijn van SUEZ-TRACTBEL-, met uvitzondering van: (i) de
estaande verbintenissen inzake pensiaenen en brugpensioenen (of equivalent) jegens de werknemers met het.
ANPCB-statuut en de werknemers met een statuut gelijkgesteld aan dat van de gas- en elektriciteitssector die
met pensioen of brugpensioen zijn op de datum van het verlijden van de authentieke akte van de overdracht’
an de bedrijfstak, met alle rechten en verbintenissen in verband met de bedrijfstak, (ii) van de Parent Company:
Guarantee ap 29 maart 2005 verleend door SUEZ-TRACTEBEL in het kader van het « Reganosa » project en;
iii) van de onroerende vergunning gelegen te Kinshasa (Kongo), avenue Kananga, Ngaliema (quartier Binza’
Pigeon), 30b,

soho

Voor eensluidend analytisch uittreksel.

Samen neergelegd: expeditie, volmacht en staat activa en passiva.

{getekend) Damien HISETTE, geassocieerde notaris te Brussel.

 
   
 
  
   
   
   
   

 

 

 

: , nde : perso(o)n(en)
bevoegd de rechtspersoon len aanzien van derden te vertegenwoordigen
Verso : Naam en handtekening.
