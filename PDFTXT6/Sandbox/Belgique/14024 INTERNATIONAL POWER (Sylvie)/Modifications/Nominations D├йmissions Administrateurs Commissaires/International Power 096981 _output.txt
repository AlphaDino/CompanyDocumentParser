 
  
    
 
  

Mod Word 11.1

In de bijlagen bij het Belgisch Staatsblad bekend te maken 1 Kople
na neerlegging ter griffie van de akte

BRUSsE,

29 AVR. 2014

Griffie

1*

 

   

Ondernemingsnr: 0403203264
Benaming

qolut): INTERNATIONAL POWER

(verkort) :
Rechtsvorm ; Naamloze vennootschap

Zetel: Simon Bolivarlaan 34 - 1000 Brussel
(volledig adres)

Onderwerp akte : STATUTAIRE BENOEMINGEN

De gewone algemene vergadering heeft ter zitting van 22 april 2014:

: - de opdracht van bestuurder van de heer Dirk Beeuwsaert voor een termijn van drie jaar hemieuwd. Deze
: opdracht zal een einde nemen na afloop van de gewone algemene vergadering van 2017;

‘ - akte genomen van de ontslagen als bestuuder van de heren Philip De Cnudde, Geert Peeters en Guy
Richelle op datum van 24 maart 2014;

i - de heer Pierre Guiollot, wonende 2 rue Ramponneau te 78600 Mesnil Le Roi (Frankrijk), voorlopig tot
i: bestuurder benoemd door de Raad van Bestuur van 24 maar 2014, définitief benoemd om de opdracht van de
| heer Geert Peeters te voltooien. Deze opdracht zal een einde nemen na afloop van de gewone algemene
' vergadering van 2016;

: - de heer Jean-Paul Deffontaine, wonende Avenue Hebron 98 te 1950 Kraainem, voorlopig tot bestuurder
‘  benoemd door de Raad van Bestuur van 24 maar 2014, définitief benoemd om de opdracht van de heer Philip
! De Cnudde te voltooien. Deze opdracht zal een einde nemen na afloop van de gewone algemene vergadering
' van 2016;

i ~beslist de opdracht van bestuurder van de heer Guy Richelle niet te verlenen en dus het aantal
1 bestuurders tot 3 terug te brengen.

De optrachten van bestuurder zijn onbezoldigd,
De Raad van Bestuur heeft de heer Dirk Beeuwsaert in de functie van gedelegeerd bestuurder bevestigd.

J.P. Deffontaine D. Beeuwsaert
Bestuurder Gedelegeerd bestuurder

Bijlagen bij het Belgisch Staatsblad - 12/05/2014 - Annexes du Moniteur belge

Op de iaatete blz, van Luik.8 vermeiden : Recto : Naam en hoedanigheid van de instrumenterende notaris, hetzij van de perso(o)n(en) :
bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen
Verso : Naam en handtekening,
