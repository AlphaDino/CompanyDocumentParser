    
 

 

Cople qui sera publiée aux annexes du Moniteur belge
aprés dépat de l’acte au greffe

  

   
    

 

tH

| Dénommnation: SUEZ-TRACTEBEL
| Forme juridique. Société Anonyme
Siége: Place du Tréne1 - 1000 Bruxelles
N° d'entreprise . 0403203264
‘ Objet de Pacte : NOMINATIONS STATUTAIRES

   

 

Vt

*06093920*

 

L'assemblée générale ordinaire en sa séance du 25 avril 2006 a:

- réélu en qualité d’administrateur pour un nouveau terme de trois Mme Valérie Bernis, MM. Yves Boel,:
' Patrick Buffet, Vicomte Etienne Davignon, MM. Thierry de Rudder, Yves-Thibault de Sliguy, Gérald Frére, Jean’
‘ Gandois, Comte (Richard) Goblet d’Alvielia, MM. Jean-Pierre Hansen, Gérard Lamarche, Jocelyn Lefebvre,’
' Gérard Mestrallet, Emmanuel van tnnis et Karel Vinck. Ces mandats viendront @ échéance a issue de
. fassemblée générale ordinaire de 2009; .
: - décidé de ne pas pourvoir au remplacement du mandat d’administrateur du Comte Maurice Lippens. Le
‘nombre d'administrateurs est ainsi ramené a 16.

Y. de Gaulle J.P. Hansen
Administrateur Administrateur délégué

‘
'
:

 

o.
op
a
Vo
Oo
Z
=
g
3
5
n
x
5
1
Oo
S
S
a
=
Ss
2
RH
o
7
og
os
ss
2
oS
%
8
n
Ss
9
4
ab
at
o
a
3
a
=
oO
q
&
Ss
=
a

 

Mentionner sur la dermiére page du VoletB+ Au recta: Nom et qualité du notuire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter la personne morale a légard des tiers
Au verso Nom et signature
 
  
  

 

In de bijlagen bij het Belgisch Staatsblad bekend te maken kopie
na neerlegging ter griffle van de akte

wn

" Benaming: SUEZ-TRACTEBEL
: : Rechtsvorm : Naamloze vennootschap :
Zetel: Troonplein 1 - 1000 Brussel
Ondernemingsnr: 0403203264
» Voorwerp akte: STATUTAIRE BENOEMING

   
  
   

 

(Anca

*06093921*

   

@AUSSEL

Griffie

   

: De gewone algemene vergadering, ter zitting van 25 apn 2006, heeft :

: + Mevrouw Valérie Bernis, de heren Yves Boél, Patrick Buffet, Burggraaf Etienne Davignon, de heren Thierry.

: de Rudder, Yves-Thibault de Silguy, Gérald Frare, Jean Gandois, Graaf (Richard) Goblet d'Alvialla, de heren:
' Jean-Pierre Hansen, Gérard Lamarche, Jocelyn Lefébvre, Gérard Mestrallet, Emmanuel van Innis en Karel;

: Vinek, tot bestuuder herbenoemd voor een termijn van 3 jaar goed. Deze opdrachten zullen een einde nemen’

! na afloop van de gewone algemene vergadering van 2009,

i - beslist niet over te gaan tot de vervanging van de opdracht van bestuurder van Graaf Maurice Lippens.:

‘Het aantal bestuurders is dus naar 16 teruggebracht.

Y. de Gaulle J.P. Hansen
Bestuurder Gedelegeerd bestuurder

 

:
‘
'
:

Bylagen bij het Belgisch Staatsblad -09/06/2006- Annexes du Moniteur belge

;
‘

Op de laatste biz van > Luk B vermelden : Recto Naam e en Y hoedanigheiad van nde instrumenterende notaris, hetzj van nde perso(oyn(en)
bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen

Verso . Naam en handtekening

 
 
  
 

 

 

cnn

*06093922*

: ; Forme juridique :

" Qbijet de Pacts :

Copie qui sera publiée aux annexes du Moniteur belge
aprés dépét de I'acte au greffe

BRUXELLES

al

: SUEZ-TRACTEBEL

Société Anonyme

: Place du Tréne1 - 1000 Bruxelles
> 0403203264

DELEGATION DE POUVOIRS

  

38 ~05~ 2005

Greffe

  

 

Par sa résolution exprimée par écrit en date du 2 mai 2006, le consell d'administration a arrété les pouvoirs
de gestion Journatiére qui s'énonce dorénavant comme suit :

1) Gestion journaliére de la société

Sans préjudice de la représentation générale de la société en tous actes par deux administrateurs de la
société en vertu de l'article 13 des statuts, le Conseil d’administration délégue la représentation de la société an
ce qui concerne Ia gestion journaliére & MM =

Gérard MESTRALLET Président
Jean-Pierre HANSEN Administrateur délégué
Yves de GAULLE Administrateur
Emmanuel van INNIS Adminsstrateur
Gérard LAMARCHE Administrateur

agissant deux a deux,

2) Gestion journaliére des divisions
Par ailleurs, dans le cadre de la division a laquelle ils/elles ressortissent, ta représentation de la société en

 

ce qul concere la gestion journaliére de la division concernée est déléguée *

Pour ta division EG! :
i M, Dirk BEEUWSAERT, ainsi qu’'a
MM. Pierre CHAREYRE

 

Philip DE CNUDDE
Derrick GOSSELIN
Enc KENIS

Henri MEYERS
Jacques NIEUWLAND.

: Les personnes sous (il) agiront chacune d’elles conjointement avec M. Dirk BEEUWSAERT ou un
{' administrateur disposant de la représentation en matiére de gestion journaliére de ta société ; M.Dirk
i} BEEUWSAERT pourra également agir conjointement avec un desdits administrateurs.

at, pour la division Engineering . :

MM. Paul RORIVE {jusqu'au 15 avril 2006)

 

Biylagen bij het Belgisch Staatsblad -09/06/2006- Annexes du Moniteur belge

Georges CORNET (a partir du 1° avril 2006), ainsi qu'a
. Yanick BIGAUD

Bernard GILLIOT

Marc LEPIECE

Christian PIERLOT

André TERLINDEN

dean VAN VYVE.

: Les personnes sous (ii) agiront chacune d'elles conjomtement avec MM. Paul RORIVE (jusqu’au 15 avril,
' § 2006) ou Georges CORNET (a partir du 1° avril 2006) ou un administrateur disposant de la représentation en
‘| matiére de gestion journaliére de {a société ; MM. Paul RORIVE (jusqu'au 15 avril 2006) ou Georges CORNET
‘€a partir du 1™ avril 2006) pourra également agir conjointement avec un desdits admunisirateurs. :

Mentionner sur la demiére page dul Volet B Aur ecto * Nom et qualite du notaire instrumentant ou de la personne ou ‘des personnes

ayant pouvoir de représenter la personne morale 4 j'égard des hers
Au verso : Nom et signature
foniteur

belge

8
af
o
a)
3
=
2
S
3
3
wn
oO
*
5
1
Oo
S
S
a
—
Ss
°
=
a
9
g
xo
wat
2
oO
8
”
sg
4
&b
of
vo
a
~~
vo
a
>
Oo
g,
3
s
=
a

 

Volet B ~ Suite

C. Mandats fonctionnels permanents (extrait)

Matiéres juridiques

Les documents concernant la gestion des affaires juridiques adressés aux administrations, avocats, .
. notaires, officiers ministénels et autorités de marché et de contréle des marchés sont signés par MM. Guldo De '
_ Clercq, Pierre-Jean Histaire ou Patrick van der Beken agissant conjointement avec MM. Gérard Mestrallet ou .
! Jean-Pierre Hansen ou Yves de Gaulle ou Emmanuel van Innis ou Gérard Lamarche ou Dirk Beeuwsaert ou :
Paul Rorive (jusqu'au 15 avril 2006) ou Georges CORNET (a partir du 1° avril 2008).

Matidres fiscales
Les documents concemant la gestion des affaires fiscales, notamment les déclarations fiscales, a
fYexception des déclarations en matiére de TVA et de précompte mobiller sont signés par MM. Guido Vanhove '
ou Jean Schellekens agissant confointement avec MM. Gérard Mestrallet ou Jean-Pierre Hansen ou Yves de -
» Gaulle ou Emmanuel van Innis ou Gérard Lamarche ou Dirk Beeuwsaert ou Paul Rorwve (jusqu'au 15 avril
1 2006) ou Georges CORNET (a partir du 1° avril 2006).

Matiéres Immobiliéres

Les contrats de bail, d'usufruit, de leasing immobiller et analogues, tant en qualité de preneur que de
‘ donneur, ainsi que les acquisitions et aliénations immobilléres sont signés par MM. Alexis van den Abeele ou
‘ Pierre-Jean Hislaire, agissant conjointement avec MM. Gérard Mestrallet ou Jean-Pierre Hansen ou Yves |
‘de Gaulie ou Emmanuel van Innis ou Gérard Lamarche

Ressources humaines

Les contrats et las avenants aux contrats d'emploi, les actes de ticenciement du personnel, !'octroi de toute
rémunération, hors cadre contractuel ou statutaie, A verser sous quelque forme que ce soit et les autres '
documents engageant Ja société envers le personnel sont signés par MM. Marc Janssens de Varebeke ou Jean '
: Perpéte ou Paul-Emile Timmermans agissant conjointement avec MM. Gérard Mestrallet ou Jean-Pierre »
Hansen ou Yves de Gaulle ou Emmanuel van Innis ou Gérard Lamarche ou Jacques Nieuwland.

i La présente délégation annule et remplace tous pouvoirs attribués antérieurement.

 

! Y. de Gaulle J.P, Hansen
: Administrateur Administrateur délégué
Mentionner sur la dernlére p page du Volet B - auresto | Nom et qualité du notaire instrumentant ou de la personne cu des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso . Nom et signature
    
  
   

4
: Rares In de bijlagen bij het Belgisch Staatsblad bekend te maken kopie
iN na neerlegging ter griffle van de akte

“cn

*06093923*

 

yssel

 

30 OS Zine

Griffle

 

i Benaming: SUEZ-TRAGTEBEL
: i : Rechtsvorm : Naamloze vennootschap
: Zetel Troonplein 1 - 1000 Brussel
Ondernemingsnr > 0403203264
. Voorwerp akte : OVERDRACHT VAN BEVOEGDHEDEN ,

Bij schriftalijk besiuit dd. 2 mel 2006 heeft de raad van bestuur de machten tot het dageijks bestuur
. toegekend dat voartaan Is vasigesteld als volgt :

: 1) Dagelijks bestuur van de vennootschap

Ongeacht de algemene vertegenwoordiging van de vennootschap in alle handelingen door twee!
* bestuurders van de vennootschap krachtens artikel 13 van de statuten, delegeert de Raad van Bestuur de!
vertegenwoordiging van de vennootschap inzake het dagelijks bestuur aan de heren :

: _ Gérard MESTRALLET Voorzitter

! »  Jean-Plerre HANSEN Gedelegeerd bestuurder

: Yves de GAULLE Bestuurder !
: Emmanuel van INNIS Bestuurder

' Gérard LAMARCHE Bestuurder !
! die allen twee aan twee handelen. ‘

2) Dagelijks bestuur van de divisies
Bovendien wordt, In het kader van de divisie waaronder zi) tessorteren, de vertegenwoordiging van de
_ vennootschap inzake het dagelijks bestuur van de betrokken divisie gedelegeerd aan :

Voor de divisie SE) :
@) Dhr Dirk BEEUWSAERT, alsook aan
(ii) HH. Pierre CHAREYRE
Phillp DE CNUDDE
Derrick GOSSELIN
Erle KENIS
Henri MEYERS
Jacques NIEUWLAND
De personen sub (ii) zullen telkens gezamenlljk optreden met de heer Dirk BEEUWSAERT of met een!
, bestuurder die beschikt over de vertegenwoordiging inzake dagelijks bestuur van de vennootschap; de heer’
_ Dirk BEEUWSAERT zal eveneens gezamenliik mogen optreden met 6én van de voornoemde bestuurders.

en, voor de divisie Engineering :
0] HH. Paul RORIVE (tot 15 apni 2006)
Georges CORNET (met ingang op 1 april 2006), alsook aan
(ii) HH Yanick BIGAUD ‘
Bernard GILLIOT '
Mare LEPIECE
Christan PIERLOT
André TERLINDEN
Jean VAN VYVE
De personen sub (ji) zullen telkens gezameniijk optreden met de heren Paul RORIVE (tot 15 april 2006) of,
: Georges CORNET (met ingang op + april 2006), of met een bestuurder die over de vertegenwoordiging
: beschikt Inzake het dagelijks bestuur van de vennootschap; de heren Paul RORIVE (tot 15 aprit 2006) of
: * Georges CORNET (met ingang op 1 april 2006) zuilen eveneens gezameniijk mogen optreden met één van de‘
: voornoamde bestuurders. \

Bijlagen bij het Belgisch Staatsblad -09/06/2006- Annexes du Moniteur belge

Op de faatste biz van cuk @ vermelden . Recto “Naame en n hoedanigheid van de instrumenterende notans, 5, hetzy ve van de perso(oyn(en)
bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen

Mersg : Naam en nandtekening
 

 

, Voor: Luik B - Vervolg
behouden

Baigiceh 3) Permanente functionele mandaten (ulttrekset) i

Staatsblad

'
‘
:

Juridische aangelegenheden :
De documenten met betrekking tot het beheer van jundische aangelegenheden gericht aan de-
administraties, advocaten, notarissen, ministeriéle ambtenaren, marktautoriteiten en marktcontrole-instanties '
‘worden ondertekend door de heren Guido De Clercq, Pierre-Jean Hislaire of Patrick van der Beken ,
 gezamenllijk optredend met de heren Gérard Mestrallet of Jean-Pierre Hansen of Yves de Gaulle of Emmanuel '
‘van Innis of Gérard Lamarche of Dirk Beeuwsaert of Paul Rorive (tot 15 april 2006) of Georges Comet (met |
;Ingang op 1 april 2006)
'  Fiscale aangelegenheden
: De documenten met betrekking tot het beheer van de fiscale aangelegenheden, in het bijzonder de
‘ belastingaangiften, met ultzondering van de aangiften inzake BTW en roerende voorheffing, worden.
-ondertekend door de heren Guldo Vanhove of Jean Schellekens gezameniijk optredend met de heren Gérard
. Mestrallet of Jean-Plerre Hansen of Yves de Gaulle of Emmanuel van Innis of Gérard Lamarche of Dirk
‘ Beeuwsaert of Paul Rorive (tot 15 april 2006) of Georges Cornet (met:ingang op 1 april 2006).

Immobilién

De huurcontracten, contracten inzake vruchtgebrulk, leasing van onroerende goederen en gelykaardige
‘ contracten zowel als huurder als als verhuurder, alsook de verwervingen en vervreemdingen van onroerende -
goederen, worden ondertekend door de heren Alexis van den Abeele of Pierre-Jean Hislaire, gezamenliik
optradend met de heren Gérard Mestrallet of Jean-Pierre Hansen of Yves de Gaulle of Emmanuel van Innis of |
, Gérard Lamarche.
; , Human resources
: De tewerkstellingscontracten en hun aanhangsels, de ontslagdaden aan het personeel, de toekenning .
: van om het even welke bezoldiging, bulten het contractueel of statutair kader, ongeacht de vorm waarin het"
i moet worden uitgekeerd, en de overige documenten die de onderneming binden ten aanzien van het personeel .
: ‘worden ondertekend door de heren Marc Janssens de Varabeke of Jean Perpéte of Paul-Emile Timmermans :
‘ gezamenlijk optredend met de heren Gérard Mestrailet of Jaan-Pierre Hansen of Yves de Gaulle of Emmanuel '
: van Innis of Gérard Lamarche of Jacques Nieuwland

Deze delegatie vernietigt en vervangt alle voorheen toegekende bevoegdheden. '

¥. de Gaulle J.P. Hansen ;
Bestuurder Gedelegeerd bestuurder ‘

Bilagen bij het Belgisch Staatsblad -09/06/2006- Annexes du Moniteur belge

1
t

Op de laatste blz. van Luk B vermeiden Recto . Naam en hoedanigheid van de mstrumenterende notans, hetzy van de perso(o)n(en)
bevoegd de rechtsperscon ten aanzien van cerden te vertagenwoordigen

Verso — Naam en handtekening
