Mad 20

in de bijlagen bij het Belgisch Staats blad bekend te maken kopie
na neerlegging ter griffie van de akte

== Tnniin nee.

*07010417*

   
  
  
  

 

eg er

yee:

BES

     
  
  

09 -o1- 2007

Griffie

 

  

Ondernemingsnr.. 0403203264
Benaming :

wot). SUEZ-TRACTEBEL :

 

' Rechtsvorm Naamloze vennootschap :
Zetel Troonplein 1- 1000 Brussel ,
i Onderwerp akte : NEERLEGGING VAN NOTULEN

 

Bij toepassing van artikel 556 van het Wetboek van vennootschappen, neerlegging van de notulen van de
bijzondere algemene vergadering van 28 december 2006.

E. van Innis J.P. Hansen
Bestuurder Gedelegeerd bestuurder

v
mel
oO
O
3
3
8
:
1
™
Qo
OQ
a
S
=
ce
1
ZU
&
3
so
wo
8
ww
Gq
oO
2
acy
>)
faa)
3
ot
iat
2
3
2
=
mM

bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen
Verso * Naam en handtekening

 
Réserve

au
Moniteur
belge

z=
QD
wo
5
oO
=
3
3
8
:
t
he
2
Q
N
aang
=
<
é
—
'
So
x
a
:
wn
aq
9
QD
a
oO
mM
8
a
=
oO
3
=
a

   
    
   

 

 

:
‘
‘

Mod 2.0

siXA=C% ~Copie qui sera publiée aux annexes du Moniteur belge
aprés dépét de l’acte au greffe

‘AIP

*0701

 
 

awl
ov 8
03 prance

Greffe

 

N° d'entreprise 0403203264
Dénomination

enenter) SUEZ-TRACTEBEL

Forme jundique : Société Anonyme
Siége’ Place du Tréne 1 ~ 1000 Bruxelles
Objet. de Pacte: DEPOT D'UN PROCES-VERBAL

Afin de se conformer a farticle 556 du Code des sociétés, dépdt du procés-verbal de l'assembiée générale:

1+ spéciala du 28 décembre 2006 :

E. van Innis J.P Hansen
Administrateur Administrateur délégué

Mentionner sur la derniere page du Volet B Au_racto « Nom et quahté du potarre instrumentant ou ude la personne ou des personnes

ayant pouvolr de représenter la personne morale 4 l'égard des tiers
Awyerso Nom et signature

 
DEPOSE AU GREFFEDUTRBUNALDE COMER 9 a4 anny

DE BRU) XELLES,| 3 LE sstiecnasoteumssesbncnsneiecesiclsivevensenenats
: QBGREFFIER,

.
&

SUEZ -TRACTEBEL

Société Anonyme

  

Siége social : Place du Tréne 1, Bruxelles
Tribunal de Commerce de Bruxelles < oe
RPM Numéro d’entreprise 0403 203 264 %

PROCES-VERBAL DE L'ASSEMBLEE GENERALE SPECIALE DES
ACTIONNAIRES TENUE LE 28 DECEMBRE 2006

La séance est ouverte 4 10h30, sous la présidence de Monsieur Jean-Pierre Hansen,
administrateur délégué de la société.

é

1. CONSTITUTION DU BUREAU

Monsieur le président désigne comme secrétaire Monsieur Patrick van der Beken.

2. DEPOT DES PIECES
Dépét est fait sur le bureau :
- des procurations des mandataires des actionnaires représentés.

-».Ces documents-sont paraphés par. le -bureau.-.

3. CONSTITUTION DE L'ASSEMBLEE
Les actionnaires suivants sont ici représentés :

1. SUEZ, société anonyme de droit francais, ayant son siége social 16, rue de la Ville
l'Evéque a 75008 Paris (France), proprictaire de 110.603.522 actions, représentée par
Monsieur Jean-Pierre Hansen, en vertu d'une procuration sous seing privé, qui restera
annexée au présent procés-verbal.

2. SOPRANOR, société anonyme de droit francais, ayant son siége social 16, rue de la
Ville I'Evéque a 75008 Paris (France), propriétaire de 1.000 actions, représentée par
Monsieur Patrick van der Beken, en vertu d'une procuration sous seing privé qui
restera annexée au présent procés-verbal.

3. GENFINA, société anonyme de droit belge, ayant son siége social Place du Tréne 1 4
1000 Bruxelles, propriétaire de 30.178 actions, représentée par Monsieur Patrick van
der Beken, en vertu d'une procuration sous seing privé qui restera annexée au présent
procés-verbal.
””

2.

La liste des présences constatant que toutes les actions sont représentées, il n’y a pas lieu
de justifier des convocations.

L’assemblée se reconnait valablement constituée et apte a délibérer sur son ordre du jour.

4. DECLARATION DU PRESIDENT

Le président informe l’assemblée. qu’un emprunt de 2 GEUR a été accordé 4 SuEz-
TRACTEBEL par décision du Conseil d’administration d’7 ENERGY EUROPE INVEST, en date
du 18 mai 2006.

En application de l’article 556 du Code des sociétés qui stipule qu’il est du ressort
exclusif d’une assemblée générale de conférer 4 des tiers des droits donnant naissance a
un engagement a charge de la société lorsque l’exercice de ces droits dépend d’un
changement de contréle exercé sur elle, il est demandé a l’assemblée d’approuver |’article

@ 14 de la convention de prét réglant les cas d’exigibilité immédiate de |’emprunt et
particuli¢rement son point h) lequel stipule que :

« Outre les cas d'exigibilité anticipée résultant de l'application du droit commun, le
Préteur a le droit de rendre le présent Prét exigible, immédiatement et de plein droit, par
simple lettre recommandée, sans qu'il y ait lieu de remplir aucune formalité judiciaire,
en cas de:
h) changement de contréle :
> si l'Emprunteur n'était plus contrélé directement ou indirectement par
SUEZ.(ou le Groupe SUEZ); ... .
> si toute personne (ou personnes agissant de concert) autre que Gaz de
France (ou le groupe Gaz de France) venait a contréler directement ou
indirectement SUEZ (ou le Groupe SUEZ) » .

5. ORDRE DU JOUR
Le sujet suivant figure a l’ordre du jour :

— En application de l’article 556 du Code des sociétés, approbation du point h) de
Particle 14 de la convention de prét entre ENERGY EUROPE INVEST et SUEZ-
TRACTEBEL stipulant |’exigibilité immédiate de l’emprunt en cas de changement de
contréle.
v

6. APPROBATION DU POINT h) DE L’ARTICLE 14 DE LA CONVENTION DE
PRET ENTRE ENERGY EUROPE INVEST ET SUEZ-TRACTEBEL

Afin de se conformer a Varticle 556 du Code des sociétés, l’assemblée marque accord a

Punanimité des voix sur la disposition du point h) de l’article 14 stipulant l’exigibilité
immédiate de l’emprunt en cas de changement de contréle.

Le président prie le secrétaire de donner lecture du procés-verbal.
Aucune remarque n’étant formulée, le procés-verbal est approuve.

Le président léve la séance a 11h15.

Annexes: 3 procurations

(suit le texte néerlandais)
NEERGELEGD T CPAFFIE VAN DE RECHTBAN 9 -N- 9007
WAN KOOPHANCR: i: SRUSSEL, De .

SUEZ - TRACTEBEL

Naamloze Vennootschap

 

Zetel: Troonplein 1, B-1000 Brussel
Gerechtelijk arrondissement van Brussel
RPR Ondernemingsnummer 0403 203 264

NOTULEN VAN DE BIJZONDERE ALGEMENE VERGADERING VAN
AANDEELHOUDERS VAN 28 DECEMBER 2006

De vergadering wordt geopend om 10.30 uur, onder het voorzitterschap van de heer
Jean-Pierre Hansen, gedelegeerd bestuurder van de vennootschap.

1. SAMENSTELLING VAN HET BUREAU

De voorzitter duidt de heer Patrick van der Beken als secretaris aan.

2. NEERLEGGING VAN DE STUKKEN

Op het bureau wordt het volgende neergelegd:

de volmachten van de gevolmachtigden van de vertegenwoordigde aandeelhouders.

Deze documenten zijn door het bureau geparafeerd.

3. SAMENSTELLING VAN DE VERGADERING

De volgende aandeelhouders zijn hier vertegenwoordigd :

1.

SUEZ, naamloze vennootschap naar Frans recht, met maatschappelijke zetel te 75008
Parijs (Frankrijk), rue de la Ville l'Evéque 16, eigenares van 110.603.522 aandelen,
vertegenwoordigd door de heer Jean-Pierre Hansen, krachtens een onderhandse
volmacht die aan de voorliggende notulen gehecht zal blijven;

SOPRANOR, naamloze vennootschap naar Frans recht, met maatschappelijke zetel te
75008 Parijs (Frankrijk), rue de la Ville l'Evéque 16, eigenares van 1.000 aandelen,
vertegenwoordigd door de heer Patrick van der Beken, krachtens een onderhandse
volmacht die aan de voorliggende notulen gehecht zal blijven;

GENFINA, naamloze vennootschap, met maatschappelijke zetel te 1000 Brussel,
Troonplein 1, eigenares van 30.178 aandelen, vertegenwoordigd door de heer Patrick
van der Beken, krachtens een onderhandse volmacht die aan de voorliggende notulen

gehecht zal blijven.
De aanwezigheidslijst stelt vast dat alle aandelen vertegenwoordigd zijn.
Daar alle aandelen vertegenwoordigd zijn, is er geen reden de bijeenroepingen te
verantwoorden.

De vergadering erkent dat zij geldig samengesteld is en dat zij bevoegd is om te
beraadslagen over haar agenda.

VERKLARING VAN DE VOORZITTER

De Voorzitter stelt de vergadering ervan in kennis dat er op datum van 18 mei 2006, en
op beslissing van de Raad van bestuur van ENERGY EUROPE INVEST, een lening van
2 GEUR aan SUEZ-TRACTEBEL werd toegekend.

Bij toepassing van artikel 556 van het Wetboek van vennootschappen, dat bepaalt dat
enkel de algemene vergadering rechten aan derden kan toekennen die een verplichting te
haren laste doen ontstaan, wanneer de uitoefening van deze rechten afhankelijk is van een
verandering van de controle die op haar wordt uitgeoefend, wordt de algemene
vergadering verzocht om haar goedkeuring te verlenen aan artikel 14 van de
leningsovereenkomst waarin de gevallen van onmiddellijke opeisbaarheid van de lening
staan beschreven, en inzonderheid punt h) dat stipuleert dat:

«Naast de gevallen van vervroegde opeisbaarheid voortvloeiend uit de toepassing van
het gemeen recht, heeft de Lener het recht om deze Lening onmiddellijk en van

.. rechtswege -opeisbaar te maken middels een aangetekend schrijven, zonder dat hiervoor

enige gerechtelijke formaliteiten dienen te worden vervuld; in het geval van:

h) verandering van controle :
> indien de Ontlener niet langer rechtstreeks of onrechtstreeks zou worden
gecontroleerd door SUEZ (of de SUEZ Groep);
> indien enig andere persoon (of personen handelend in overleg) dan Gaz de
France (of de Gaz de France groep) rechtstreeks of onrechtstreeks controle
over SUEZ (of de SUEZ Groep) zou verwerven » .

5. AGENDA

Het volgende onderwerp is op de agenda geplaatst:

— Bij toepassing van artikel 556 van het Wetboek van vennootschappen, goedkeuring
van punt h) van artikel 14 van de leningsovereenkomst tussen ENERGY EUROPE
INVEST en SUEZ-TRACTEBEL tot bepaling van de onmiddellijke opeisbaarheid van de
lening bij een verandering van controle.
6.

6. GOEDKEURING VAN PUNT _h) VAN ARTIKEL 14. VAN ___ DE

LENINGSOVEREENKOMST_TUSSEN ENERGY EUROPE INVEST EN _SUEZ-
TRACTEBEL

Teneinde zich te schikken naar artikel 556 van het Wetboek van vennootschappen
verklaart de vergadering zich eenparig akkoord met punt h) van artikel 14 tot bepaling
van de onmiddellijke opeisbaarheid van de lening bij een verandering van controle.

De Voorzitter verzoekt de secretaris lezing te geven van de notulen.

Aangezien er geen opmerkingen zijn, worden de notulen goedgekeurd.

De Voorzitter heft de zitting op om 11.15 uur.

Bijlagen: 3 volmachten

Le Secrétaire / De Secretaris Le Président / De Voorzitter

Liste des présences / Aanwezigheidslijst

PIL.

SOPRANOR S.A. SUEZ S.A.
représentée par / vertegenwoordigd door représentée par / vertegenwoordigd door
Patrick van der Beken Jean-Pierre Hansen

 

représentée par / vertegenwoordigd door
Patrick van der Beken
