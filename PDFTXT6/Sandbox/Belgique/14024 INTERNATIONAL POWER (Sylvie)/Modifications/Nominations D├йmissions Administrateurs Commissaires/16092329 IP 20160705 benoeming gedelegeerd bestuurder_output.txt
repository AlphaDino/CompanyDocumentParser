 
 
  

Mod Word 11.4

In de bijlagen bij het Belgisch Staatsblad bekend te maken kopie
na heerlegging ter griffie van de akte

au greffe du trounelakiffiemmerce

eyo

  

. Ondernemingsnr: 0403.203.264 , ‘
Benaming :

wouit): International Power
(verkort) +

 

Rechtsvorm : Naamloze vennootschap

Zetel: Simon Bolivariaan 34 - 1000 Brussel
(voliedig adres) .

: Onderwerp akte : BENOEMING VAN DE GEDELEGEERD BESTUURDER - OVERDRACHT VAN :
BEVOEGDHEDEN :

De Raad van bestuur heeft ter zitting van 25 april 2016 Dhr. Sven DE SMET, als gedelegeerd bestuurder;
. van de vennootschap, ter vervanging van Dhr. Dirk BEEUWSAERT aangeduid, met ingang vanaf de datum van:
‘ deze zitting. \

De Raad van bestuur heeft de overdracht van bevoegdheden gewijzigd als volgt:

1. Dagelijks bestuur

Ongeacht de algemene vertegenwoordiging van de vennootschap in alle handelingen door twee
' bestuurders van de vennootschap krachtens artikel 13 van de statuten, delegeert de Raad van Bestuur de’.
vertegenwoardiging van de vennootschap inzake het dagelijks bestuur aan: ,

HH. Sven DE SMET Gedelegeerd bestuurder

Jean-Paul DEFFONTAINE

Jan STERCK :
Mw. Florence VERHAEGHE
Dhr. Emmanuel van INNIS,

die allen twee aan twee handelen.
2. Bijzondere mandaten

De Raad heeft het operationele beleid van de vennootschap (inclusief het dagelijkse beheer) toevertrouwd
, aan de Gedelegeerd bestuurder op een niet-exclusieve basis. :

De Raad delegeert aan de heren S. DE SMET, J.P, DEFFONTAINE en J. STERCK en Mevrouw. Florence:

: VERHAEGHE, die gezamenlijk handelen, de bevoegdheid om middels een algemeen reglement, bijzondere:

' permanente zogenaamde “functionele” mandaten toe te kennen die het operationele beheer van de:

onderneming (inclusief het dagelijkse beheer) mogelijk maken. Dit reglement verduidelijkt het doel en het:
financieel belang van de verbintenissen, die aldus worden toegekend.

woomemnnnrneenee ~~ Bijlagen bij het Belgisch Staatsblad - 05/07/2016 - Annexes du Moniteur belge

Deze delegatie vernietigt en vervangt alle eerder toegekende bevoegdheden.

Jean-Paul Deffantaine Jan Sterck
Bestuurder . Bestuurder

Op de laatste biz. van Luik B vermeiden : ‘Recto : Naam en hoedanigheid van de instrumenterende notaris, hetzij van de perso(o)n(en) :
bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen
Verso : Naam en handtekening.

 

 
