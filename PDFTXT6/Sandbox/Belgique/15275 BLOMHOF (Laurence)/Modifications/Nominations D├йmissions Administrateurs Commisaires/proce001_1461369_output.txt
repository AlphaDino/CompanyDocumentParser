BLOMHOF
Société Anonyme

Siége social : Place du Tréne 1 — 1000 Bruxelles
RPM numéro d’entreprise 0405.671.420

PROCES-VERBAL DE L’ASSEMBLEE GENERALE ORDINAIRE
DES ACTIONNAIRES DU 2 MAI 2013

Ouverture de la séance

La séance est ouverte 4 17h00, a Bruxelles sous la présidence de M. Guido Vanhove.

Composition du bureau
Le président désigne comme secrétaire M. Michael Delmée.

L’assemblée désigne comme scrutateurs M. Thierry van den Hove et Mme Suzanne
Fondu qui acceptent.

Dépét des piéces

Les piéces suivantes sont déposées sur le bureau :

- le rapport de gestion ainsi que le rapport du commissaire sur les comptes au
31 décembre 2012;

- le bilan, le compte de résultats ainsi que l'annexe arrétés au 31 décembre 2012;
- la procuration de l'actionnaire représenté.

Constitution de l'assemblée

Cette liste des présences constate que l'actionnaire unique est représenté. Il
représente les 45.000 actions représentatives du capital social.

L'assemblée se reconnait valablement constituée et apte a délibérer sur son ordre du
jour qui comporte :

1. Rapport de gestion et rapport du commissaire.
2. Comptes annuels au 31 décembre 2012.

3. Décharge a donner aux administrateurs et au commissaire.
4. Nomination statutaire.

113
Rapport de gestion et rapport du commissaire

Le Président rappelle que chaque actionnaire a pu prendre connaissance du rapport
de gestion présenté par le conseil d'administration et du rapport du commissaire. II
propose a l'assemblée de dispenser le bureau de {a lecture de ces rapports.

Cette proposition est acceptée a l'unanimité.

Comptes annueils

Le bilan, dont le total des rubriques a I'actif et au passif s'éléve a
9.511.290,52 EUR, le compte de résultats et l'annexe arrétés au 31 décembre 2012
sont soumis au vote de l'assemblée.

L’exercice se solde par un bénéfice de 196.454,67 EUR. Compte tenu du bénéfice
reporté de 1.346.260,34 EUR et de la dotation de 9.822,73 EUR 4 la réserve légale, le
bénéfice a affecter s’éléve 4 1.532.892,28 EUR qu'il est proposé a l’assemblée de
reporter.

Le bilan, le compte de résuitats et l'annexe, arrétés au 31 décembre 2012 ainsi que
l'affectation sont approuvés a l'unanimité des voix.

Décharge aux administrateurs et au commissaire

Le Président met aux voix la décharge a donner aux administrateurs pour leur gestion
et au commissaire pour sa mission de contrdle durant l'exercice social 2012.

L'assemblée approuve la décharge aux administrateurs a l'unanimité des voix.

L'assemblée approuve la décharge au commissaire a !'unanimité des voix.

Nomination statutaire

L’assembiée est informée de ce que le mandat de commissaire de la SC s.f.d. SCRL
DELOITTE Réviseurs d'entreprises, vient a @chéance a issue de la présente
assemblée.

L’assemblée approuve a l'unanimité des voix, le renouvellement, pour un terme de
trois ans, du mandat de commissaire de la SC sf.d. SCRL DELOITTE Réviseurs
d’entreprises, représentée par M. Laurent Boxus. Ce mandat viendra a échéance a
l'issue de l'assemblée générale ordinaire de 2016.

L’assemblée approuve également de fixer le montant des émoluments a 1.600,00 EUR
par an non indexés.

2/3
9. Signature du procés-verbal

Le Président invite le bureau et l’actionnaire s'il le souhaite a signer le présent procés-

verbal.
. v4
a f Se aus Vacs

Le secrétaire Le Président Les scrutateurs

La séance est levée a 18h00.

 

LISTE DES PRESENCES

   
 

DF SUEZ COMMUNICATION
Représenté par

3/3
