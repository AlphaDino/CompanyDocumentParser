Mod PDF 11.1

Copie a publier aux annexes du Moniteur belge
apres dépdt de l'acte au greffe

Réservé
au

Déposé

Moniteur _ *16323779*
belge

o
>
o
Q
.
D>
o
=
c
3
=
D>
3
wn
®
x<
©
c
Cc
<x
1
o
©
S
a
N
=
=
=
3
a
'
3
&
a
n
2
o
©
£
n
x=
o
2
>
o
a
~
o
x=
5
c
o
D
&
=

21-11-2016

Greffe

  

 

0666554504

N° d'entreprise :
Dénomination (erent): ~~ ENGIE Sun4Business

(en abrégé) : S4B

Forme juridique : Société anonyme

Siége : Boulevard Simon Bolivar 34
(adresse compléte) 1000 Bruxelles

Objet(s) de I'acte : Constitution

D’aprés un acte regu par Maitre Samuel WYNANT, notaire associé a Bruxelles, le 18 novembre
2016, il résulte que :

wel

1. Lasociété anonyme Electrabel, ayant son siége social a 1000 Bruxelles, Boulevard Simon
Bolivar 34, numéro d’entreprise 0403.170.701, ici représentée par Madame Frangoise SOTIAU,
domiciliée a Sart-Dames-Avelines, rue de Thyle, 2,

wel

2. La société anonyme Orka, ayant son siége social a 9810 Nazareth, Axeldreef 1B, numéro
d'entreprise 0811.943.745, ici représentée par Monsieur Jan Aster HEYSE, domicilié a Nazareth,
Axeldreef, 1B, et la SPRL ALFIN, numéro d’entreprise 0867.463.971 représentée elle-méme par son
représentant permanent ANCION Jean Michel, domicilié a Jozef Hertogslaan, 5, Wilrijk, nommés a
ces fonctions le 27 mai 2014.

Ci-aprés dénommeées : "les comparants".

wel

-* CONSTITUTION *-

1. Forme juridique — dénomination — siége
ll est constitué une société anonyme, qui sera dénommée ENGIE Sun4Business, en abrégé "S4B".
Le siége social est établi pour la premiére fois 4 1000 Bruxelles, Boulevard Simon Bolivar, 34.

2. Capital — actions — libération.
Le capital social est fixé a deux millions d’euros (2.000.000 EUR). Il est entierement souscrit et est
libéré a concurrence de vingt-cing pour-cent (25%).
ll est représenté par deux mille (2.000) actions, souscrites en espéces au prix de mille (1.000) euros
chacune, comme suit :

+ Electrabel déclare souscrire mille (1.000) actions qu'elle libére a concurrence de vingt-cing pour-
cent (25%), restant redevable de la libération du solde. Electrabel forme le groupe d’actionnaires A.

* Orka déclare souscrire mille (1.000) actions qu'elle libére a concurrence de vingt-cing pour-cent
(25%), restant redevable de la libération du solde. Orka forme le groupe d’actionnaires B.
Les comparants déclarent et reconnaissent que toutes et chacune de ces actions ont été souscrites
et libérées comme dit ci-dessus et qu'en conséquence, la société a, dés a présent, a sa disposition
une somme de cing cent mille (500.000) euros
wl.
La comparante sub 1 déclare sur I'honneur, par l'organe de son représentant, ne pas répondre a la
définition de PME donnée par l'article 2 de la loi-programme du dix février mil neuf cent nonante-huit.
Les comparants déclarent dés lors sur I'honneur que la société présentement constituée ne répond
elle-méme pas a la définition de PME donnée par cette loi.
-* STATUTS *-

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter l'association ou la fondation a I'égard des tiers
Au verso : Nom et signature.
Mod PDF 11.1

Réservé

   

Volet B_ - suite

au
Moniteur TITRE I. CARACTERE DE LA SOCIETE.
belge Article 1. Forme - Dénomination.

La société a la forme d'une société anonyme.
Elle porte la dénomination : "ENGIE Sun4Business", en abrégé "S4B".

Il peut étre fait usage isolément de la dénomination compléte ou abrégée.
Article 2. Siége.
Le siége social est établi a 1000 Bruxelles, Boulevard Simon Bolivar 34, arrondissement judiciaire de
Bruxelles.
ll peut étre transféré en tout autre lieu en Belgique par décision du conseil d'administration.
Tout transfert du siége social sera publié aux annexes au Moniteur belge par les soins du conseil
d'administration.
La société peut établir, par décision du conseil d'administration et dans le respect des dispositions
légales en matiére d'emploi des langues en Belgique, des siéges administratifs ou d'exploitation, des
fillales, des succursales et des agences en Belgique et a I'étranger.
Article 3. Objet.
La société a pour objet, en Belgique et a I'étranger, tant pour son compte propre que pour compte de
tiers ou en participation avec des tiers, de détenir des participations dans des sociétés belges ou
étrangéres, existantes ou a constituer, actives dans le développement, la construction, |’exploitation,
lentretien, le financement et le démantélement d’installations photovoltaiques ou d’autres
technologies renouvelables et aux nouvelles techniques qui y sont étroitement liées, sur des biens
immobiliers d’entreprises ou d’organismes publics.
Elle peut rassembler des fonds pour procéder a des investissements dans la production d’énergies
alternatives, durables et renouvelables et, dans ce cadre, procéder a l’achat, la vente, |’installation,
exploitation et la maintenance de moyens de production d’électricité issue d’énergies alternatives,
durables et renouvelables.

Elle peut accomplir toutes opérations civiles, industrielles ou commerciales, immobiliéres ou
mobiliéres se rapportant directement ou indirectement, en tout ou en partie, a l'une ou l'autre branche
de son objet ou qui seraient de nature a en développer ou a en faciliter la réalisation ; en particulier,
conclure des accords pour l’acquisition, l’établissement de droits réels ou la gestion de biens
immobiliers, voire leur location, ainsi que toute opération s’y rapportant directement ou indirectement.
Elle peut s'intéresser par toutes voies a toute société ou entreprise ayant un objet similaire ou
connexe au sien ou dont l'objet serait de nature a faciliter, méme indirectement, la réalisation du sien.
Elle peut de méme conclure toutes conventions de collaboration, de rationalisation, d'association ou
autres avec de telles sociétés ou entreprises.

Elle peut se porter caution et donner toute sireté personnelle ou réelle en faveur de toute personne
ou société, liée ou non. Elle peut développer, acheter, vendre, prendre en licence ou donner des
brevets, savoir-faire et autres actifs immatériels similaires. Elle peut exercer les fonctions
d'administrateur, gérant et liquidateur.
Article 4. Durée.
La durée de la société est illimitée.
TITRE Il. CAPITAL - TITRES.
Article 5. Capital social.
Le capital social est fixé a deux millions (2.000.000) d’euros.
ll est représenté par deux mille (2.000) actions sans désignation de valeur nominale, libéré a
concurrence de vingt-cing (25) pour-cent.
vole.
TITRE III.- ADMINISTRATION - CONTROLE.
Article 13. Conseil d'administration.
La société est administrée par un conseil d'administration composé de quatre (4) membres qui sont
nommeés pour six ans au plus par l’assemblée générale, sur base d’une liste de candidats proposée
par:

* Le groupe d’actionnaires A : un maximum de deux (2) administrateurs, ci-aprés les
« Administrateurs A » ;

* Le groupe d’actionnaires B : un maximum de deux (2) administrateurs, ci-aprés les
« Administrateurs B ».
Le conseil d’administration doit 6tre composé a tout moment d’au moins un (1) Administrateur A et
d’au moins un (1) Administrateur B.
La liste de candidats doit étre transmise aux actionnaires au plus tard a la convocation a l’'assemblée
générale de laquelle la nomination est un point a l’ordre du jour. Les actionnaires s’engagent a voter
en faveur des candidats qui sont proposés conformément a cet article.
Si une personne morale est nommée administrateur, elle est tenue de désigner un représentant
permanent parmi ses associés, gérants, administrateurs ou membres du personnel, qui exercera le

Bijlagen bij het Belgisch Staatsblad - 23/11/2016 - Annexes du Moniteur belge

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter l'association ou la fondation a I'égard des tiers
Au verso : Nom et signature.
   

Réservé
au
Moniteur

belge

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

Volet B - suite Mod PDF 11.1
mandat d'administrateur au nom et pour le compte de cette personne morale.

La durée du mandat des administrateurs ne peut excéder six ans. Si le nombre d’administrateurs
tombe, pour quelque raison que ce soit, en dessous du minimum prévu par les statuts, les
administrateurs sortant restent en fonction aussi longtemps que Il'assemblée générale — ou, en cas
d’application de l'article 14 ci-dessous, le conseil d’administration - ne pourvoit pas a leur
remplacement.
Les administrateurs peuvent a tout moment étre révoqués par l'assemblée générale statuant a la
majorité simple.
Chaque membre du conseil d’administration peut démissionner au moyen d'une notification écrite au
conseil d’administration.
Les administrateurs sortants sont rééligibles. Le mandat des administrateurs sortants non réélus
cesse immédiatement a l'issue de l'assemblée générale ordinaire.
Article 14. Vacance prématurée.
En cas de vacance au sein du conseil d'administration, les administrateurs restants ont le droit d'y
pourvoir provisoirement jusqu’a la nomination d’un nouvel administrateur par l’assemblée générale.
La nomination définitive est mise a l’ordre du jour de la prochaine assemblée générale. Tout
administrateur ainsi nommé par l'assemblée générale termine le mandat de l'administrateur qu'il
remplace.
Article 15. Présidence.
Le conseil d'administration élit, a la majorité simple, un président. Le conseil d’administration peut
également désigner un secrétaire du conseil d’administration, qui ne doit pas étre un administrateur
ni un actionnaire.
Article 16. Réunions du Conseil d’administration.
Le conseil d'administration se réunit sur convocation de son président. Le conseil d’administration est
également convoqué si deux administrateurs en font la demande.
Les convocations mentionnent le lieu, la date, l'heure et l’ordre du jour de la réunion. Elles sont
envoyées au moins deux jours francs avant la réunion par lettre, télécopie, courrier électronique ou
tout autre moyen écrit. Dans des cas exceptionnels, lorsque le délai de convocation mentionné ci-
dessus n’est pas approprié, le délai de convocation peut étre plus court. Si nécessaire, une
convocation peut étre effectuée par téléphone en complément des modes de convocation
mentionnés ci-dessus.

En cas d’empéchement du président, le conseil d’administration est présidé par le membre du
conseil d’administration le plus agé.

Si tous les administrateurs sont présents ou valablement représentés et marquent leur accord sur
ordre du jour, il ne doit pas étre justifié de la régularité de la convocation.

Les réunions du conseil d’administration peuvent étre valablement tenues par video-conférence ou
par conférence téléphonique. La réunion est dans ce cas considérée comme ayant été tenue au
siége social de la société pour autant qu’un administrateur au moins ait pris part physiquement a la
réunion depuis ce siége.
Article 17. Délibérations.

Le conseil d'administration ne peut délibérer valablement que si la majorité des administrateurs de
chaque groupe d’actionnaires est présente ou représentée. Si ce quorum n’est pas atteint, un
nouveau conseil sera convoqué avec le méme ordre du jour qui pourra valablement délibérer et
prendre des décisions sur l’ordre du jour. La convocation a cette seconde réunion sera envoyée au
moins deux jours francs avant la réunion.

Cette seconde réunion doit se tenir au plus t6t le septiéme jour et au plus tard le quatorziéme jour
aprés la premiére réunion et peut décider sur les points qui figuraient a l'ordre du jour. Dans des cas
exceptionnels, lorsque les délais de convocation mentionnés ci-dessus ne sont pas appropriés, les
délais de convocation peuvent étre plus courts.

Les décisions du conseil d’administration sont prises a la majorité simple des voix de tous les
administrateurs présents ou représentés sauf dans les cas ou la loi ou les statuts en disposent
autrement. Les votes blancs et irréguliers ne peuvent étre ajoutés aux voix émises. En cas de
partage des voix, la voix du président n’est pas prépondérante.

Le conseil d'administration ne peut valablement délibérer sur des points qui ne sont pas mentionnés
a l'ordre du jour qu’avec l’accord de l’ensemble du conseil d’administration et pour autant que tous
les administrateurs soient présents ou représentés.

Tout administrateur peut donner procuration a un autre administrateur par lettre, télécopie, courrier
électronique ou tout autre moyen écrit pour le représenter a une réunion du conseil d'administration.
Un administrateur peut détenir un nombre illimité de procurations, sans qu’il ne puisse représenter
tous les administrateurs.

Sans préjudice des régles légales relatives aux conflits d’intérét, si une décision qui est soumise pour
approbation au conseil d’administration peut soulever un conflit d’intéréts fonctionnel pour un
actionnaire (ou une personne liée a un actionnaire) avec la Société, les Administrateurs A ou les

 

ayant pouvoir de représenter l'association ou la fondation a I'égard des tiers
Au verso : Nom et signature.
   

Réservé
au
Moniteur

belge

Bijlagen bij het Belgisch Staatsblad - 23/11/2016 - Annexes du Moniteur belge

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

Mod PDF 11.1

Volet B - suite

Administrateurs B, en fonction de la catégorie pertinente de l’actionnaire concerné, le notifient au
conseil d’administration et aux autres administrateurs avant que le conseil d’administration délibére
sur ce point de l’ordre du jour.

Le procés-verbal fait mention des motifs du conflit d’intéréts de l’administrateur concerné.
L’administrateur concerné peut prendre part a la délibération et au vote.

Dans les cas ou la loi le permet, qui doivent demeurer exceptionnels et 6tre diment justifiés par
l'urgence et I'intérét social, les décisions du conseil d'administration peuvent étre prises par
consentement unanime des administrateurs exprimé par écrit.

Article 18. Procés-verbaux.

Les délibérations et les décisions du conseil d'administration, y compris celles adoptées au cours de
vidéo-conférences, de conférences téléphoniques ou par consentement écrit unanime, sont
constatées dans des procés-verbaux signés par au moins deux administrateurs. Ces procés-verbaux
sont consignés dans un registre spécial. Les procurations sont annexées aux procés-verbaux.

Les copies ou extraits des procés-verbaux, a produire en justice ou ailleurs, sont signés par le
président du conseil d'administration ou deux administrateurs. Ce pouvoir peut étre délégué a un
mandataire ou, le cas échéant, au secrétaire.

Article 19. Compétence du conseil d’administration Gestion journaliére.

Le conseil d'administration a le pouvoir d'accomplir tous les actes utiles ou nécessaires a la
réalisation de l'objet social, a l'exception des actes réservés expressément par la loi ou les statuts a
l'assemblée générale.

Le conseil d'administration peut déléguer a un mandataire, qui ne doit pas nécessairement étre
actionnaire ou administrateur, tout ou partie de ses pouvoirs pour des objets spéciaux ou déterminés.
Le conseil d'administration peut conférer la gestion journaliére des affaires de la société, ainsi que la
représentation de la société en ce qui concerne cette gestion, a une ou plusieurs personnes,
administrateurs :I’/les administrateur(s) délégué(s). Si la gestion journaliére est confiée a plusieurs
administrateurs, ceux-ci doivent étre nommeés sur proposition de chaque groupe d’actionnaires.
Article 20. Rémunération.

Le mandat d’administrateur n’est pas remunéré, sauf décision contraire de l’assemblée générale.
wel.

Article 22. Représentation.

La société est valablement représentée dans tous ses actes, y compris ceux ou intervient un
fonctionnaire public ou un officier ministériel, ainsi qu'en justice par deux administrateurs agissant
conjointement dont un Administrateur A et un Administrateur B, et n’ayant pas a justifier a l’égard des
tiers d’une décision préalable ou d’un mandat du conseil d’administration. La représentation en
justice vaut tant pour les actions en justice que pour les instances judiciaires, tant en tant que partie
demanderesse qu’en tant que partie défenderesse, en ce compris pour les procédures devant le
Conseil d’Etat et devant les juridictions administratives inférieures. Le cas échéant, une notification
ou un rapport au sujet de l’opération effectuée dans ce contexte est donné a posteriori au conseil
d’administration.

Lorsqu’une ou plusieurs personnes sont chargées de la gestion journaliére, la société est
représentée valablement dans toutes ses actions de gestion journaliére par une personne chargée
de la gestion journaliére, n’ayant pas a justifier a l'égard des tiers d’une décision préalable ou d’un
mandat du conseil d’administration.

La société est par ailleurs valablement représentée par tout mandataire spécial agissant dans les
limites de ses pouvoirs.

Article 23. Comité d’experts

Le conseil d’administration peut constituer un comité d’experts (dont les membres peuvent ou non
étre administrateurs) et en fixer sa composition et ses missions.

TITRE IV.- ASSEMBLEES GENERALES.

Article 24. Réunions.

ll est tenu chaque année, au siége social ou a tout autre lieu désigné dans la convocation, une
assemblée générale ordinaire le deuxiéme mardi du mois de juin, a dix heures.

Si ce jour est férié, l'assemblée se tient le premier jour ouvrable suivant, a la méme heure.

Article 25. Représentation et admission aux assemblées générales.

Tout actionnaire peut se faire représenter a l'assemblée générale par un mandataire spécial,
actionnaire ou non, qui sera porteur d'un pouvoir spécial, qui pourra étre donné sous forme de simple
lettre, telégramme ou télécopie et dont le conseil d'administration peut déterminer, le cas échéant, la
forme.

Article 26. Bureau.

L'assemblée générale est présidée par le président du conseil d'administration ou, a défaut, par un
autre administrateur.

Le président désigne le secrétaire qui peut ne pas étre actionnaire et l'assemblée peut choisir deux

ayant pouvoir de représenter l'association ou la fondation a I'égard des tiers
Au verso : Nom et signature.
Réservé
au
Moniteur

belge

   

Bijlagen bij het Belgisch Staatsblad - 23/11/2016 - Annexes du Moniteur belge

Volet B - suite Mod PDF 11.1
scrutateurs parmi les actionnaires présents.

Article 27. Prorogation.

Toute assemblée générale, ordinaire, spéciale ou extraordinaire peut, sur décision du conseil
d'administration, étre prorogée séance tenante a trois semaines.

La seconde assemblée délibérera sur le méme ordre du jour et statuera définitivement. Les
formalités d'admission et de représentation accomplies pour assister a la premiére assemblée
restent valables pour la seconde.

Article 28. Droit de vote.

Chaque action donne droit a une voix.

Article 29. Délibérations — Procés-verbaux.

L'assemblée générale ne peut délibérer que sur les objets portés a l'ordre du jour, alors méme quill
s'agirait de la révocation d'administrateurs ou de commissaires.

Sauf dans les cas prévus par la loi ou par les présents statuts, l'assemblée statue valablement quel
que soit le nombre de titres représentés et a la majorité des voix.

Les procés-verbaux des assemblées générales sont signés par les membres du bureau et les
actionnaires qui le demandent.

Les copies ou extraits de ces procés-verbaux sont signés, soit par le président du conseil
d'administration, soit par un administrateur-délégué, soit par deux administrateurs.

TITRE V. EXERCICE SOCIAL COMPTES ANNUELS REPARTITION.

Article 30. Ecritures sociales.

L'exercice social commence le premier janvier et se termine le trente et un décembre de chaque
année.

A cette derniére date, le conseil d'administration établit les comptes annuels, ainsi que, le cas
échéant, son rapport de gestion.

Article 31. Distribution.

Sur le solde bénéficiaire, il est prélevé cing pour cent au moins pour la formation du fonds de réserve
légale, ce prélevement cessant d'étre obligatoire lorsque la réserve atteint le dixieme du capital
social.

Le surplus est a la disposition de l'assemblée générale qui, sur proposition du conseil
d'administration, décidera chaque année de son affectation.

Article 32. Paiement des dividendes.

Le paiement des dividendes se fait annuellement aux époques et aux endroits indiqués par le conseil
d'administration.

Le conseil d'administration pourra, sous sa propre responsabilité et dans le respect des dispositions
légales en la matiére, décider le paiement d'acomptes sur dividende et fixer la date de leur paiement.
TITRE VI. DISSOLUTION LIQUIDATION.

Article 33. Dissolution.

La dissolution de la société peut étre prononcée a tout moment par l'assemblée générale des
actionnaires délibérant dans les formes requises pour les modifications aux statuts.

Article 34. Répartition.

Apres le paiement de toutes les dettes et charges de la société et le remboursement du capital social
réellement libéré, le solde sera réparti par parts égales entre toutes les actions.

wel.

-* DISPOSITIONS FINALES *-

1. Nominations des premiers administrateurs.
Le nombre d'administrateurs est fixé initialement a quatre (4).
Sont nommés en qualité d’administrateurs, pour une durée de six (6) ans :
Sur proposition du groupe d’actionnaires A :
+ Monsieur Frangois Thoumsin, domicilié a 4607 Dalhem, Voie du Thier (FEN) 23.
+ Madame Francoise Sotiau, domiciliée a 1495 Sart-Dames-Avelines, Rue de Thyle 2, qui est
présente et qui accepte ce mandat.
Sur proposition du groupe d’actionnaires B :
* la société anonyme Orka NV, prénommée, représentée dans cette fonction par son représentant
permanent Jan Heyse, domicilié a 9810 Nazareth, Axeldreef 1B ; et
+ la société privée a responsabilité limitée Alfin BVBA, ayant son siége a 2610 Wilrijk, Jozef
Hertogslaan 5, numéro d’entreprise 0867.463.971, représentée dans cette fonction par son
représentant permanent Jean-Michel Ancion, domicilié 4 2610 Wilrijk, Jozef Hertogslaan 5, qui sont
représentées ici et acceptent ces mandats
Sauf réélection, ces mandats viendront a échéance a l’issue de l'assemblée générale ordinaire de
2023.
La nomination n'aura d'effet qu'au jour de l'acquisition par la société de la personnalité morale.
Les fonctions d'administrateurs ne sont pas rémunérées.

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter l'association ou la fondation a I'égard des tiers
Au verso : Nom et signature.
   

Réservé
au
Moniteur

belge

Volet B - suite Mod PDF 11.1
Par dérogation aux dispositions de l'article 14 des statuts, les administrateurs ci-dessus nommés
pourront, en vue de la premiére réunion du conseil d'administration, se faire représenter par une
seule et méme personne, administrateur ou non.

2. Président du conseil d'administration — Administrateur-délégué.
Les personnes désignées ci-avant administrateurs, présentes ou représentées comme il a été
exposé, déclarent prendre a l'unanimité les décisions suivantes :
- est appelé aux fonctions de président du conseil d'administration, pour la durée de son
mandat d'administrateur : Frangois Thoumsin, prenommeé ;
- est appelé aux fonctions d'administrateur-délégué, pour la durée de son mandat
d'administrateur : Orka N.V., prénommeé, représentée par Jan Heyse, prénommeée, laquelle exercera
tous les pouvoirs de gestion journaliére de la société et de représentation de la société en ce qui
concerne cette gestion, avec faculté de subdéléguer.
Ces fonctions ne sont pas rémunérées.
Les nominations n'auront d'effet qu'au jour de l'acquisition par la société de la personnalité morale.

3. Premier exercice social.
Le premier exercice social commencera le jour de l'acquisition par la société de la personnalité
morale et finira le trente et un décembre 2017.
La premiére assemblée générale ordinaire aura donc lieu en 2018.

4. Début des activités.
Le début des activités de la société est fixé a son immatriculation a la Banque Carrefour des
Entreprises.
vole
F. Mandat

Est donné a Monsieur Thierry QUINET pour l’accomplissement de toutes les formalités liées

a la constitution de la société.
wel
Pour extrait analytique conforme.
Déposé en méme temps : expédition et procuration
(signé) Damien HISETTE, notaire associé a Bruxelles.

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter l'association ou la fondation a I'égard des tiers
Au verso : Nom et signature.
