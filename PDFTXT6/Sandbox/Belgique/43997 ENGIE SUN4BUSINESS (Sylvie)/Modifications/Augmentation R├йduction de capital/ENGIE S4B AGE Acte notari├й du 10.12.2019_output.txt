ACG/71197-004 ACTE DU 10/12/2019

VA N H A LT E R E N Société Répertoire n° 39954

NOTAIRES - NOTARISSEN

E:BxlV A:2

 

ENGIE Sun4Business
En abrégé S4B
Société anonyme,
A 1000 Bruxelles, Boulevard Simon Bolivar, 34
Arrondissement judiciaire de Bruxelles
Numéro d’entreprise 0666.554.504
*AK

Constituée suivant acte du notaire Samuel Wynant, notaire associé a Bruxelles,
du 18 novembre 2016, publié aux annexes au Moniteur belge sous le numéro
2016-11-23 / 0323779.

Dont les statuts ont été modifiés suivant acte du notaire Damien Hisette, notaire
associé a Bruxelles, du 8 mai 2018, publié aux annexes au Moniteur belge sous le
numéro 2018-05-31 / 0084431.

 

AUGMENTATION DU CAPITAL SOCIAL
MODIFICATIONS AUX STATUTS

 

L'AN DEUX MILLE DIX-NEUF

Le dix décembre,

Devant Samuel WYNANT, notaire a Bruxelles (quatrieme canton), associé de «
Van Halteren, Notaires Associés », a 1000 Bruxelles, rue de Ligne 13.

Au siége social.

S'est réunie l'assemblée générale extraordinaire des actionnaires de la société
anonyme ENGIE Sun4Business, dont le siége social est établi a 1000 Bruxelles,
Boulevard Simon Bolivar, 34.

Les membres du bureau de l'assemblée ont requis le notaire soussigné de
prendre acte des déclarations et constatations suivantes.

-* Bureau *-
La séance est ouverte a 09.30 heures sous la présidence de Monsieur Thiery
QUINET

Le Président désigne comme secrétaire lui-méme.

 

-* Exposé du président *-
Van Halteren | Le Président expose que:
Notaires |. Composition de l'assemblée.

Associés Tous les actionnaires de la société sont présents ou représentés, lesquels
déclarent étre propriétaires du nombre d'actions suivant :
1. La société anonyme Electrabel, ayant son
siege social a 1000 Bruxelles, Boulevard
Simon Bolivar 34, numéro d’entreprise 2.700

 

 

PREMIER ROLE
 

 

0403.170.701, propriétaire de deux mille

sept cents actions

ici représenté par Monsieur Thierry QUINET,

en vertu d’une procuration sous seing privé

qui restera ci-annexée.
2. la société privée a responsabilité limitée

Alfin, ayant son siége social a 2610 Wilrijk,

Jozef Hertogslaan 6, numéro d’entreprise

0867.463.971, propriétaire de deux mille

sept cents actions 2.700

ici représenté par Monsieur Thierry QUINET

prénommé, en vertu d’une procuration sous

seing privé qui restera ci-annexée.

Ensemble, cing mille quatre cents actions

représentant l'intégralité du capital social. 5.400
Il. Ordre du jour.
La présente assemblée a pour ordre du jour :
1. Augmentation de capital a concurrence de 2.600.000 EUR pour le porter
de 5.400.000 EUR a 8.000.000 EUR par I’émission de 2.600 nouvelles actions
ordinaires, identiques a celles existantes et jouissant des mémes droits et
avantages a partir de la répartition afférente a l'exercice social commencé le 1°
janvier 2019 et a compter de leur émission.
Ces actions nouvelles seront souscrites pour le prix global de 2.600.000 EUR et
libérées en espéces lors de la souscription a concurrence d’1.000.000 EUR.
- Souscription - Libération.
- Constatation de la réalisation effective de l’augmentation de capital.

2. Modification de l'article 5 des statuts a l'effet de le mettre en
concordance avec la situation nouvelle du capital social.
3. Attribution de pouvoirs en vue de l'exécution des décisions prises.

II. Convocations.

ll résulte de la liste de présence susvisée que tous les actionnaires sont présents
ou représentés et que, par conséquent, il n'y a pas lieu de justifier d'un avis de
convocation.

Les administrateurs ont été convoqués conformément aux dispositions légales
ou ont renoncé aux formalités de convocation.

IV. Quorum.

Pour pouvoir délibérer valablement sur les points a l'ordre du jour, l'assemblée
doit réunir au moins la moitié du capital.

V. Droit de vote - Majorité.

Chaque action donne droit a une voix et pour étre valablement prises, les
résolutions entrainant une modification aux statuts doivent réunir une majorité
de trois/quarts des voix.

-* Validité de l'assemblée *-
Ces faits vérifiés et reconnus exacts par l'assemblée, celle-ci constate qu'elle est
valablement constituée et apte a délibérer.

-* Résolutions *-

2

 
Ensuite, le Président soumet a l'adoption de l'assemblée les résolutions suivantes

PREMIERE RESOLUTION.

L’assemblée décide d’augmenter le capital social a concurrence de 2.600.000
EUR pour le porter de 5.400.000 EUR a 8.000.000 EUR par I’émission de 2.600
nouvelles actions ordinaires, identiques a celles existantes et jouissant des
mémes droits et avantages a partir de la répartition afférente a l'exercice social
commencé le 1* janvier 2019 et a compter de leur émission.

Ces actions nouvelles seront souscrites pour le prix global de 2.600.000 EUR et
libérées en espéces lors de la souscription a concurrence de 1.000.000 EUR.
Délibération.

Cette résolution est adoptée par l'assemblée a l'unanimité des voix.
SOUSCRIPTION - LIBERATION.

Ensuite la société anonyme Electrabel et la société privée a responsabilité limitée
Alfin, préqualifiées et représentées comme il a été exposé, aprés avoir entendu
lecture de tout ce qui précéde, déclare par l'organe de leur représentant :

a) avoir une parfaite connaissance des statuts de la société et en connaitre
parfaitement la situation financiére;

b) et souscrire les 2.600 actions nouvelles dont la création vient d'étre décidée
comme suit :

- la anonyme Electrabel : 1.300 nouvelles actions;

- la société privée a responsabilité limitée Alfin : 1.300 nouvelles actions.

Ensuite les souscriptrices liberent leur souscription par un versement en espéces,
de sorte que la somme d’un million d’euros (1.000.000 EUR) se trouve, dés a
présent, a la libre disposition de la société a un compte spécial ouvert a cet effet
au nom de la société auprés de Banque Tiodos.

Une attestation de ce dépdt restera ci-annexée.

CONSTATATION DE LA REALISATION EFFECTIVE DE L’AUGMENTATION DE CAPITAL.
L'iassemblée constate et requiert le notaire instrumentant d'acter que
augmentation de capital est effectivement réalisée et que le capital social est
ainsi porté a 8.000.000 EUR, représenté par 8.000 actions, sans désignation de
valeur nominale, entiérement libérées.

DEUXIEME RESOLUTION.

Comme conséquence de la résolution qui précéde, l'assemblée décide de
remplacer l’article 5 des statuts :

Le capital social est fixé a huit millions d’ euros (8.000.000 EUR).

Il est représenté par huit mille (8.000) actions sans désignation de valeur
nominale.

Délibération.

Cette résolution est adoptée par l'assemblée a l'unanimité des voix.

TROISIEME RESOLUTION.

L'assemblée décide de conférer tous pouvoirs, avec faculté de subdéléguer :

- auconseil d'administration pour l'exécution des résolutions qui précédent;

- a Mesdames Stéphanie Ernaelsteen et Madame Anne-Catherine Guiot,
agissant séparément, pour I'établissement du texte coordonné des statuts.

 

 

 

 

DEUXIEME ROLE
 

 

Délibération.
Cette résolution est adoptée par l'assemblée a l'unanimité des voix.

-* Cl6ture *-
La séance est levée a 09.33 heures.

-* Droit d’écriture *-
Le droit d’écriture (Code des droits et taxes divers) s’éleve a nonante-cing euros
(95 EUR) et est payé sur déclaration par le notaire soussigné.
DONT PROCES-VERBAL.
Dressé aux date et lieu indiqués ci-dessus.
Aprés lecture intégrale et commentée, les membres du bureau et les membres
de l'assemblée qui en ont exprimé le désir ont signé avec le notaire.
(Suivent les signatures).

 
es Ot Ke OL Voce 33354

Procuration
A.G.E. de la société ENGIE Sun4Business

 

LA SOUSSIGNEE

La société anonyme Electrabel, ayant son siége social 4 1000 Bruxelles, Boulevard Simon
Bolivar 34, numéro d’entreprise 0403.170.701,

Propriétaire de 2.700 actions de la société anonyme ENGIE Sun4Business, dont le siége
social est établi 4 1000 Bruxelles, Boulevard Simon Bolivar, 34, déclare, par les présentes,
constituer pour son mandataire spécial :
( identité du mandataire )

i

Erwin Boogaerts ou Frangoise Sotiau ou Thierry Quinet

Aux fins de la représenter a l'assemblée générale extraordinaire des actionnaires de la
société qui se tiendra avec l'ordre du jour suivant.

ORDRE DU JOUR

1. Augmentation de capital a concurrence de 2.600.000 EUR pour le porter de
i 5.400.000 EUR a 8.000.000 EUR par l’émission de 2.600 nouvelles actions

ordinaires, identiques a celles existantes et jouissant des mémes droits et avantages
a partir de la répartition afférente a l'exercice social commencé le 1° janvier 2019 et a
compter de leur émission.
Ces actions nouvelles seront souscrites pour le prix global de 2.600.000 EUR et
libérées en espéces lors de la souscription a concurrence d’1.000.000 EUR.
- Souscription - Libération.
- Constatation de la réalisation effective de l’augmentation de capital.

2. Modification de l'article 5 des statuts a l'effet de le mettre en concordance avec la
situation nouvelle du capital social.

3. Attribution de pouvoirs en vue de l'exécution des décisions prises

Spécialement et en son nom :
- souscrire 1.300 nouvelles actions et les libérer en espéces a concurrence de 500.000
EUR lors de la souscription.

Prendre part a toutes délibérations, émettre tous votes ou s'abstenir, faire toutes déclarations
en toutes matiéres, accepter ou proposer tous amendements a l'ordre du jour, signer tous
actes, procés-verbaux, registres, listes de présence et documents, substituer et en général,
faire tout ce qui sera utile et nécessaire a l'exécution du présent mandat, promettant
ratification au besoin.

Fait a Bruxelles, le 29 novembre 2019

Ch}

Patrick van der Beken Philippe Van Troeye
Administrateur Administrateur-délégué

 

TROISIEME ROLE
Ditend: oh Gob ie &. Dade. 33954 |

Procuration
A.G.E. de la société ENGIE Sun4Business

 

LA SOUSSIGNEE

La société privée a responsabilité limitée Alfin, ayant son siége social 4 2610 Wilrijk, Jozef
Hertogslaan 5, numéro d'entreprise 0867.463.971,

Propriétaire de 2.700 actions de la société anonyme ENGIE Sun4Business, dont le siége
social est établi a 1000 Bruxelles, Boulevard Simon Bolivar, 34, déclare, par les présentes,
constituer pour son mandataire spécial :

(identité du mandataire )

Erwin Boogaerts ou Jean-Michel Ancion ou Thierry Quinet

Aux fins de la représenter a l'assemblée générale extraordinaire des actionnaires de la
société qui se tiendra avec l'ordre du jour suivant.

ORDRE DU JOUR

1. Augmentation de capital a concurrence de 2.600.000 EUR pour le porter de
5.400.000 EUR a 8.000.000 EUR par l’émission de 2.600 nouvelles actions
ordinaires, identiques a celles existantes et jouissant des mémes droits et avantages
a partir de la répartition afférente a l'exercice social commencé le 1° janvier 2019 et a
compter de leur émission.

Ces actions nouvelles seront souscrites pour le prix global de 2.600.000 EUR et
libérées en espéces lors de la souscription a concurrence d’1.000.000 EUR.

- Souscription - Libération.

- Constatation de la réalisation effective de l’augmentation de capital.

2. Modification de l'article 5 des statuts a l'effet de le mettre en concordance avec la
situation nouvelle du capital social.

3. Attribution de pouvoirs en vue de l'exécution des décisions prises

Spécialement et en son nom :
- souscrire 1.300 nouvelles actions et les libérer en espéces a concurrence de 500.000
EUR lors de la souscription.

Prendre part a toutes délibérations, émettre tous votes ou s'abstenir, faire toutes déclarations
en toutes matiéres, accepter ou proposer tous amendements a l'ordre du jour, signer tous
actes, procés-verbaux, registres, listes de présence et documents, substituer et en général,
faire tout ce qui sera utile et nécessaire a l'exécution du présent mandat, promettant
ratification au besoin.

Fait a Bruxelles, le 4 décembre 2019

ae

—_ “Jean-Michel Ancion
° Administrateur-délégué
POUR EXPEDITION CONFORME,

QUATRIEME ET DERNIER ROLE
Pour l'acte avec n° de répertoire 39954, passé le 10 décembre 2019
FORMALITES DE L'ENREGISTREMENT

Enregistré quatre rédles, renvois,

au Bureau Sécurité Juridique Bruxelles 5 le 27 décembre 2019
Réference ACP (5) Volume 0 Folio 0 Case 25683.

Droits percgus: cinquante euros (€ 50,00).

Le receveur.

PREMIERE ANNEXE

Enregistré deux réles, renvois,

au Bureau Sécurité Juridique Bruxelles 5 le 27 décembre 2019
Réference ASSP (6) Volume 0 Folio 100 Case 7865.

Droits percgus: cent euros (€ 100,00).

Le receveur.

DEUXIEME ANNEXE

Enregistré deux réles, renvois,

au Bureau Sécurité Juridique Bruxelles 5 le 27 décembre 2019
Réference ASSP (6) Volume 0 Folio 100 Case 7865.

Droits percus: (voir premiére annexe).

Le receveur.
