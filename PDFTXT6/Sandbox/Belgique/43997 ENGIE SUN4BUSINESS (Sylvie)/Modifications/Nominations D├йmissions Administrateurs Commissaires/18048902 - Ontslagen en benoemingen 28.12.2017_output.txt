Mod 2.1

\ . In de bijlagen bij het Belgisch Staatsblad bekend te maken kopie
to : . - na neerlegging ter griffie van de akte

OB WAT 2018
ah we Hed@itgatatts
van Keophandel Bisse!

    

 

: Ondernemingsnr: 0666554504 — “raohbANK

Benaming
(voluit): Engie Sun4Business

Rechitsvorm: NV
Zetel: Boulevard Simon Bolivar 34, 1000 Brussel
: Onderwerp akte : Ontslag + benoeming bestuurder

 

De algemene vergadering neemt nota van het ontslag van Orka nv met als maatschappelijke zetel te 9810
Nazareth, Axeldreef 1b, ingeschreven in de Kruispuntbank der onderemingen onder het nummer
BE0811.943.745, vast vertegenwoordigd door Jan Heyse, als bestuurder "Administrateur B", met ingang van
28/12/2017.

Bij algemene vergadering van 28/12/2017 wordt Jean-Michel Ancion, gedomicilieerd te 2610 Wilrijk
(Antwerpen), Jozef Hertogslaan 5, met rijksregisternummer 60.07.09-349.11, benoemd als bestuurder
"Administrateur B", met ingang van 28/12/2017 voor een periode van 6 jaar.

Mevr, Frangoise Sotiau
Administrateur A

~~ Bijlagen bij het Belgisch Staatsblad - 20/03/2018 - Annexes du Moniteur belge

Op de laatste blz. van Luik B vermélden : Recto : Naam en hoedanigheid van de instrumenterende notaris, hetzij van de perso(o)n(en)
bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen
Verso : Naam en handtekening.

 

 
