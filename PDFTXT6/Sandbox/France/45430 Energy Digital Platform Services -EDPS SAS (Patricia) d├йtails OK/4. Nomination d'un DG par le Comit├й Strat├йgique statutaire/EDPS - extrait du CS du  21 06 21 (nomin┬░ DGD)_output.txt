Energy Digital Platform Services
Société par Actions Simplifiée au Capital de 3 390 000 Euros
Siége Social : 14-16 rue Touzet Gaillard, 93400 Saint-Ouen-sur-Seine
849 069 471 RCS Bobigny

(la « Société »)

EXTRAIT DES DELIBERATIONS
DU COMITE STRATEGIQUE
DU 21 JUIN 2021

 

e Proposition de nomination, d’un Directeur Général Délégué — fixation de ses pouvoirs

Le Comité Stratégique, sur proposition de la Présidente, décide de nommer M. Nicolas HENN, en qualité de
Directeur Général Délégué, pour la durée du mandat de la Présidente, soit jusqu’a l'issue de Assemblée
générale statuant sur les comptes de |'exercice clos au 31 12 2021.

M. Nicolas HENN exercera ses fonctions de Directeur Général Délégué dans la limite des pouvoirs définis ci-

apres :

I. Pouvoirs de représentation

1.

2.

Signer la correspondance courante de la Société ;

Remplir toutes formalités nécessaires pour soumettre la Société aux lois, arrétés et reglements de
tous pays ou elle pourrait étre amenée a faire des opérations ;

Représenter la Société a l’egard de toutes personnes physiques ou morales, publiques ou privées
ainsi que toutes administrations et autres instances (presse, etc.) tant en France qu’a l'étranger ;

Certifier tous documents et émettre tous certificats ou attestations ;

Formuler toutes demandes d’autorisation ou d’agrément et généralement contracter tous
engagements liés aux dites demandes ;

Déclarer toutes créances au passif de tous débiteurs faisant l'objet de procédures, de redressement
ou liquidation judiciaire et plus généralement faire le nécessaire ;

Accepter, au nom de la Société, toutes fonctions y compris en tant qu’administrateur, membre de
directoire, de conseil de surveillance ou de tout autre organe statutaire ;

Représenter et engager la Société dans toutes assemblées des sociétées, filiales et participations,
associations, groupements ou syndicats ; a cette fin, formuler toutes propositions, prendre part a
toutes délibérations, émettre tous votes, donner tous quitus ou approbations ;

Proposer au Président la nomination de toute personne en tenant compte de sa compétence et de
son expérience, au nom de la Société, a toutes fonctions y compris en tant qu’administrateur,
membre de directoire, de conseil de surveillance ou de tout autre organe statutaire, dans toutes
filiales et participations.
Pouvoirs d’engagement

Sans préjudice :

e des limitations prévues par la loi ;
e des avis et décisions rendus par le Comité Stratégique visé a l'article 15 des statuts de la
Société ;
e des restrictions prescrites par la présente décision ;
1. Faire ouvrir et fonctionner au nom de la Société tous comptes de chéques postaux ainsi que tous

comptes courants dans tous établissements de crédit, sociétés ou caisses publiques quiils

aviseront, en déterminer les conditions de fonctionnement, y déposer toutes sommes, titres et
valeurs ;

2. Payer toute facture relative a un contrat passé ;

3. Etablir et signer tous mémoires de sommes qui peuvent ou pourront étre dues a la Société pour
quelque cause que ce soit par toutes administrations publiques, communales, départementales
ou de |’Etat ou en général par toutes personnes physiques ou morales ;

4. Recevoir de toutes administrations publiques et de I’Etat, des départements, villes et communes

et, en général, de toutes collectivités publiques ou privées et de toutes personnes, les sommes qui

peuvent étre dues a la Société, 4 quelque titre et pour quelque cause que ce soit, en donner
quittances ;

5. Négocier, et signer au nom de la Société tout accord de confidentialité avec toutes personnes ou

sociétés, publiques ou privées (y compris avec toutes administrations publiques ou collectivités
locales) ;

Jusqu’a 250.000 euros HT ou sa contre-valeur en devises étrangére par opération :
6. Négocier et engager toute dépense qui serait nécessaire au fonctionnement de la Société ;

7. Conclure auprés des véhicules financiers du groupe ENGIE tous contrats de préts, facilités de
crédit ou autre permettant le financement de la Société ;

8. Négocier, et faire toutes acquisitions de biens meubles utiles a objet social ; les céder ;
9. Négocier, acquérir ou céder, par tout mode, toutes créances ;
Jusqu’a 500.000 euros HT ou sa contre-valeur en devises étrangére par opération :

10. Négocier, passer, proroger, résilier ou renouveler tous contrats ou marchés avec toutes personnes

ou sociétés, publiques ou privées (y compris avec toutes administrations publiques ou collectivités
locales) relatifs a l’activité ;

11. Recevoir toutes sommes, obliger la Société a tous paiements, exiger toutes sommes dues a la
Société a quelque titre que ce soit ;

Pouvoirs généraux en matiére de ressources humaines

Dans la mesure ou le Président déciderait de recruter du personnel au sein de la société :

1.

En collaboration avec le Président de la Société et/ou le Directeur dont dépend le(s) collaborateur(s)
concernés procéder au recrutement, prendre toute décision réglementaire, collective ou individuelle
relative a la gestion, a ‘a remunération et a la discipline des personnels de la Société dans le respect
des réglements en matiére de personnel ; régulariser et signer tous avenants aux contrats, protocoles,
compromis et toutes autres transactions ;
Vi.

2. En accord avec le Président de la Société conduire les négociations avec les organisations salariales

sur tout sujet le nécessitant (statut du personnel, grille de salaires, retraites, durée de travail, plan
social...) ; négocier et signer tous accords d'entreprise, convention collective ou réglement intérieur ;

3. Représenter la Société en sa qualité d’employeur auprés des syndicats et des instances salariales ;

assister aux réunions du Comité Social et Economique ou a toute autre instance représentative du
personnel et les présider ; organiser toute consultation obligatoire ;

4. En matiére d’hygiéne et de sécurité, auprés des collaborateurs qui lui sont rattachés, prendre les

mesures relatives a :

> lasécurité a ’égard de ce personnel, afin notamment de prévenir :
- toute atteinte 4 sa sécurité,
- toute mise en danger de la personne d’autrui,
- tout homicide ou coups et blessures involontaires,

> [hygiéne et la salubrité nécessaires a la santé de ce personnel ;

5. Prendre toute décision réglementaire d’organisation des services placés sous son autorité.

Pouvoirs en matiére de traitement de données a caractére personnel

Veiller & la mise en ceuvre de la Politique de protection des données personnelles du Groupe et en
contréler l'application effective.

Pouvoirs en matiére d’éthique et de compliance

Prendre toute mesure nécessaire pour déployer auprés des collaborateurs qui lui sont rattachés, les
politiques éthiques et compliance du Groupe, s'assurer de leur mise en ceuvre, en contréler l’application
effective ; rendre compte de l'ensemble de ces actions (dans les conditions fixées par les procédures
du Groupe en la matiére).

Pouvoirs en matiére de devoir de vigilance

Prendre toute mesure nécessaire afin d’identifier les risques et prévenir les atteintes graves envers les
droits humains et les libertés fondamentales, la santé et la sécurité des personnes ainsi que
l'environnement résultant des services placés sous son autorité ; le cas échéant, il en est de méme
pour les activités des sous-traitants ou fournisseurs avec lesquels est entretenue une relation
commerciale établie, lorsque ces activités sont rattachées a cette relation, dans le cadre du plan de
vigilance publié par le groupe ENGIE.

Pour remplir ses fonctions, le Directeur Général Délégué dispose d’une indépendance et d’une autonomie

totale ainsi que de ta compétence, de l’autorité et des moyens matériels, humains, techniques et financiers

nécessaires pour exercer les pouvoirs qui lui sont attribués par la présente décision.

En conséquence, le Directeur Général Délégué devra prendre toutes les mesures nécessaires afin
d’'assurer que les services placés sous son autorité respectent de fagon effective l'ensemble des
obligations qui lui incombent dans les domaines qui relévent des pouvoirs qui tui ont été confiés.

Le Directeur Général Délégué informera par écrit et sans délai, de l’impossibilité ot il se trouverait
d’assumer ses responsabilités, notamment dans les hypothéses ou il estimerait que les moyens qui lui
sont attribués ne sont pas suffisants.
attention du Directeur Général Délégué est attirée sur le fait que sa responsabilité pénale personnelle
est susceptible d’étre engagée en cas d'infraction aux dispositions législatives et réglementaires dont i!
doit assurer le respect, notamment en matiére de réglementation d’hygiéne et sécurité du travail et de lutte
anti-corruption, que cette infraction soit commise par lui-méme ou par un salarié travaillant sous sa
responsabilité. :

Le Directeur Général Délégué déclare par conséquent connaitre la réglementation en vigueur dans les
domaines ci-dessus énoncés ainsi que les sanctions pénales applicables en cas de non-respect de cette
réglementation.

Le Directeur Général Délégué est autorisé a subdéléguer une partie des présents pouvoirs a un ou
plusieurs de ses collaborateurs. Avant de procéder a la subdélégation, le Directeur Général Délégué
s’assurera que le candidat pressenti dispose de l’autorité, de la compétence et des moyens nécessaires &
lexercice des pouvoirs qu’il entend lui confier.

 

Pour extrait certifié conforme

  
 

La Présidente
