GDF International
Société par Actions Simplifige au Capital de 3 971 640 000 euros
Siége Social : 1, Place Samuel de Champlain, 92400 COURBEVOIE
622 048 965 R.C.S. NANTERRE
La « Société »
-00000-

PROCES-VERBAL DES DECISIONS DES ASSOCIES
DU 25 MAI 2022

-00000-

Conformément a l’article 15.2 des statuts de la Société qui prévoit que « Les décisions de
associé unique ou des associés sont prises au choix du Président soit en Assemblée
Générale soit sous forme de consultation écrite y compris pour l’approbation des comptes
annuels.», le Président a consulté les sociétés :

- ENGIE SA Société Anonyme au capital de 2 435 285 011 Euros dont le siége social
est 1, place Samuel de Champlain — 92400 COURBEVOIE immatriculée sous le n°
542 107 651 au R.C.S. NANTERRE ; et

- SOPRANOR Société par actions simplifiée au capital de 100 800 Euros dont le siége
social est 1, place Samuel de Champlain — 92400 COURBEVOIE immatriculée sous
le n° 384 689 071 au R.C.S. NANTERRE

Seuls associés de la Société (les « Associés ») sur l’ordre du jour suivant :

% Rapport de gestion du Président et rapport général du Commissaire aux comptes sur
les comptes de l’exercice clos le 31 décembre 2021 ;

%Approbation des comptes de l’exercice clos le 31 décembre 2021 ;

%SAffectation du résultat ;

Absence de conventions relevant de larticle L227-10 et suivants du Code de
Commerce ;

% Arrivée a échéance du mandat du Commissaire aux comptes titulaire ;

% Arrivée a échéance du mandat du Commissaire aux comptes suppléant ;

%Pouvoirs en vue des formalités ;

% Transformation de la Société en société anonyme ;

‘Adoption des nouveaux statuts de la Société sous sa nouvelle forme ;

Nomination des administrateurs de la Société sous sa nouvelle forme ;

‘4Maintien de la durée de l’exercice social en cours ;

% Pouvoirs en vue des formalités.

Poregisté a: SERVICE DE.T.A PURLICITR FONCTERE RT DR
LIENREGISTREMENT

NANTERRE 3
Le 30/05 2022 Dossier 2022 00069702, référence 9214P03 2022 A 0299) &
Korvegiswement :125€  Penalités :0€ ov ey z
Toral liquide; Cent vingt-cing Furos SP Pr
Montant regu: Cent vingt-cing Euros & se »
ri s . s
On
ll est rappelé que les comptes de l’exercice clos le 31 décembre 2021 ont été établis et arrétés
par le Comité de Direction de la Société et que l'ensemble des documents et informations
prévus par les dispositions légales, réglementaires et statutaires — et notamment les comptes
de l'exercice clos le 31 décembre 2021 ainsi que le texte des projets de décisions — ont été
communiqués aux Associés par le Président ou mis a leur disposition au siége social de la
Société, et que linventaire a été tenu a leur disposition au siége social de la Société, le tout
dans le délai requis par les dispositions légales, réglementaires et statutaires.

Le cabinet Ernst & Young et Autres a été informé de la présente consultation des Associés.
En conséquence les décisions suivantes ont été prises par les Associés et constatées dans le
présent procés-verbal :

Premiére résolution

Les Associés, aprés avoir pris connaissance du rapport de gestion du Président et du rapport
général du Commissaire aux Comptes sur les opérations de l'exercice clos le 31 décembre
2021, approuvent les comptes de I'exercice clos le 31 décembre 2021 tels quiils lui sont
présentés et toutes les opérations et mesures traduites par ces comptes ou résumées dans

ces rapports.

Cette résolution a été adoptée a l’unanimité par les Associés.

Deuxiéme résolution

Les Associés, constatant que le bénéfice de l’exercice clos le 31 Décembre 2021 s’éléve a
252 021 791 €, décident de l’'affecter de la facon suivante :

- Résultat de lexercice ...... 00.00 cece ceeceecseaenecscenceesecs 252 021 791 €
- Dotation a la réserve légale .....0... oo. cceeeceeeeeceeeeeee (9 675 611) €

- Dotation aux autres réserves 2000... oe cee ccceeeeeecee aes -

- Report a nouveau de l’exercice précédent ............... § 434 €

- Soit un bénéfice distripuable .......0..00... 0.0 cccccceeeeeeeeeee 242 351 614 €

- Distribution d’un dividende de 0,9749 Euro par action .... (242 342 592) €
- Solde a reporter & nouveau ...... 0... .e cee eeeceeeseeeeeseeeeeees 9022 €
Les Associés conformément aux dispositions de l'article 243 bis du Code Général des Impéts,
prennent acte que le dividende sera versé au titre de l’exercice 2021 au plus tard le 30 juin
2022.

Les Associés prennent acte que le dividende distribué au titre des trois exercices précédents
s'éléve a :

   
  
  
  
    
 

Dividende net

 

Nombre d’actions

  
  
  
  
 

 

Exercice

    
   
  

 

rémunérées (en €)
248 582 000 2,5687
248 582 000 1,2326

248 582 000 0,1436

Cette résolution a été adoptée a l’unanimité par les Associés.

Troisiéme résolution

Les Associés, prennent acte qu’aucune des conventions visées a l'article L.227-10 et suivants
du Code de commerce, n’a été conclue ou exécutée au cours de l’exercice 2021.

Cette résolution a été adoptée 4 l’unanimité par les Associés.

Quatriéme résolution

Les Associés, aprés avoir constaté que le mandat de Commissaire aux comptes titulaire de la
société ERNST & YOUNG et Autres arrive 4 expiration a issue de la présente décision,
décident de le renouveler pour une durée de six exercices soit jusqu’a l’issue de la décision
des associés appelée a statuer sur les comptes de l’exercice clos le 31 décembre 2027.

Cette résolution a été adoptée 4 ’unanimité par les Associés.

Cinquiéme résolution

Les Associés, aprés avoir constaté que le mandat de Commissaire aux comptes suppléant de
la société AUDITEX arrive 4 expiration a l’issue de la présente décision, décident de ne pas le
renouveler ainsi que l’y autorise l'article L823-1 du Code de Commerce.

Cette résolution a été adoptée a l’unanimité par les Associés.
Sixiéme résolution

Les Associés donnent tous pouvoirs au porteur d'un original, d'une copie ou d'un extrait du
procés-verbal et particuligrement 4 LEXTENSO SERVICES, domiciliée La Grande Arche de
La Défense — Paroi Nord - 1, Parvis de La Défense — 92044 PARIS LA DEFENSE, pour
effectuer tous dépdts et formalités ot: besoin sera.

Cette résolution a été adoptée a l’unanimité par les Associés.

ll — TRANSFORMATION DE LA SOCIETE

Septiéme résolution

Les Associés, aprés avoir entendu la lecture du rapport du Président et de celui du
Commissaire aux comptes sur les capitaux propres en application de article L 225-244 du
code de commerce,

prennent acte de l'attestation du Commissaire aux comptes mentionnant que les
capitaux propres de la Société sont au moins égaux au capital social

et décident, aprés constatation que toutes les conditions légales requises sont
remplies, de transformer la Société en société anonyme 4a conseil d’administration a
compter de ce jour.

Cette transformation réguliérement effectuée n'entrainera pas la création d'une personne
morale nouvelle.

La durée de la Société, son objet et son siége social ne sont pas modifiés.

Son capital reste fixé a la somme de 3 971 640 000 euros, divisé en 248 582 000 actions,
libérées intégralement.

Les Associés prennent acte de ce que

les mandats de Président de Monsieur Alexandre GEOFFROY et de Directeur Général
Délégué de Monsieur Jean-Marc TURCHINI ainsi que les fonctions de Madame
Laurence JATON en qualité de personne ayant le pouvoir de diriger, gérer ou engager
a titre habituel la Société prennent fin automatiquement, immédiatement et de plein
droit du fait de la transformation de la Société en société anonyme ;

la transformation de la Société en société anonyme ne met pas fin au mandat de la
société ERNST & YOUNG et Autres (438 476 913 RCS Nanterre), commissaire aux
comptes titulaire de la Société dont le mandat a été renouvelé par la quatrieéme
résolution.

Cette résolution a été adoptée 4 l’unanimité par les Associés.
Huitiéme résolution

Les Associés, en conséquence de la décision de transformation de la Société en société
anonyme,

- prennent acte de la nécessité pour la Société de procéder a la modification de ses
statuts,

- adoptent en conséquence article par article, puis dans son ensemble, le texte des
statuts de la Société sous sa nouvelle forme tel qu’il lui a été soumis

- prennent acte que les nouveaux statuts remplacent avec effet immédiat les anciens
statuts auxquels ils se substituent, et s’appliquent en particulier aux résolutions qui
suivent.

Cette résolution a été adoptée a l’unanimité par les Associés.

Neuviéme résolution

Les Associés nomment, a compter de ce jour, en qualité d’administrateurs et pour une durée
de 3 ans qui arrivera a échéance a l’issue de l’'assemblée générale appelée a statuer sur les
comptes de l’exercice clos le 31 décembre 2024 :

- Monsieur Alexandre GEOFFROY
- Madame Laurence JATON
- Monsieur Jean-Marc TURCHINI

Les trois nouveaux administrateurs ont fait savoir par avance qu’ils acceptaient ce nouveau
mandat et ne faisaient objet d’aucune incompatibilité, interdiction ou déchéance leur en
interdisant l’exercice.

Cette résolution a été adoptée a ’unanimité par les Associés.

Dixiéme résolution
Les Associés,

- décident que la durée de l’exercice social en cours, qui sera clos le 31 décembre 2022,
n’a pas a étre modifiée du fait de la transformation de la Société en société anonyme ;

-  prennent acte que les dispositions statutaires, légales et réglementaires régissant la
Société sous sa nouvelle forme s’appliquent a la présentation, au contrdle et a
approbation des comptes de l’exercice en cours ;
- décident que le Conseil d’administration de la Société sous sa nouvelle forme,
présentera un rapport de gestion portant sur l’intégralité de l’exercice en cours et que
le Président sortant est dispensé d’établir ce rapport sur la période écoulée depuis le
premier jour de l’exercice en cours.

L’Assemblée Générale statuera sur lesdits comptes conformément aux ragles fixées par les
nouveaux statuts et les dispositions du Code de commerce relatives aux sociétés anonymes.

Elle statuera également sur le quitus 4 accorder au Président de la Société sous son ancienne
forme.

Cette résolution a été adoptée a l’unanimité par les Associés.

Onziéme résolution

Les Associés donnent tous pouvoirs au porteur d'un original, d'une copie ou d'un extrait du
procés-verbal et particuligrement 4 LEXTENSO SERVICES, domiciliée La Grande Arche de
La Défense — Paroi Nord - 1, Parvis de La Défense — 92044 PARIS LA DEFENSE, pour
effectuer tous dépdts et formalités ol besoin sera.

Cette résolution a été adoptée a l’unanimité par les Associés.

Se eke

Le présent acte, constatant les décisions des Associés, sera mentionné sur le registre des
délibérations.

Fait a Courbevoie le 25/05/2022 a 9 heures.

M
Le Président

Alexandre GEOFFROY
