ENGIE Transition Services
Société par Actions Simplifiée au capital de 40 000 Eur
Siége Social : 1, Place Samuel de Champlain - 92400 Cour
833 285 679 RCS NANTERRE

£ RIWALNVN

 

sonbyqnd sasueuy sop pedyoursd moyonue3 a7

CONSTATATION DES DECISIONS DE L’ASSOCIE UNIG Pn

4 décembre 2020

 

ORERO V ODT EbUFIZG sEURYES “EEHELOOO OZOZ AEG OZOZ ZI/ L&T

Conformément a l'article 21 des statuts de la Société qui prévoit que « Les
peuvent 6tre prises soit en Assemblée Générale soit sous forme de consultatic
approbation des comptes», la société GDF International Société par Actions
3 971 640 000 Euros, dont le siége social est situé 1 Place Samuel de Cham,
(92400), immatriculée sous le n° 622 048 965 au R.C.S. de NANTERRE, Associé Unique de la SAS
ENGIE Transition Services a été consulté par le Président afin de délibérer sur ordre du jour suivant :

‘> Augmentation du capital social de 6 000 000 d’Euros afin de le porter de

40 000 Euros a 6 040 000 Euros par I'émission de 600 000 actions nouvelles a libérer en nu-
méraire ;

% Modification corrélative des statuts ;

% Pouvoirs pour les formalités.

Tous les documents devant d'aprés la législation sur les sociétés commerciales ou les statuts étre
transmis a l’Associé Unique, lui ont été communiqués.

Le Cabinet ERNST&YOUNG et Autres, Commissaire aux Comptes, a été diment informé de la pré-
sente consultation.

Aux termes de cette consultation, les résolutions ont été adoptées par |'Associé Unique de la fagon
suivante :

PREMIERE RESOLUTION :

L’Associé Unique, aprés avoir entendu la lecture du rapport du Président et constatant que le capital
social est intégralement libéré, décide d'augmenter le capital de 6 000 000 d’Euros afin de le porter de
40 000 Euros a 6 040 000 Euros par I'émission de 600 000 actions nouvelles d'une valeur nominale
de 10 Euros chacune, a émettre au pair. Ces actions seront libérées en numéraire en totalité dés leur
souscription.

Les souscriptions et versements seront regus au siége social et déposés a la banque Société Géné-
rale sur le compte n° FR76 3000 3031 7500 0434 8189 851.

LNIWALLSIOAANAT

HC LY AedONOd HUE VLA SOAS | B Sasa
Les actions nouvelles porteront jouissance a compter de la date de réalisation définitive de l’augmen-
tation du capital et seront assimilées dés cette date aux actions anciennes et soumises a toutes les
dispositions statutaires.

A chaque action ancienne est attaché un droit de souscription négociable dans les conditions et selon
les modalités prévues par les statuts. L’Associé Unique peut décider de renoncer a titre individuel a
ses droits de souscription soit sans indication de bénéficiaires, soit au profit d’un ou plusieurs bénéfi-
ciaires dénommés. Cette renonciation doit étre faite dans les conditions et sous les réserves prévues
par les statuts pour les cessions d’actions.

Les propriétaires, cessionnaires ou bénéficiaires de droits de souscription jouissent d’un droit de sous-
cription a titre réductible. Les actions non souscrites a titre irréductible seront attribuées aux proprié-
taires, cessionnaires ou bénéficiaires de droits de souscription qui auront souscrit un nombre d’actions
supérieur a celui auquel ils pouvaient souscrire a titre irréductible et ce, proportionnellement 4 leurs
droits de souscription et, en tout état de cause, dans la limite de leurs demandes.

Si les souscriptions n’ont pas absorbé la totalité du montant de l'augmentation de capital, le Président
pourra limiter le montant de l'augmentation de capital au montant des souscriptions recueillies a la
condition que celui-ci atteigne les trois quarts au moins de l'augmentation de capital.

Cette résolution est adoptée par I’Associé Unique.

DEUXIEME RESOLUTION

L’Associé Unique décide
- auregard des informations et des documents qui lui ont été communiqués lors de la consultation

- etaprés avoir renoncé au bénéfice des dispositions des articles L 225-141, L 225-142 et R 225-
120 du Code de Commerce,

de souscrire l’intégralité de l'augmentation de capital visée a la résolution ci-dessus telle qu’adoptée et
de libérer sa souscription a hauteur de 6 000 000 d’Euros ; a cet effet il transmet un bulletin de sous-
cription signé.

En conséquence, |’Associé Unique constate que les 600 000 actions nouvelles ont été entiérement
souscrites, soit l'intégralité des actions a émettre, et que augmentation de capital visée a la précédente
résolution a été entiérement souscrite et sera libérée sur le compte bancaire ouvert a la banque Société
Générale.

Cette résolution est adoptée par l’Associé Unique.

Page 2 sur 4
TROISIEME RESOLUTION

L’Associé Unique, décide, sous la condition suspensive de la réalisation définitive de I'augmentation du
capital objet des résolutions précédentes constatée par le certificat de dépét des fonds émis par la
Société Générale, que les articles 7 et 8 des statuts seront modifiés comme suit :

« ARTICLE 7 : APPORTS EN NUMERAIRE

La société GDF INTERNATIONAL a fait I'apport d'une somme de 40 000 Euros, soit 100 % des actions.

Cette somme de 40 000 Euros a été déposée a la SOCIETE GENERALE - agence entreprises de
Paris Opéra, 50 Boulevard Haussmann, 75009 Paris - sur le compte n° 30003 03620
00043589121/52, au nom de Ia Société en cours de constitution, ainsi qu'en atteste un certificat de
ladite banque.

En rémunération des apports consentis a la Société, il est attribué aux associés 4 000 actions libérées
intégralement.

Les actions émises par la Société ont obligatoirement la forme nominative.

Aux termes d’une décision de I’Associé Unique en date du 4 décembre 2020, le capital social a été
augmenté de 6 000 000 d’Euros par émission de 600 000 actions nouvelles entiérement souscrites par
GDF INTERNATIONAL et libérées en totalité en numéraire».

ARTICLE 8 : CAPITAL SOCIAL

Le capital est fixé a Six Millions Quarante Mille (6 040 000) Euros.

Il est divisé en Six Cent Quatre Mille (604 000) actions, entiérement libérées, représentant chacune
une quotité du capital. »

Cette résolution est adoptée par l’Associé Unique.

QUATRIEME RESOLUTION

L'Associé Unique délégue tout pouvoirs au porteur d'une copie ou d’extrait des présentes et en parti-
culier a la Gazette du Palais, service des formalités, a l'effet d’accomplir toutes formalités légales.

Cette résolution est adoptée par Il’Associé Unique.

ahh

 

Claire BRABEC-LAGRANGE
Présidente
Page 3 sur 4
