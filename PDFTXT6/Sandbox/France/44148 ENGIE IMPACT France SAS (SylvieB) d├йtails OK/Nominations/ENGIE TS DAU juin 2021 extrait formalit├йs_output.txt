ENGIE Transition Services
Société par Actions Simplifiée au capital de 6 40 000 Euros
Siége Social : 1, Place Samuel de Champlain - 92400 Courbevoie
833 285 679 RCS NANTERRE

CONSTATATION DES DECISIONS DE L’ASSOCIE UNIQUE
DU 30 JUIN 2021

Extrait

QUATRIEME RESOLUTION

L’Associé Unique, aprés avoir analysé la situation de la Société telle qu'elle apparait a
l'examen des comptes de |'exercice clos le 31 décembre 2020 approuvés ce jour, et aprés
avoir constaté que les capitaux propres étaient devenus inférieurs a la moitié du capital
social, décide de dissoudre de fagon anticipée la Société.

Si la dissolution est rejetée, ’Associé Unique prend acte que la Société est tenue, au plus
tard a Ja cléture du deuxiéme exercice suivant celui au cours duquel la constatation des
pertes est intervenue, de réduire son capital d'un montant au moins égale a celui des pertes
qui n'ont pu étre imputées sur les réserves si, dans ce délai, les capitaux propres n'ont pas
été reconstitués 4 concurrence d'une valeur au moins égale a la moitié du capital social.

Cette résolution est rejetée par l’Associé Unique.

CINQUIEME RESOLUTION

L’Associé Unique prend acte de la démission, 4 compter du 30 juin 2021, de Madame Claire
BRABEC-LAGRANGE de son mandat de Président et décide de nommer en remplacement
Monsieur Benoit HEMON pour une durée de trois exercices arrivant a échéance a l’issue de
lapprobation des comptes de |'exercice clos le 31 décembre 2023.

Monsieur Benoit HEMON exercera ses pouvoirs conformément a l'article 16 des statuts de la
Société.

Cette résolution est adoptée par l’Associé Unique.

SIXIEME RESOLUTION

L’Associé Unique délégue tous pouvoirs au porteur d’une copie ou d’extrait des présentes et
en particulier a la Gazette du Palais, service des formalités, a l'effet d’accomplir toutes
formalités légales.

Cette résolution est adoptée par l’'Associé Unique.

 

Le Président
