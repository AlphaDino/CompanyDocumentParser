ENGIE INVESTISSEMENTS 66
Société par Actions Simplifi¢e au capital de 40 000 Euros
Siége Social : 1, Place Samuel de Champlain - 92400 Courbevoie
833 285 679 RCS NANTERRE

PROCES-VERBAL DE L’ASSEMBLEE GENERALE
DU 28 JUIN 2019

Extrait

QUATRIEME RESOLUTION
L’Associé Unique décide de modifier les statuts

e en supprimant l’obligation de tenir une Assemblée Générale pour l’'approbation des
comptes et la constatation de la perte de la moitié du capital social en laissant le
choix au Président de choisir entre la tenue d’une Assemblée Générale et la
consultation écrite ; en conséquence |’Associé Unique décide de modifier ainsi qu’il
suit l'article 18 des statuts intitulé « Consultation des Associés » et 29 « Capitaux
propres inférieurs a la moitié du capital social :

« ARTICLE 18 : CONSULTATION DES ASSOCIES

Les décisions des associés peuvent étre prises soit en Assemblée Générale soit sous forme
de consultation écrite y compris pour I’approbation des comptes. » Le reste de l'article
demeure inchangé.

« ARTICLE 29 : CAPITAUX PROPRES INFERIEURS A LA MOITIE DU CAPITAL SOCIAL

Si, du fait de pertes constatées dans les documents comptables, les capitaux propres de la
Société deviennent inférieurs a la moitié du capital social, la collectivité des associés ou
lassocié unique est tenu de décider dans les quatre mois qui suivent l'approbation des
comptes ayant fait apparaitre ces pertes, a I'unanimité, s'il y a lieu a dissolution anticipée de
la Société. » Le reste de l'article demeure inchangé.

e en remplagant dans les statuts le terme d’« Assemblée Générale » par « décision
collective des associés ou de l’'associé unique » ; en conséquence I’Associé Unique
décide de modifier les articles 15 et 16 des statuts intitulés « Nomination du
Président » et « Attributions et pouvoirs du Président » :

« ARTICLE 15 : NOMINATION DU PRESIDENT

1. La Société est représentée a I'égard des tiers par un Président, personne physique ou
personne morale, associé ou non de Ia société.

2. Le Président est nommé pour une durée de trois ans par la collectivité des associés ou
associé unique et pour la premiére fois par les présents statuts.
3. Les fonctions du Président prennent fin, a l'expiration de son mandat, en cas de
démission, d'incapacité ou de décés. Si le Président, personne physique, vient a
dépasser |'age de 65 ans, il est réputé démissionnaire d'office a l'issue de la plus
prochaine décision de la collectivité des associés ou de I’associé unique.

En cas d'incapacité ou de décés, I'associé le plus diligent est tenu de convoquer la
collectivité des associés dans les meilleurs délais.

En cas de démission, le Président doit communiquer sa décision a chacun des
associés par lettre recommandée ou par lettre remise en mains propres contre
décharge, un mois au moins avant la date souhaitée de prise d'effet et convoquer une
la collectivité des associés ou I'associé unique afin de pourvoir a son remplacement.

4. Le Président est également révocable par décision unanime de la collectivité des
associés ou de l’associé unique, ou par décision de justice pour juste motif.

ARTICLE 16 : ATTRIBUTIONS ET POUVOIRS DU PRESIDENT

1. Le Président représente la Société a I'égard des tiers.

Dans les rapports avec les tiers, le Président est investi des pouvoirs les plus étendus
pour agir en toute circonstance au nom de la Société dans fa limite de I’objet social et
Sous réserve des pouvoirs expressément attribués par la loi (article L.227-9 du Code
de commerce) a Ia collectivité des associés.

Le Président est autorisé a consentir des subdélégations de pouvoirs pour une ou
plusieurs opérations déterminées.

2. Le Président assume, sous sa responsabilité, la direction de la Société.
Rémunération du Président.

Le Président peut avoir droit, en rémunération de sa fonction, a un traitement fixe ou
proportionnel ou a la fois fixe et proportionnel, aux bénéfices ou au chiffre d'affaires
dont les modalités de fixation et de reglement sont déterminées a I'unanimité par la
collectivité des associés ou par l’'associé unique.

En outre, le Président a droit au remboursement de ses frais de représentation et de
déplacement sur justification.

Cette rémunération et ces frais sont comptabilisés en frais généraux de la Société. »
e en supprimant l’obligation de rédiger un rapport de gestion dans les cas prévus par la

loi; en conséquence l’Associé Unique décide de modifier l'article 26 des statuts
intitule « Comptes annuels » :
« ARTICLE 26 : COMPTES ANNUELS

A la cléture de chaque exercice, le Président dresse l'inventaire et les comptes annuels
comprenant le bilan, le compte de résultat et une annexe. If établit le cas échéant
conformément a l'article L232-1 du Code de Commerce un rapport sur la situation de la
Société et I'activité de celle-ci pendant I'exercice écoulé. Ces documents seront mis a
la disposition du Commissaire aux Comptes dans les conditions légales et
réglementaires en vigueur dans les sociétés par actions simplifies et seront soumis a
l'approbation des associés. »

Cette résolution est adoptée par l’Associé Unique.
CINQUIEME RESOLUTION

L’Associé Unique donne pouvoir au Secrétaire de ladite réunion pour certifier conforme des
copies ou extraits du présent procés-verbal.

L'Associé Unique délégue tous pouvoirs au porteur d’une copie ou d’extrait des présentes et
en particulier a la Gazette du Palais, service des formalités, 12 rue de la Chaussée d’Antin —
75009 Paris, a l'effet d’accomplir toutes formalités légales.

Cette résolution est adoptée par I’Associé Unique.

)

Extrait certifié conforme

Le Président
