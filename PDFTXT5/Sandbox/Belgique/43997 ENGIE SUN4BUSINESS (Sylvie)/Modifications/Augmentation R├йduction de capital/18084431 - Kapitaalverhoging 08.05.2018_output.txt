‘Mod Word 15.4

 
 
  
  

Copie 4 publier aux annexes au Moniteur belge
aprés dépét de l'acte au greffe

1d 288

BU greiic Gu ti Gretad de commerce

 

N° d'entreprise : 0666.554,.504 '
Dénomination :
(en enter): ENGIE Sun4Business :
(en abrégé) ; {
Forme juridique : Société anonyme

; Adresse compléte du sige: 1000 Bruxelles, Boulevard Simon Bolivar, 34

- obiet de Pacte : AUGMENTATION DU CAPITAL SOCIAL - MODIFICATIONS AUX STATUTS

i

t
'
'
t
4
t
t
'
t
t
‘
t
‘
t
t
{
t
‘
;
t
t
‘

Sp : D’aprés un procés-verbal regu par Maitre Damien HISETTE, notaire & Bruxelles (quatriéme canton), associé!
iS , de « Van Halteren, Notaires Associés », 4 1000 Bruxelles, rue de Ligne 13, le 08 mai 2018, il résulte que : :
‘hy wachene

‘Ss PREMIERE RESOLUTION.

ta ' _Liassemblée décide d’augmenter le capital social 4 concurrence de 3.400.000 EUR pour le porter de:
ic : 2,000,000 EUR a@ 5.400.000 EUR par l’émission de 3.400 nouvelles actions ordinaires, identiques a celles:
’ ' existantes et jouissant des mémes droits et avantages a partir de la répartition afférente a l'exercice social:
5 ! commencé le ter janvier 2018 et 4 compter de leur émission. ‘
‘Dn Ces actions nouvelles seront souscrites pour le prix global de 3.400.000 EUR et libérées en espéces lors de’
ig la souscription 4 concurrence de 25%, soit 850.000 EUR. i
12 : J i
‘= ' vecbene

5 DEUXIEME RESOLUTION.

 

Comme conséquence de Ia résolution qui précéde, l'assemblée décide de remplacer l'article 5 des statuts :

Le capital social est fixé a cing millions quatre cent mille euros (5.400.000 EUR).

ll est représenté par cing mille quatre cents (5.400) actions sans désignation de valeur nominale.

wobec

TROISIEME RESOLUTION.

L'assemblée décide de conférer tous pouvoirs, avec faculté de subdéléguer :

-au conseil d'administration pour l'exécution des résolutions qui précédent; :

~4 Mesdames Stéphanie Ernaelsteen et Madame Anne-Catherine Guiot, agissant séparément, pour,
‘établissement du texte coordonné des statuts.

wah

Pour extrait analytique conforme.

Déposé en méme temps : expédition, procuration, attestation bancaire et coordination des statuts i

(signé) Damien HISETTE, notaire associé 4 Bruxelles. i

 

  

 

   

 

Teoeereseeee Bijlagen bij het Belgisch Staatsblad - 31/05/2018"

‘
'
t
i
t
'
t
'
t
t
t
‘
i
‘
i
1
t
‘
'
‘
t

‘Mentionner eur ia derniave page du Volet BP “Au recto : Norn et qualité du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter la personne morale @ régard des tiers
Auverso : Nom et signature (pas applicable aux actes de type « Mention >).

 

 

 
