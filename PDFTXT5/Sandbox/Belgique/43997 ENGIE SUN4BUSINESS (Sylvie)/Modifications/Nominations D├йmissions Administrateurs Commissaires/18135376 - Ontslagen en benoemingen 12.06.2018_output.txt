 
  
     
      
  

MOD WORD 11.1

Copie a publier aux annexes du Moniteur belge

aprés dépét de l'acte au greffe

anacalDany la
35376*

      

ad 2

  

23 AUT 2016

au greffe cu tribunal ce commerce
franco} 8ttite Bruxelles

  

N° d'entreprise : 0666554504 —
Dénomination

(enentie): ENGIE Sun4Business

   

{en abrégé) : S4B
Forme juridique : société anonyme

Siége : Bd Simon Bolivar 34 - 1000 Bruxelles
(adresse compléte)

Objet(s) de l’acte :Nomination - Demission

L'assemblée ordinaire des actionnaires qui s'est tenue le 12 juin 2018 acte ce qui suit:

L’'assemblée est informée qu’en vue d’assurer la continuité de la gestion de la société,

l'exercice du mandat d'administrateur B vacant de la société anonyme Orka, ayant son
siége social 4 9810 Nazareth, Axeldreeft 18, numéro d'entreprise 081 1.943.745, a été
confié provisoirement a M. Jean-MichelAncion par le Conseild'administration a la date

du 19 janvier 2018.

L'assemblée est également informée de la démission de son mandat d'administrateur
B de Monsieur Jean-Michel Ancion a la date du 23 mai 2018.

L'assemblée approuve a l'unanimité des voix ja nomination définitive de

Monsieur Cédric Ancion, demeurant Rue Faider 11 & 1060 Bruxelles, appelé
provisoirement en qualité d'administrateur B par le Conseil d'administration en date du
23mai 2018 pour achever le mandat de Monsieur Jean-Michel Ancion. Ce mandat
viendra a échéance a l'issue de I'assemblée générale ordinaire de 2022.

le procés-verbal est déposé au greffe du Tribunal de commerce de Bruxelles

Francoise Sotiau
Administrateur

'
'
‘
'
+
‘
t
t
t

Mentionner sur la derniére page du VoletB: Au recto: Nom et qualité du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter ia personne morale a légard des tiers
Au_verso : Nom et signature

 
