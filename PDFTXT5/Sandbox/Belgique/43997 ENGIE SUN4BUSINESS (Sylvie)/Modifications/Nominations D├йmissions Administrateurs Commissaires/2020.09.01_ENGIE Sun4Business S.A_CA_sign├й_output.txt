DocuSign Envelope ID: 3075437A-D5D5-4499-9F05-F6568A2A2ACO

ENGIE SUN4BUSINESS S.A.
CONSEIL D’ADMINISTRATION DU 1°8 SEPTEMBRE 2020

Résolutions unanimes et écrites des administrateurs en application de
l’article 7 :95 du Code des sociétés et des associations (CSA)

 

Le Conseil d’administration prend par la présente, en conformité avec l’article 7 :95 du
CSA, les résolutions suivantes :

1. NOMINATIONS STATUTAIRES

Francois Thoumsin et Frangoise Sotiau font part aux membres du Conseil de leur
démission en tant qu’administrateur avec effet au 1 septembre 2020.

Le Conseil en prend acte et les remercie pour les services rendus 4 la société au
cours de leur mandat.

Le Conseil d’administration décide de nommer provisoirement administrateur
Michaél De Koster, domicilié 4 1400 Nivelles, rue des Combattants 16/1, avec effet
au 1% septembre 2020 et de le désigner en qualité de président du Conseil
d’administration de la société en remplacement de M. Thoumsin avec effet 4 la
méme date.

Le Conseil d’administration décide également de nommer provisoirement

administrateur Dean Van Raemdonck, domicilié 4 2600 Berchem, Lambert
Briesstraat 36 - bus 1, avec effet au 1% septembre 2020.

Les mandats d’administrateur de Michaél De Koster et Dean Van Raemdonck
devront étre confirmés par la prochaine assemblée générale.

Michaél De Koster remercie le Conseil de la confiance qui lui est ainsi témoignée et
rend ensuite hommage a la contribution rendue & la société par Francois Thoumsin
et Francoise Sotiau.

2. Réglement de pouvoirs

Le Conseil approuve le réglement de pouvoirs repris ci-joint (annexe 1).
DocuSign Envelope ID: 3075437A-D5D5-4499-9F05-F6568A2A2ACO

L’assemblée donne pouvoir a Messieurs Thierry Quinet et
Francois-Xavier van der Mersch ainsi que Mesdames Nadia Lambert, Sylvie Deconinck et
Britt Colman, agissant seul, avec faculté de délégation, pour remplir toutes les formalités
requises en vue de la publication des décisions prises dans les annexes du Moniteur belge et
4 la Banque-Carrefour des Entreprises.

DocuSigned by: DocuSigned by:
Jean—Mideel Ruciow Francois Thoumsiw.
E9DS9865A94A4F4. 19DF6C08834C459.
Alfin S.P.R.L., Fr. Thoumsin
représentée par son représentant permanent Président
J.-M. Ancion,

Administrateur-délégué

DocuSigned by:
DocuSigned by:

Cédric Avion Francoise Sotiau
F31E2F2B7B2F445. 47CB7EE61BFO45E.

Cédric Ancion Francoise Sotiau
Administrateur Administrateur
