Wind4Flanders cvba
(W4F cvba)

Zetel van de vennootschap:Simon Bolivarlaan 34, 1000 Brussel
BTW : BE 0628.836.449 RPR Brussel

De zitting wordt geopend om 16.00 uur onder voorzitterschap van de heer Rik SOENS.

De vergadering duidt aan als stemopnemers de, Aen Drongeris Rowman en

en als secretaris de heer Rumold Lambrechts.

Uit de aanwezigheidslijst, die aan deze notulen wordt gehecht teneinde er een integrerend
geheel mee uit te maken, blijkt dat de vergadering geldig is samengesteld en bevoegd om
over de voorliggende agenda te beraadslagen en te besluiten. De volmachten, waarvan de
fespectieve vertegenwoordigers der vennoten drager zijn, werden gedeponeerd bij het
bureau, dat de regelmatigheid ervan controleerde en bevestigde.

De voorzitter stelt tevens vast en verklaart dat de oproepingen tot deze vergadering, per
aangetekende brief van 13 april 2018 werden verricht. Er werd vastgesteld dat geen
aanvraag tot het agenderen van bijkomende punten is ontvangen. De agendabevestiging
werd aan de vennoten aldus bij herinneringsbijeenroeping per 4 mei jl. betekend. Ook de
leden van de raad van bestuur en de commissaris werden via separate uitnodigingsbrieven in
kennis gesteld van de zitting der jaarvergadering van heden.

De bewijzen van dit alles zijn neergelegd bij het bureau, dat ter zake de nodige
controleverrichtingen uitvoerde.

De voorzitter zet vervolgens uiteen dat deze gewone algemene vergadering bijgevolg
volgende agendapunten in behandeling neemt :

1. Versiag van de raad van bestuur over het boekjaar 2017

2. Verslag van de commissaris over het boekjaar 2017

3. Jaarrekening (balans, resultatenrekening en toelichting) en bestemming van het
resultaat over het boekjaar 2017

4. Kwijting te verlenen aan de bestuurders en de commissaris

5. (Her)Benoeming van de commissaris

6. Statutaire mededelingen.

De uiteenzetting en de verkiaringen door de voorzitter, mede namens het bureau, inzake
voormelde voorafgaande aktenemingen en vaststellingen worden door de vergadering als
juist erkend.

Vervoigens wordt de behandeling der agendapunten, die daar waar nodig aan de hand onder
meer van de op voorhand toegezonden documentatie worden becommentarieerd, als volgt
doorgevoerd.
BERAADSLAGING EN BESLUITNEMING
EERSTE BESLUIT

De vergadering neemt met instemming akte enerzijds van het ‘Verslag van de raad van
bestuur over het boekjaar 2017’ en anderzijds van het ‘Verslag van de commissaris over het
boekjaar 2017’.

Dit besluit wordt met eenparigheid van stemmen genomen.

TWEEDE BESLUIT

De vergadering hecht, na bespreking, haar goedkeuring aan de jaarrekening over het
boekjaar, afgesloten per 31 december 2017, bevattende de balans, de resultatenrekening en
de toelichting (met inbegrip van de gehanteerde waarderingsregels), en eveneens bevattende
de resultaatsbestemming en -verwerking. In uitvoering daarvan en aansluitend daarop, beslist
de vergadering om inzonderheid ook goedkeuring te verienen ten aanzien van het voorstel
om van het te bestemmen winstsaldo ad 2.440.796,18 euro, een bedrag van 122.039,81 euro
te doteren aan de wettelijke reserves, 77.722,77 euro over te dragen naar het boekjaar 2018
en 2.241.033,60 euro — onder de vorm van dividend — ter beschikking en verdeling te stellen
onder de in aanmerking komende vennoten, titularissen van aandelen, a rato van het effectief
aandelenbezit (voor het volgestorte gedeelte en — in voorkomend geval — pro rata temporis)
per 31 december 2017. De betrokken dividendbedragen worden betaalbaar gesteld per 18
mei 2018.

Dit besluit wordt met eenparigheid van stemmen genomen.

DERDE BESLUIT

De vergadering beslist om afzonderlijk kwijting te verlenen enerzijds aan de bestuurders en
anderzijds aan de commissaris, voor de uitoefening van hun mandaat en/of opdracht tijdens
het boekjaar 2017.

Dit besluit wordt met eenparigheid van stemmen genomen.

VIERDE BESLUIT

(Her)Benoeming van de commissaris

Bij gelegenheid van de zitting van deze jaarvergadering verstreek het mandaat van de
commissaris van W4F, met name de Burgerlijike Vennootschap onder de vorm van een
codperatieve vennootschap met beperkte aansprakelijkheid “Deloitte Bedrijfsrevisoren’,
vertegenwoordigd door de heer Dirk CLEYMANS, Bedrijfsrevisor.

De vergadering besluit om te herbenoemen als commissaris van W4F de Burgerlijke
Vennootschap onder de vorm van een codperatieve vennootschap met beperkte
aansprakelijkheid “Deloitte Bedrijfsrevisoren’, met vennootschapszetel te 1930 Zaventem,
‘Gateway building’, Luchthaven Nationaal 1 J, vertegenwoordigd door de heer Dirk
CLEYMANS, Bedrijfsrevisor, dit — conform artikel 40 der statuten — voor een nieuwe
mandaatperiode van drie jaar, aldus verstrijkend. onmiddellijk na de zitting der gewone
algemene vergadering van 2021. De basisvergoeding voor de courante controle-opdracht van
de commissaris-revisor inzake de financiéle toestand en de jaarrekening van onze
vennootschap wordt vastgelegd op 5.830,00 euro (geindexeerd per refertedatum 1/01/2018)
op jaarbasis (exclusief B.T.W. en exclusief de bijdragen aan het Instituut voor
Bedrijfsrevisoren).

Dit besluit wordt met eenparigheid van stemmen genomen.
 

VIJFDE BESLUIT

Statutaire mededelingen

De vergadering neemt er akte van dat, op basis van de besluitneming van de raad van
bestuur van Wind4Flanders d.d. 12 april 2018, de vennootschap Zefier cvba, met zetel van de
vennootschap te 1000 Brussel, Ravensteingalerij 4, bus 2, aanvaard werd en toegetreden is
als vennoot in Wind4Flanders, dit punctueel in opvolging van de Vlaamse
financieringsverenigingen Figga, Fingem en Finilek - deel uitmakend van de vennotengroep B
van Wind4Flanders -, ingevolge de op 29 maart 2018 ten aanzien van en in de schoot van
Figga, Fingem en Finilek voltrokken besluitnemingen aangaande de splitsing door
overneming van voornoemde financieringsverenigingen met inwerkingtreding per 1 april
2018, met overgang van de activiteitstak “hernieuwbare energie’, en meer bepaald de
respectieve participaties van Figga, Fingem en Finilek in Wind4Flanders, naar Zefier cvba.

De vennotenstructuur (en kapitaalsituatie) van Wind4Flanders, zoals dit ook blijkt uit het
geactualiseerde vennotenregister van Wind4Flanders, is dienaangaande als volgt gewijzigd:

 

 

 

 

 

 

 

|, Aandeelhouders | . Aantal aandelen | Mh. idlooe ae eer My ten |
Electrabel | 861.936 | 229.561 |
Zefier | 466.920 | 94.908 |
IBE | 101.788 | Nihil |
IKA 293.228 134.653 |
TOTAAL | 1.723.872 459.122 |

 

Gerapporteerd wordt tevens omtrent de door de raad van bestuur per 12 april 2018
geconcretiseerde en/of ingeplande verdere (formaliserings- of uitvoerings)stappen met
betrekking tot deze aangelegenheid, dit - in voorkomend geval - ook inzake de amendering
van bepaalde vigerende constitutieteksten van Wind4Flanders.

Dit (aktenemings)besluit wordt met eenparigheid van stemmen genomen.

Na afhandeling van de agenda wordt, op verzoek van de voorzitter, onderhavig versiag door
de secretaris voorgelezen en vervolgens door de leden van het bureau der vergadering en
door de vertegenwoordigers der vennoten, die dit wensen, ondertekend.

De zitting wordt gesloten om%é.55 uur,

Secretaris Voorzitter

  

mopnemers QR Acens
¢ ys Boumoin

Vertegenwoordigers der vennoten

 
