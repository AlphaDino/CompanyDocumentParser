Mod PDF 19.01

Copie a publier aux annexes au Moniteur belge
apres dépdt de l'acte au greffe

Réservé
au LJ

Déposé

Moniteur *21325901*
belge

o
>
o
Q
.
)
@
#
c
3
=
>
s
wn
o
x<
©
Cc
Cc
<x
1
-
nN
So
Qa
N
+
S
S
3
a
'
3
&
a
n
2
o
©
£
n
x=
o
2
>
o
a
~
o
x=
5
c
o
D
&
=

21-04-2021

Greffe

  

 

N° d'entreprise : 0422672748
Nom
(en entier): SENEC

(en abrégé) :

Forme légale : Société anonyme

Adresse complete du siege Chaussée de Ruisbroek 85
: 1190 Forest

Objet de I'acte : DEMISSIONS, NOMINATIONS

Une erreur matérielle s’est glissée dans |'extrait du procés-verbal de l'Assemblée Générale Spéciale
des Actionnaires, publié le 15 février 2021 au Moniteur belge sous la référence 2021-02-15 /
0310312. Au début de I’extrait, il y a lieu de lire : « Il résulte du procés-verbal de l’'assemblée
générale spéciale des actionnaires du 1 février 2021 (...) » au lieu de « Il résulte du procés-verbal
de l’'assemblée générale spéciale des actionnaires du 1 février 2020 (...) »

(signé) Damien HISETTE, notaire associé a Bruxelles.

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
