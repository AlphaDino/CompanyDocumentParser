SENEC
Société Anonyme

Siége social : Rue du Monténégro 152, Forest (1190 Bruxelles)
Registre du Commerce de Bruxelles n° 441.353

 

 

PROCES-VERBAL DE L'ASSEMBLEE GENERALE ANNUELLE DES ACTIONNAIRES
DU 27 AVRIL 2000

Le 27 avril 2000 a 12 heures 4 Uccle, rue Gatti de Gamond 254, s'est tenue l'assemblée
générale ordinaire de la société anonyme SENEC.

La séance est présidée par M. Jacques REYMANN qui désigne comme secrétaire Mme
Jenny LEBRUN-PETERS et propose a l'assemblée de désigner comme scrutateurs MM. Patrick
VILAIN et Frédéric de SCHREVEL qui acceptent.

Ces personnes prennent place au bureau.

M. le Président expose que l'assemblée est réunie conformément 8 l'article 26 des statuts
et que les 32.069 actions représentant le capital social étant toutes représentées, il n'y a pas lieu
de justifier des convocations. : ,

Etablissant que l'assemblée est valablement constituée pour délibérer sur l’ordre du jour,

la liste des présences est annexée au présent procés-verbal ainsi que les procurations données a
leurs mandataires par les actionnaires empéchés (annexe 1).

Ordre du jour :
Le Président donne lecture de l’ordre du jour :
1. Rapport de gestion du conseil d'administration et rapport du commissaire.

2. Examen et approbation des comptes annuels au 31 décembre 1999.
Affectation du résultat.

3. Décharge aux administrateurs et au commissaire.
4. Conseil d’administration : réélection.

5. Nomination d’un commissaire et fixation de ses émoluments.
RAPPORT DE GESTION DU CONSEIL D’ ADMINISTRATION ET RAPPORT
DU COMMISSAITRE.

L'assemblée dispense le bureau de la lecture du rapport de gestion du Conseil
d'administration et du rapport de contréle du commissaire, ces documents ayant été remis
a chacun des membres présents. Ces rapports seront déposés a la B.N.B. et seront tenus
au siége social a la disposition. Une copie de ces rapports est annexée au présent procés-
verbal (annexes 2 et 3).

EXAMEN ET APPROBATION DES COMPTES ANNUELS AU 31 DECEMBRE
1999, AFFECTATION DU RESULTAT.

M. le président ouvre la discussion sur les comptes annuels arrétés au 31 décembre 1999,
qui comprennent le bilan, le compte de résultats et l'annexe.

La discussion étant close, le bilan et le compte de résultats sont mis aux voix et approuvés.
Le total des rubriques du bilan s'éléve, a l'actif et au passif, 4 64.435.374 BEF et le total
du compte de résultats, en charges et en produits, 4 123.386.244 BEF.

L'exercice se cléture par un bénéfice de 471.515 BEF. Compte tenu du bénéfice reporte
de l’exercice antérieur, soit 3.576.763 BEF, le bénéfice disponible s’éléve 4 4.048.278
BEF que l’assemblée, a l’unanimité, décide de reporter 4 nouveau.

DECHARGE AUX ADMINISTRATEURS ET AU COMMISSAIRE.

Conformément a l'article 79 des lois coordonnées sur les sociétés commerciales, le
président invite l'assemblée a se prononcer sur Ja décharge a accorder aux administrateurs
et au commissaire pour l'exercice de leur mandat pendant l'exercice social 1999.

Par vote spécial, cette décharge est accordée a I'unanimité aux administrateurs, MM.
Jacques REYMANN, Christian PONCELET, Frédéric de SCHREVEL, Etienne OLEFFE
et Patrick VILAIN pour !’exercice de leur mandat du 01.01.99 au 31.12.99.

Par vote spécial, cette décharge est accordée 4 Tunanimité au commissaire, la §.C.C.R.L.
DUPONT, GHYOOT, KOEVOETS, PEETERS, ROSIER & Co, représentée par M. Marc
GHYOOT, pour I’exercice de son mandat du 01.01.99 au 31.12.99.

CONSEIL D’ADMINISTRATION : REELECTION.

Sur proposition du Président, l’assemblée est invitée 4 se prononcer au sujet du
renouvellement du mandat de M. Etienne OLEFFE, administrateur.
L’assemblée, 4 l’unanimité, réélit M. Etienne OLEFFE aux fonctions d’administrateur
pour un terme de six ans venant 4 échéance a l’issue de l’assemblée générale ordinaire de’
2006. Son mandat sera exercé A titre gratuit.

5. COMMISSAIRE : NOMINATION ET FIXATION DES EMOLUMENTS.

Le mandat de commissaire de la S.C.CR.L. DUPONT, GHYOOT, KOEVOETS,
PEETERS, ROSIER & Co, représentée par M. Marc GHYOOT, vient 4 échéance 4 l’issue
de l’assemblée générale ordinaire de ce jour.

Sur proposition du Président, l’assemblée est invitée 4 se prononcer au sujet de la
nomination en qualité de commissaire, pour un terme de 3 ans venant a échéance 4 l’issue
de l’assemblée générale ordinaire de 2003, de la S.C.C. DELOITTE & TOUCHE
Réviseurs d’entreprises, établie Berkenlaan, 6 4 1831 Diegem, inscrite au tableau des
membres de 1"Institut des Réviseurs d’Entreprises sous le n° B025 et représentée par M.
Daniel KROES.

L’assemblée approuve cette nomination a l’unanimité des voix.

M. le Président propose a l’assemblée de fixer le montant des émoluments annuels du
commissaire 4 90.000 BEF, prorata temporis. .

L’assemblée approuve cette proposition a Punanimité des voix.
+
+ +
Liordre du jour étant épuisé, la séance est levée 4 12 heures 30, aprés lecture et

approbation du présent procés-verbal et aprés que M. le président eut invité les actionnaires qui
le désiraient 4 signer ce document.

LE SECRETAIRE, LE PRESIDENT, —— “LES SCRUTATEURS,

  
