SENEC

société anonyme

 

Siége social :
Chaussée de Ruisbroek 85
1190 BRUXELLES

TVA BE 0422.672.748 - RPM Bruxelles

PROCES-VERBAL DE L'ASSEMBLEE GENERALE ORDINAIRE
DES ACTIONNAIRES DU 25 JUIN 2020

Le 25 juin 2020 a 12h00, s'est tenue l'assemblée générale ordinaire des actionnaires de SENEC,
société anonyme.

La séance est présidée par Monsieur Stanislas de Pierpont qui désigne comme secrétaire Madame
Nadia Lambert.

En raison de la crise du coronavirus, l'assemblée marque son accord a l’unanimité sur le fait de ne
pas désigner de scrutateurs.

Ces personnes prennent place au bureau.

Monsieur le Président expose :
- que l'assemblée est réunie conformément a l'article 26 des statuts ;

- que les actionnaires sont ici représentés :

- La société de droit belge T.E.M. sa/nv, dont le siége social est situé a 1190 Bruxelles, Rue de
Fierlant 112, propriétaire de 42.958 actions, représentée par Monsieur Stanislas de Pierpont, en
vertu d'une procuration sous seing privé, qui restera annexée au présent procés-verbal ;

- La société de droit belge COFELY SERVICES sa/nv, dont le siége social est situé a 1000
Bruxelles, Boulevard Simén Bolivar 34, propriétaire de 18.412 actions, représentée par
Monsieur Stanislas de Pierpont, en vertu d'une procuration sous seing privé, qui restera
annexée au présent procés-verbal.

Monsieur le Président constate que les deux actionnaires sont représentés. Ils représentent
ensemble les 61.370 actions représentatives du capital. Il ne doit dés lors pas étre justifié de
l'accomplissement des formalités de convocation.

L'assemblée se reconnait valablement constituée et apte a délibérer sur son ordre du jour, qui
comporte :

1. Rapport de gestion du Conseil d’administration.
Rapport du commissaire.

2. Approbation des comptes annuels au 31 décembre 2019.
Affectation du résultat.

3. Décharge aux administrateurs et au commissaire.
4. Conseil d'administration : démissions / nominations.

5. Rapport spécial du Conseil d’administration conformément a l'article 7:228 du Code des sociétés
et des associations.
SENEC sa
Procés-verbal AGO 25.06.2020 2/3
1. RAPPORT DE GESTION ET RAPPORT DU COMMISSAIRE

L'assemblée dispense le bureau de la lecture du rapport de gestion et du rapport du commissaire ; ces
documents ayant été remis a chacun des membres présents.

Une copie de ces rapports est annexée au présent procés-verbal (annexes 1 et 2).
2. APPROBATION DES COMPTES ANNUELS AU 31 DECEMBRE 2019. AFFECTATION DU
RESULTAT

Monsieur le Président ouvre la discussion sur les comptes annuels qui comprennent le bilan, le
compte de résultats et l'annexe.

La discussion étant close, Monsieur le Président met aux voix l'approbation de ces comptes annuels
arrétés au 31 décembre 2019.

A l'unanimité, l'assemblée approuve les comptes tels qu’ils lui sont présentés, le total des rubriques
du bilan s’élevant, a l’actif et au passif, a 3.452.420,93 EUR et le total de celles du compte de
résultats, en charges et produits, a 9.860.397,97 EUR.

L’exercice se solde par une perte nette de -307.856,72 EUR.

Compte tenu de la perte reportée de l’exercice précédent de -206.913,80 EUR, le montant total a
affecter s’éléve a -514.770,52 EUR que l’assemblée décide de reporter a l’exercice suivant.

Conformément a l’article 3 :6 §1° 6° du Code des sociétés et des associations, le rapport de gestion
a repris la justification de l’'application des régles d’évaluation dans l’optique d’une continuité.

Les actionnaires confirment l’objectif de continuer le support de la société.

3. DECHARGE AUX ADMINISTRATEURS ET AU COMMISSAIRE

L'assemblée, a l'unanimité et par vote spécial, donne décharge pleine et entire aux administrateurs
et au commissaire pour l'exercice 2019.

4. CONSEIL D’ADMINISTRATION : DEMISSIONS / NOMINATIONS

Le mandat d’administrateur de M. Philippe LAURENT arrive a échéance a l’issue de la présente
assemblée générale et il n’en sollicite pas le renouvellement.

L’assemblée prend acte de la démission de M. Stanislas de PIERPONT de son mandat
d’administrateur avec effet a la date de la présente assemblée générale.

L’assemblée décide a l’unanimité des voix de nommer administrateur pour une durée de quatre
ans :

- Mme Bénédicte LEFEVRE, domiciliée rue du Moulin d’'Havré 14 a 7021 Havre ;

- M.Jan DE SMET, domicilié J. van Gijsellaan 44 a 1780 Wemmel.

Leur mandat arrivera a échéance a l’issue de l’assemblée générale ordinaire de 2024 statuant sur
les comptes de l’exercice 2023 et sera exercé a titre gratuit.

L’assemblée décide également a l’unanimité d’augmenter le nombre des administrateurs de trois a
quatre et de nommer administrateur M. Jan FEIJEN, domicilié Transvaalstraat 11 a 2600 Berchem,
pour une durée de quatre ans. Son mandat arrivera a échéance a l’issue de l’'assemblée générale
ordinaire de 2024 statuant sur les comptes de l’exercice 2023 et sera exercé a titre gratuit.
SENEC sa
Procés-verbal AGO 25.06.2020 3/3

5. RAPPORT SPECIAL CONFORMEMENT A L'ARTICLE 7 :228 DU CODE DES SOCIETES ET
DES ASSOCIATIONS

Il ressort des comptes annuels relatifs a l’exercice 2019 que l’actif net de la société s’éléve a
278 KEUR, ce qui représente un montant inférieur a la moitié du capital social de 597 KEUR.

Le Président donne lecture du rapport spécial établi par le conseil d’administration, en exécution de
l'article 7 :228 du Code des sociétés et des associations, duquel une copie se trouve en annexe 3.

Sur base des arguments avancés par les administrateurs dans leur rapport spécial, l’assemblée
décide a l'unanimité, sur proposition du conseil d’administration, de poursuivre l’activité de la
société.

L’assemblée donne pouvoir, avec faculté de délégation, a tout employé de |’étude des notaires
« VAN HALTEREN, Notaires associés », dont les bureaux sont situés a 1000 Bruxelles, rue de
Ligne 13, aux fins de signer les formulaires de publication d’un extrait du procés-verbal de la
présente assemblée générale et de faire le nécessaire pour le dépdét au greffe du Tribunal de
l'entreprise compétent ainsi que pour sa publication au Moniteur belge.

L'ordre du jour étant épuisé, la séance est levée a 12h30 aprés lecture et approbation du présent
procés-verbal et aprés que Monsieur le Président eut invité les actionnaires qui le désiraient a signer
ce document.

Signé électroniquement Signé électroniquement
par Lambert Nadia par Stanislas de Pierpont
Elmine M (Signature)
Date : 03/07/2020 Date : 07-07-2020
08:36:52 15:39:18
Nadia LAMBERT Stanislas de PIERPONT
Secrétaire Président, représentant de

T.E.M. sa/nv et COFELY SERVICES sa/nv
