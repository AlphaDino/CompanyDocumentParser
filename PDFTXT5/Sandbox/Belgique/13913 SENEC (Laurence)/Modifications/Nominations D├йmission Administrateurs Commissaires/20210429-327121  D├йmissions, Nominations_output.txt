Réservé
au LJ
Moniteur

belge

 

Bijlagen bij het Belgisch Staatsblad - 29/04/2021 - Annexes du Moniteur belge

Mod PDF 19.01

Copie a publier aux annexes au Moniteur belge
apres dépdt de l'acte au greffe

Déposé
*21327121*

27-04-2021

Greffe

 

 

N° d'entreprise : 0422672748
Nom
(en entier): SENEC

(en abrégé) :

Forme légale : Société anonyme

Adresse complete du siege Chaussée de Ruisbroek 85
: 1190 Forest

Objet de I'acte : DEMISSIONS, NOMINATIONS

Démissions
Le 21 avril 2021, la Société a pris acte de la démission

* de Monsieur Jan Feijen en qualité d’administrateur de la Société, prenant effet a partir du 1er
avril 2021.

+ de Monsieur Chris Sap en qualité d’administrateur de la Société, prenant effet a partir du 30 avril
2021.
La Société décide de donner procuration, avec faculté de délégation, a chaque employé de I’étude
des notaires « Van Halteren », ayant son siége a 1000 Bruxelles, rue de Ligne 13, a qui elle donne
pleins et entier pouvoirs pour agir en son nom et pour la représenter, qualitate qua, auprés du
registre des personnes morales, des guichets d’entreprises et l’office TVA compétent a l’effet d’y
effectuer toutes les démarches nécessaires pour effectuer toute publication dans le Moniteur Belge,
et en conséquence, remplir, signer et déposer tous formulaires, y compris les formulaires de
publication, faire toutes déclarations et en général, faire tout ce qui serait nécessaire ou utile a |’
exécution du présent mandat, en promettre au besoin ratification.
Pour extrait conforme.
(signé) Damien HISETTE

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
