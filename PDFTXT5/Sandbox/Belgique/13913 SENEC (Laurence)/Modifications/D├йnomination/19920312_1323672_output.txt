Handels- en Jandbouwvennootschappen — Bijlage tot het Belgisch Staatsblad van 12 maart 1992
Societes commerciales et agricoles — Annexe au Moniteur belge du 12 mars 1992

920:

312/65

 

Handelsvennootschappen 920312-126 — 920312-141 Sociétés commerciales

Liassemblée a également décidé de modifier les

N. 920312 — 126
« Prodira-Senec », société anonyme

Chaussée de Vilvorde 98-100
1210 Bruxelles

RC. Bruxelles 441353 — T.V.A. 422.672.748

MODIFICATION DE LA DéNOMINATION
MODIFICATION AUX STATUTS

Il résulie du procés-verbal dressé par Maitre Edwin VAN
LAETHEM, notaire & Ixelles, le cing février mil neuf cent
nonante-deux, enregisiré deux réles, un renvoi & Ixelles ler
Bureau, le onze février mil neuf cent nonante-deux, volume
5/110, folio 37, case 16, au droit de : sept cent cinquante francs,
que l'assembiée générale extraordinaire des actionnaires de la
sociélé anonyme “PRODIRA-SENEC”, ayant son siége social &
Bruxelles (B. 1120 Bruxelles), chaussée de Vilvorde, 98-100, a
pris les résolutions suivantes :
1. MODIFICATION DE LA DENOMINATION SOCIALE
en "SENEC”™.
Liarticie | des statuts a été modifié en conséquence.

2, MODIFICATION DE L'ARTICLE 31 DES STATUTS
Pour extrait analytique conforme :

(Signé} Edwin Van Laethem,
notaire.

DEPOT SIMULTANE :

- l'expédition du procés-verbal dressé par le notaire Edwin VAN
LAETHEM, 8 Ixeiles, le 5 février 1992,

+ 2 procurations sous seing privé;

- les statuts coordonnés au 5 février 1992.

Déposé, 2 mars 1992.

1 1450 T.V.A. 19 p.c. 276 1726

(23214)

N. 920312 — 127

« Leunen & Partners, Member of the Crystal Group »,
sociéte anonyme

Rue du Sceptre 33
Ixelles (1040 Bruxelles)
T.V.A. 415.688.946

MODIFICATION DE LA DENOMINATION SOCIALE
AUGMENTATION DE CAPITAL
NOMINATION D'ADMINISTRATEUR

It résulte d'un procés-verbal dressé par Maitre
Didier GYSELINCK, Notaire soussigné & Bruxelles, le
dix-sept février mil neuf cent nonante-deux,
enregistré a Ixelies, troisieme bureau, le vingt-
cing février de ia méme année, volume 335 ,
folio 16 ,case 1 , que l'assembiée générale
extraordinaire des actionnaires a décideé :

1. de modifier la dénomination sociale en
"LEUNEN & PARTNERS, Member of the Carat
Crystal Media Group”, en abrégé “LEUNEN &
PARTNERS

2. d'augmenter le capital & concurrence de
DEUX MILLIONS DE FRANCS pour le porter de QUATRE
MILLIONS DE FRANCS a SIX MILLIONS DE FRANCS par
la création de deux mille actions nouvelles, jouissant
des mémes droits et avantages que les actions
existantes, souscrites au prix de mille francs et
entierement libérées.

En conséquence de ce qui précéde, les articles
1, 5 et 6 des statuts ont été modifiés.

Handelsvenn. — Soc.commerc. — 1¢ kwart./1let trim.

mémes articles dans le texte néérlandais des statuts.

3. de nommer en tant qu'administrateur
Monsieur Jacques FRANCOIS, demeurant 4 Uccle,
avenue de Beersel, 29, Son mandat viendra a
expiration immédiatement aprés l'assembliée générale
de mil neuf cent nonante-sept.

Pour extrait analytique conforme :

(Signé) Didier Gyselinck,
notgire.

Pig a : ~-
- une expédition avec trois annexes (deux
Procurations et l’attestation bancaire).
- taxte coordonné des statuts.

Déposé a Bruxelles, 2 mars 1992 (A/66579).

2 2900 T.V.A. 19 p.c. 551 3451
(23209)
N. 920312 — 128
« Leunen & Partners, Member of the Crystal Group »,
naamloze vennootschap
Scepterstraat 33
Elsene (1040 Brussel)
BTW 415.688.946
gecodrdineerde tekst van de statuten
Neergelegd te Brussel, 2 maart 1992 (A/66579).
(Mededeling) 1450 BTW 19 pct. 276 1726
(23210)

N. 920312 — 129
« Servimo Idesbald », société anonyme
Rue Charies Degroux 115
Etterbeek (1040 Bruxelles)

R.C. Bruxelles 546626 — R.C. Veurne 32924
T.V.A. 442.864.980

DEMISSIONS ~ NOMINATIONS

Il résulte des procés-verbaux de
1’ assemblée générale et du con-
seil d'administration du 04/02/
1992 tenus au siége :

- la démission & partir du
04/02/1992 :

* de Monsieur Jacques WALCKIERS,
en qualité d'administrateur et
d' administrateur-délégué,

* dela S.A. "SERVIMO KOKSIJDE",
en qualité d'administrateur,

* de Monsieur Francis WALCKIERS
en qualité d’'administrateur-
délégué et président du conseil
a‘ administration,

- la nomination :

* de Monsieur Patrick CLAERHOUT
en qualité d'administrateur et
président du conseil d' adminis-
tration,

920312*9

 
