Handels- en landbouwvennootschappen — Bijlage tot het Belgisch Staatsblad van 29 mei 2002

Sociétés commerciales et agricoles — Annexe au Moniteur belge du 29 mai 2002

De raad beslist om vanaf 1 oktober 2001 de Heer E.
WANTEN, als bestuurde te benoemen. (2640

MORTSEL,Deurnestraat 2916)
De raad beslist om machten van dagelijks bestuur,

die werden toegekend aan de Heer Y. CROMME-
LINCK toe te kennen aan G. PUSKAS vanaf 1 okto-
ber2007. G. PUSKAS wonende 2550
KONTICH, Pierstraat 330.

Voor eensluidend uittreksel :
(Get.} M. Adriansen, (Get.) G. Puskas,

 

bestuurder. bestuurder.
Neergelegd, 17 mei 2002.
2 101,30 BIW 21% 21,27 122,57 EUR
(72267)
* N.=20020529 — 508
BELGISCHE BUNKEROLIE

MAATSCHAPPIJ

Naamloze Vennootschap

Kiwiweg 80,
8380 BRUGGE

Brugge nr. 27.772

BE 403.091.319

ONTSLAG EN BENOEMING VAN BESTUUR-
DERS

Uittreksel uit het proces-verbaal van de raad van
bestuur van 29 maart 2002

De raad neemt kennis van het ontslag van de Heren
R. Laden op 1 januari 2002 en E. Wanten op 1 fe-
bruari 2002.

In toepassing van artikel 519 van het Wetboek der
Vennootschappen en artikel 22 der statuten, beslist
de Raad te voorzien in de vervanging van de Heren
R. Laden en E. Wanten en de Heer, B. Ortegat, wo-
nende te 1150 Sint-Pieters-Woluwe, Clos du Sois
Pianté 5 met terugwerkende kracht vanaf 1 januari
2002, en de Heer Jean-Mare Debaty, wonende te
1332 Rixensart, rue du Vallon 54, met terugwerken-
de kracht vanaf 1 februari 2002 als bestuurders te
benoemen.

De eerstkomende algemene vergadering zal
overgaan tot de definitieve benoeming, waarbij
de nieuwe bestuurders het mandaat van de ont-
slagnemende bestuurders zullen uitdoen.

259

Voor eensluidend uittreksel :
(Get.) M. Adriansen, (Get.) G. Puskas,

bestuurder. bestuurder.
Neergelegd, 17 mei 2002.
2 101,30 BTW 21% 21,27 122,57 EUR
(72268)

—

‘N--20020529 — 509

“Zandvliet Power” .

Naamloze vennootschap

2018 Antwerpen, Mechelsesteenweg 271

OPRICHTING NAAMLOZE VENNOOTSCHAP

Uittreksel uit een akte verleden voor Notaris Michel ROBEYNS
te Antwerpen, optredend bij ambtsvertening voor Notaris Johan
KIEBOOMS te Antwerpen, tijdelijk belet, op vijftien’ mei
tweeduizend en twee, dragende de melding : "Geregistreerd
zeven bladen een renvooien te Antwerpen derde kantoor der
tegistratie op 16 MEI'2002 boek 5/425 blad 45 vak 2. Ontvangen
: drichonderddentig euro (330 EUR) De ontvanger wn, (getekend)
R.SMETS”

1. De identiteit_van de natuurliike personen of van de rechts-

personen die de oprichtingsakte hebben ondertekend of uit wier

naam zij werd ondertekend:
1. De  coSperatieve vennootschap met  beperkte

aansprakelijkheid “SOCIETE POUR LA COORDINATION
DE LA PRODUCTION ET DU TRANSPORT DE
L'ENERGIE ELECTRIQUE", in het Nederlands
“MAATSCHAPPI VOOR COORDINATIE VAN
PRODUKTIE EN TRANSPORT VAN ELEKTRISCHE
ENERGIE”, afgekort “C.P.T.E.", gevestigd te 1000
Brusssel, Boomkwekerijstraat 20.

2, De vennootschap naar Duits recht “RWE POWER

Aktiengesellschaft", gevestigd te 45128 Essen (Duitsland),
Huyssenallee 2.
chtsvorm van de ven
naamloze vennootschap

b) De naam van de vennootschap:

“Zandviiet Power”
3. Het doel van de vennootschap:
De vennootschap heeft tot doel de constructie, het
verwerven, het bezit, het laten gebruiken door derden en/of
de mogelijke verkcop van installaties of delen van
installaties die gezamentijk of afzonderlijk electriciteit eof
stoom opwekken of in verband met dergelijke opwekking

tschap:
260

 

Handels- en landbouwvennootschappen — Bijlage tot het Belgisch Staatsblad van 29 mei 2002
Sociétés commerciales et agricoles — Annexe au Moniteur belge du 29 mai 2002

worden aangewend op de site van de naamloze
vennootschap BASF Antwerpen gelegen aan de Scheldetaan
te Antwerpen.

De vennocotschap heeft voorts tot doel het beheer, het
gebruiken, het bedrijven en/of het exploiteren van installaties
of delen van installaties die gezamenlijk of afzonderlijk
electriciteit en/of stoom opwekken of in verband met
dergelijke opwekking worden aangewend op de site van de
naamloze vennootschap BASF Antwerpen gelegen aan de
Scheldelaan te Antwerpen en/of de aan- en/of verkoop van
voornoemde opgewekte elektriciteit en/of stoom van en/of
aan de aandeelhouders van de vennootschap en/of aan de
naamloze vennootschap BASF Antwerpen.

De vennootschap kan alle hoegenaamde handels-,
nijverheids-,  financitle, roerende en onroerende
verrichtingen uitvoeren, die rechtstreeks of onrechtstreeks in
verband staan met haar doel of die van aard zijn dit te
begunstigen.

De vennootschap kan bij wijze van inschrijving, inbreng,
fusie, samenwerking, financitle tussenkomst of anderszins
een belang of deelneming verwerven in alle bestaande of nog
op te richten vennootschappen, ondernemingen, bedrij-
vigheden of verenigingen zonder onderscheid dewelke een
zelfde, gelijkaardig of verwant doel nastreven of die haar
onderneming kunnen bevorderen.

4. De zetel van de vennootschap:

5.

6.

2018 Antwerpen, Mechelsesteenweg 271
De duur van de vennootschap;

De vennootschap werd opgericht voor een onbepaalde tijd.

a) Het bedrae van het maatschappelijk kapitaal:

zesenzestig duizend euro (€.66.000), verdeeid in zestig
(60) aandelen met elk een nominale waarde van duizend
honderd euro (€.1.100).

b) Het gestorte bedrag:

Het maatschappelijk kapitaal is volledig geplaatst en
volgestort.

7. De bepalingen betreffende

a) de_bestemming van_de_winst_en_uitkering van een

interimdividend:

Het batig saldo van de resultatenrekening vormt de te
bestemmen winst van het boekjaar.

Van deze winst wordt tenminste vijf ten honderd (5%)
voorafgenomen om de wettelijke reserve te vormen totdat
deze één/tiende (1/10) van het maatschappelijk kapitaal
bedraagt. :

Over het saldo bestist de algemene vergadering bij
gewone meerderheid van stemmen op voorstel van de
raad van bestuur. .

b) de wijze van vereffening -

onder de aandeelhouders naar verhouding van het aantal
aandeien dat zi} bezitten.

8. Het begin en einde van het boekjaar;

én januari — éénendertig december van elk jaar, en voor de
eerste maal op éénendertig december tweeduizend en twee
(2002).

9. a) Gewone ene vergadering:

de derde donderdag van de maoand apri] om tien uur, en
voor de eerste maal in het jaar tweeduizend en drie
(2003).

b) De voorwaarden_voor toelating:

Om tot de algemene vergadering te worden toegelaten
moeten de houders van aandelen minstens drie (3)
werkdagen (in Belgi#) voor de vergadering aan de raad
van bestuur schriftelijk hun voornemen meedelen om aan
de algemene vergadering deel te nemen, indien de raad
van bestuur dit in de oproeping vereist.

c) Stemrecht

Elk aandeel geeft recht op één stem.

10.a) Benoeming van bestuurders en commissaris:

Werden tot bestuurders benoemd:
* Op voordracht van de houder van aandelen klasse “A”

1. De heer Xavier Votron, wonend te 7181
Arquennes,Bois de Sapins 11.
2, De heer Alfred Becquaert, wonend te 9120 Beveren,
Dennenlaan 40.
3. De heer Jean-Pierre Boydens, wonend te 2960 Brecht,
Houtsniplaan 15.
“Op voordracht van de houder van aandelen klasse “B”
4, De heer Manfred Kehr, wonend te 45721 Haltem
(Duitsland), Markenkamp 45.
5. Mevrouw Ursula Krane, wonend te 44141 Dortmund
(Duitsland), Leinbergerstrasse 23.
6. De heer Andreas Bowing, wonend te 45699 Herten
(Wuitsland), Kirchstrasse 82.
Hun opdracht eindigt bij de stuiting van de jaarverga-
dering van tweeduizend en acht (2008).
Hun opdracht wordt niet bezoldigd.
Werd aangesteld tot commisaris ;
De burgerlijke codperatieve vennootschap “Deloitte &
Touche Bedrijfsrevisoren”, hierbij vertegenwoordigd
door de heer Josephus Viaminckx, kantoor hioudend te
2018 Antwerpen, Lange Lozanastraat 270.
Zijn opdracht eindigt bij de sluiting van de
jaarvergadering van tweeduizend en vijf (2005).

b) Bevoegdheden van de raad:

De raad van bestuur is bevoegd om alle handelingen te
verrichten die nodig of dienstig zijn tot verwezentijking
van het doel van de vennootschap, behoudens die waar-
voor volgens de wet alleen de algemene vergadering van
de aandeelhouders bevoegd is.

c) Vertegenwoordiging van de vennootschap;

Onverminderd de algemene vertegenwoordigingsmacht
van de raad van bestuur als. college, wordt de
vennootschap in en buiten rechte rechtsgeldig verbonden
door twee gezamenlijk optredende bestuurders, waarvan
er €én werd voorgedragen door de aandeethouders van de
Klasse A en €én door de aandeelhouders van de klasse B;
alsdan moeten 2ij geen bewijs van een voorafgaand
besluit van de raad van bestuur voorleggen.

De vennootschap wordt eveneens in en buiten rechte
rechtsgeldig vertegenwoordigd wat het dagelijks bestuur
aangaat door de twee gezamenlijk optredende
gedelegeerden tot Tit bestuur die geen bewijs van een
voorafgaand besluit moeten voorleggen.

De vennootschap wordt eveneens in en buiten rechte
rechtsgeldig vertegenwoordigd door twee of meer
gezamenlijk optredende lasthebbers binnen de perken
van hun bijzondere volmacht dewelke hen wordt
verleend door de hierboven vermelde personen die de
vennootschap in en buiten rechte rechtsgeldig kunnen
verbinden.

11. Bijzondere volmacht

Bijzondere volmacht werd verleend met recht van
indeplaatsstelling, aan de heren Michel Lazard, wonend te
1040 Brussel, Plantanenstraat 20; Luc Bodenhorst, wonend
te 1140 Brussel, Goede Herderstraat 53/bus 54; en Erwin
Boogaerts, wonend te 2600 Berchem (Antwerpen), Leerkens
19; teneinde :

a) inschrijving, wijziging en opheffing te vorderen in of van

het handelsregister ter griffie van de bevoegde rechtbank
van koophandel;
Handels- en landbouwvennootschappen — Bijlage tot het Belgisch Staatsblad van 29 mei 2002
Sociétés commerciales et agricoles — Annexe au Moniteur belge du 29 mai 2002
—_—  ——eSSSSSSSSSSSSSSSSSSSSSSSSSSSSsSSSSSSSSSSSeee

b) eventuele inschrijving van deze vennootschap te vorderen
als belastingplichtige bij het bestuur van de Belastingen
_op de Toegevoegde Waarde.

Hiermede tegelijk neergelegd : Afschrift van de oprichtingsakte
dd. 15 mei 2002, met in bijlage : bankattest, twee volmachten, en
uittreksel. .

Voor ontledend uittreksel :

(Get.) Michel Robeyns,
notaris.

Neergelegd te Antwerpen, 17 mei 2002 (A/71561).
4 202,60, BIW 21% 42,55

 

N. 20020529 — 510

KEYAN

besloten vennootschap met beperkte aanpsrakelijkheid

2800 Schoten, Klaproosiaan 5

OPRICHTIG

Er biijkt uit een akte verleden voor notaris Stefan Van Tricht te
Schoten op 14 mei 2002 dat een bestoten vennootschap met
beperkte aansprakelljkheid werd opgericht als volgt :

Qprichters; .

1, PAULUSSEN Gary, geboren te Ekeren-op tweeéntwintig

maart negentienhonderd vierenzestig, wonende te 2900 Scho-
. ten, Alfons Servaislei 18 bus 3,

2. RUST Syivie Claudia Nancy, geboren te Wirlijk op zevenen-
twintig juli negentienhonderd achtenzestig, wonende te 2900
Schoten, Klaproosiaan 5.

Daam: de naam van de vennootschap is KEYAN

zetel: de zetet is gevestigd te 2900 Schoten, Klaproosiaan 5.
doel: De vennootschap heeft tot doel :
Patrimoniumvennootschap: beleggingen in onroerende goede-
ren; het verwerven, vervreemden, uitbaten, valoriseren, verka-
velen, ordenen, huren en verhuren, verbouwen onder alle vor-
men van bebouwde en onbebouwde onroerende gaederen.

Het organiseren van - en deelnemen aan zakenreizen, confe-
renties, meetings, seminaries, privaatcursussen cursussen voor
algemene, bedrijfsmatige, beroeps- en/of technische vorming,
het organiseren van beurzen, evenementen, tentoonstellingen,
demonstraties in alle sectoren.

De aan- en verkoop, impart- en export van goederen en dien-
sten.

De vennootschap mag deelnemen door inbreng, fusie, inschrij-
ving of anderszins in alle bestaande of op te richten vennoot-
schappen of ondernemingen met een gelijkaardig of aanver-
want voorwerp. Zij kan ook functies van bestuurder, commissa-
tis of vereffenaar van zulke vennootschappen uitoefenen, Zij
kan zich borg stellen voor verbintenissen van derden.

Buur; De duur van de vennootschap is onbepaatd,

245,15 EUR
(72524)

Kapitaat

Het kapitaal van de vennootschap bedraagt ACHTTIENDUI-
ZEND ZESHONDERD EURO (18.600,00 EUR). Het is verdeeld
in honderd zesentachtig gelijke aandelen die elk één/honderd
zesentachtigste van het kapitaal vertegenwoordigen.

Het kapitaal wordt onderschreven door de hiema volgende in-
brengen:

1. Inbreng in specién:

Door de heer Paulussen Gary, en mevrouw Rust Syviie wordt
er jeder een inbreng in geld gedaan van duizend euro (€
1.000,00), waarvoor ieder van hen tien aandelen ontvangt. De-
ze inbrengen worden volledig volstort. Ingevoige deze storting
is een bedrag van tweeduizend euro (€ 2.000,00) ter beschik-
king van de vennootschap, zoals blijkt uit het aangehechte
bankattest, afgeleverd door Argenta Spaarbank NV te 2018
Antwerpen op 18 aprit 2002.

2. Inbreng in natura: Door de echtelieden Paulussen-Rust wor-
den de beide nagemelde onroerende goederen ingebracht:

- deze eigendommen worden door partijen geschat op hun
waarde in volle eigendom, als volgt :

het eigendom Rodeborgstraat 61, Schoten op zesentachtigdui-
zend zevenhonderd euro (€ 86.700) en het eigendom Lage
Kaart 342, Brasschaat op honderd achtendertigduizend negen-
honderd euro (€ 138.900)

a) In ruil voor deze inbrengen’worden de volgende aandelen
toegekend:

aan de heer Paulussen Gary: drie&ntachtig aandelen

aan mevrouw Rust Sylvie: drientachtig aandelen

b) Daarenboven zullen de volgende bedragen ingeschreven
worden op een lopende rekening ten name van de oprichters:
op een lopende rekening ten name van Paulussen Gary: hon-
derd en vierduizend vijfhonderd euro (€ 104.500), op een lo-
pende rekening ten name van Rust Sytvie: honderd en vierdul-
zend vijfhonderd euro (€ 104.500)

Ingevolge deze Inbrengen in natura, vergoed als gezegd, en de
beschikbaarheid van deze eigendommen voor de vennoot-
schap, dienen deze aandelen beschouwd te worden als volledig

- volstort.

Verslag van de oprichters

Om te voldoen aan de bepaling van artikel 219 van het Wet-
boek van Vennootschappen hebben de oprichters een versiag
uitgebracht over de voorgenomen inbreng op achttien maart
tweeduizend en twee.

Versiag van de bediijfsrevisor.

In verband met de inbreng in natura werd een verslag opge-
maakt door de heer Roland Vos, handelend voor de 8VBA R.
Vos & Partners Bedrijfsrevisoren te Hasselt op vijftien maart

, tweeduizend en twee, welk versiag besluit wat volgt:

“Ondergetekende, 8.V.B.4. R. Vos & Partners Bedrijfsrevisoren
vertegenwoordigd door de heer Roland Vos, bednjfsrevisor te
3511 Hasselt, Galgenbergstraat 42 verklaart overeenkomstig
de bepalingen van artikel 120bis van de Gecodrdineerde Wet-
ten op de Handelsvennootschappen met inachtname van de
norm voorgeschreven door het Instituut der Bedrijfsrevisoren
dal:

1. de beschrijving van de inbreng aan de normale vereisten van
duidelijkheid en nauwkeurigheld beantwoordt:

2. da toegepast methode van waardering van de onroerende
goederen voor een inbrengwaarde van tweehonderd vijfentwin-
tig duizend zeshonderd (225.600,-) Euro in het geheel geno-
men en onder nomale omstandigheden bedrijfseconomisch
verantwoord is;

3. de inbreng belast is met een hypothecaire inschrijving in
twee rang voor 37.184,03 Euro in hoofdsom en voor 3.718,40
Euro in bijhorigheden ten voordele van de N.V. Argenta Spaar-
bank, Zij behoudt het recht van zaakgevolg: = +

4. de inbreng belast is met een hypothecaire inschrijving voor
138.820,37 Euro in hoofdsom en voor 13.882,04 Euro in bijho-
figheden ten voordele van de N.V. Argenta Spaarbank. Zij be-
houdt het recht van zaakgevolg;

5. da als tegenprestatie verstrekte vergoeding, voor de inbreng
in natura rechtmatig en billijk is, en bestaat uit:

- drieéntachtig (83) volledig volstortte aandelen zonder aandui-
ding van nominale waarde en een inboeking op lopende reke-
ning van honderd en vier duizend vijfhonderd (104.500) Euro op
naam van de heer Gary Paulussen;

261
