Mod PDF 19.01

Copie a publier aux annexes au Moniteur belge
apres dépdt de l'acte au greffe

Réservé
au LJ

Déposé

Moniteur *22353007*
belge

o
>
o
Q
.
)
@
#
c
3
=
>
s
wn
o
x<
©
Cc
Cc
<x
1
a
a
So
a
N
o
S
S
3
a
'
3
&
a
n
2
o
©
£
n
x=
o
2
>
o
a
~
o
x=
5
c
o
D
&
=

19-08-2022

Greffe

  

 

N° d'entreprise : 0403203264
Nom
(en entier): INTERNATIONAL POWER

(en abrégé) :

Forme légale : Société anonyme

Adresse complete du siége Boulevard Simon Bolivar 34
: 1000 Bruxelles

Objet de I'acte : DEMISSIONS, NOMINATIONS

ll résulte du procés-verbal des résolutions écrites prises par l’actionnaire unique du 01 juillet 2022

wl.

5. Démission d’un administrateur

L’actionnaire unique prend acte de la démission de Monsieur Jan Sterck en qualité d’administrateur
de la Société, prenant effet a partir de ce jour et souhaite le remercier pour ses services rendus.

6. Renouvellement du mandat/Nomination des administrateurs et rémunération

L’actionnaire unique décide, conformément a la proposition du conseil d’administration, de
renouveler les mandats de Monsieur Jean-Paul Deffontaine et Madame Florence Verhaeghe, en
qualité d’administrateur de la Société. Le précédent mandat prend fin a partir de ce jour.
L’actionnaire unique décide, conformément a la proposition du conseil d’administration, de nommer
Madame Mireille Van Staeyen, domicilié a Marcel de Backerstraat 2a, 2180 Ekeren, Belgique, en
qualité d’administrateurs de la Société.

Les mandats des administrateurs prennent effet a partir de ce jour et viendront a échéance lors de |’
assemblée générale ordinaire qui délibérera sur les comptes annuels de |l’exercice social cléturé au
31 décembre 2024.

L’actionnaire unique décide que les mandats ne seront pas rémunérés.

7. Procuration pour les formalités

L’actionnaire unique décide de donner procuration, avec faculté de délégation, a chaque employé de
l'étude des notaires « Van Halteren », ayant son siége a 1000 Bruxelles, rue de Ligne 13, a qui elle
donne pleins et entier pouvoirs pour agir en son nom et pour la représenter, qualitate qua, auprés du
registre des personnes morales, des guichets d’entreprises et l’office TVA compétent a l’effet d’y
effectuer toutes les démarches nécessaires pour effectuer toute publication dans le Moniteur Belge,
et en conséquence, remplir, signer et déposer tous formulaires, y compris les formulaires de
publication, faire toutes déclarations et en général, faire tout ce qui serait nécessaire ou utile a I’
exécution du présent mandat, en promettre au besoin ratification.

wl.

Pour extrait conforme.

(signé) Samuel WYNANT

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
