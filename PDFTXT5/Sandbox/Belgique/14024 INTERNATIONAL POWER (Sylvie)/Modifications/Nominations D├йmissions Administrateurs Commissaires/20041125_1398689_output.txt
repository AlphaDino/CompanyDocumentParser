 
   
   

 

FEES In de bijiagen bij het Belgisch Staatsblad bekend te maken kopie
na neerlegging ter griffie van de akte

ace

*04161869
|

eeees ne tenes ween ewe eet nene echt nee chen ne te eet 8 ne rere ee tee chee

‘ Benaming : SUEZ-TRACTEBEL

i Rechisvorm : Naamloze vennootschap
Zeta!’ Troonplem 1 - 1000 Brussel
: Ondeinemingsnr. 0403203264

: Voorwery akte: STATUTAIRE BENOEMING ; Benoeming van een bestuurder
OVERDRACHT VAN BEVOEGDHEDEN

  
       
   

Voor-
behouden
aan het
Belgisch
Staaisblad

 

i
‘
'
'
t
'
'
‘
‘
i

: De bijzondere algemene vergadering, ter zitling van 25 oktober 2004, heeft beslist het aantal bestuurders:
! vast te stellen op zeventien en in die hoedanigheid te benoemen de heer Yves de GAULLE, wonende 8 rue!
" @Aguesseau te F-92100 Boulogne Billancourt (Frankryk). Deze opdracht zal een einde nemen na afloop van,
: de gewone algemene vergadering van 2007 :
: By schrifteliik bestuit dd 25 oktober 2004 heefi de raad van bestuur hem de machten tot het dagelyks!
: bestuur toegekend dat vooriaan Is vastgesteld als volgt *

: 1} Dageliks bestuur van de vennootschap

: Ongeacht de algemene vertegenwoordiging van de vennootschap In atle handelingen door twee’
: { bestuurders van de vennootschap krachtens artikel 13 van de statuten, delegeert de Raad van Bestuur de!
: vertegenwoordiging van de vennootschap inzake het dagelyks bestuur aan de heren :

i Gérard MESTRALLET Voorzitter
; Jean-Pierre HANSEN Gedelegeord bestuurder :
i. Yves de GAULLE Bestuurder i
ht Emmanuel van INNIS Bestuurder
i Gérard LAMARCHE Bestuurder

1 die allen twee aan twee handelen. }

2} Dagelyks bestuur van de divisies :
Bovendien wordt, in het kader van de divisie waaronder zij ressorferen, de vertegenwoordiging van de!
vennootschap inzake het dagelyks bestuur van de betrokken divisie gedelegeerd aan :

1, Voor de divisie EGI-
tt (i) Dhr Dirk BEEUWSAERT, alsook aan
(ii) Mevr. Micheline BOSSAERT
HH. Phihp DE CNUDDE
Derrick GOSSELIN
Eric KENIS }
: Henri MEYERS
! Jacques NIEUWLAND ‘
} Nicolas TISSOT
Oe personen sub (ii) zulten telkens gezamenlik optreden met de heer Dirk BEEUWSAERT of met een}
| bestuurder die beschikt over de vertegenwoordiging nzake dagelijks bestuur van de vennootschap; de heer’
: Duk BEEUWSAERT zal eveneens gezamenlyjk mogen optreden met één van de voornoemde bestuurders

 

Bijlagen bij het Belgisch Staatsblad -25/11/2004- Annexes du Moniteur belge

;

Op de jaatste ble van Luk B vermolden . Recto * Naam en hoedanigheid¢ van de instrumenterende notans, hetzy van de persofe)n{en}
bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen

Verso Naam en handtekening
 

Voor-
behouden
aan het
Beigisch

Staatsblad

Bijlagen bij het Belgisch Staatsblad -25/11/2004- Annexes du Moniteur belge

‘
'
'

 

 

Luik B - Vervolg

en, voor de divisie Engineering :

(0) Dhr Paul RORIVE, alsook aan

(un) HH. Lours DELCROIX
Bernard GILLIOT '

, Didier MALLIEU ‘

Chnstian PIERLOT
André TERLINDEN
Jean VAN VYVE
De personen sub (11) zulien telkens gezamenlijk optreden met de heer Paul RORIVE of met een bestuurder ’
die over de vertegenwoordiging beschikt inzake het dageljks bestuur van de vennootschap; de heer Paul
RORIVE zal eveneens gezamenlijk mogen optreden met één van de voornoemde bestuurders.

C Permanente functionele mandaten (uittreksel)

Jurdische aangelegenheden ‘

De documenten met betrekking tot het beheer van juridische aangelegenheden gericht aan de‘
administraties, advocaten, notarissen, ministenele ambtenaren, marktautoriteiten en marktcontrole-instanties
‘worden ondertekend door de heren Guido De Clercq, Pierre-Jean Hislaire of Patrick van der Beken
gezamenlyk optredend met de heren Gérard Mestrallet of Jean-Pierre Hansen of Yves de Gaulle of Emmanuel
van Innis of Gérard Lamarche of Dirk Beeuwsaert of Paul Rorive.

Fiscale aangelegenheden
De documenten met betrekking tot het beheer van de fiscale aangelegenheden, in het byzonder de
belastingaangiften, met uitzonderng van de aangiften inzake BTW en roerende voorheffing, worden
onderiekend door de heren Guido Vanhove of Michel Authler of Jean Schellekens gezamenlijk optredend met.
_de heren Gérard Mestrallet of Jean-Pierre Hansen of Yves de Gaulle of Emmanuel van Innis of Gérard’
“ Lamarehe of Dirk Beeuwsaert of Paul Rorive :
Immobihén
: De huurcontracten, contracten Inzake vruchtgebruik, leasing van onroerende goederen en gelijkaardige.
‘ contracten zowel als huurder als als verhuurder, alsook de verwervingen en vervreemdingen van onroerende
goederen, worden ondertekend door de heren Alexis van den Abeele of Pierre-Jean Hislatre, gezamenlijk
optredend met de heren Gérard Mestrallet of Jean-Pierre Hansen of Yves de Gaulle of Emmanuel van Innis of
Gérard Lamarche

Human resources
De tewerksteflngscontracten en hun aanhangsels, de ontslagdaden aan het personeel, de toekenning
van om het even welke bezoldiging, buiten het contractueel of statutair kader, ongeacht de vorm waann het
_ Moet worden ultgekeerd, en de overige documenten die de ondememing binden ten aanzien van het personeel
worden ondertekend door de heren Marc Janssens de Varebeke of Jean Perpéte of Paul-Emile Timmermans .
gezamenlyk optredend met de heren Gérard Mestrallet of Jean-Pierre Hansen of Yves de Gaulle of Emmanuel
van Innis of Gérard Lamarche of Jacques Nieuwland.

'

Deze delegatie vernietigt en vervangt alle voorheen toegekende bevoegdheden.

E. van Innis J.P, Hansen
Bestuurder Gedelegeerd bestuurder

 

 

Op de laatste hiz van Luik B vermelden Recto . Naam en hoedanigheid van de instrumenterende notaris, hetzy van de perso(o)n(en)

bevoegd de rechtspersoon ten aanzien van derden te veitegenwoordigen
Verso Naam en handtekening
 

 

 
  
 
     
   
 

pause
2, (a Rie item Copie qui sera publiée aux annexes du Moniteur belge
aprés dépét de l'acte au greffe

cai

*04161870*
1

  

Résorvé
aut
Moniteur

belge

 

 

! pénomination: SUEZ-TRACTEBEL
i Fonne juridique: Société Anonyme
Siege Place du Tréne1 - 1000 Bruxelles
N° d'entreprise : 0403203264

: Objet de Pacte: NOMINATION STATUTAIRE : Nomination d'un administrateur
DELEGATION DE POUVOIRS

ence este see even een eseenemenwer nent

Lassembiée générale spéciale en sa séance du 25 octobre 2004 a décidé de porter le nombre:
' d'adminstrataurs a dix-sept et de nommer en cette qualité Monsieur Yves de GAULLE, demeurant 8 rue:
! d'Aguesseau 4 F-92100 Boulogne Billancourt (France). Ce mandat viendra & échéance a lissue de l'assemblée!
i générale ordinaire de 2007.

‘Par sa résolution exprimée par écrit en date du 25 octobre 2004, le consell d'administration Iu a attripué les.
: pouvoirs de gestion journaliére qui s‘énonce dorenavant comme sult :

1) Gestion journaliére de la société

Sans préjudice de la représentation générale de la société en tous actes par deux administrateurs de la
société en vertu de Particle 13 des statuts, le Conseil d'administration délégue la représentation de fa société en,
ce qui concerne la gestion journaliére a MM :

Gérard MESTRALLET Président
; Jean-Pierre HANSEN Administrateur délégué
t! Yves de GAULLE Admunistrateur :
:} Emmanuel van INNIS Administrateur :
‘Gérard LAMARCHE Administrateur :

agissant deux a deux.

2) Gestion journaliére des divisions
Par ailleurs, dans fe cadre de la division a laquelle tls/elles ressortissent, la représentation de la société en}
+ ce qui concere la gestion journaliére de [a division concermée est déléguée . :
Pour la division EGI : :
a M. Dirk BEEUWSAERT, ainsi qu'a
(ii) Mme Micheline BOSSAERT
: MM) Philip DE CNUDDE
: Derrick GOSSELIN
Enlc KENIS
: Henn MEYERS
: Jacques NIEUWLAND
Nicolas TISSOT.
Les personnes sous (i!) agiront chacune d’elles conjointement avec M Dirk BEEUWSAERT ou un’
administrateur disposant de la représentation en matiére de gestion journaliére de la société ; M Dirk’
, BEEUWSAERT pourra également agir conjointement avec un desdits administrateurs.

Bijlagen bij het Belgisch Staatsblad -25/11/2004- Annexes du Moniteur belge

i

Mantionner sur la derniere page du Yolet 8 Aurecto Nom et qualité du notaire instrumentant ou de ia personne ou des personnes
ayant pouvoir de représenter la personne morale a | egard des ters

Au verso . Nom et signature

‘
:
‘
‘
i
i
'
:
'
t
'
‘
;
LL
 

   
   
 

Réserve Volet B - Sule
au ~ ~
Moniteur et, pour la division Engineering :
belge a () M. Paul RORIVE, ainsi qué
(1) MM. Louis DELCROIX
Bernard GILLIOT
' Didier MALLIEU
: . Christian PIERLOT
: ii André TERLINDEN
‘ Jean VAN VYVE.
; Les personnes sous (j1} agiront chacune d’elles conjointement avec M. Paul RORIVE ou un administrateur
! disposant de la représentation en matiére de gestion journaliére de ia société ; M. Pauli RORIVE pourra
également agir conjointement avec un desdits administrateurs.

 

C. Mandats fonctionne!s permanents (extrait)

:  Matiéres jundiques
Les documents concernant la gestion des affaires jundiques adressés aux administrations, avocats,
‘ ‘notaires, officiers ministériels et autontés de marché et de contréle des marchés sont signés par MM. Guido De,
! Clereg, Prerre-Jean Hislaire ou Patrick van der Beken agissant conjointement avec MM. Gérard Mestrallet ou’
: Jean-Pierre Hansen ou Yves de Gaulle ou Emmanuel van [nnis ou Gérard Lamarche ou Dirk Beeuwsaert ou
* Paul Rorive.

:
‘
i

Matiéres fiscales

‘Les documents concermant la gestion des affaires fiscales, notamment Jes déclarations fiscales, 4
exception des déclarations en matiére de TVA et de précompte mobiher sont signés par MM. Guido Vanhove :

‘ou Michel Authier ou Jean Schelfekens agissant conjointement avec MM Gérard Mestrallet ou Jean-Pierre ‘

Hansen ou Yves de Gaulle ou Emmanuel van Innis ou Gérard Lamarche ou Dirk Beeuwsaert ou Paul Rorive

 

 

 

 

Matiéres immoblliéres
Les contrats de bail, d'usufruit, de leasing immobilier et analogues, tant en qualité de preneur que de:
‘donneur, ainsi que les acquisitions et allénations immobiliéres sont signés par MM. Alexis van den Abeele ou |
Pierre-Jean Hislaire, agissant conjointement avec MM. Gérard Mestrallet ou Jean-Pierre Hansen ou Yves
de Gaulle ou Emmanuel van Innis ou Gérard Lamarche

‘Ressources humaines
Les contrats et les avenants aux contrats d'emploi, les actes de Iicenciement du personnel, l'actroi de toute
_témunération, hors cadre contractuel ou statutaire, a verser sous quelque forme que ce soit et les autres
‘documents engageant la société envers le personnel sont signés par MM Marc Janssens de Varebeke ou Jean
Perpéte ou Paul-Emile Timmermans agissant conjointement avec MM Gérard Mestrallet ou Jean-Pierre |
_ Hansen ou Yves de Gaulle ou Emmanuel van Innis ou Gérard Lamarche ou Jacques Nieuwland

La présente délégation annule et remplace tous pouvoirs attnbués antérieurement

E van Innis J.P. Hansen
Administrateur Administrateur délégué

 

Bijlagen bij het Belgisch Staatsblad -25/11/2004- Annexes du Moniteur belge

 

   

 

Mentionner sur ja derniere page du VoletB* Au recto Non) et qualité du notaire instrurmentant ou de la persome ou des personnes
ayant pouvoir de représenter la personne morale 4 légard des ters

Auverso Nom ct signature
