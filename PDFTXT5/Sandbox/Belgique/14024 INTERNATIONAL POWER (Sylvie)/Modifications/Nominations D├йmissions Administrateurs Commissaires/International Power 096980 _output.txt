Mentionner sur la derniére page du Volet B:

 
   
   

MOD WORD 14,1

Copie a publier aux annexes du Moniteur belge
aprés dépét de l’acte au greffe

BRUXELLES

29 AVR. 201%

Greffe

 

  

0403203264

 

N° c’entreprise :
Dénomination

(en entier) : INTERNATIONAL POWER

(en abrégé) :
Forme juridique: Société Anonyme

Siége: Boulevard Simon Bolivar 34 - 1000 Bruxelles
(adresse compléte)

Objet(s) de !’'acte ‘NOMINATIONS STATUTAIRES

L'assemblée générale ordinaire, en sa séance du 22 avril 2014, a:

- renouvelé le mandat d'administrateur de M. Dirk Beeuwsaert pour un terme de trois ans. Ce mandat
viendra 4 échéance a issue de !'assemblée générale ordinaire de 2017 ;

- pris acte de la démission de leur mandat d’administrateur de MM. Philip De Cnudde, Geert Peeters et Guy
Richelle a la date du 24 mars 2014;

-nommé définitivement M. Pierre Guiollot, demeurant 2 rue Ramponneau a 78600 Mesnil Le Roi (France),
appelé provisoirement en qualité d'administrateur par le Conseil d'administration du 24 mars 2014 pour achever
le mandat de M. Geert Peeters. Ce mandat viendra a échéance a l'issue de l'assemblée générale ordinaire de
2016;

~nommé définitivement M, Jean-Paul Deffontaine, demeurant Avenue Hebron 98 4 1950 Kraainem, appelé
provisoirement en qualité d’administrateur par le Conseil d'administration du 24 mars 2014 pour achever le
mandat de M. Philip De Cnudde. Ce mandat viendra 4 échéance 4 l'issue de l'assemblée générale ordinaire de
2016;

-décidé de ne pas pourvoir au remplacement de M. Guy Richelle et de ramener ainsi le nombre
d'administrateurs a 3.

Les mandats d’administrateur ne sont pas rémunérés.
Le Conseil d'administration a reconduit M. Dirk Beeuwsaert en qualité d'administrateur délégué.

J.P. Deffontaine D. Beeuwsaert
Administrateur Administrateur délégué

Au recto : Nom et qualité du notaire instrumentant ou dela personne ou des personnes
ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature

 

 

 
