Mod 2.4

In de bijlagen bij het Belgisch Staatsblad bekend te maken kopie
na neerlegging ter griffie van de akte

IMAL

*07154308*

 
   

 

    
     

 

 : Ondememingsnr: 403203264
Benaming

wout) SUEZ-TRACTEBEL

 

t Rechtsvorm ; Naamfoze vennootschap
: Zetel: 1000 Brussel, Troonplein 1

Onderwerp akte : WIJZIGING VAN HET DOEL VAN DE VENNOOTSCHAP -
STATUTENWIJZIGING

Uit een proces-verbaal verleden voor Meester Damien HISETTE, geassocieerde notaris te Brussel, op 27°
september 2007, -Gereglstreerd 5 blade(n) geen renvooi(en). Op het iste registratiekantoor Brussel. Op 8'
oktober 2007. Boek. 37, blad 73, vak 03. Ontvangen vijfentwintig euro (25 €. De Wn eerstaanwezend,
: Inspecteur (getekend) J.L Deschepper -, biijkt het dat:

wed

-* VOORAFGAANDE VERSLAGEN EN VERKLARINGEN *-

Overeenkomstig artikel 559 van het Wetboek van vennootschappen, heeft de raad van bestuur een versiag
opgesteld met de verantwoording van de wijziging van het doel van da vennootschap voorgestelde in de
agenda. Bij dat verslag wordt een staat van activa en passiva van de vennootschap per dertig juni tweeduizend
en zeven gevoegd.

De commissaris, de burgerlijke vennootschap onder de vorm van codperalieve vennoatschap met beperkte
aansprakelykheid "Deloitte Bedrijfsrevisoren’, waarvan de kantoren gevestigd zijn te 1050 Brussel, Louizalaan;
! 240, vertegenwoordigd door Laurent Boxus en Philip Maeyaert, bedrijfsrevisoren, heeft een verslag opgesteld:
; over die staat in toepassing van .../...

EERSTE BESLUIT.
De vergadering beslist artikel 3 van de statuten te wijzigen om het doel aan te vullen door toevoeging aan,
de eerste alinea van een derde lid dat luldt als volgt :
« - het nemen van belangen onder de vorm van aandeten of andere financiéle instrumenten, jn Belgié en in
alle andere landen :
(a) in alle vennootschappen die een soartgelijk of aanverwant doel hebben en
(b)in alte vennootschappen met financieel of gelijkaardig doel waarvan de activiteit nuttlg is voor de
ontwikkeling van zowel de vennootschap en haar dochterondernemingen als van alle andere vennootschappen
van de groep waarvan de vennootschap deel uitmaakt. »
wal
TWEEDE BESLUIT.
De vergadering beslist artikel 10 van de statuten te wijzigen door toevoeging van de volgende tekst na de
eerste zin van de tweeda alinea :
: «Deze vergaderingen, inclusief de beraadslagingen en de stemmingen, kunnen op voorstel van de voorzitter
‘ verlopen via elk telecommunicatiemiddel, onder meer mondeling of visueel, dat debatten iussen geografisch:
van elkaar verwijderde deelnemers mogelijk maakt. In dit geval wordt de vergadering geacht plaats te vinden op
. de zetel en worden alle deelnemende bestuurders geacht aanwezig te zijn.»
wh.
Voor eensluidend analytisch uittrekeel
Samen neergelegd: expeditie, volmachten, verslagen en bijgewerkte tekst der statuten.
{getekend) Damien HISETTE, geassocieerde notans te Brusset

Sb
=
o
2
g
3
S
5
5
g
x”
E
1
™
Q
Qo
g
So
=
~~
ioe)
N
1
sg
s
=
Oo
2
s
8
n
a
3
a
oo
=
o
~
3
a
i”
oO
G
=p
G3
=
=
mM

Op de laatste blz van Luik. 8 vermelden . Recto Naam en hoedanigheid van dei instrumenterende notaris, hetzy van de perso(o)n(en)
bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen

Verso ' Naam en handtekening.
Mentionner sur la derniare page du VoletB: Aurecto’ Nom et quallté du notaire instrumentant ou de ta personne ou des personnes.

WU

  

Bilagen bi het Belgisch Staatsblad -23/10/2007- Annexes du Moniteur belge

 

 

 

 

Mocs 2.0

Copie qui sera publiée aux annexes du Moniteur belge
aprés dépét de l'acte au greffe

 

*07154309*

 

N° d'entreprise : 403203264
! Dénomination

by enente). SUEZ-TRACTEBEL

 

Forme juridique - Société anonyme
Siége: 1000 Bruxelles, Place du Tréne 1

Objet de 'acte: MODIFICATION DE L’OBJET SOCIAL — MODIFICATIONS DES STATUTS
D’aprés un procés-verbal regu par Maltre Damien HISETTE, notaire associé a Bruxelles, le 27 septembre

2007, - Enregistré 5 rdle(s) sans renvoi(s). Au ter bureau de l'enregistrement de Bruxelles. Le 8 octobre 2007.
Vol. 37, fol. 73, case 03. Regu : vingt-cing euros (25 €). Le Receveur(s), (signé) J.L Deschepper -, il résulte que

: ode

i -" RAPPORTS ET DECLARATIONS PREALABLES *-

: Le conseil d’administration de la société a établi un rapport justifiant la modification de objet social
i proposée @ lordre du jour, confarmément a l'article 559 du Code des sociétés. A ce rapport est joint un état,
!  résumant la situation active et passive de la société arrété au trente juin deux mille sept.

Le commissaire, la société civile a forme de société coopérative a responsabilité limitée "Deloitte Réviseurs
d'Entreprises", dont les bureaux sont établis 4 1050 Bruxelles, avenue Louise 240, représentée par Messieurs
: ' Laurent Boxus et Philip Maeyaert, réviseurs d'entreprises, a établi un rapport sur Iedit état en application des
! « dispositions précitées.
be Un exemplaire de ces rapports restera ci-annexé.
PREMIERE RESOLUTION.
L'assamblée décide de modifier Particle 3 des statuts afin de campléter l'objet social en ajoutant au premier
: alinéa un troisiéme tiret libellé comme suit :
« - la prise de participations en actions ou autres instruments financiers, en Belgique et dans tous pays
:* étrangers :
(a) dans toutes soclétés dont l'objet est similaire ou connexe au sien et
(b) dans toutes sociétés a objet financier ou analogue dont l'activité est utile au développement tant de la
société et de ses filiales que de toutes autres sociétés du groupe dont fait partie fa société. »
; hae
':  SECONDE RESOLUTION.
:  L'assemblée décide de modifier l'article 10 des statuts en ajoutant aprés la premiére phrase du deuxiéme:
! + alinéa le texte sulvant:
« Ces réunions, en ce compris les délibérations et votes, peuvent, sur proposition de son président, étre,
‘ tenues via tout moyen de télécommunication, notamment oral ou visuel, qui permette des débats entre des
participants géographiquement éloignés. Dans ce cas, la réunion est réputée se tenir au siége social et tous les
: ! administrateurs participants y tre présents. » :
an :
Pour extrait analytique conforme.
Déposé en méme temps : expédition, procurations, rapports et coordination des statuts
(signé) Damien HISETTE, notaire associé 4 Bruxelles

ayant pouvoir de représenter la personne morale a l'égard des ters
Auyerso Nom et signature

 
