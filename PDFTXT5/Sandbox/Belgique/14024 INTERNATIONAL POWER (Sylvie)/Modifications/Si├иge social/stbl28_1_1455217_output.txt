 
 
  
 
   
 
   
   

  

Voor-
behoude

aan het
Belgisct
Staatsbla

bij het Belgisch Staatsblad - 03/05/2012 - Annexes du Moniteur belge

 

Bijla

Op de laat

  
 

 

Mod Word 14.1

In de bijlagen bij het Belgisch Staatsblad bekend te maken kopie
na neerlegging ter griffie van de akte

nN

 

BRUSSEL.
20 APR 2012

  
 

Griffie

  

| | Ondememingsnr: 0403203264
i Benaming i

: (voluit}: SUEZ-TRACTEBEL

(verkort)
Rechtsvorm : Naamloze vennootschap

Zetel: Troonplein 1 - 1000 Brussel
(volledig adres}

: Onderwerp akte : VERPLAATSING VAN DE ZETEL ;
OVERDRACHT VAN BEVOEGDHEDEN :

    
   
  

 

u
‘
«
:
1
4
t
t
'
b
.
‘
»
'
'
ty
vt
4
1
'

 

De raad van bestuur heeft ter zitting van 3 april 2012 beslist om de zete!l van de vennootschap te!
verplaatsér van 1000 Brussel, Troonplein 1, naar 1000 Brussel, Simon Bolivarlaan 34, dit met ingang van;
: 3 april 2012. :

De Raad van bestuur heeft ook de overdracht van bevoegdheden gewijzigd als volgt :

4) Dagelijks bestuur van de vennootschap . :

_ Ongeacht de algemene vertegenwoordiging van de vennootschap in alle handelingen door twee:
* bestuurders van de vennootschap krachtens artikel 13 van de statuten, delegeert de Raad van Bestuur de:
: vertegenwoordiging van de vennootschap inzake het dagelijks bestuur aan de heren ; :

: Eric KENIS,
' die allen twee aan twee handelen.

Dirk BEEUWSAERT Gedelegeerd bestuurder :
Philip DE CNUDDE Bestuurder :
Emmanuel van INNIS :
Jacques NIEUWLAND :

2) Bijzondere mandaten

: De Raad van bestuur heeft het operationele beleid van de vennootschap (inclusief het dagelikse beheer):
: toevertrouwd aan de Gedelegeerd bestuurder. :

: De Raad van bestuur delegeert aan de heren Dirk Beeuwsaert en Philip De Cnudde, die gezamenijk’
! optreden, de bevoegdheid om middels een algemeen reglement, bijzondere permanente zogenaamde:
: functionele” mandaten toe te kennen die het operationele beheer van de onderneming (inclusief het dagelijkse’
beheer) mogelijk maken. Dit reglement verduidelijkt het doe! en het financieel belang van de verbintenissen die.
, aldus worden toegekend. :
' 3) Permanente functionele mandaten {uittreksel) i
i Juridische aangelegenheden \
: De documenten met betrekking tot het beheer van juridische aangelegenheden gericht aan de,
| administraties, advocaten, notarissen, ministeriéle ambtenaren, marktautoriteiten en marktcontrole-instanties,
: worden ondertekend door de heren Jean-Paul Deffontaine, Patrick van der Beken of Peter Verbiest gezameniljk,
: optredend met de heren Dirk Beeuwsaert of Jacques Nieuwland of Eric Kenis of Philip De Cnudde of Guido:
| De Clereq, :

1
a
be
t
i
i
;
i
t
i
t
{
‘
:
:
i
i
ti
it
‘
;
;
:
i
r
rt
fi
'
:
:
i
:
i
:
'
:
:
‘
i
:
}
}
:
i
!
'
:
:
{
!
i
t
'
i
:
‘
t

ste blz. van Luik.B vermelden ; Recto ; Nant en hoedanigheid van de instrumenterende notatis, hetzij van de perso(o)n(en}
bevoegd de rechtspersoon ten aanzien van derden te vertegenwoordigen

Verso : Naam en handtekening.
 
  

  
   
   
      

Veoor-
behouden
aan het
Belgisch
Staatsblad

 

Fiscale aangelegenheden

De documenten met betrekking tot het beheer van de fiscale aangelegenheden, in het bijzonder de
belastingaangiften, met witzondering van de aangiften inzake BTW en roerende voorheffing, worden
ondertekend door de heren Jean-Claude Corbisier, Thierry van den Hove, Olivier Lamalle of mevrouw Florence

Verhaeghe gezamenlijk optredend met de heren Dirk Beeuwsaert of Jacques Nieuwland of Erio Kenis of Philip '
‘De Cnudde of Guido Vanhove. ‘

Immobilién

De huurcontracten, contracten inzake vruchtgebruik, leasing van onroerende goederen en gelijkaardige .
contracten zowel als huurder als als verhuurder, alsook de verwervingen en vervreemdingen van onroerende ‘
. goederen, worden ondertekend door de heren Xavier Aguirre, Jean-Louis Samson of mevrouw Hilde Somers, '
+ gezamenliik optredend met de heren Dirk Beeuwsaert of Jacques Nieuwland of Eric Kenis of Philip De Cnudde
of Patrick van der Beken of Jean-Paul Deffontaine.

Human resources

De tewerkstellingscontracten en hun aanhangsels, de ontslagdaden aan het personeel, de toekenning ,
‘van om het even welke bezoldiging, buiten het contractueel of statutair kader, ongeacht de vorm waarin het:
moet worden uitgekeerd, en de overige documenten die de onderneming binden ten aanzien van het personeel '
worden onderiekend door de heren fvan Duvillers, David Martens, Xavier Geels of mevrouw Brigitte Bocqué, '
gezamenilijk optredend met de heren Emmanuel van Innis of Dirk Beeuwsaert of Jacques Nieuwland of Eric
Kenis of Philip De Cnudde.

 

De énderhavige delegatie annuleert en vervangt alle eerdere andersluidende bepalingen.

Ph. De Cnudde D. Beeuwsaert
Bestuurder Gedelegeerd bestuurder

Bijlagen bij het Belgisch Staatsblad - 03/05/2012 - Annexes du Moniteur belge

Op de Jaatste blz. van ‘Luik B vermelden : Recto : Naam en hoedanigheid van de instrumenterende notaris, ‘hetzij van de perso(o)n(en)
bevoegd de rechtspersoen ten aanzien van derden te vertegenwoordigen

Verso : Naam en handtekening

 
 
 
 
 
  
     
  
 
 

MOD WORD 11.1

 

Copie a publier aux annexes du Moniteur belge
aprés dépé6t de l'acte au greffe

Réservé:
< N pruxcues

20 ApR a

 

: N° d'entreprise : 0403203264
Dénormination

(enentiey: SUEZ-TRACTEBEL
(en abrégé) :

Forme juridique: Société Anonyme

Siége: Place du Tréne 1 - 1000 Bruxelles !
' (adresse compléte}

  

Objet(s} de ’acte :TRANSFERT DU SIEGE SOCIAL
ii DELEGATION DE POUVOIRS :
: Le conseil d'administration en sa séance du 3 avril 2012 a décidé de transférer le siége social de la société,
i de la place du Tréne 1 4 1000 Bruxelles vers le boulevard Simon Bolivar 34 a 4000 Bruxelles, avec effet au
+: 3 avril 2012. :

Le conseil a également arraété la délégation de pouvoirs comme suit «

1) Gestion journaliére de la société :

: Sans préjudice de la représentation générale de la société en tous actes par deux administrateurs de la:
: société en vertu de article 13 des statuts, le Conseil d'administration délégue la représentation de la société en;
: ¢e qui concerne la gestion journaliére 4 MM : :

Dirk BEEUWSAERT Administrateur délégué :
Philip DE CNUDDE Administrateur :
Emmanuel van INNIS

1

Jacques NIEUWLAND :
Eric KENIS :
agissant deux a deux, .

2) Mandats spéciaux

i

\

: ' Le Conseil délégue 4 l'administrateur délégué la conduite opérationnelle (en ce compris fa gestion:
1 : Journaliére) de la société de maniére non exclusive, :
a ‘
i : Le Conseil délégue 4 MM. Dirk Beeuwsaert et Philip De Cnudde, agissant deux a deux, le pouvoir de!
i | conférer par un réglement général, des mandats spéciaux permanents dits « fonctionnels » permettant la:
}. gestion opérationnelle de la société (en ce compris la gestion journaliére). Ce réglement précise l'objet et:
1: Timportance financlére des engagements ainsi autorisés. - :
i 3) Mandats fonctionnels permanents (extrait)

Bijlagen bij het Belgisch Staatsblad - 03/05/2012 - Annexes du Moniteur belge

Matiéres juridiques .
: Les documents concemant la gestion des affaires juridiques adressés aux administrations, avocats,'
| notaires, officlers ministériels et autorités de marché et de contréle des marchés sont signés par MM, Jean-Paul:
: Deffontaine, Patrick van der Beken, Peter Verbiest agissant conjointement avec MM. Dirk Beeuwsaert oui

. Jacques Nieuwland ou Eric Kenis ou Philip De Cnudde ov Guido De Clereq.

Mentionner sur la derniére page du VoletB: Aurecto ; Nom et qualité du notaire instrumentant ou de ja personne ou des personnes
ayant pouvoir de représenter la personne morale al'égard des tiers

Au verso : Nom et signature
   
  

Réservé Volet B- ~ Suite

au |i oo wee noes sso seens - TT estes sees
Moniteur |: Matiéres fiscales :

Les documents concernant la gestion des affaires fiscales, notamment les déclarations fiscales, 4°
‘l'exception des déclarations en matiére de TVA et de précompte mobilier sont signés par MM. Jean-Claude -
- Corbisier, Thierry van den Hove, Olivier Lamalle ou Mme Florence Verhaeghe agissant conjointement avec ,
: MM. Dirk Beeuwsaert ou Jacques Nieuwland ou Eric Kenis ou Philip De Cnudde ou Guido Vanhove.

     

Matiéres immobiliéres
Les contrats de bail, d'usufruit, de leasing immobilier et analogues, tant en qualité de preneur que de’
donneur, ainsi que les acquisitions et aliénations immobiliéres sont signés par MM. Xavier Aguirre, Jean-Louis :
Samson ou Mme Hilde Somers, agissant conjointement avec MM. Dirk Beeuwsaert ou Jacques Nieuwland ou:
: Eric Kenis ou Philip De Cnudde ou Patrick van der Beken ou Jean-Paul Deffontaine.
Ressources humaines
Les contrats et les avenants aux contrats d'emploi, les actes de licenciement du personnel, f'octroi de toute ;
-rémunération, hors cadre contractuel ou statutaire, A verser sous quelque forme que ce soit et les autres
documents engageant la société envers le personnel sont signés par MM. Ivan Duvillers, David Martens, Xavier ,
. Geels ou Mme Brigitte Bocqué, agissant conjointement avec MM. Emmanue! van Innis ou Dirk Beeuwsaert ou
Jacques Nieuwland ou Eric Kenis ou Philip De Cnudde.

Cette délégation annule et remplace toute résolution antérieure contraire.

Ph. De Cnudde D. Beeuwsaert
Administrateur Administrateur délégué

Bijlagen bij het Belgisch Staatsblad - 03/05/2012 - Annexes du Moniteur belge

  

Mentionner sur la derniére page du Vole B: Aurecte: Nomet et qualité d du netaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter la personne morale a l'égard des tiers

Au verso : Nom et signature
