 

    
   

 

au

0
oh
o
we
:
5
3
3
*%
5
:
W
S
S
Q
S
=
aq
‘
3
8
£
a
3
a
a
o
a
~
3
a
=
‘5
5
2
i>
a

    

Monites
belge

  

WRcu 4 Copie qui sera publiée aux annexes du Moniteur belge

“nen

 

 

23 -05 285,

 

Greffe

 

Dénomination: SUEZ-TRACTEBEL
Forme jundique Société anonyme
Siege Place du Tréne, 1a 1000 Bruxelles
N° d'entreprise 0403.203 264
Objet de |'acte: SCISSION PAR ABSORPTION - SOCIETE BENEFICIAIRE

Extrait du procés-verbal dressé par les Notaires Jean-Philippe Lagae et Joan-Luc Indekeu, a
Bruxelles, le 27 avril 2005, enregistré dix-neuf réles, trois renvois au 2e bureau de !'Enregistrement
de Forest le vingt-huit avril 2005, vol 29 folio 78 case 20. Regu vingt-cing euros (€ 25). L'Inspecteur
principal a.i. (sigaé) NATHALIE BRAUNS.
1. Liassemblée générale extraordinaire a approuvé la scission de la société anonyme DISTRIHOLD,
ayant son siége social 4 1000 Bruxelles, Placc du Tréne |, registre des personnes morales numéro
0476.776.873, par ie transfert, suite 4 sa dissofution sans liquidation, de l'intégralité de son
patrimoine, activement et passivement, a ses actionnaires, la société anonyme SUEZ-TRACTEBEL.
ct la société coopérative 4 responsabilité limitée PUBLIGAZ, prorata participationis, sans que la
société anonyme SUEZ-TRACTEBEL ne devienne associée de la société coopérative a
responsabilité limitée PUBLIGAZ ou que la société coopérative 4 responsabilité limitée PUBLIGAZ
ne devienne actionnaire de la société anonyme SUEZ-TRACTEBEL.
Le commissaire conclut son rapport comme suit:
«La scission de DISTRIHOLD, sans constitution de sociétés nouvelles, consiste en |e transfert de
Vintégralité du patrimoine de celle-ci, sur la base dune situation active et passive arrétée au 31
décembre 2004, au profit de ses actionnaires (Suez-Tractcbel et Publigaz) au prorata des
participations détenues par ceux-ci, par suite de la dissolution sans liquidation de DISTRIHOLD, ce
qui entraine un apport de patrimoine en nature 4 SUEZ-TRACTEBEL.
Tl aura été proposé aux actionnaircs de DISTRIHOLD de faire usage de la possibilité offerte par
Particle 736 §5 du Code des Sociétés de répartir les actions et parts des sociétés bénéficiaires (Suez-
Tractebel et Publigaz) de maniére non-proportionnelle par rapport & leurs participations respectives
dans le capital de DISTRIHOLD. Cette répartition proposée qui devra étre adoptée a 'unanimité des
actionnaires de DISTRIHOLD consiste & attribucr 4 SUEZ-TRACTEBEL 100% de la rémunération
des partics du patrimoine qui lui sont transférées et 0% A PUBLIGAZ De méme, il sera attribué &
PUBLIGAZ 100% de la rémunération des parties du patrimoine qui lui sont transférée et 0% A
SUEZ-TRACTEBEL.
SUEZ-TRACTEBEL a cnsuite constaté que, en exécution de |’article 740 §2 du Code des Sociétés,
aucune action nouvelle de SUEZ-TRACTEBEL ne pouvait étre crééo, ni remise 4 son propre profit.’
{1 ne sera dés lors pas procédé 4 une augmentation de capital de SUEZ-TRACTEBEL, ni d’ailleurs,
par identité de motifs en PUBLIGAZ
Au terme de nos travaux de contréle, nous sommes d’avis que :
a) L’opération a été contrélée conformément aux normes édictées par |’Institut des Reviseurs
@ Entreprises en matiére d’apports en nature et que l’organc de gestion de Ja société
bénéficiairc de l’ apport cst responsable de l’évaluation des biens apportés ;
b) —_ La description de chaque apport en nature répond a des conditions normales de précision et de
clarté ;
c) Les modes d’évalnation de apport en nature retenus par Ics parties sont raisonnables ct non-
arbitraires,

Bee Ste a OE na PUL she CU Gow pS felts
ar la persenpr morale d Peyard des fare

  

 
 

  

PNUWoLL Go

Lib ole MeO Tat

 
 

Moniteur

beige

()
2p
QO
Oo
g
x
a
5
3
g
Ps
5
1
»
2
2
a
<
8
=
aq
t
3
2
og
oS
8
n
a
oO
2
&b
®@
mM
3B
a
=
5
8
2
im
fan}

 

7
Volet B - Sutte

Nous croyons enfin utile de rappeler que notre mission ne consiste pas 4 nous prononcer sur le
caractére légitime et équitable de !’opération, sur la valeur de apport ou de la compensation
attribuée en contrepartie. »

2. Lassemblée générale extraordinaire a approuvé la scission de la société anonyme FLUXHOLD,
ayant son siége social A 1000 Bruxelles, Place du Tréne 1, registre des personnes morales numéro
0476.776.972, par le transfert, suite 4 sa dissolution sans liquidation, de Sintégralité de son
patrimoine, activement et passivement, a ses actionnaires, la société anonyme SUEZ-TRACTEBEL
et la société coopérative a responsabilité limitée PUBLIGAZ, prorata participationis, sans que la
société anonyme SUEZ-TRACTEBEL ne devienne associée de Ia société coopérative a
responsabilité limitée PUBLIGAZ ou que la société coopérative a responsabilité limitée PUBLIGAZ
ne devienne actionnaire de la société anonyme SUEZ-TRACTEBEL."

Le commissaire conclut son rapport comme suit:

« La scission de FLUXHOLD, sans constitution de sociétés nouvelles, consiste en le transfert de
\’intégralité du patrimoine de celle-ci, sur la base d’une situation active et passive arrétée au 31
décembre 2004, au profit de ses actionnaires (Suez-Tractebel et Publigaz) au prorata des
participations détenues par ceux-ci, par suite de la dissolution sans liquidation de FLUXHOLD, ce
qui entraine un apport de patrimoine en nature 4 SUEZ-TRACTEBEL.

It aura été proposé aux actionnaires de FLUXHOLD de faire usage de la possibilité offerte par
Particle 736 §5 du Code des Sociétés de répartir les actions et parts des sociétés bénéficiaires (Suez-

Tractebel et Publigaz) de manitre non-proportionnelle par rapport 4 leurs participations respectives :
dans le capital de FLUXHOLD. Cette répartition proposée qui devra étre adoptéc 4 l’unanimité des |

actionnaires de FLUXHOLD consiste a attribuer 4 SUEZ-TRACTEBEL 100% de la rémunération

des parties du patrimoince qui {ui sont transférées et 0% 4 PUBLIGAZ. Do méme, il sera attribué a.

PUBLIGAZ 100% de la rémunération des parties du patrimoine qui lui sont transférée et 0% A

SUEZ-TRACTEBEL.

SUEZ-TRACTEBEL a ensuito constaté que, en exécution de Particle 740 §2 du Code des Sociétés,

aucune action nouvelle de SUEZ-TRACTEBEL ne pouvait étre créée, ni remise 4 son propre profit.

Tl ne sera dés lors pas procédé & une augmentation de capital de SUEZ-TRACTEBEL, ni d’ailleurs,

par identité de motifs en PUBLIGAZ.

Au terme de nos travaux de contréle, nous sommes d’avis que :

a) _ L’opération a été contrélée conformément aux normes édictées par 1’Institut des Reviseurs
d’Entreprises en matiére d’apports cn nature et que Vorgane de gestion de la société
bénéficiaire de l’apport est responsable de 1’ évaluation des bicns apportés ;

b) —_ La description de chaque apport en nature répond a des conditions normales de précision ct de
clarté ;

c) Les modes d’évaluation de l’apport en nature rctenus par les parties sont raisonnables et non-
arbitvaires.

Nous croyons enfin utile de rappeler que notre mission ne consiste pas 4 nous prononcer sur le

caractére légitime et équitable de l’opération, sur Ja valeur de apport ou de la compensation

attribuée en contrepartic. »

3. Le capital social de la société SUEZ-TRACTEBEL n'a pas été augmenté.

Sur le plan du droit des sociétés et du droit commercial, la scission prendra effet au jour des

assemblées décidant Ja scission. Comptablement, la scission prendra effet au premier janvier deux

mille cing, a 7éro heure.

POUR EXTRAIT ANALYTIQUE CONFORME

Signé: Jean-Philippe Lagae, Notaire

Déposés en méme temps: une expédition, deux procurations, le rapport du conscil d'admmistration

(art 602 du Code des sociétés), les rapports du commissaire (art 602 du Code des sociétés)

2 ONO ST GUUS CU NG. LISI Ue tly OU Me I redid Ob Gat paber ee
eyant nouver de reorésenter la parsonne morte a legard des ters

Mon Gt ony aul

 
 
  
   
      

behouden
aan het
Belgisch
Staatsblad

Biylagen bij het Belgisch Staatsblad -24/05/2005- Annexes du Moniteur belge

 

hs eee Inde bijlagen by het Belgisch Staatsblad bekend te maken kopie
na neerlegging ter griffie van de akte

WV

*05072415*

—______—__}
Benaming: SUEZ-TRACTEBEL
Rechisvorm: Naamloze vennootschap
Zetel. Troonplein 1 te 1000 Brussel
Ondernemingsrir. 0403.203.264
Voorwerp akte: SPLITSING DOOR OVERNEMING - VERKRIIGENDE VENNOOTSCHAP

Uittreksef uit het proces-verbaal opgesteld door Notarissen Jean-Philippe Lagae en Jean-Luc

Indekeu, te Brussel, op 27 april 2005, geregistreerd negentien bladen drie renvooien op het 2°

Registratiekantoor van Vorst op achtentwintig april 2005, bock 29, blad 78, vak 20. Ontvangen

vijfentwintig euro (€ 25). De Ea. Inspecteur a.i. (getekend) NATHALIE BRAUNS.

1. De vergadering keurt de splitsing goed, conform het splitsingsvoorstel, van de naamloze

vennootschap DISTRIHOLD, met vennootschapszetel te 1000 Brussel, Troonplein 1,

rechtspersonenregister nummer 0476,776.873, door de overgang, tengevolge van haar ontbinding

zonder vereffening, van haar gehcle vermogen, zowel de rechten als de verplichtingen, op haar
aandeelhouders, de naamloze vennootschap SUEZ-TRACTEBEL cn de codperatieve vennootschap
met beperkte aansprakelijkhcid PUBLIGAS, prorata participationis, zonder dat de naamloze
vemnootschap SUEZ-TRACTEBEL vennoot wordt van de codperatieve vennootschap met beperkte
aansprakelijkheid PUBLIGAS of dat de codpceratieve vennootschap met beperkte aansprakelijkheid

PUBLIGAS aandeelhouder wordt van de naamloze vennootschap SUEZ-TRACTEBEL.

De commissaris besluit zijn verslag ais volgt:

"De splitsing van DISTRIEOLD, zonder oprichting van nieuwe vennootschappen, betreft de

integrale vermogensoverdrachl, op basis van de boekhoudkundige staat van activa en passiva per 31

december 2004, ten gunste van haar aandcelhouders (Suez-Tractcbel en Publigas) volgens hun

prorata participatie, gevolgd door de ontbinding zonder vereffening van DISTRIHOLD, hetgeen
aanleiding geeft tot een inbreng in natura in SUEZ-TRACTEBLL.

Er zal aan de aandeelhouders van DISTRIHOLD worden voorgesteld om gebruik te maken van de

mogelijkheid die artike! 736,§5 van het Wetboek van Vennootschappen biedt om dc aandelen en

deelbewijzen van de verkrijgende vennootschappen (Suez-Tractebel en Publigas) niet in verhouding
tot hun respectievelijke deelnemingen in DISTRIHOLD te verdelen.

De voorgestelde verdcling, die unaniem door de aandeelhouders van DISTRIHOLD aanvaard dient

te worden, bestaat hierin dat SUYIZ-TRACTEBEL 100% van de vergoeding van het op haar

overgedragen gedeelie van het vermogen zal worden toegekend en PUBLIGAS 0%. Op dezelfde
manier zal PUBLIGAS 100% van de vergoeding van het op haar overgedragen gedeclte van het

vermogen worden toegekend en SUEZ-TRACTEBEL 0%.

SUEZ-TRACTEBEL heeft vervolgens vastgestcld dat overeenkomstig artikel 740§2,1° van het

Wetboek van Vennootschappen cr geen enkel nieuw aandeel van SUEZ-TRACTLBEL kan worden

gecreterd of ten gunstc van haar kan worden overhandigd Hierdoor zal cr niet worden oveigegaan

tot een kapitaalverhoging door SUEZ-TRACTEBEL en evenmin door PUBLIGAS omwille van
dezelfde redenen.

Bij het begindigen van onze controlewerkzaamheden zijn wij van oordeel dat :

a) De verrichting werd nagezien overeenkomstig de normen uitgevaardigd door het Instituut der
Bedrijfsrevisorcn inzake inbreng in natura ew dat het bestuursorgaan van de vennootschap
(SUE#-TRACTEBEL) verantwoordelijk is voor de waardering van de ingebrachte
bestanddelen;

b) De beschriyving van elke inbreng in natura beantwoordt aan dc normale vereisten betreffende
nauwkeurigheid en duidclijkheid;

    

 

 

Up we Pte ot san bat gi Ger beaker BT On MOR Sige Gal Von Gu bSutebncalctesiad Nuledo, Motel] Vais wo PCrocyd Hut)

hoorsoon 12.1 2an7len van dered tt vertaganwoerdigen

 

soe

 
 

Luik B - Vervoig
c) De waarderingsmethoden van de inbreng in natura, weerhouden door de betrokken partijen,

redelijk en objectiof zijn.

Wij willen er ten slote aan herinneren dat onze opdracht er niet bestaat een uitspraak te doen

betreffende de rechtmatigheid en bilhjkheid van de verrichting.”

2. De vergadering keurt de splitsing goed, conform het splitsingsvoorstel, van de naamloze

vennootschap FLUXHOLD, met vennootschapszetel te 1000 Brussel, Troonplein 1,
rechtspersonenregister nummer 0476.776.972, door de overgang, tengevolge van haar ontbinding
zonder vereffening, van haar gehele vermogen, zowel de rechten als de verplichtingen, op haar
aandeelhouders, de naamloze vennootschap SUEZ-TRACTEBEL en de codperatieve vernootschap
met beperktc aansprakelijkheid PUBLIGAS, prorata participationis, zonder dat de naamloze
vennootschap SUEZ-TRACTEBEL vennoot wordt van de codperatieve vennootschap met beperkte
aansprakelijkheid PUBLIGAS of dat de codperatieve vennootschap met beperkte aansprakelijkheid
; : PUBLIGAS aandeethouder wordt van de naamloze vennootschap SUEZ-TRACTEBEL.

3 : De commissaris besluit zijn verslag als volgt:

} i “De splitsing van FLUXHOLD, zonder oprichting van nieuwe vennootschappen, betreN de.
integrale vermogensoverdracht, op basis van de boekhoudkundige staat van activa en passiva per 31
december 2004, ten gunste van haar aandeclhouders (Suez-Tractebel en Publigas) volgens hun
prorata participatie, gevolgd door de ontbinding zonder vereffening van FJ]-UXHOLD, hetgeen
aanleiding geeft tot cen inbreng in natura in SUEZ-TRACTEBEL.

fr zal aan de aandeelhouders van FLUXHOLD worden voorgesteid om gebruik te maken van de
mogelijkheid die artikel 736,§5 van het Wetbock van Vennootschappen biedt om de aandelen en
deelbewijzen van de verkrijgende vennootschappen (Suez-Tractebel en Publigas) niet in verhouding
tot hun respectievelijke declnemingen in FLUXHOLD te vordelen.

De voorgestelde verdeling, die unaniem door de aandeelhouders van FLUXHOLD aanvaard dient te
worden, bestaat hierin dat SUEZ-TRACTEBEL 100% van de vergoeding van het op haar
overgedragen gedeelte van het vermogen zal worden toegekend en PUBLIGAS 0%. Op dezelfde
{ manier zal PUBLIGAS 100% van de vergoeding van het op haar overgedragen gedeelte van het

! -vermogen worden toegekend cn SUEZ-TRACTEBEL 0%.

SUEZ-TRACTEBEL hecft vervolgens vastgesteld dat overeenkomstig artikel 740§2,1° van het

Wetboek van Vennootschappen er geen enkel nieuw aandccl van SUEZ-TRACTEBEL kan worden
-gecreéerd of ten gunste van haar kan worden overhandigd. Hierdoor zal er niet worden overgegaan

tot cen kapitaalverhoging door SUEZ-TRACTEBEL en evenmin door PUBLIGAS omwille van

dezel {de redenen.

Bij het beéindigen van onze controlowerkzaamheden zijn wij van oordeel dat :

a) De verrichting werd nagezien overeenkomstig de normen uitgevaardigd door het Instituut der
Bedrijfsrevisoren inzake inbreng in natura en dat het bestuursorgaan van de vennootschap
(SUEZ-TRACTEBEL) verantwoordelijk is voor de waardering van de ingebrachte
bestanddelen;

b) ‘De beschrijving van elke inbreng in natura beantwoordt aan de normale vereisten betreffende
nauwkeurigheid en duidelijkheid;

c) De waarderingsmethoden van de inbreng in natura, weerhouden door de betrokken partijen,
redelijk en objectief zijn.

Wij willen er ten stote aan herinneren dat onze opdracht er niet bestaat een uitspraak te doen

betreffende de rechtmatigheid en billijkhcid van de verrichting.”

3. Vennootschaps- en handelsrechtelijk, zal de splitsing van de vennootschappen FLUXHOLD en
! DISTRIHOLD een aanvang nemen op de dag waarop de vergaderingen de splitsing hebben beslist.
: Boekhoudkundig gezien, zal de splitsing cen aanvang nemen op één januari twecduizend en vijf, om
nul uur.

Het maatschappelijk kapitaal van de vennootschap SUEZ-TRACTEBEL wordt niet verhoogd.

VOOR EENSI_LUIDEND ONTLEDEND UITTREKSEL
Getekend: Jcan-Philippe Lagae, Notaris.

Neergelegd samen met cen uitgifte, twee volmachten, het verslag van de raad van bestuur (art 602
Wb. Venn.), de verslagen van de commissaris (art 602 Wb. Venn.)

 

‘
+
;
t
'

Bijlagen bij het Belgisch Staatsblad -24/05/2005- Annexes du Moniteur belge

Lrg Gt ldo Ce wd UAC O VeMelcert  aieGee Naam en noedaMyhers van Ge instumenterenae Notans, Reted vou Ge Betso(uh Hse
heyeontl do raertgegryoan tee oye an ven mayer bs wertaacpyicardn,

  
