Mod 2.0

rT
| V i Aeltsils}} Copie qui sera publiée aux annexes du Moniteur belge
a

apres dépot de l’acte au greffe

vie MILITIA YE

belge *09011273*

     
 
      

Greffe

 

N° d'entreprise : 0435084986 :
Dénomination
(enenie), GENFINA
Forme juridique: Société coopérative a responsabilité limitée
Siége: Place du Tr6ne 1 - 1000 Bruxelles
Qhijet de l'acte: NOMINATIONS STATUTAIRES - DELEGATION DE POUVOIRS

L'assemblée générale ordinaire du 18 décembre 2008 a:

- renouvelé fe mandat d’administrateur de Monsieur Etienne De Ranter pour un nouveau terme de six ans..
Ce mandat viendra a échéance a lissue de l'assembiée générale ordinaire de 2014:
- pris acte de la démission de son mandat d’administrateur de Monsieur Gérard Lamarche a I'issue de la’
* présente assemblée;
-nommé Monsieur Stéphane Brimont, demeurant 8 rue de la Prairie 8 78290 Croissy-sur-Seine (France):
* pour achever le mandat de Monsieur G. Lamarche. Ce mandat viendra a échéance 4 I'issue de lassembiée:
: générale ardinaire de 2012: :
- décidé de porter le nombre d’administrateurs 4 six et nommé 4 en cette qualité Monsieur Marc Haestier,:
demeurant 7 rue de la Seine 4 78360 Moniesson (France). Ce mandat viendra a écnéance A Vassembtée;
générale ordinaire de 2014; :
- renouvelé le mandat de commissaire de la $.C. s.£.d. SCRL Deloitte Réviseurs d’entreprises, représentée:
- par Monsieur Laurent Boxus, pour un nouveau terme de trois ans. Ce mandat viendra a échéance lassemblés:
annuelle de 2011.

Monsieur Stéphane Brimont a été appelé 4 la présidence du Conseil d'administration,

Gestion journaliére

Le conseil, conformément a l'article 21 des statuts, a délégué la gestion journaliére ainsi que la,
représentation de la société en ce qui conceme cette gestion a MM. Stéphane Brimont, président, Robert-i
Olivier Leyssens, administrateur délégué, Etienne De Ranter, Marc Haestier, Pierre-J. Hislaire et Guidoi
Vanhove, administrateurs, agissant deux a deux, ainsi qu’a MM. Patrick van der Beken, Christian Vanden Bremt;
et Thierry van den Hove, l'un d’eux agissant conjointement avec un des administrateurs précités.

 

Cette délégation annule et remplace tous pouvoirs attribués antérieurement.

P.J. Hislaire R.O, Leyssens
Administrateur Administrateur délegué

®
2
®
2
.
a
©
=
c
3
=
>
oT
w
®
Bod
®
c
c
<<
1
@
oS
S
q
Q
=
oO
Q
A
N
1
oa
&
2
wy
B
©
©
£
a)
x=
Oo
2
2
®
a
~
©
x=
2
Cc
©
aD
&
co

Mentionner sur la demiére page du VoletB: Au recto : Nom et qualité du noiaire instrumentant ou de ia personne ou des personnes
ayant pouvoir de représenter la personne morale a légard des tiers
Au verso . Nom et signature
