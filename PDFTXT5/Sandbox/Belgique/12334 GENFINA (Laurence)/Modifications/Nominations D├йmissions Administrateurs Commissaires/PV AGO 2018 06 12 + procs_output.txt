GENFINA

SCRL

Siége social : Boulevard Simon Bolivar 34 - 1000 Bruxelles
RPM Bruxelles n° 0435.084.986

PROCES-VERBAL DE L'ASSEMBLEE GENERALE ORDINAIRE
DES ASSOCIES TENUE LE 12 JUIN 2018

Ouverture de la séance

La séance est ouverte 4 15.00 heures, au siége social, sous la présidence du Comte
Etienne DAVIGNON.

Constitution du bureau

Le Président désigne comme secrétaire M. Michael DELMEE.

L'assemblée désigne comme scrutateurs MM. Thierry van den HOVE et Xavier
AGUIRRE qui acceptent.

Ces personnes prennent place au bureau.

Dépét des piéces

Les piéces suivantes sont déposées sur le bureau :

- le rapport de gestion ainsi que le rapport du commissaire ;

- le bilan, le compte de résultats ainsi que l'annexe arrétés au 31 décembre 2017;
— les procurations des associés représentés.

Constitution de l'assemblée

Cette liste des présences constate que les 3 associés sont représentés, Ils représentent
ensemble 386.674 actions formant la totalité du capital.

Les administrateurs et commissaire ont renoncé aux formalités de convocation.
Toutes les formalités légales et statutaires ayant été accomplies, l'assemblée se reconnait

réguliérement convoquée, valablement constituée et apte a délibérer sur son ordre du
jour.
Ordre du jour

1. Rapport de gestion du Conseil d’administration et rapport du commissaire sur
lexercice cloturé le 31 décembre 2017.

2. | Approbation des comptes annuels arrétés au 31 décembre 2017.

3. Décharge aux administrateurs et au commissaire.

4. Nominations statutaires.

Rapport de gestion et rapport du commissaire

Le Président rappelle que chaque associé a pu prendre connaissance du rapport de
gestion présenté par le conseil d'administration et du rapport du commissaire. II propose
a l'assemblée de dispenser le bureau de la lecture de ces rapports.

Cette proposition est acceptée a l'unanimité.

Comptes annuels

Le bilan, dont le total s'éléve 4 1.283.449.039 €, le compte de résultats et l'annexe
arrétés au 31 décembre 2017, ainsi que l’affectation ci-dessous sont soumis au vote de
l'assemblée.

Bénéfice de Il’ exercice H 7.949.955 €
Perte reportée de l’exercice précédent : 615.733.904 €
Perte a affecter ; 607.783.949 €
Perte a reporter : 607.783.949 €

Le bilan, le compte de résultats et l'annexe, arrétés au 31 décembre 2017, ainsi que
Paffectation sont approuvés a I'unanimité des voix.

Décharge aux administrateurs et au commissaire

Le Président met aux voix la décharge 4 donner aux administrateurs pour leur gestion et
au commissaire pour sa mission de contréle durant l’exercice cléturé 31 décembre 2017.

L'assemblée approuve la décharge aux administrateurs et au commissaire a l'unanimité
des voix.
9. Nominations Statutaires
Conseil d’administration
Les mandats d’administrateur du Comte Etienne Davignon de MM. Thierry
van den Hove, Patrick van der Beken, Sergio Val et Xavier Aguirre viennent 4
échéance a l'issue de la présente assemblée.
L’assemblée approuve a l’unanimité des voix le renouvellement des mandats
d’administrateur de MM. Thierry van den Hove, Patrick van der Beken, Sergio Val et
Xavier Aguirre pour un terme de trois ans (échéance al’ AGO 2021).
L’assemblée approuve également a l’unanimité des voix le renouvellement du mandat
d’administrateur du Comte Davignon pour un terme d’un an et acte la désignation par le
Conseil du Comte Davignon en qualité de Président et d’administrateur-délégué pour la

durée de son mandat d’administrateur (échéance a AGO 2019).

Les mandats d’administrateurs ne sont pas rémunérés.

10. Signature du procés-verbal

L'ordre du jour étant épuisé, le Président invite le bureau et les associés qui le souhaitent
a signer le présent procés-verbal.

La séance est levée 4 15.50 heures.

Le Président
Y .
Na

"i
Le Secrétaire Les Scrutateurs

f

  
Liste des présences

/ Asti

SOPRANOR YA. ENGIE S.A.
représentée par-\.. yar, dan Veve, représentée par ©. Ya yr enor
en vertu d’une procuration sous seing privé en vertu d’une procuration Sous seing privé

  

ELECTRABEK S.A.
représentée par X- Yous
en vertu d’une procuration swus seing privé
PROCURATION

La soussignée ENGIE SA
1, Place Samuel de Champlain
F- 92400 COURBEVOIE (France)

propriétaire de 386.672 parts de la SCRL GENFINA établie 4 1000 Bruxelles,
Boulevard Simon Bolivar 34

déclare constituer pour son mandataire spécial,
te. EDA
Monsieur/ Ma agate | Sand Cena 1 a CDA VA GAL OM oo ceccessseesseeen

avec effet de la représenter a |’assemblée générale ordinaire des actionnaires de cette
société qui se tiendra 4 Bruxelles, Boulevard Simon Bolivar 34, le 12 juin 2018, ainsi
qu’a toute assemblée générale qui serait convoquée ultérieurement, par suite de remise
ou d’ajournement, avec l’ordre du jour suivant :

1. Rapport de gestion du conseil d’administration et rapport du commissaire sur
l’exercice cloturé le 31 décembre 2017.

2. Approbation des comptes annuels arrétés au 31 décembre 2017.

3. Décharge aux administrateurs et au commissaire.

4. Nominations statutaires.

Aux effets ci-dessus, |’autoriser 4 prendre part 4 toutes délibérations sur les objets
portés a l’ordre du jour de l’assemblée, amender l'ordre du jour, émettre tous votes,

signer tous actes et piéces, élire domicile, substituer et, en général, faire tout ce qui sera
utile et nécessaire pour l’exécution du présent mandat.

(oy. lp prove Wo

+

es
“2X Monern

(Faire précéder la signature de la mention manuscrite
"BON POUR POUVOIR")
PROCURATION

La soussignée ELECTRABEL S.A.
Boulevard Simon Bolivar 34
1000 Bruxelles

propriétaire de 1 action de la SCRL GENFINA. établie 4 1000 Bruxelles, Boulevard
Simon Bolivar 34

déclare constituer pour son mandataire spécial,
. we
Monsieur/ M Oh Xe PGR RAM cc cccscececscsssssscsvscssssscesssssssrssvaveess

avec effet de la représenter a l’assemblée générale ordinaire des actionnaires de cette
société qui se tiendra 4 Bruxelles, Boulevard Simon Bolivar 34, le 12 juin 2018, ainsi
qu’a toute assemblée générale qui serait convoquée ultérieurement, par suite de remise
ou d’ajournement, avec |’ordre du jour suivant :

1. Rapport de gestion du conseil d’administration et rapport du commissaire sur
l’exercice cléturé le 31 décembre 2017.

2. Approbation des comptes annuels arrétés au 31 décembre 2017.

3. Décharge aux administrateurs et au commissaire.

4. Nominations statutaires.

Aux effets ci-dessus, l’autoriser 4 prendre part a toutes délibérations sur les objets
portés a l’ordre du jour de l’assemblée, amender l'ordre du jour, émettre tous votes,
signer tous actes et piéces, élire domicile, substituer et, en général, faire tout ce qui sera
utile et nécessaire pour |’exécution du présent mandat.

sit, Binsgellene eseed 6, oo covets 2018

?

TB pene pouch ()% four ~fouteat

  

QW Va Weoete

PR en dr Sreleon

1 Peut étre laissé en blanc.
. (Faire précéder la signature de la mention manuscrite
"BON POUR POUVOIR")
PROCURATION

La soussignée SOPRANOR SA
1, Place Samuel de Champlain
F- 92400 COURBEVOIE (France)

Propriétaire de 1 part de la SCRL GENFINA établie 4 1000 Bruxelles, Boulevard
Simon Bolivar 34

déclare constituer pour son mandataire spécial,

avec effet de la représenter 4 l’assemblée générale ordinaire des actionnaires de cette
société qui se tiendra 4 Bruxelles, Boulevard Simon Bolivar 34, le 12 juin 2018, ainsi
qu’a toute assemblée générale qui serait convoquée ultérieurement, par suite de remise
ou d’ajournement, avec l’ordre du jour suivant :

1. Rapport de gestion du conseil d’administration et rapport du commissaire sur
l’exercice cléturé le 31 décembre 2017.

2. Approbation des comptes annuels arrétés au 31 décembre 2017.

3. Décharge aux administrateurs et au commissaire.

4. Nominations statutaires.

Aux effets ci-dessus, l’autoriser 4 prendre part 4 toutes délibérations sur les objets
portés a l’ordre du jour de l’assemblée, amender l'ordre du jour, émettre tous votes,

signer tous actes et piéces, élire domicile, substituer et, en général, faire tout ce qui sera
utile et nécessaire pour l’exécution du présent mandat.

Fait a £823.....,1e..22. sg _— 2018

me Rites

“A Rewaea

1 Peut étre laissé en blanc.

(Faire précéder la signature de la mention manuscrite
"BON POUR POUVOIR")
