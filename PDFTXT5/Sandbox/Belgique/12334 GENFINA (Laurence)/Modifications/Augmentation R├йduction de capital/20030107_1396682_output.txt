\-

   

 

 

 

 

  
     

 
 
   
 

 

: f EORMULE IV oa
Déposé au greffe du tribunal de commerce
cnc bes
*03002159* le
ACTES et EXTRAITS D'ACTES a publier aux annexes du Moniteur Belge g
a déposer au greffe accompagnés d'une copie

[TEXTE DACTYLOGRAPHIE OU IMPRIME

1. Inscription :

Registre des groupements européens d'interét
économique
—_ —
Registre des groupements d'ntérét économique
n°
Regisire des sociétés civiles a forme commerciale
ye n°

Dénomination de la société ou du groupement
(telle qu'elle apparait dans les statuts}

Moniteur belge

Forme juridique (en entier}

Siége (rue, numéro, code postal, commune)

Registre du commerce (siége tribunal + n°)

Numéro T.V.A. ou numéro Registre national
des personnes morales (T.V.A. non assujetti)

sblad -07/01/2003- Annexes du

rg

. Objet de l'acte

Texte de l'acte ou de l'extrait de l'acte a publler aux
annexes du Moniteur beige

Le texte doit étre rédigé sans rature ni correction,
il ne peut dépasser les limites du cadre imprimé;
utiliser le cas échéant, une ou plusieurs pages
supplémentaires, établies sur papier libre, tout en
respectant des cofonnes d'une fargeur de 94 mm
selon ja modéfe tenu a ia disposition des
intéressés au greffe des tribunaux de commerce.

Bilagen bij het Belgisch Staat:

Grandeur minimum du caractére a respecter

Direction du Moniteur belge
Rue de Louvain 40-42
1000

Numéro du chéque ou de l'assignation: «
Numéro de compte wee

 

 

Registre des sociétés étrangéres non visées par les
articles 81 et 82 du Code des sociétés
n°
Registre des sociétés agricoles
ol > n°

"GENFINA"

Société anonyme.

1000 Bruxelles, rue Royale, 30,

Bruxelles 508 868

BE 435.084.986

Augmentation de capital par apport en nature
Modifications aux statuts.
XXXXX

Il résulte d'un procés-verbal dressé par Maitre Jean-
Luc INDEKEU, notaire a Bruxelles, le onze décembre deux
mille deux, portant la mention d'enregistrement °
“Enregistré six réles, deux renvois, au quatnéme bureau de
(Enregistrement de Bruxelles, la seize décembre deux
mille deux, volume 5/27, folio 69, case 18. Regu vingt-cing
euros Le Receveur, ai. , L'inspecteur Principal (signé) V
HUBERT.", ce qui suit .

XXX,

Gonelusions, du rapport établi conformément 4 l'article

602 du Code des Sociétés par fe commissaire de la
le SOCI6TS COO)

  

 

 

é
concernant l'apport en nature par la Société Génerale de
Belgique SA de 7.047.791 actions de ta SA Centre de
Coordination Générale valorisées & EUR 888.666.345,68
en vue de faugmentation de capital envisagée par la S.A.
"Genfina" nous permettent d'affirmer que :

Signature + Nom et qualité
En fin de texte au cas ot acte ou extrait comporte plus d'une page

 

Mod 274 {7000}

 
 

 

Bijlagen bij het Belgisch Staatsblad -07/01/2003- Annexes du Moniteur belge

- lopération a été controlée conformément aux
normes édictées par l'Institut des Réviseurs d'Entreprises ;
le Conseil d'administration de la société est responsable de
l'évaluation des biens apportés, ainsi que de la
détermination du nombre d’actions ou de parts a émettre
par la société en contrepartie de l'apport en nature,

- la description de l'apport repond a des conditions
normales de précision et de clarté ; .

- le mode d'évaiuation arrété par les parties est justifié
par les principes de l'économie d'entreprise et conduit a
une valeur apport gul correspond au moins au nombre et
au pair comptable et a la prime d'émission des actions a
émettre en contrepartie de sorte que l'apport en nature
nest pas surévalué, . .

La rémunération attribuée en contrepartie de l'apport
en nature consiste en l'émission de 15934 nouvelles
actions pa Genfina représentant un mon-tant de capital de
EUR 393 713.206,00. Ces nouvelles actions seront
complétement libérées et sans mention de valeur
nominale La prime d'émission s‘élavera ainsi A EUR
494 953 139,68."

Capital social souscrit apres auarmen tation : cing cent
quarante-quatre millions quatre cent trente-huit mille cent
at Six euros, entigrement W ers, h be CI a wie

ouvoirs . jonsieur Stéphane De Clercq, domicili
rue Atséne Matton, 44 a 1302 Dion-Valmont et Madame
Sylvie Deconinck, domiciliée avenue du Bois Claude, 17, a
0 Ottignies, avec pouvoir d’agir séparément et avec
facuité de subdélégation, aux fing de modifier I'inscription
de la societé au (egistre du commerce de Bruxelles.
POUR EXTRAIT ANALYTIQUE CONFORME
{signé) Jean-Luc INDEKEU _

Déposés en méme temps une expédition du procés-
verbal modifiant les statuts, deux mandats, rapports du
commissaire et du  conseil d'administration établis
conformement a l'article 602 du Code des Sociétes, une
attestation du commissaire

L
