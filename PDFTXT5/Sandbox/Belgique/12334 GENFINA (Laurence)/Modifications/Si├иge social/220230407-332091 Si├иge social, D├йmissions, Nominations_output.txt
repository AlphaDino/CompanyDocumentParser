Réservé
au LJ
Moniteur

belge

 

Bijlagen bij het Belgisch Staatsblad - 07/04/2023 - Annexes du Moniteur belge

Mod PDF 19.01

Copie a publier aux annexes au Moniteur belge
apres dépdt de l'acte au greffe

Déposé
*23332091*

05-04-2023

Greffe

 

 

N° d'entreprise : 0435084986
Nom
(en entier): GENFINA

(en abrégé) :

Forme légale : Société a responsabilité limitée

Adresse complete du siége Boulevard Simon Bolivar 34
: 1000 Bruxelles

Objet de I'acte : SIEGE SOCIAL, DEMISSIONS, NOMINATIONS

Il résulte du procés-verbal du conseil d’administration du 31 mars 2023 que le conseil a pris les
décisions suivantes :

(...)

Conseil d'administration

Le Comte Davignon a démissionné de son mandat le 15 novembre 2022, avec effet au 31 décembre
2022.

Sergio Val a également démissionné de son mandat avec effet au 31 décembre 2022.

Le Conseil propose d I'Assemblée générale de ne pas remplacer ces administrateurs.

(...)

6. Siége

Le Conseil confirme son accord de principe sur le déplacement de son siége au numéro 36
Boulevard Simon Bolivar, avec effet au ter avril 2023. Il mandate les administrateurs ainsi que
Michael Delmée, Danielle Timperman et Sylvie Deconinck, agissant seuls ou deux a deux pour
prendre toutes les formalités requises dans ce cadre.

(...)

Pour extrait conforme.

(signé) Damien HISETTE

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes

ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
