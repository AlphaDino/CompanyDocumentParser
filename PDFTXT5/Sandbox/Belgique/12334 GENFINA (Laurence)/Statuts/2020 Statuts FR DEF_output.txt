GENFINA
Société 4 responsabilité limitée
Siége social : 1000 Bruxelles, Boulevard Simon Bolivar 34
Numéro d'entreprise : 0435.084.986

STATUTS

Société constituée sous la forme d’une société anonyme par acte regu par Maitre Jean-
Luc Indekeu, notaire 4 Bruxelles, le 18 aotit 1988, publié a 1’ Annexe au Moniteur belge
du 16 septembre 1988, sous le numéro 880916-1.

La société a été transformée en société coopérative 4 responsabilité limitée par décision
de l'assemblée générale extraordinaire du 29 janvier 2008.

Les statuts ont été modifiés en dernier lieu, transformant la société en une société a
responsabilité limitée, par décision de l’assemblée générale extraordinaire du 30 juin
2020.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Dates des actes Dates des publications N° des publications
a l'Annexe au Moniteur belge

21.03.1989 18.04.1989 890418-121
rectificatif 18.05.1989 8905 18-25
30.07.1992 26.08.1992 920826-77
17.08.1999 01.10.1999 991001-14
16.07.2002 02.09.2002 20020902-111484
11.12.2002 07.01.2003 20030107-002159
03.11.2003 26.11.2003 03124048
06.02.2004 23.03.2004 04047209
31.05.2005 20.06.2005 05086362
29.01.2008 26.02.2008 08031018
16.10.2012 05.11.2012 12179650
23.06.2017 20.07.2017 17105583
30.06.2020 14.07.2020 20332465

 

 

 

 
-2-

TITRE IL.
FORME - DENOMINATION - OBJET - SIEGE_- DUREE

Article 1

La société est une société 4 responsabilité limitée au sens du Code des Sociétés
et des Associations (« CSA »). Elle a pour dénomination "GENFINA" (ci-aprés, la
« Société »).

Article 2

La société a pour objet :

1. la création, l'acquisition et le développement — par voie d'apport, de fusion,
d'achat, de souscription et de cession d'actions, obligations ou autres intéréts ou au-
trement — de toutes sociétés, entreprises et associations, en particulier celles exergant
leurs activités dans les domaines de I'énergie, de l'eau et de la propreté, de la banque,
de l'assurance, de l'ingénierie, des télécommunications, du traitement de l'informa-
tion, de l'électronique, des communications, des médias ou des secteurs similaires ou
connexes;

2. l'étude, la conception, la recherche et le développement, la fabrication, les
services, le négoce, la gestion, l'exploitation, le financement et toutes autres activités,
intéressant les domaines susvisés ou les secteurs similaires ou connexes;

3. l'administration, le financement, I'assistance financiére, technique, admi-

nistrative ou de gestion, l'octroi de garanties, la réalisation d'opérations financiéres,
de trésorerie et de couverture de risques, en faveur de toutes sociétés, entreprises ou
associations.
La société peut réaliser son objet soit directement soit indirectement, en son nom
propre ou pour compte de tiers, en effectuant, tant en Belgique qu’'a I'étranger, toutes
opérations quelconques mobiliéres, immobiliéres, commerciales, civiles, financiéres
ou industrielles, de nature 4 favoriser ledit objet ou celui des sociétés, entreprises ou
associations faisant partie du groupe auquel la société appartient.

Article 3

Le siége de la Société est établi dans la Région de Bruxelles-Capitale.
Il peut étre transféré en tout endroit de la Région de Bruxelles-Capitale ou de la région
de langue frangaise de Belgique, par simple décision de l’organe d’administration (ci-
aprés, le « Conseil d’administration » ou le « Conseil »») qui a tous pouvoirs aux fins de
faire constater authentiquement la modification statutaire éventuelle qui en résulte, sans
que cela ne puisse entrainer une modification de la langue des statuts

Article 4

La Société peut établir, par décision du conseil d'administration, des siéges ad-
ministratifs, siéges d'opération, siéges d'exploitation, succursales ou agences, tant en
Belgique qu’a 'étranger.

Article 5
La durée de la Société est illimitée.
-3-

TITRE I.
PATRIMOINE DE LA SOCIETE

Article 6

En rémunération des apports effectués, trois cent quatre-vingt-six mille six
cent septante-quatre (386.674) actions ont été émises, intégralement et incondition-
nellement souscrites.

Article 7

L’assemblée générale appelée a délibérer sur |’émission d’ actions nouvelles in-
dique notamment dans ses résolutions leur degré de libération.

Les actions nouvelles 4 souscrire en numéraire doivent étre offertes par préfé-
rence aux actionnaires existants, proportionnellement au nombre d’actions qu’ils dé-
tiennent.

Le droit de souscription préférentielle peut étre exercé pendant un délai d’au
moins quinze (15) jours a dater de l’ouverture de la souscription.

L’ouverture de la souscription avec droit de préférence ainsi que son délai
d’exercice sont fixés par l’organe qui procéde a |’émission et sont portés a la connais-
sance des actionnaires par courrier électronique, ou, pour les personnes dont elle ne
dispose pas d’une adresse électronique, par courrier ordinaire, 4 envoyer le méme jour
que les communications électroniques. Si ce droit n’a pas enti¢rement été exercé, les
actions restantes sont offertes conformément aux alinéas précédents par priorité aux
actionnaires ayant déja exercé la totalité de leur droit de préférence. Il sera procédé de
cette maniére, selon les modalités proposées par le conseil d’administration, jusqu’a ce
que l’émission soit entiérement souscrite ou que plus aucun actionnaire ne se prévale
de cette faculté.

L’assemblée générale peut décider que des apports se font sans émission d’ac-
tions nouvelles sans que les équilibres entre actionnaires ne soient modifiés, et ce a la
majorité simple des voix et par acte authentique.

Article 8

Toutes les actions futures doivent étre intégralement libérées, sauf décision
contraire reprises dans les conditions d’émission établies par le conseil d’adminis-
tration.

Les fonds propres ne sont pas distribuables 4 concurrence de nonante-neuf
millions sept cent trois mille huit cent cinquante euros (EUR 99.703.850) constituant
la partie indisponible des capitaux propres de la Société.

Pour les nouveaux apports, les conditions d’émission détermineront s’ils sont
également inscrits sur ce compte de capitaux propres indisponible. A défaut de sti-
pulation a cet égard dans les conditions d’émission, ils sont présumés ne pas étre
inscrits sur ce compte de capitaux propres indisponible.

En cas d’apport sans émission de nouvelles actions, ils sont présumés ne pas
étre inscrits sur ce compte de capitaux propres indisponible.
Article 9

Les libérations 4 effectuer en numéraire ou par apports en nature sur les ac-
tions non encore entiérement libérées doivent étre faites aux lieux et aux dates que
le conseil d’administration détermine. Les libérations appelées s'imputent de facon
égale sur l'ensemble des actions dont l'actionnaire est titulaire.

Le conseil d’administration peut autoriser les actionnaires a libérer leurs ac-
tions par anticipation; dans ce cas, il détermine les conditions auxquelles les libéra-
tions anticipées sont admises.

Article 10

Les actions sont nominatives.

Elles sont inscrites dans le registre des actions nominatives; ce registre con-
tiendra les mentions requises par le CSA. Les titulaires d’actions peuvent prendre
connaissance de ce registre relatif 4 leurs titres.

Le conseil d’administration (notamment via un mandataire) veillera aux
adaptations requises dans le registre des actions de la Société. Le registre peut étre
tenu de maniére électronique. Le conseil d’administration (notamment via un man-
dataire) peut délivrer des certificats attestant de son contenu.

Article 11

Les actions sont librement cessibles entre actionnaires. Elles ne sont cessibles
a d’autres cessionnaires que moyennant |’agrément préalable du conseil d’adminis-
tration statuant 4 la majorité simple.

L'admission de nouveaux actionnaires sera constatée par l'apposition de leur
signature, précédée de la date en regard du nom, sur le registre des actions de la
Société.

Article 12

Pour étre admis comme actionnaire le candidat doit :
a) 6tre agréé par le conseil d’administration statuant a la majorité simple;
b) adhérer sans réserve aux statuts et au réglement d'ordre intérieur si un tel régle-
ment a été approuvé et, pour ce faire, apposer sa signature personnellement ou par
mandataire dans le registre des actions;
c) souscrire 4 une (1) action au moins.

Article 13

Un actionnaire ne peut ni démissionner, ni opérer de retrait d’actions, pour
tout ou partie des actions qu’il détient, entiérement libérées ou non, que moyennant
l'accord de l'assemblée générale statuant a la majorité des trois quarts (3/4) des ac-
tions émises.

La demande peut étre introduite a tout moment de |’année.
En cas de démission, |’actionnaire a droit uniquement a recevoir la valeur de ses
actions telle qu'elle résultera des derniers comptes annuels approuvés tenant compte
du montant réellement libéré (non encore remboursé).

Tout actionnaire peut étre exclu pour juste motif ou s'il cesse de remplir les
conditions d'admission prévues par les présents statuts.
-5-

L'exclusion est prononcée par l'assemblée générale.

Elle ne pourra étre prononcée qu'aprés que l'actionnaire dont l'exclusion est
demandée aura été invité 4 faire connaitre ses observations. La décision d'exclusion
doit étre motivée par le conseil d’administration. II est fait mention de l'exclusion
dans le registre des actions.

L'actionnaire exclu a droit au remboursement de la valeur de ses actions telle
qu'elle résultera des derniers comptes annuels approuvés, sous les mémes modalités
et réserves que l'actionnaire démissionnaire tenant compte du montant réellement
libéré (non encore remboursé).

Article 14

Moyennant respect des articles pertinents du CSA, la Société peut acquérir
ses propres actions, entiérement libérées, moyennant une décision de l’assemblée
générale statuant 4 la majorité des trois quarts (3/4) des actions émises.
Cette offre d’ acquisition est faite a tous les actionnaires par classe d’actions sauf si
cette décision est prise 4 l’unanimité par une assemblée générale out |’ensemble des
actionnaires étaient présents ou représentés.
Il peut étre procédé, au moment de |’ acquisition, a l’annulation de tout ou partie de
ces actions moyennant adaptation des statuts.
Si les actions sont conservées en auto-détention, il est constitué une réserve indispo-
nible d’un montant égal a la valeur d’inventaire des actions acquises. Les actions en
auto-détention n’ont pas le droit de vote.

Article 15

Le conseil d’administration peut suspendre l'exercice des droits afférents aux
actions faisant l'objet d'une copropriété, d'un usufruit ou d'un gage, jusqu'a ce qu'une
seule personne soit désignée comme étant, a l'égard de la Société, propriétaire de ces
actions.

TITRE III.
ADMINISTRATION - DIRECTION - REPRESENTATION _- CONTROLE.

Article 16

La société est administrée par un conseil d'administration composé de trois
(3) membres au minimum nommés pour six (6) ans au plus par l'assemblée générale
des actionnaires et révocables par elle. Ils forment un organe collégial au sens du
CSA.

Les administrateurs sont rééligibles.

Les fonctions des administrateurs sortants et non réélus prennent fin immé-
diatement a l'issue de l'assemblée générale ordinaire.

Le conseil d'administration élit en son sein un président et peut élire un vice-
président.

Article 17

En cas de vacance d'un ou plusieurs mandats d'administrateur, les membres
restants du conseil d'administration peuvent pourvoir provisoirement au remplace-
ment jusqu'a la prochaine assemblée générale qui procéde 4 la confirmation de la
-6-

nomination. A défaut de confirmation, le mandat de l’administrateur coopté prend
fin aprés l’assemblée générale, sans que cela porte préjudice a la régularité de la
composition du Conseil d’administration jusqu’a cette date.

Article 18

Les administrateurs ne contractent aucune obligation personnelle relative-
ment aux engagements de la Société. Ils sont responsables 4 I'égard de la Société de
l'exécution de leur mandat et des fautes commises dans leur gestion.

Article 19

Le conseil d’administration se réunit au siége ou 4 l'endroit indiqué dans les
convocations chaque fois que l'intérét de la Société l'exige et en tout cas, chaque fois
que deux (2) administrateurs au moins le demandent.

Les convocations sont faites 4 chacun des administrateurs huit (8) jours avant
la réunion, sauf cas d'urgence, avec communication de l'ordre du jour.

Le conseil d’administration se réunit valablement sans convocation si tous
les administrateurs sont présents ou représentés et ont marqué leur accord sur l'ordre
du jour.

Les réunions sont présidées par le président du conseil ou, en cas d'empéche-
ment de celui-ci, par un administrateur désigné par ses collégues.

Toute communication aux actionnaires, administrateurs et commissaire est
suffisamment rencontrée lorsqu’effectuée par moyen électronique (dont courrier
électronique sauf demande spécifique au conseil d’administration).

Article 20

Le conseil ne peut délibérer et statuer que si la majorité de ses membres est
présente ou représentée.

Chaque administrateur peut, par tout moyen de transmission, donner a l'un
de ses collégues pouvoir de le représenter 4 une séance du conseil et d'y voter a sa
place.

Les réunions, en ce compris les délibérations et votes, peuvent, sur proposi-
tion de son président, étre tenues via tout moyen de télécommunication, notamment
oral ou visuel, qui permette des débats entre des participants géographiquement éloi-
gnés. Dans ce cas, la réunion est réputée se tenir au siége de la Société et tous les
administrateurs participants y étre présents.

Les décisions sont prises 4 la majorité des voix. En cas de partage, celle du
président de la réunion est prépondérante. Si, dans une séance du conseil réunissant
la majorité requise pour délibérer valablement, un ou plusieurs administrateurs s'abs-
tiennent, les résolutions sont valablement prises 4 la majorité des autres membres du
conseil, présents ou représentés.

Les décisions du conseil d’administration peuvent étre prises par consente-
ment unanime des administrateurs exprimé par écrit, a l’exception des actes notariés.

Article 21

Les délibérations du conseil d’administration sont constatées par des procés-
verbaux signés par le président et les administrateurs qui le souhaitent. Ces procés-
verbaux sont consignés ou reliés en un recueil spécial sous une forme quelconque, y
-7-

compris la forme électronique. Les copies ou extraits, 4 produire en justice ou ailleurs,
sont signés par un ou plusieurs administrateurs ayant le pouvoir de représentation.

Article 22

L'assemblée générale peut allouer aux administrateurs un émolument fixe ou
des jetons de présence 4 porter 4 charge du compte de résultats.

Le conseil d'administration est autorisé également 4 accorder aux administra-
teurs chargés de fonctions ou missions spéciales, une rémunération particuliére 4
prélever sur les frais généraux.

Article 23

Le conseil d'administration a le pouvoir d'accomplir tous les actes nécessaires
ou utiles a la réalisation de l'objet social, a l'exception de ceux que la loi ou les statuts
réservent a l'assemblée générale.

Article 24

Le conseil peut créer en son sein ou en dehors un comité d’audit dont il dé-
termine la composition et les compétences.

Le conseil d'administration peut déléguer la gestion opérationnelle (en ce
compris la gestion journaliére) ainsi que la représentation de la société en ce qui
concerne cette gestion 4 un ou plusieurs délégués agissant dans ce cas deux a deux.
Le conseil nomme et révoque les délégués a cette gestion qui sont choisis dans ou
hors de son sein, fixe leur rémunération et détermine leurs attributions.

Le conseil d'administration ainsi que les délégués a la gestion opérationnelle
(en ce compris la gestion journaliére) dans le cadre de cette gestion, peuvent égale-
ment conférer des pouvoirs spéciaux et déterminés 4 une ou plusieurs personnes de
leur choix.

Article 25

La société est représentée dans tous actes, y compris ceux oii intervient un
fonctionnaire public ou un officier ministériel, ou en justice en ce compris le conseil
d’Etat et les autres juridictions administratives :

- soit par deux administrateurs agissant conjointement;

- soit, dans les limites de la gestion opérationnelle (en ce compris la gestion
journaliére), par le délégué 4 cette gestion s'il n'y en a qu'un seul, et par deux délégués
agissant conjointement s'ils sont plusieurs.

Elle est, en outre, valablement engagée par les mandataires spéciaux agissant
dans les limites de leur mandat.

Article 26

Le contréle des comptes de la société est confié 4 un commissaire au moins,
nommé par l'assemblée générale pour une durée de trois (3) ans, rééligible et révo-
cable par elle.

Si par suite de décés ou pour un autre motif, il n'y a plus de commissaire, le
conseil d'administration doit convoquer immédiatement I'assemblée générale pour
pourvoir a cette vacance.
-8-

Les fonctions des commissaires sortants et non réélus prennent fin immédia-
tement aprés l'assemblée générale ordinaire.

La mission et les pouvoirs des commissaires sont ceux que leur assigne le
CSA.

L'assemblée générale détermine les émoluments des commissaires corres-
pondant a leurs prestations de contrdle des comptes. Toutefois, le conseil d'adminis-
tration peut attribuer au commissaire des émoluments pour des missions spéciales; il
informe la plus prochaine assemblée générale ordinaire par le rapport de gestion, de
l'objet de ces missions et de leur rémunération.

TITRE IV.
ASSEMBLEES GENERALES.
Article 27
L'assemblée générale a les pouvoirs qui sont déterminés par la loi et les présents
statuts. Toutefois, une extension statutaire des pouvoirs n’est pas opposable aux tiers,
sauf si la Société prouve que le tiers en avait connaissance ou ne pouvait l’ignorer
compte tenu des circonstances, sans que la seule publication des statuts suffise a
constituer cette preuve.

Article 28

L'assemblée générale se tient au siége social ou a I'endroit indiqué dans les
avis de convocation.

L’assemblée générale ordinaire a lieu le deuxiéme mardi du mois de juin a
quinze heures ou, si ce jour est un jour férié légal, le premier jour ouvrable suivant,
autre qu'un samedi.

Article 29

Le conseil d’administration, et le cas échéant le commissaire, convoque les as-
semblées générales tant ordinaires qu'extraordinaires.

L'assemblée doit étre convoquée lorsque |’intérét social l’exige ou a la demande
d'un ou plusieurs actionnaires justifiant qu'ils représentent un dixiéme du nombre d’ac-
tions en circulation sur base du registre des actions 4 cette date. Dans ce dernier cas, le
ou les actionnaires indiquent au conseil d’administration les sujets proposés a I’ ordre
du jour. L’assemblée générale doit alors étre convoquée dans un délai de trois (3) se-
maines de la demande.

Les convocations aux assemblées générales contiennent |’ordre du jour. Elles
sont faites par e-mails envoyés quinze jours au moins — sauf renonciation - avant l’as-
semblée aux actionnaires, aux administrateurs et, le cas échéant, aux titulaires d’obli-
gations convertibles nominatives, de droits de souscription nominatifs ou de certificats
nominatifs émis avec la collaboration de la Société et aux commissaires.

Toute communication aux actionnaires, administrateurs et commissaire est suf-
fisamment rencontrée lorsqu’effectuée par moyen électronique (dont e-mails) sauf de-
mande spécifique au conseil d’administration.

Article 30
Pour étre admis a l'assemblée générale, les actionnaires doivent, si le conseil les
-9-

y invite, six (6) jours ouvrables avant la tenue de l'assemblée, informer la Société de
leur intention d'y assister, ainsi que du nombre d’actions pour lequel ils entendent pren-
dre part au vote.

Article 31

Tout actionnaire peut se faire représenter a l'assemblée générale par un manda-
taire, actionnaire ou non, par le biais d’une procuration écrite pour y voter en ses lieu
et place.
Le conseil d’administration peut arréter la formule de procuration et exiger que celle-
ci soit déposée 4 I'endroit indiqué par lui et dans le délai qu'il fixe.
Une procuration octroyée reste valable pour chaque assemblée générale suivante dans
la mesure ow il y est traité des mémes points de l’ordre du jour, sauf si la Société est
informée d’une cession des actions concernées.

Article 32

L'assemblée est présidée par le président du conseil d’administration ou, a dé-
faut, par un administrateur désigné par ses collégues.

Le président de l'assemblée choisit le secrétaire. Si le nombre de personnes pré-
sentes le permet, I'assemblée choisit deux scrutateurs, actionnaires ou non, sur propo-
sition du président de I'assemblée. Le président, le secrétaire, et le cas échéant les scru-
tateurs, forment ensemble le bureau.

Article 33

Chaque action donne droit 4 une voix et un droit égal dans la répartition des
bénéfices au sein de chaque classe. Il en va de méme en ce qui concerne le produit de
la liquidation de la Société.

Article 34

L'assemblée générale ne peut délibérer que sur les points figurant 4 son ordre
du jour, sauf si toutes les personnes 4 convoquer sont présentes ou représentées, et, dans
ce dernier cas, si les procurations le mentionnent expressément.

Sauf les cas prévus par la loi, les décisions sont prises 4 la simple majorité des
voix valablement exprimées, quel que soit le nombre de titres représentés, sans tenir
compte des abstentions.

Les votes se font par main levée ou par appel nominal, 4 moins que l'assemblée
n'en dispose autrement. Les votes relatifs 4 des personnes se font au scrutin secret sur
toute demande d’un actionnaire.

En cas de nomination, si aucun candidat ne réunit la majorité des voix, il est
procédé a un scrutin de ballottage entre les candidats qui ont obtenu le plus de voix. En
cas d'égalité du nombre de suffrages 4 ce scrutin, le plus 4gé des candidats est élu.

Article 35

Lorsque l'assemblée générale est appelée a décider d'une modification aux
statuts, elle ne peut valablement délibérer que si ceux qui assistent (ou sont représen-
tés) a l'assemblée représentent la moitié du nombre total des actions émises. Si cette
derniére condition n'est pas remplie, une nouvelle convocation est nécessaire et la
nouvelle assemblée délibére valablement, quelle que soit la portion représentée.
-10-

Une modification des statuts n'est admise, sauf exception légale, que si elle
réunit les trois quarts (3/4) des voix valablement exprimées (sans tenir compte des
abstentions).

Article 36

Le conseil d’administration a le droit, aprés l’ouverture des débats, de proroger,
séance tenante, la décision relative 4 l'approbation des comptes annuels 4 trois (3) se-
maines. Cette prorogation, notifiée avant la cléture de la séance et mentionnée au procés-
verbal, n'annule pas les autres décisions prises. Elle ne peut avoir lieu qu’une fois.

Les actionnaires doivent étre convoqués 4 nouveau pour la date que fixera le
conseil d’administration.

Les formalités remplies pour assister 4 la premiére séance, en ce compris le
dép6t des titres et procurations, resteront valables pour la seconde. L’accomplissement
de formalités non encore remplies sera admis dans les délais statutaires.

La seconde assemblée générale a le droit d’arréter définitivement les comptes
annuels.

Article 37

Les procés-verbaux des assemblées générales sont signés par les membres du
bureau et par les actionnaires qui le demandent et, sauf pour les procés-verbaux authen-
tiques, sont consignés ou reliés dans un recueil spécial tenu au siége sous une forme
quelconque, y compris électronique sécurisée.

Les expéditions, copies ou extraits 4 délivrer aux tiers sont signés par un ou
plusieurs membres du conseil d’administration.

Toute communication aux actionnaires, administrateurs et commissaire dans le
cadre de toute séance d’assemblée générale est suffisamment établie par moyen élec-
tronique (dont courrier électronique).

TITRE V
INVENTAIRES ET COMPTES ANNUELS - BENEFICE ET REPARTITION

Article 38

L'exercice social commence le premier janvier et se termine le trente et un dé-
cembre de chaque année.

Le conseil d’administration dresse un inventaire et établit, conformément a la
loi, les comptes annuels, qui comprennent le bilan, le compte de résultats et l'annexe.
Il rédige en outre le rapport de gestion.

Un mois au moins avant l'assemblée générale, l'inventaire, les comptes annuels
et le rapport de gestion sont remis avec les piéces aux commissaires qui doivent faire
leur rapport.

Article 39
Les comptes annuels, le rapport de gestion et le rapport des commissaires sont
adressés aux actionnaires en méme temps que la convocation.

Article 40
-11-

Aprés avoir pris connaissance du rapport de gestion et du rapport des commis-
saires, l'assemblée générale délibére sur les comptes annuels.

Aprés leur adoption, elle se prononce par un vote spécial sur la décharge des
administrateurs et commissaire(s).

Article 41

L'excédent favorable du compte de résultats constitue le bénéfice net.

Le solde éventuel est mis a la disposition de l'assemblée qui, sur proposition du
conseil d’administration, en détermine l'affectation pro rata temporis et liberationis.

Article 42
Le conseil d’administration peut procéder au paiement de dividendes, et
d’acomptes sur dividendes, dans le cadre des articles pertinents du CSA.

TITRE VI
DISSOLUTION — LIQUIDATION

Article 43

En cas de dissolution de la Société, l'assemblée générale régle le mode de liqui-
dation et nomme un ou plusieurs liquidateurs dont elle détermine les pouvoirs et les
émoluments. Cette nomination se fait sous réserve de la confirmation ou de l’homolo-
gation par le président du tribunal de I’entreprise compétent.

Elle conserve le pouvoir de modifier les statuts si les besoins de la liquidation
le justifient.

Article 44

Aprés le paiement des dettes et charges de la Société, le solde servira d’abord
au remboursement des versements effectués en libération des actions.

Si les actions ne se trouvent pas libérées toutes dans une égale proportion, les
liquidateurs, avant de procéder 4 la répartition prévue 4 l'alinéa qui précéde, doivent
tenir compte de cette diversité de situations et rétablir l'équilibre en mettant toutes les
actions sur un pied d'égalité absolue, soit par des appels de fonds complémentaires 4
charge des actions insuffisamment libérées, soit par des remboursements préalables, en
espéces ou en nature, au profit des actions libérées dans une proportion supérieure.

Le solde des actifs et passifs sera réparti entre les actions, par quotités égales.

TITRE Vil
DISPOSITIONS GENERALES

Article 45

Pour tous litiges entre la Société et ses actionnaires, obligataires, administra-
teurs, commissaire(s) et liquidateurs relatifs aux affaires de la Société et a l'exécution
des présents statuts, compétence exclusive est attribuée a un collége arbitral en nombre
impair, composé conformément au réglement de conciliation et d’ arbitrage du CEPANI
ou, a défaut de celui-ci, conformément aux régles du code judiciaire; toutefois, la So-
ciété, si elle est demanderesse, sera en droit de porter le différend devant tout autre
tribunal compétent.

-12-
-13-

Article 46

Les actionnaires, obligataires, commissaire(s) et liquidateurs domiciliés a
l'étranger et n'ayant fait aucune élection de domicile en Belgique diment notifiée a la
Société, sont censés avoir élu domicile au siége oti tous actes peuvent valablement leur
étre signifiés ou notifiés, la Société n'ayant pas d'autre obligation que de les tenir a la
disposition du destinataire.

CERTIFIE CONFORME.

Le 24 novembre 2020

Thierry van den Hove Etienne Davignon
Administrateur Président
