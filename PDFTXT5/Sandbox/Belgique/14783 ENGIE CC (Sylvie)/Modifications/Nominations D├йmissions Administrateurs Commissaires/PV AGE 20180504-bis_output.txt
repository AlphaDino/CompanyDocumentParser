ENGIE CC

Coéperatieve vennootschap met beperkte aansprakelijkheid
1000 Brussel, Simon Bolivarlaan 34

RPR Ondernemingsnummer: 0442.100.363

AFSCHAFFFFING VAN BOEKHOUDKUNDIGE DIVISIE

Het jaar tweeduizend en achttien, op 4 mei om 14 uur.
Te Brussel, in de zetel van de vennootschap,
WERD GEHOUDEN:
De buitengewone algemene vergadering der vennoten van de codperatieve vennootschap met
beperkte aansprakelijkheid ENGIE CC, gevestigd te 1000 Brussel, Simon Bolivarlaan 34,
ingeschreven in het rechtspersonenregister van Brussel onder nummer 0442.100.363.
Vennootschap opgericht onder de rechtsvorm van een naamloze vennootschap op 31 juli 1990,
bij akte verleden voor Notaris Jean-Luc Indekeu te Brussel, bij uittreksel verschenen in de
bijlagen tot het Belgisch Staatsblad van 19 oktober 1990, onder nummer 901019-135.
De vennootschap werd omgezet in een codperatieve vennootschap met beperkte
aansprakelijkheid bij besluit van de buitengewone algemene vergadering van 24 december
2002. De statuten werden voor het laatst gewijzigd, bij besluit van de buitengewone algemene
vergadering van vennoten van 20 november 2015, bij akte verleden voor Notaris Damien
Hisette te Brussel, bekendgemaakt in de bijlagen tot het Belgisch Staatsblad van 21 december
2015 onder nummer 15177025.
Bureau
De zitting wordt geopend onder het voorzitterschap van de heer Patrick van der Beken.
De voorzitter duidt als secretaris aan de heer Michael Delmée.
De vergadering kiest als stemopnemers:
_1) Mevrouw Claire de Meester;
2) de heer Thierry van den Hove.
Samenstelling van de vergadering

Aanwezigheidslijst
De algemene vergadering wordt samengesteld uit de hierna genoemde vennoten, die volgens

hun verklaringen het aantal aandelen bezitten zoals hierna vermeld:
Aantal aandelen
1. de codperatieve vennootschap met beperkte aansprakelijkheid
ENGIE Energy Management, gevestigd te B-1000 Brussel, Simon Bolivarlaan 34, 1
2. de naamloze vennootschap ELECTRABEL INVEST LUXEMBOURG, gevestigd te

L-1611 Luxemburg, 65 avenue de la Gare, 1
3. de naamloze vennootschap ENGIE INVEST INTERNATIONAL,
gevestigd te L-1611 Luxemburg, 65 avenue de la Gare 23.369.320

TEZAMEN : 23.369.322

1/7
Volmachten
De vennoot sub 1 is hier vertegenwoordigd door de heer Patrick van der Beken, krachtens een
volmacht hier aangehecht.
De vennoot sub 2 is hier vertegenwoordigd door de heer Thierry van den Hove, krachtens een
volmacht hier aangehecht.
De vennoot sub 3 is hier vertegenwoordigd door de Mevrouw Claire de Meester, krachtens
een volmacht hier aangehecht.

Toelichting van de voorzitter
De voorzitter leidt de vergadering in en zet het volgende uiteen:

1. Het vaste gedeelte van het maatschappelijk kapitaal van de vennootschap
bedraagt 1.250.000 EUR. Het ingeschreven kapitaal - vaste en veranderlijke gedeelte
samengevoegd - bedraagt 5.842.330.500 EUR, verdeeld in 23.369.322 aandelen zonder
vermelding van nominale waarde.

IL. Uit de aanwezigheidslijst blijkt dat alle vennoten op de vergadering
vertegenwoordigd zijn. Aangezien alle vennoten vertegenwoordigd zijn, dient de verzending
van geschreven oproepingsbrieven niet te worden aangetoond. De vergadering is aldus
bevoegd om rechtsgeldig te beraadslagen en geldige besluiten te treffen.

ll. — Alle vennoten hebben zich akkoord verklaard dat de vergadering beraadslaagt
over de volgende agenda:

AGENDA
1. OMZETTINGEN VAN AANDELEN
Om (i) 2.226.984 aandelen van categorie 'J' om te zetten in evenveel aandelen van categorie
'A', met dezelfde kenmerken en die dezelfde rechten vertegenwoordigen,
(ii) er de gevolgen uit te trekken voor de afschaffing van de overeenkomstige
boekhoudkundige divisie J, en
(iii) volmachten te verlenen aan de Raad van bestuur om de PPL conform aan te passen.

2. STATUTAIRE BENOEMINGEN
Managementcomités
Vaststelling

De toelichting van de voorzitter wordt door alle aanwezige leden van de vergadering als juist
erkend. :

Beraadslagingen - Besluiten
Hierna neemt de vergadering de agenda in behandeling en treft na beraadslaging de volgende
besluiten:

Eerste Besluit
De vergadering besluit tot de omzetting van:
- 2.226.984 aandelen van categorie 'J', volledig volgestort,

in evenveel aandelen van categorie 'A' met dezelfde kenmerken en die dezelfde rechten

vertegenwoordigen, zijnde 2.226.984 aandelen van categorie 'A', voor 46,62%
volgestort.

2/7
Deze in aandelen van categorie 'A' omgezette aandelen van categorie 'J' worden alle
gehouden door ENGIE INVEST INTERNATIONAL SA

Bijgevolg beslist de algemene vergadering de boekhoudkundige divisie J af te schaffen met
ingang van 1 mei 2018. Ze mandateert de leden van de Raad van bestuur, die twee per twee
zullen optreden, om de nodige aanpassingen uit te voeren voor de in dit kader met EMI
afgesloten overeenkomsten.

De nieuwe verdeling van het kapitaal van de vennootschap is als volgt:

 

 

Categorie _Gehouden door Aantal aandelen_Ingeschr. kapitaal__Niet-volgestort kap. _ Volgestort kapitaal
A ENGIE LI. 9.127.423 2.281.855.750 -441.425.000 1,840.430.750
ENGIE EM 1 250 250

B ENGIE LI. 4.080.558 1.020.139.500 -2.815,280 1.017.324.220
D ENGIE LI. 513.281 128.320.250 -1.125.000 127.195.250
E ENGIE LI. 9.648.058 2.412.014.500 -15.706.875 2.396.307.625
EIL 1 250 0 250

 

 

   

 

 

Tweede Besluit
Managementcomités
In vervolg op de wijzigingen in de voornoemde categorieén bevestigt de algemene
vergadering de volgende samenstelling van de managementcomités:
- categorie A: Xavier Aguirre, Grégoire de Thier en Thierry van den Hove;
- categorie B: Arnaud Rotsart en Florence Verhaeghe;
- categorie D:Thierry van den Hove;
- categorie E: Patrick Gaussent.

Stemming
Al deze besluiten worden aangenomen met eenparigheid van stemmen.

Sluiting van de zitting
De agenda volledig afgehandeld zijnde, verklaart de voorzitter de zitting geheven.

 

 

 

Opgesteld te Brussel, op voormelde datum en plaats.
Na gedane voorlezing hebben de leden van het bureau deze notulen getekend.
(volgt de Franse tekst)

 

 

3/7
ENGIE CC
Société coopérative a responsabilité limitée
1000 Bruxelles, Boulevard Simon Bolivar 34
RPM Numéro d’Entreprise : 0442.100.363

CONVERSIONS DE PARTS
SUPPRESSION DE DIVISION COMPTABLE
NOMINATIONS STATUTAIRES

 

 

L'an deux mille dix-huit, le 4 mai 4 14 heures.
A Bruxelles, au siége social

 

S'EST REUNIE:
Liassemblée générale extraordinaire des associés de la société coopérative a responsabilité
limitée ENGIE CC, établie 4 1000 Bruxelles, Boulevard Simon Bolivar 34, immatriculée au
registre des personnes morales 4 Bruxelles sous le numéro 0442.100.363.
Société constituée sous la forme d’une société anonyme le 31 juillet 1990, suivant acte recu
par le Notaire Jean-Luc Indekeu a Bruxelles, publié par extrait aux annexes du Moniteur belge
du 19 octobre 1990 sous le numéro 901019-134.
La société a été transformée en société coopérative a responsabilité limitée par décision de
l'assemblée générale extraordinaire du 24 décembre 2002. Les statuts ont été modifiés en
dernier lieu par décision de l'assemblée générale extraordinaire des associés du 20 novembre
2015, suivant acte regu par le Notaire Damien Hisette 4 Bruxelles ; la publication par extrait a
été faite aux annexes du Moniteur belge du 21 décembre 2015 sous le numéro 15177024.
Bureau

La séance est ouverte sous la présidence de Monsieur Patrick van der Beken.
Le président désigne comme secrétaire Monsieur Michael Delmée.
L'assemblée choisit comme scrutateurs :

1. Madame Claire de Meester;

2. Monsieur Thierry van den Hove.

Composition de l'assemblée

Liste des présences
L'assemblée générale est composée des associés cités ci-dessous, lesquels déclarent détenir le

nombre de parts sociales ci-aprés indiqué :

Nombre de parts sociales
1. la société coopérative a responsabilité limitée ENGIE Energy Management,

établie 4 B-1000 Bruxelles, boulevard Simon Bolivar 34, 1
2. la société anonyme ELECTRABEL INVEST LUXEMBOURG, établie A

L-1611 Luxembourg, 65 avenue de la Gare 1
3. la société anonyme ENGIE INVEST INTERNATIONAL,

établie 4 L-1611 Luxembourg, 65 avenue de la Gare 23,369,320
SOIT AU TOTAL : 23.369.322

AIT
- Procurations
L’associé sous 1 est valablement représenté par Monsieur Patrick van der Beken, en vertu
d’une procuration ci-annexée.
L’associé sous 2 est valablement représenté par Monsieur Thierry van den Hove, en vertu
d’une procuration ci-annexée.
L’associé sous 3 est valablement représenté par Madame Claire de Meester, en vertu d’une
procuration ci-annexée.

 

Exposé du président
Le président dirige les débats et expose ce qui suit :
I. La partie fixe du capital social de la société s'éléve 4 1.250.000 EUR. Le
capital souscrit - partie fixe et partie variable cumulées - s’établit 4 5.842.330.500 EUR,
divisé en 23.369.322 parts sans mention de valeur nominale.

Jt Il résulte de la liste de présence que tous les associés sont représentés
l'assemblée. Tous les associés étant représentés, il n'a pas lieu de justifier de l’envoi de
convocations écrites. L’assemblée est habilitée 4 délibérer et 4 statuer valablement.

 

Ill. — Tous les associés se sont déclarés d'accord que l'assemblée délibére sur l'ordre
du jour suivant : ,
ORDRE DU JOUR
1. CONVERSIONS DE PARTS
Afin
(i) de convertir 2.226.984 parts de catégories « J » en autant de parts de catégorie « A » ayant
les mémes caractéristiques et représentatives des mémes droits,
(ii) d’en tirer les conséquences en ce qui concerne la suppression de la division comptable J
correspondante, et
(iii) d’octroyer des mandats au conseil d’administration afin d’adapter le PPI en conséquence.

2. NOMINATIONS STATUTAIRES
Comités de management

Constatation
L'exposé du président est approuvé par tous les membres présents de l'assemblée.
Délibérations — Résolutions
L'assemblée aborde son ordre du jour et, aprés délibération, prend la résolution suivante:

CONVERSIONS DE PARTS

 

Premiére résolution

L’assemblée décide de convertir : :
- 2.226.984 parts de catégorie « J » libérées 4 concurrence de 46,62%

en autant de parts de catégories «A » ayant les mémes caractéristiques et

représentatives des mémes droits, 4 savoir 2.226.984 parts de catégorie « A»
libérées 4 concurrence. de 46,62%.

5/7
Ces parts J converties en parts A sont toutes détenues par ENGIE INVEST
INTERNATIONAL SA

L’assemblée générale décide en conséquence la suppression de la division comptable J, avec
effet au 1* mai 2018. Elle mandate les membres du Conseil d’administration, agissant deux a
deux, pour effectuer les ajustements nécessaires aux conventions conclues avec EII dans ce
cadre.

La nouvelle répartition du capital de la société se présente dés lors comme suit :

      
  

 

 

Catégorie Détenues par__Nbre parts Capital souscrit__ Capital non libéré Capital libéré
ENGIE LL. 9.127.423 2.281.855.750 ~441.425.000 1.840430.750
ENGIE EM 1 250 250
B ENGIE LI. 4.080.558 1.020.139.500 -2.815.280 1.017.324.220
D ENGIE LI. 513.281] 128.320.250 -1.125.000 127.195.250
E ENGIE LI. 9.648.058 2.412.014,500 -15.706.875 2.396.307.625
EIL 1 250 0 250

 

 

NOMINATIONS STATUTAIRES

 

 

Seconde résolution
Comités de Management
Suite aux changements dans les catégories précitées, |’assemblée générale confirme la
composition suivante des comités de management :
- catégorie A : Xavier Aguirre, Grégoire de Thier et Thierry van den Hove ;
- catégorie B : Amaud Rotsart et Florence Verhaeghe ;
- catégorie D : Thierry van den Hove ;
- catégorie E : Patrick Gaussent.

Vote
Toutes ces décisions sont prises a l’unanimité des voix.

Cléture de la séance
L'ordre du jour étant épuisé, le président déclare la séance levée.

DONT PROCES-VERBAL
Fait et dressé 4 Bruxelles, date et lieu que dessus.

Lecture faite, les membres du bureau ont signé ce procés-verbal. ff
a! rf ude 7 e
Scrutateurs

Secrétaire

 

 

 

 

   

6/7
Liste des présences / Aanwezigheidslijst

2, wl

—,/

ENGIE Energy Management ELECTRABEL INVEST LUXEMBOURG
représentée par / vertegenwoordigd door représentée par / vertegenwoordigd door
Patrick van der Beken Thierry van den Hove

ENGIE INVEST INTERNATIONAL

représentée par / vertegenwoordigd door
Claire de Meester

WT
follies: oa eed en ode
_— Se Ped
coal cain alt
. aul -
nie asthe aa
as Hi /
: 7
