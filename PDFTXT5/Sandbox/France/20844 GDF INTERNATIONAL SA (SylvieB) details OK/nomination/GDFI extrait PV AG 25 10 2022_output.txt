GDF INTERNATIONAL
Société Anonyme au capital de 3 971 640 000 Euros
1, Place Samuel de Champlain - 92400 COURBEVOIE
622 048 965 R.C.S NANTERRE

ASSEMBLEE GENERALE ORDINAIRE
DU 25 OCTOBRE 2022

 

EXTRAIT

Premiére Résolution

L’Assemblée Générale prend acte de la démission a compter du 25 octobre 2022 de
Monsieur Alexandre GEOFFROY de son mandat d’administrateur et nomme en
remplacement Monsieur Patrick BAETEN pour une durée de trois ans qui arrivera a
échéance a l'issue de l’'assemblée générale appelée a statuer sur les comptes de l’exercice
clos le 31 décembre 2024.

Cette résolution, mise aux voix, est adoptée a l’unanimité.

Deuxiéme Résolution
L’Assemblée Générale donne tous pouvoirs au porteur d'un original, d'une copie ou d'un

extrait du procés-verbal et particuligrement a Lextenso Services, pour effectuer tous dépdts
et formalités ot besoin sera.

Cette résolution, mise aux voix, est adoptée a l’'unanimiteé.

Laurence JATON
P-DG
