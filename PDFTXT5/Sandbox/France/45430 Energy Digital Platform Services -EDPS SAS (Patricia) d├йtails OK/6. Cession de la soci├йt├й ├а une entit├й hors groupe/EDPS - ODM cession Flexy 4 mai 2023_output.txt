 
 
 
      
 

 
 

ENERGY DIGITAL PLATFORM SERVICES Cope")
Société par actions simplifiée au capital de 3.390.000 euros
Siége social : 1, place Samuel de Champlain 92400 Courbevoie, France

849 069 471 R.C.S Nanterre

ORDRE DE MOUVEMENT
de valeurs mobiliéres non admises aux opérations d’un dépositaire central
(Article L. 228-1 et R. 228-10 et suivants du Code de commerce - Article R. 211-1 et suivants du Code monétaire et financier)

     
   

Jouissance
pleine
propriété

NATURE DES TITRES (2)

ACTIONS ORDINAIRES

 

 

 

NATURE DU MOUVEMENT ®) Cession

en lettres en chiffres
QUANTITE Trois-cent trente-neuf mille 339.000

WN) S T=

N° de compte : 2

NOM et PRENOM (ou raison sociale) : SOPRANOR S.A.S

Adresse : 1, place Samuel de Champlain, 92400 Courbevoie
384 689 071 R.C.S. Nanterre
Administrateur des Titres (s'il y a lieu) :

BENEFICIAIRE

N° de compte : 3 ee
NOM et PRENOM (ou raison sociale) : FLEXY SA
Adresse : Chaussée d’Alsemberg 842, 1180 Bruxelles, Belgique

BE0561.879.725

Administrateur des Titres (s'il y a lieu) :
En cas de nouveau titulaire, voir renseignements au verso.

VISA DE 'EMETTEUR = ORDRE EMIS

BON POUR NOTIFICATION DE LA CESSION ET A | Courbevoie fe |_4 mai 2023

INSCRIPTION AU COMPTE DU BENEFICIAIRE

BON POUR CESSION DE 339.000 ACTIONS
mention a recopier)

HON [ow nab hres bon ei A —— jo eerttoan dt. 235.

Cespin ob jaicriphn

he haharte. a

SOPRANOR S.A.S
Représentée par : Monsieur Patrick van der Beken

 
