import os
import sys
from pdf2image import convert_from_path
import pytesseract

def extract_text_from_pdf(pdf_path):
    text_parts = []
    images = convert_from_path(pdf_path)

    for image in images:
        text = pytesseract.image_to_string(image, lang='eng')
        text_parts.append(text)

    return ''.join(text_parts)

def extract_text_from_folder_and_save(input_folder, output_folder):
    for root, _, files in os.walk(input_folder):
        for file in files:
            if file.endswith('.pdf'):
                pdf_file = os.path.join(root, file)
                try:
                    text_from_pdf = extract_text_from_pdf(pdf_file)

                    relative_path = os.path.relpath(pdf_file, input_folder)
                    output_file_path = os.path.join(output_folder, os.path.splitext(relative_path)[0] + '_output.txt')

                    os.makedirs(os.path.dirname(output_file_path), exist_ok=True)
                    with open(output_file_path, 'w', encoding='utf-8') as output_file:
                        output_file.write(text_from_pdf)

                except Exception as e:
                    print(f"Error occurred while processing '{pdf_file}': {e}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python extract_pdf_text.py input_folder output_folder")
        sys.exit(1)

    input_folder_path = sys.argv[1]
    output_folder_path = sys.argv[2]

    extract_text_from_folder_and_save(input_folder_path, output_folder_path)
