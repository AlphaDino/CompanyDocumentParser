Sociétes commerciales et agricoles — Annexe au Mdniteur belge du 18 moi 1989

Handels- en lundbouwvennootschappen — Bijlage tot het Belrisch Staatsblad van 18 mei 1089

,

Sociétés commerciales 890518-25 — 890518-46 Handelsvennootschappen

N. 890518 — 25

« Genfina », société anonyme

Rue Royale 30
Bruxelles

RC. Bruxelles 508868
RECTIFICATION

Rectification de la publication nuaéro

190418-122.
mxaan

Capital souscrit : siz mt{lliards quatre-vingts
millions de france, entidrement libéré.

Rominetion @'edminiatrateure : Monsieur Jean-
Pierre RUQUOIS. administrateur de sociétés. demeu-
Fant & 1100 Bruxelles, 41, avenue de Ploréal.

Monsieur 2b: Georgy DESCHUYTENEER, edminiatra-
teur de sociét 4 urant & 7870 Deux-Acren, 33,
rue des A

Les mandate expirent A l'assemblée d'octobre
mil neuf cent quetre-vingt-quatorse.

Pouvotre adatnistraeteurs et exercice de
£ t Le eociaté eat repréeentée dane lee actes,
Y compris ceux 00 intervient un fonctionnaire public
6u un officter ministériel, et en justice, par deur
adsinistrateurs agissant conjointenent.

Exercice socf{alt : du premier juillet eu trente
chaqui ltexercica en cours ayant

   

 

 

   

  

 

 

 

 

 

   

juin

 

commencé le premier déceabre mil neuf cent quatre- -

wingt-huit ee cléturant au trente juin #11 neuf cent
quatre-vingt-neuf ayant don. une durée de sept
aois.

Resend) $e générale ordinaire : le trots e
mardi G’octobre & quinsa heur: et pour le preatére
foie en etl neuf cent qudtre-vingt-neuf.

Pour extrait analytique conforme :

 

 

   

 

 

.
(Signe) Jean-Luc Indekeu,
notaire.
Dépose¢, 3 mai 1989.
1 1200 T.V.A. 19 p.c. 228 1428

(37256)

 

N. 890518 — 26

« Medilex »,
société de personnes 4 responsabilité limitée

Avenue Montjoie 293, bte 1
Uecle (1180 Bruxelles)

RC. Bruxelles 472056 — T.V.A. 427.681.413

ADAPTATION DE LA RAISON SOCIALE
AUGMENTATION DE CAPITAL
ADAPTATION -- MISE A JOUR DES STATUTS

D'un acte regu par nous, Yves Dechamps,
fMotaire résidant 4 Schaerbeek-Bruxelles, le dix-
sept avril mil neuf cent quatre-vingt-neuf portant
la relation d'enregistrement suivante: "Enregistré
& Schaerbeek 3e bureau - six réjes - un renvo1, le
19 AVR,1989 - Vol.281 F°BS C13 - Regu: deux mille
cing cents francs (2.500F) Le Receveur (signé) J.
VANSANTVOET."

Etant le procés-verbal d'une assemblée
générale extra-ordinaire de la société de personnes
A responsabilité limitée "MEDILEX", @tablie a Uccle
- 1180 Bruxelles, avenue Montjoie, n°293/bte 1;

Société constituée suivant arte regu le vingt-
et-un juin mit mauf cant quatre-vingt-cing, par Je
Notaire James Dupont, de résidence a Bruxelles
(Annexe au Moniteur belge de date-numéro 850709-
235)3

Soc. commerce. — Handelsvenn, — 2° trim./2e kwart.

1} résulte:

l.Adaptation de ta raison sociale &@ la forme
de société privée a responsabilité limitée.

2. Augmentation de capital & concurrence de
cing cent mille francs, pour le porter de deux cent
cinquante mille francs & sept cent cinquante mille
francs, par a création de cing cents parts
sociales nouvelles d'une valeur nominale ge mitie
francs chacune, du méme type et joutssant des mémes
droits et avantages que les parts soc.ales
préexistantes et participant aux bénéfices 4
compter de leur libération intégrale, souscrites en
fnuméraire et au pair et & libérées intégralement au
moment de leur souscription.

3. Adaptation des statuts tant aux résolutions
@voquées ci-dessus qu'é la réforme du droit des
sociétés, par vole d'adoption d'un teste Jes
statuts, entrarement adapté et mis & jour, et dont
11 résulte que:

La société a pour objet toutes activites ce
service aux professions Jibéralos et indépendantes,
ainsi qu'aux entreprises, associations et sociétes,
en matidére d'admintstration, de secrétariat et de
comptabilité, ainsi que la prestation de tn-s
services pouvant s'y rapporter de prés ou de loin,

La socié@té peut réaliser son objet social pour
son compte ou pour le compte d'autrus, en tous
lieux, de toutes les maniéres et selon les
modalités qui lui pararssent les mieux approcriées.

Elle peut faire. en Belgique et a l'étranger,
d'une fagon générale, toutes oper ations
commerciales ou civiles,
immobilséres. en relation quelconque avec son
objet ou pouvant en facititer ta réatisation, Elle
peut effectuer tous placements en valeurs
mobiliéres et s'intéresser par voie d'associatio%,
d'apport ou de fusion, da souseription, ae
participation, d' intervention Financiere Ou
autrement, dans toutes sociétés ou entreor:ses
existantes ou 4 créer,

La société a une durée illimitée.

Le capital social est fixe a sept cert
cinquante mille francs et est reprasenté par sept
cent Cinquante parts socyales d'une valeur nominate
da mille francs chacune, toutes intégralement
libérées,

La gérance est confiée & un ou plusieurs
gérants.

Chaque gérant est invest! des pouvoirs les
plus étendus pour faire tous les actes,
d'administration et de disposition qu: intéressent
la société.

La gérance peut déléguer Ya gestian
jJournaliére de la société a un ou plusieur
gérants, ou encore a un directeur, et déléguer a
tout mandataire des pouvoirs spéciaun détermings,

Tows actes engageant la société, tous pouvorrs
@t  procurations, toutes révocations d’agants,
d'employés ou da salariés de la société sont, on
cas de pluralité de gérants, signés par un seul
d'entte eux..

mobi liéres ou

890518°3

890518/17
