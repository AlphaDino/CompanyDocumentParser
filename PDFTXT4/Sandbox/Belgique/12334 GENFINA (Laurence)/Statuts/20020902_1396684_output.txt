| /
aT

*02111484*

FORMULE IV
Déposé au greffe du tnbunal de commerce
BEPOGE ALCS COS AGDNAL DE COLIM: atk
DE BHUNTIe ef eee

   

    
   

RESERVE TRIBUNAL DE COMMERCE

RESERVE AU MONITEUR BELGE

 

 

ACTES et EXTRAITS D'ACTES 4 publier aux annexes du Moniteur Belge (5
a déposer au greffe accompagnés d'une copie

TEXTE DACTYLOGRAPHIE OU IMPRIME

 

 

 

1. Inscription :
Registre des groupements européens d'intérét

économique
n°

Registre des groupements d'intérét économique

> n°

. Registre des sociétés civiles 4 forme commerciale
> n°

Dénomination de la société ou du groupement

Numéro T.V.A. ou numéro Registre national

isch Staatsblad -02/09/2002- Annexes du Moniteur belge

Texte de l'acte ou de l'extrait de lacte 4 publier aux
annexes du Moniteur belge.

Le texte doit étre rédigé sans rature mt correction,
it ne peut dépasser les limites du cadre imprimé,
utiliser le cas échéant, une ou plusieurs pages
supplémentaires, établies sur papier libre, tout en
respectant des colonnes d'une largeur de 94 mm
selon le modéle tenu a la disposition des
intéressés au greffe des tribunaux de commerce.

Bijlagen bij het Belg:

Grandeur minimum du caractére a respecter

Direction du Moniteur belge
Ruc de Louvain 40-42
1000

Numéro du chéque ou de lassignation :
NUMErO de COMPHE!.... cseseeeceeesesees

 

PO #
246-0728729-204

 

(telle qu'elle apparait dans les statuts) >
Forme juridique (en entier) >
Sidge (rue, numéro, code postal, commune) b>

Registre du commerce (siége tribunal + n°) b

des personnes morales (T.V.A. non assujetti) (>

7. Objet de Pacte b>

 

Registre des sociétés étrangéres non visées par les
articles 81 et 82 du Code des sociétées
n°
Registre des sociétés agricoles
Hi >

“GENFINA’

Société anonyme.

1000 Bruxelles, rue Royate, 30
Bruxelles 508 868

BE 435 084.986

Modification de la date de l'assembiée générale
ordinalre.
Modifications aux statuts

XXXXX

ll résulte d'un procés-verbal dressé par Maltre Jean-
Luc INDEKEU, notaire & Bruxelles, le seize juillet deux
mille deux, portant la mention d'enregistrement
“Enregistré deux réles, sans rénvoi, au quatriéme bureau
de j'Enregistrement de Bruxelles, le vingt-trois juillet deux
mille deux, volume 27, folio 9, case 1 ecu vingt-cing
euros E/A Inspecteur A.l, Inspecteur princ. a.1 (signé) B.
VAN THUYNE.", ce qui suit.

XXXXX
Agsemplge générale ordinaire : le troisiame mardi du
mots doctobra de chaque année, a quinze heures et, si ce
jour est un Jour féré légal, le eon ouvrable suivant, au

sige social Ou a |'endroit indiqué dans les convocations, et
pour la premiére fois dés l'année deux mille deux.

POUR EXTRAIT ANALYTIQUE CONFORME
(signé) Jean-Luc INDEKEU.
Déposés en méme temps une expédition du procés-
verbal modifiant les statuts, une liste de présence, deux

mandats LL
- ¢

Signature + Nom et qualité
En fin de texte au cas ol 'acte ou rextrait comporte plus d'une page

 

 

Mod 271 (7000)

 
