 

920408/256 Handels- en landbouwvennootschappen — Bijlage tot het Belgisch Staatsbiad van 8 april 1992
Sociétés commerciales et agricoles — Annexe au Moniteur belge du 8 avril 1992

 

- ontlasting aan de vereffenaar, de Heer Het mandaat van commissaris-revisor van de
Jan Oskar Jansson, verleend wordt. B.V.B.A. Tysmans, Van den Bergh & C° ver-

~- de vereffening definitief afgesloten is. tegenwoordigd door de heer J.L. Van den

~ de boeken en bescheiden van de Bergh, bedrijfsrevisor, werd hernieuwd voor
vennootschap zullen neergelegd worden bij een periode-van drie jaar.

de heer Jan Oskar Jansson, wonende te
1640 Sint-Genesius-Rode, Golflaan, 34, die
ze gedurende vijf jaar zal bewaren. Brussel 16 meee ety 08 1

et.) J.O. Horner,

Voor ontledend uittreksel : bestuurder.
(Get.) Jan Oskar Jansson, Neergelegd, 30 maart 1992.
vereffenaar.
1 1450 BTW19 pet. 276 1726

Echt verklaard,

Bijlage : Proces-verbaal van de algemene

 

 

 

vergadering. (30795)
Neergelegd, 30 maart 1992. N. 920408 — 413
1 1450 BTW 19 pct. 2768 1726
« Senec », société anonyme
(30793)
Chaussée de Vilvorde 98-100
1120 Bruxelles
nN. 920408 — 411 R.C. Bruxelles 441353 — T.V.A. 422.672.748
« Uniden Benelux », naamloze vennootschap TRANSFERT DU SIEGE SOCIAL.
Koning Albert-I laan 64
Wemmel Le conseil d’administration, réuni le 5
mars 1992 a, & l‘unanimité, confirmé le
ELR. Brussel 521150 — BTW 437.786.437 transfert du siége social a 1060 Bruxel-
RECRTZETTING. les, rue du Monténégro, 152, Aa dater du
In de akte van vervroegde ontbinding verleden voor onder- ler février 1992.
getekende notaris Coppens te Vosselaar op vier februari (Signé) P. Vilain, (Signé) J. Reymann,
negentiennonderd tweeénnegentig, gepubliceerd in de bij- administrateur. président.
lagen tot het Belgisch Staatsblad van achtentwintig fe- Déposé, 30 mars 1992.
bruari daarna, onder het nummer 920228-418 moet de eerste
beslissing gelezen worden als volgt: 1 1450 TVA. 19 pc. 278 1726
"De naamloze vennootschap UNIDEN BENELUX werd vervroegd (30796)

“ontbonden en bestaat enkel nog voor de noodwendigheden
“van haar vereffening en dit met ingang van éénendertig

 

 

“januari negentienhonderd tweeénnegentig en niet “éenen- N. 920408 — 414
“dertig december negentienhonderd tweeénnegentig"." « Centre Debat de Développement clinique
Voor ontledend uittreksel : et d’Enregistrement européens », société anonyme
(Get.} J. Coppens, Boulevard Brand Whitlock 87
notaris. Woluwe-Saint-Lambert (1200 Bruxelles)
om Neergelegd, 30 maart 1992. RC. Bruxelles 546460 — T.V.A, 444.327.306
1 1450 BTW 19 pet. = 2761726 Extrait du procés-verbal du_conseil

(30794) d'administration de la S.A. Centre Debat

de développement clinique et d'enregistre-

 

N. 920408 — 412 ment européens , du 9 mars 1992.
« Graham Miller & Co (Benelux) », naamloze vennootschap TRANSFERT DU SIEGE SOCIAL.

Holleweggaarde 6C

Schaarbeek (1030 Brussel) Il est proposé, conformément 4 l'article

E.R. Brussel 334199 2, alinéa 2 des statuts, de transférer
HERBENOEMING BESTUURDERS EN COMMISSARIS- le siége social 4 1050 - Bruxelles, avenue
REVISOR :
_—_—— Louise, 250.
Uittreksel uit de notulen van de jaarver- Cette résolution est acceptée a
gadering der aandeelhouders van 4 maart 1 sage
1992, lunanimité.

Le transfert sera effectif le ler avril

Het mandaat van bestuurder van de Heren

J.S. Horner, W.J. Bolton en J. Beynon werd 1992.

hernieuwd voor een periode van één jaar, (Signé) Jacques Vanpe,
eindigend, behoudens herbenoeming, na de administrateur délégue.
jaarvergadering der aandeelhouders van 1993, Déposé, 30 mars 1992.

De mandaten van de bestuurders zijn niet 1 1450 TVA. 19 pe. 276-1726
bezoldigd. (30811)

 
