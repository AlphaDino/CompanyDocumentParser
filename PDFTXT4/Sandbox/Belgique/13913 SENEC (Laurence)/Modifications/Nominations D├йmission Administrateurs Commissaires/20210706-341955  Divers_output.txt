Mod PDF 19.01

Copie a publier aux annexes au Moniteur belge
apres dépdt de l'acte au greffe

Réservé
au LJ

Déposé

Moniteur *21341955*
belge

02-07-2021

Greffe

  

 

\ N° d'entreprise : 0422672748
Nom
(en entier): SENEC

(en abrégé) :

Forme légale : Société anonyme

Adresse complete du siege Chaussée de Ruisbroek 85
: 1190 Forest

Objet de l'acte : DIVERS

ll résulte des résolutions écrites et unanimes prises par les actionnaires le 24 juin 2021 que les
actionnaires ont pris les décisions suivantes :

5. Renouvellement du mandat du commissaire

Les actionnaires décident, conformément a la proposition du conseil d’administration, de renouveler
le mandat du commissaire de la Société, Deloitte Réviseurs d’Entreprises SRL, dont le siége est sis
a 1930 Zaventem, Luchthaven Brussel Nationaal 1 J, Gateway Building, représentée par Monsieur
Dirk Cleymans, pour l’exercice de son mandat.

Le mandat du commissaire viendra a échéance lors de l'assemblée générale ordinaire des
actionnaires qui délibérera sur les comptes annuels de l’exercice social cl6turé au 31 décembre
2023.

Pour extrait conforme.

(signé) Damien HISETTE

Bijlagen bij het Belgisch Staatsblad - 06/07/2021 - Annexes du Moniteur belge

Mentionner sur la derniére page du VoletB: Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter la personne morale a l'égard des tiers
Au verso : Nom et signature (pas applicable aux actes de type "Mention").
