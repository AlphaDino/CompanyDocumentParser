TIN Mod Word 15.1
_ (- “\ Copie a publier aux annexes au Moniteur belge
Long aprés dépét de l’acte au greffe

    
     
 

| ~PSSe/Recy le

Moniteur
belge

07 SEP, 2017

&U Sreffe du tiBirPde comme
opnone de Bruxelles

   

N° d’entreprise : 0422.672.748
Dénomination

(en enter): SENEC-SENERCOM-CONFORT MULTI SERVICES
(en abrégé) : SENEC
Forme juridique: société anonyme
Adresse compiéte du siége : Chaussée de Ruisbroek 85 - 1190 Bruxelles

Objet de l’acte: Réélection et nomination administrateurs / Augmenter nombre
d'administrateurs

Décision de l'Assemblée générale ordinaire des actionnaires du 29 juin 2017 :

Le mandat d’administrateur et Président de Monsieur Xavier SINECHAL arrive a échéance a l’issue de
PAssemblée générale ordinaire de 2017. L’Assemblée renouvelle son mandat pour une période de trois ans,
soit jusqu’a issue de Assemblée générale ordinaire de 2020 statuant sur les comptes de l’exercice 2019.

L'Assemblée décide de nommer Monsieur Philippe LAURENT, domicilié Avenue Eléonore 33 a
1150 Bruxelles, a la fonction d'administrateur pour une durée de trois ans, soit jusqu'a I'issue de I'Assembiée
générale ordinaire de 2020 statuant sur les comptes de |'exercice 2019.

7 - Annexes du Moniteur belge

L'Assemblée décide en outre d’augmenter le nombre d'administrateurs de trois 4 quatre.

Pour extrait conforme
SENEC-SENERCOM-CONFORT MULTI SERVICES S.A.

X. SINECHAL, Président
S. de PIERPONT, Administrateur

mages)

ee Bijlagen bij het Belgisch Staatsblad

9)

Mentionner sur Ja derniére page du Volet B : Au recto : Nom et qualité du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de représenter la personne morale a fégard des tiers
Au verso : Nom et signature (pas applicable aux actes de type « Mention »).

 
