   
 
  
 
    

OREM ote,

Cople qui sera publiée aux annexes du Moniteur belge
aprés dép6t de !’acte au groeffe

<= RANA

belge *05070059*

 
    

 

!  Dénomnation: SENEC
Forme juridique * Société anonyme
i i Siége Chaussée de Ruisbroeck 85 1190 Bruxelles
| Ndenteprise 0422672748
Qbyet de l'acte: Démission / nominations / modification du nombre
Décision de Assemblée générale extraordinaire des actlonnaires du 24 septembre 2004 :

   

L’Assemblés prend acte de la démission de Monsieur Patrick VILAIN de son mandat d’admmistrateur en
date du 23 septembre 2004

i L'Assembiée nomme Monsieur Xavier SINECHAL, domicilié rue Pieter Marchand 13 4

: 1970 Wazembeek-Oppem et Monsieur Joannes SAERENS, domicilié Brusselsestraat 128 a

1840 Londerzeel, a la fonction d'administrateur. Leur mandat prendra fin a issue de fAssemblée générale
; ordinaire de 2011 statuant sur les comptes de l'exercice 2010.

i L’Assembiée porte le nombre d'administrateurs de quatre a cing,

Ces mandats sont gratuits.

Pour extrait conforme
SENEC S.A

i E. OLEFFE, Président
. C. PONCELET, Administrateur-Directeur général

Bijlagen bij het Belgisch Staatsblad - 18/05/2005- Annexes du Moniteur belge

Mentionner sur la desniére page du Vofet 8 Aurecto Nom et qualite du notaire instrumentant ou de la personne ou des personnes
ayant pouvoir de represente: la personne morale a \égard des tiers

Au verso Nom et signatuie
