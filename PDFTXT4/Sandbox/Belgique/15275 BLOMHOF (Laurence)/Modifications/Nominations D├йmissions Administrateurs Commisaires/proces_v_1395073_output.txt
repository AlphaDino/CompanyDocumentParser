BLOMHOF
Société Anonyme

Siége social : Place du Tréne 1 — 1000 Bruxelles
RPM numéro d'entreprise 0405.671.420

PROCES-VERBAL DE L’ASSEMBLEE GENERALE SPECIALE
DES ACTIONNAIRES DU 22 MAI 2007

Ouverture de la séance

La séance est ouverte a 17h00, a Bruxelles sous la présidence de M. Guido
VANHOVE.

Composition du bureau

Le président désigne comme secrétaire Mme Anne-Frangoise LAMBOTTE.
L’assemblée désigne comme scrutateurs MM. Piet DEJONGHE et Jean-Claude
CORBISIER qui acceptent.

Ces personnes prennent place au bureau.

Dépét des piéces
Les piéces suivantes sont déposées sur le bureau :

- le rapport de gestion ainsi que le rapport du commissaire;
- le bilan, le compte de résultats ainsi que l'annexe arrétés au 31 décembre

2006;
- la liste des présences, diment signée par les mandataires des actionnaires
représentés, ainsi que les procurations de ces derniers;

Le Président demande aux scrutateurs de parapher ces documents et de signer
la liste des présences.
Constitution de I'assemblée

Cette liste des présences constate que les deux actionnaires sont représentés.
lls représentent ensemble les 45.000 actions représentatives du capital social.

L'assemblée se reconnait valablement constituée et apte a délibérer sur son
‘ordre du jour qui comporte :

4
Rapport de gestion et rapport du commissaire.
Comptes annuels au 31 décembre 2006.

Décharge a donner aux administrateurs et au commissaire.
Commissaire : renouvellement du mandat.
Constatation de la cessation du caractére indisponible d’une réserve.

akwWwNnN>

Avant d’aborder l’ordre du jour, l'assemblée ratifie a l'unanimité la décision du
conseil de tenir l'assemblée a une date différente de la date statutaire, soit ce
22 mai.

Rapport de gestion et rapport du commissaire

Le Président rappelle que chaque actionnaire a pu prendre connaissance du
rapport de gestion présenté par le conseil d'administration et du rapport du
commissaire. II propose a l'assemblée de dispenser le bureau de la lecture de
ces rapports.

Cette proposition est acceptée a l'unanimité.

Comptes annuels

Le bilan, dont le total des rubriques a I'actif et au passif s'éléve a
9.700.260,43 EUR le compte de résultats, dont le total des rubriques en charges
et en produits s'éléve a 540.541,72 EUR et l'annexe arrétés au 31 décembre
2006 sont soumis au vote de l'assemblée.

L'exercice se solde par un bénéfice de 361.278,55 EUR. Compte tenu du
bénéfice reporté de 230.182,67 EUR et du transfert d’un montant de
9.568,69 EUR des réserves indisponibles vers les réserves disponibles, le
bénéfice a affecter s’éléve a 601.029,91 EUR que le Conseil propose d’affecter
comme suit :

e Dotation a la réserve légale : 18.063,93 EUR
e Dotation aux autres réserves : 9.568,69 EUR
e Bénéfice a reporter : 573.397,29 EUR

Le bilan, le compte de résultats et l'annexe, arrétés au 31 décembre 2006 ainsi
que l'affectation sont approuvés a I'unanimité des voix.

Décharge aux administrateurs et au commissaire

Le Président met aux voix la décharge a donner aux administrateurs pour leur
gestion et au commissaire pour sa mission de contréle durant l'exercice social
2006.

L'assemblée approuve la décharge aux administrateurs a l'unanimité des voix.

2/4
10.

L'assemblée approuve la décharge au commissaire a l'unanimité des voix.
Nominations statutaires
Conseil d’administration

L’assembiée est informée de la démission de Monsieur Luc Bertrand en date du
13 novembre 2006. :

L’assembliée décide a l'unanimité des voix de ne pas pourvoir au remplacement
de Monsieur Luc Bertrand et de ramener ainsi le nombre d’administrateurs de 5
a4.

Commissaire

L’assemblée est informée que le mandat de commissaire de la SC s.f.d. SCRL
DELOITTE Réviseurs d’entreprises, vient a échéance a l'issue de la présente
assemblée.

L’assembiée approuve a l'unanimité des voix, le renouvellement, pour un
nouveau terme de trois ans, du mandat de commissaire de la SC s.f.d. SCRL
DELOITTE Réviseurs d’entreprises, représentée par M. Laurent Boxus. Ce
mandat viendra a échéance a l’issue de l’'assemblée générale ordinaire de
2010.

L'assemblée approuve également de fixer le montant des émoluments a
1.600 EUR par an non indexés.

Constatation de la cessation du caractére indisponible d’une réserve.

Le Président rappelle a l’assemblée que la constitution d’une réserve
indisponible avait été rendue obligatoire par l’arrété royal du 9 mars 1982 lequel
avait ramené le taux ISOC de 48 a 45% 4 partir de l’exercice d’imposition 1983
avec lobligation pour les sociétés de porter cette Economie d’impét a un
compte de réserve indisponible.

Cette réserve ayant été soumise a I’impdt, le maintien en réserve indisponible
ne se justifie plus.

Le Président propose a l'assemblée de constater la cessation du caractére
indisponible de ladite réserve.

L'assemblée marque accord sur cette proposition et constate la cessation de la

réserve indisponible.

Lecture du procés-verbal

Le Président prie le secrétaire de donner lecture du procés-verbal.

Aucune remarque n’étant formulée, le procés-verbal est approuvé.

3/4
11, Signature du procés-verbal

Le Président invite le bureau et les actionnaires qui le souhaitent a signer le
présent procés-verbal.

La séance est levée a 18h00.

Bk toutes

Le secrétaire Le Président Les scrutateurs

 

|
|

4/4
