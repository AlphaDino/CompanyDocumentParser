BLOMHOF
Société Anonyme

Siége Social : Boulevard Industriel, 25, 1070 Bruxelles
Registre du Commerce de Bruxelles n° 248.290
T.V.A. n° BE 405.671.420.

 

PROCES-VERBAL DE L'ASSEMBLEE GENERALE ANNUELLE DES
ACTIONNAIRES DU_10 AVREL 2001

Le 10 avril 2001, 4 11 heures, 4 1070 Bruxelles, boulevard Industriel, 25, s'est tenue
l'assemblée générale annuelle des actionnaires de BLOMHOF, société anonyme a
1070 Bruxelles.

La séance est présidée par M. Denuit
qui désigne comme secrétaire M. Van der Veken
L'assemblée choisit comme scrutateurs MM. Van Keerberghen et Oveicy

Ces Messieurs prennent place au bureau.

Monsieur le Président expose que l'assemblée est réunie conformément a 'article 26
des statuts et que les 45.000 parts sociales s.d.v.n. existantes étant représentées, il
n'y a pas lieu de justifier des convocations.

L'assemblée se reconnait valablement constituée et apte 4 délibérer sur son ordre du
jour qui comporte :

1) Rapport de gestion et rapport du commissaire ;
2) Comptes annuels au 31 décembre 2000 ;
3) Décharge aux administrateurs et au commissaire ;
4) Conseil d'administration et commissaire : élections ;
5) Capital social ;
a. conversion en Euro ;
b. augmentation du capital social ;
ce. modification de l'article 6 des statuts - capital social.

eR

Le rapport de gestion et le rapport du commissaire ainsi que les comptes annuels
sont considérés comme lus, un exemplaire de ces différents documents ayant été
remis 4 chacun des membres de I'assemblée.

Malgré la décision d'arréter l'activité principale de l'entreprise le 30 juin 1998,
l'établissement des comptes annuels sur base du principe de continuité comptable est
maintenu.
Le Président ouvre ensuite la discussion au sujet des comptes annuels arrétés au
31 décembre 2000.

Aprés délibération, M. le Président met aux voix l'approbation des comptes annuels
arrétés au 31 décembre 2000.

L'assemblée approuve ces comptes tels qu'ils lui sont présentés. L'exercice social
cl6turé au 31 décembre 2000 s'est soldé par un bénéfice de 4.758.162 BEF. Compte
tenu de la perte reportée de l'exercice précédent s'élevant 4 12.824.596 BEF, la perte
de l'exercice s'éléve 4 8.066.434 BEF. que I'assemblée générale décide de reporter.

Ces résolutions sont votées a l'unanimité.
Ensuite, a l'unanimité également, I'assemblée :

- donne décharge 4 MM. les administrateurs et commissaire de leur gestion et de
sa mission de contréle et de surveillance en ce qui concerne I'exercice cléturé au
31 décembre 2000.

- élit Messieurs Alain ALLARD, Ingénieur civil électricien mécanicien habitant
avenue de la Fosse, 2 4 1420 Braine-I'Alleud et Alain DE GENDT, candidat en
droit et comptable habitant rue de Bourgogne, 9 4 1190 Bruxelles aux fonctions

d'administrateur pour achever les mandats de Messieurs Paul-Henri DENUIT ~

(2006) et Willy VAN der VEKEN (2002).

-  élit pour un terme de trois ans, la société civile sous forme de société coopérative a
responsabilité limitée Deloitte & Touche, réviseurs d'entreprises qui désigne comme
représentant responsable Monsieur Jurgen Kesselaers, réviseur d'entreprises en
remplacement de la société civile coopérative "Ernst & Young" (B160) réviseurs
d'entreprises, représentée par Monsieur Pierre ANCIAUX, dont le mandat est venu a
échéance cette année.

- fixe les émoluments de Deloitte & Touche a 50.000 BEF/an prorata temporis.
Enfin, 4 l'unanimité, l'assemblée :

- décide de convertir le capital social de 70.000.000 BEF en 1.735.254,67 EUR
(cours de conversion 1 EUR = 40,3399 BEF) avec effet au 1% janvier 2001.

- décide d'augmenter le capital social 4 concurrence de 64.745,33 EUR pour le
porter de son montant actuel de 1.735.254,67 EUR a 1.800.000 EUR par
incorporation des réserves disponibles 4 concurrence de 64.745,33 EUR ou
2.611.820 BEF

- décide, suite aux résolutions précédentes, de modifier l'article 6 des statuts pour
avoir désormais la teneur suivante :

"article 6 - capital social

Le capital social est fixé 4 un million huit cent mille Euros et représenté par
quarante-cing mille parts sociales sans désignation de valeur nominale et
entiérement libérées."
De tout quoi il a été dressé le présent procés-verbal qui, aprés lecture, a été signé par
le Président, le Secrétaire et les Scrutateurs, aprés que le Président ait invité les
Actionnaires a le signer également.

Le Secrétaire, Le Président, Les Scrutateurs,

PES he Se
