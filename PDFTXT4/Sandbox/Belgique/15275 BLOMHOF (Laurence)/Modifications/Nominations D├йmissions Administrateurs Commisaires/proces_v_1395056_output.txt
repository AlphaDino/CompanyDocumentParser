 

BLOMHOF
Société Anonyme

Siége social : Boulevard Industriel 25 - 1070 Bruxelles
RPM numéro d'entreprise 0405.671.420

PROCES-VERBAL DE L'ASSEMBLEE GENERALE ORDINAIRE
DES ACTIONNAIRES DU 30 JUIN 2004

Ouverture de la séance

La séance est ouverte a 17h00, a Bruxelles sous la présidence de M. Patrick
LEFORT.

Composition du bureau

Le président désigne comme secrétaire Mme Anne-Frangoise LAMBOTTE.
L’assemblée désigne comme scrutateurs MM. Philippe RENARD et Piet
DEJONGHE qui acceptent.

Ces personnes prennent place au bureau.

Dépét des piéces

Les piéces suivantes sont déposées sur le bureau :

- le rapport de gestion ainsi que le rapport du commissaire;

- le bilan, le compte de résultats ainsi que l'annexe arrétés au 31
décembre 2003 ;

- la liste des présences, diiment signée par les mandataires des
actionnaires représentés, ainsi que les procurations de ces derniers;

Le Président demande aux scrutateurs de parapher ces documents et de
signer la liste des présences.

Constitution de l'assemblée

Cette liste des présences constate que les deux actionnaires sont
représentés. lls représentent ensemble les 45.000 actions représentatives

du capital social ; il n’y a donc pas lieu de justifier des convocations.

L'assemblée se reconnait valablement constituée et apte a délibérer sur
son ordre du jour qui comporte :

1/3
1. Rapport de gestion

2. Rapport du commissaire

3. Comptes annuels au 31.12.2003

4. Décharge a donner aux administrateurs et au commissaire

5. Nominations statutaires : conseil d’administration, commissaire

Avant d’aborder l’ordre du jour, l'assemblée ratifie a l'unanimité la décision du
conseil de tenir l'assemblée a une date différente de la date statutaire, soit ce
30 juin.

Rapport de gestion et rapport du commissaire

Le Président rappelle que chaque actionnaire a pu prendre connaissance
du rapport de gestion présenté par le conseil d'administration et du rapport
du commissaire., |] propose a l'assemblée de dispenser le bureau de la
lecture de ces rapports.

Cette proposition est acceptée a l'unanimité.

Comptes annuels

Le bilan, dont le total des rubriques a l'actif et au passif s'éléve a
38.893.085,11 EUR, le compte de résultats, dont le total des rubriques en
charges et en produits s'éléve 4 807.345,85 EUR et l'annexe arrétés au
31 décembre 2003 ainsi que l'affectation ci-dessous sont soumis au vote de
l'assemblée.

Perte de l'exercice : 326.668,02 EUR

Bénéfice reporté de !’exercice précédent : §21.762,22 EUR
Solde bénéficiaire : 495.094,20 EUR

L’assemblée décide de reporter la perte.

Décharge aux administrateurs et au commissaire

Le Président met aux voix la décharge a donner aux administrateurs pour
leur gestion et au commissaire pour sa mission de contréle durant
l'exercice social 2003.

L'assemblée approuve la décharge aux administrateurs a |'unanimité des
voix.

L'assemblée approuve la décharge au commissaire a l'unanimité des voix.
2
8. Nominations statutaires

L’assemblée procéde a l’élection définitive, en qualité d’administrateur, de
M. Guido Vanhove, nommé provisoirement par le conseil, le 10 décembre 2003
pour achever le mandat de M. Alain Allard, démissionnaire. Ce mandat viendra
a échéance a l’'assemblée générale ordinaire de 2008.

L'assemblée procéde a I’élection définitive, en qualité d’administrateur, de
M. Philippe Renard, nommé provisoirement par le conseil, le 10 décembre 2003
pour achever le mandat de M. Eric Van Keerberghen, démissionnaire. Ce
mandat viendra a échéance a l'assemblée générale ordinaire de 2008.

L’assemblée est informée que le mandat de commissaire de la société
DELOITTE & TOUCHE Réviseurs d’entreprises, vient a échéance a l’issue de
la présente assemblée.

L’assemblée approuve a l’unanimité des voix, le renouvellement, pour un
nouveau terme de trois ans, du mandat de commissaire de la société
DELOITTE & TOUCHE Réviseurs d’entreprises, représentée par M. Michel
Denayer. Ce mandat viendra a échéance a lissue de l’'assemblée générale
ordinaire de 2007.

L’assemblée approuve également de fixer le montant des €moluments a 1.600
EUR par an non indexés.

9. Lecture du procés-verbal

Le Président prie le secrétaire de donner lecture du procés-verbal.

Aucune remarque n'étant formulée, le procés-verbal est approuve.

10. Signature du procés-verbal

Le Président invite le bureau et les actionnaires qui le souhaitent a signer le
présent procés-verbal.

La séance est levée a 18h00.

HRKRKRERRERARR

MS Pe
He Rea udosa ‘ Ah m4,

Le secrétaire Le Président Les scrutateurs

 
