Zandvliet Power nv
Scheldelaan 600
2040 Antwerpen

Ondernemingsnummer 0477.543.470
Uittreksel uit de notulen van de Raad van bestuur van 25 september 2023

De Raad van bestuur neemt akte van het ontslag als algemeen directeur van de heer Willem
Vandamme per 1 oktober 2023.

De Raad van bestuur beslist — op voordracht van de A-aandeelhouder - de heer Joris Quix met ingang
van 1 oktober 2023 te benoemen als algemeen directeur. Het mandaat zal verstrijken op 30 april 2027.

Bevoegdheid inzake dagelijks bestuur

Onverminderd de algemene vertegenwoordiging van de vennootschap in alle handelingen door twee
bestuurders van de vennootschap, heeft de Raad van bestuur, overeenkomstig artikel 23 van de
statuten, de vertegenwoordiging van de vennootschap met betrekking tot het dagelijks bestuur aan
volgende personen gedelegeerd:

De heren

Joris Quix (voorgedragen door de A-aandeelhouder)

Eric Verrijdt (voorgedragen door de B-aandeelhouder)

("Algemeen Directeuren").

Deze personen handelen twee aan twee.

De bevoegdheden inzake het dagelijks bestuur zijn beperkt tot een bedrag van 250.000,00 euro.

>
ae
ofaerts

agthebber
