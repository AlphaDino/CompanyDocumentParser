GDF SUEZ CC
Coéperatieve vennootschap met beperkte aansprakelijkheid
1000 Brussel, Troonplein 1.
RPR Ondernemingsnummer: 0442.100.363

BENOEMING VAN BESTUURDERS

Het jaar tweeduizend en twaalf, op 14 september 2012, om 10.00 uur.
Te Brussel, in de zetel van de vennootschap,

WERD GEHOUDEN:
De buitengewone algemene vergadering der vennoten van de codperatieve vennootschap met
beperkte aansprakelijkheid GDF SUEZ CC, gevestigd te 1000 Brussel, Troonplein 1,
ingeschreven in het rechtspersonenregister van Brussel onder nummer 0442.100.363.
Vennootschap opgericht onder de rechtsvorm van een naamloze vennootschap op 31 juli 1990,
bij akte verleden voor Notaris Jean-Luc Indekeu te Brussel, bij uittreksel verschenen in de
bijlagen tot het Belgisch Staatsblad van 19 oktober 1990, onder nummer 901019-135.

 

De vennootschap werd omgezet in een codperatieve vennootschap met beperkte
aansprakelijkheid bij besluit van de buitengewone algemene vergadering van 24 december
2002. De statuten werden voor het laatst gewijzigd, bij besluit van de buitengewone algemene
vergadering van vennoten van 25 oktober 2011, bij akte verleden voor Notaris Damien Hisette
te Brussel, bekendgemaakt in de bijlagen tot het Belgisch Staatsblad van 29 november 2011
onder nummer 11179085.
Bureau

De zitting wordt geopend onder het voorzitterschap van de heer Guido De Clercq
De voorzitter duidt als secretaris aan de heer Michael Delmée.
De vergadering kiest als stemopnemers:

1) de heer Thierry van den Hove;

2) de heer Guido Vanhove.

Samenstelling van de vergadering
Aanwezigheidslijst
De algemene vergadering wordt samengesteld uit de hiernagenoemde vennoten, die volgens
hun verklaringen het aantal aandelen bezitten zoals hierna vermeld:
Aantal aandelen

1. de codperatieve vennootschap met beperkte aansprakelijkheid

GDF SUEZ Belgium, gevestigd te B- 1000 Brussel, Troonplein 1, 1
2. de naamloze vennootschap Electrabel, gevestigd te

B- 1000 Brussel, Simon Bolivarlaan 34, 1
3. de naamloze vennootschap C.E.F., gevestigd te

L-1611 Luxemburg, 65 avenue de la Gare 3.429.616
TEZAMEN : 3.429.618

1/7
 

Volmachten

De vennoot sub 1 is hier vertegenwoordigd door de heer Michael Delmée, krachtens een
volmacht hier aangehecht.
De vennoot sub 2 is hier vertegenwoordigd door de heer Thierry van den Hove, krachtens een
volmacht hier aangehecht.
De vennoot sub 3 is hier vertegenwoordigd door de heer Guido Vanhove, krachtens een
volmacht hier aangehecht.

Toelichting van de voorzitter

De voorzitter leidt de vergadering in en zet het volgende uiteen :

I. Het vaste gedeelte van het maatschappelijk kapitaal van de vennootschap

bedraagt 1.250.000 EUR. Het ingeschreven kapitaal - vaste en veranderlijke gedeelte
samengevoegd - bedraagt 857.404.500 EUR, verdeeld in 3.429.618 aandelen zonder
vermelding van nominale waarde.

Il. Uit de aanwezigheidslijst blijkt dat alle vennoten op de vergadering

vertegenwoordigd zijn. Aangezien alle vennoten vertegenwoordigd zijn, dient de verzending
van geschreven oproepingsbrieven niet te worden aangetoond. De vergadering is aldus
bevoegd om rechtsgeldig te beraadslagen en geldige besluiten te treffen.

TI. Alle vennoten hebben zich akkoord verklaard dat de vergadering beraadslaagt

over de volgende agenda :

AGENDA

STATUTAIRE BENOEMINGEN

Teneinde:

de heer Guido De Clercq definitief tot bestuurder (op voorstel van titularis van aandelen
«C»), te benoemen om de opdracht van de heer Etienne Jacolin, ontslagnemend, te
voltooien;

het aantal bestuurders te verhogen tot 15 en, in die hoedanigheid en op voorstel van
titularis van aandelen C, de heren Pierre Chareyre, Marc Janssens de Varebeke, Patrick
van der Beken en Sergio Val te benoemen.

Vaststelling

De toelichting van de voorzitter wordt door alle aanwezige leden van de vergadering als juist
erkend.

Beraadslagingen - Besluiten

Hierna neemt de vergadering de agenda in behandeling en treft, na beraadslaging de volgende
besluiten:

BENOEMING VAN BESTUURDERS

 

 

Besluit

Op voorstel van de titularis van aandelen C beslist de vergadering:

de heer Guido De Clercq, wonende Bogaardenstraat 13 te 3000 Leuven, voorlopig tot
bestuurder benoemd door de raad van bestuurder van 31 augustus 2012 definitief te
benoemen om de opdracht van de heer Etienne Jacolin, ontslagnemend, te voltooien.
Deze opdracht zal een einde nemen na afloop van de gewone algemene vergadering van
2014.

2/7
- het aantal bestuurders te verhogen tot vijftien en in die hoedanigheid:
v de heer Pierre Chareyre, wonende 3 rue David te 75116 Paris (Frankrijk),
v¥ de heer Mare Janssens de Varebeke, wonende Bosstraat 90 te 1150 Sint-Picters-
Woluwe,
v de heer Patrick van der Beken, wonende Chemin du Bois de Hal 91 te 1420 Braine-
1’ Allieud,
v de heer Sergio Val, wonende 6 rue René Bazin te 75016 Paris (Frankrijk),
te benoemen.
Deze opdrachten zullen onmiddellijk na de gewone algemene vergadering van het jaar
2018 verstrijken.
De opdrachten van bestuur zijn niet bezoldigd.
Overeenkomstig artikel 21 van de statuten wordt de vennootschap geldig in en buiten rechte
vertegenwoordigd door twee bestuurders die gezamenlijk optreden.
Stemming
Deze besluiten worden aangenomen met eenparigheid van stemmen.

 

 

Sluiting van de zitting
De agenda volledig afgehandeld zijnde, verklaart de voorzitter de zitting geheven.

WAARVAN PROCES-VERBAAL
Opgesteld te Brussel, op voormelde datum en plaats.
Na gedane voorlezing hebben de leden van het bureau deze notulen getekend.

(volgt de Franse tekst)

3/7
 

GDF SUEZ CC
Société coopérative a responsabilité limitée
1000 Bruxelles, Place du Tréne 1
RPM Numéro d’Entreprise : 0442.100.363

NOMINATION D’ADMINISTRATEURS

 

L'an deux mille douze, le 14 septembre, 4 10h00.
A Bruxelles, au siége social
S'EST REUNIE:
L'assemblée générale extraordinaire des associés de la société coopérative 4 responsabilité
limitée GDF SUEZ CC, établie a 1000 Bruxelles, Place du Tréne 1, immatriculée au registre
des personnes morales 4 Bruxelles sous le numéro 0442.100.363.
Société constituée sous la forme d’une société anonyme le 31 juillet 1990, suivant acte regu
par le Notaire Jean-Luc Indekeu 4 Bruxelles, publié par extrait aux annexes du Moniteur
belge du 19 octobre 1990 sous le numéro 901019-134.
La société a été transformée en société coopérative a responsabilité limitée par décision de
Yassemblée générale extraordinaire du 24 décembre 2002. Les statuts ont été modifiés en
dernier lieu par décision de l'assemblée générale extraordinaire des associés du 25 octobre
2011 ; la publication par extrait a été faite aux annexes du Moniteur belge du 29 novembre
2011 sous le numéro 11179084.
Bureau
La séance est ouverte sous la présidence de Monsieur Guido De Clercq.
Le président désigne comme secrétaire Monsieur Michael Delmée.
L'assemblée choisit comme scrutateurs :
1. Monsieur Thierry van den Hove;
2. Monsieur Guido Vanhove.

Composition de l'assemblée

Liste des présences
L'assemblée générale est composée des associés cités ci-dessous, lesquels déclarent détenir le
nombre de parts sociales ci-aprés indiqué :

Nombre de parts sociales

1. la société coopérative 4 responsabilité limitée GDF SUEZ Belgium,

établie 4 B-1000 Bruxelles, place du Tréne 1, 1
2. la société anonyme Electrabel, établie a

1000 Bruxelles, boulevard Simon Bolivar 34, 1
3. la société anonyme C.E.F., établie a

L-1611 Luxembourg, 65 avenue de la Gare 3.429.616
SOIT AU TOTAL: 3.429.618

4/7
Procurations
L’associé sous 1 est valablement représenté par Monsieur Michael Delmée, en vertu d’une
procuration ci-annexée.
L’associé sous 2 est valablement représenté par Monsieur Thierry van den Hove, en vertu
dune procuration ci-annexée.
L’associé sous 3 est valablement représenté par Monsieur Guido Vanhove, en vertu d’une
procuration ci-annexée.

Exposé du président
Le président dirige les débats et expose ce qui suit :

I. La partie fixe du capital social de la société s'éléve 4 1.250.000 EUR. Le
capital souscrit - partie fixe et partie variable cumulées - s’établit 4 857.404.500 EUR divisé
en 3.429.618 parts sans mention de valeur nominale.

I. Il résulte de la liste de présence que tous les associés sont représentés a
l'assemblée. Tous les associés étant représentés, il n'a pas lieu de justifier de l’envoi de
convocations écrites. L’assemblée est habilitée a délibérer et 4 statuer valablement.

 
 

TI. Tous les associés se sont déclarés d'accord que l'assemblée délibére sur l'ordre
du jour suivant :
ORDRE DU JOUR
NOMINATIONS STATUTAIRES

Afin de:

- nommer définitivement Monsieur Guido De Clercq en qualité d’administrateur (sur
proposition de titulaires de parts «C »), pour achever le mandat de Monsieur Etienne
Jacolin, démissionnaire;

- d’augmenter le nombre d’administrateurs 4 15 et de nommer en cette qualité, sur proposition de
titulaires de parts « C », Messieurs Pierre Chareyre, Marc Janssens de Varebcke, Patrick van der
Beken et Sergio Val.

Constatation
L'exposé du président est approuvé par tous les membres présents de l'assemblée.
Délibérations — Résolutions
L'assemblée aborde son ordre du jour et, aprés délibération, prend la résolution suivante:

NOMINATION D’ADMINISTRATEURS

 

 

 

 

Résolution
L’ Assemblée décide, sur proposition des titulaires de parts C :
- de nommer définitivement Monsieur Guido De Clercq, demeurant Bogaardenstraat 13 a
3000 Leuven, appelg brgyisoirement en qualité d’administrateur par le conseil
administration du #-septerabre 2012 pour achever le mandat de Monsieur Etienne
Jacolin, démissionnaire. Ce mandat prendra fin immédiatement a l’issue de l’assemblée
générale ordinaire de 2014.

5/7
- de porter le nombre d’administrateurs 4 quinze et de nommer en cette qualité :

Y Monsieur Pierre Chareyre, demeurant 3 rue David 4 75116 Paris (France),

v Monsieur Mare Janssens de Varebeke, demeurant Rue au Bois 90 4 1150 Woluwe-

Saint-Pierre,
~yY Monsieur Patrick van der Beken- Latest Chemin du Bois de Hal 91 te 1420 Braine-

, l’Alleud,

v Monsieur Sergio Val, demeurant 6 rue René Bazin — 75016 Paris (France).

Ces mandats prendront fin immédiatement a l’issue de l’assemblée générale ordinaire de

Pannée 2018.
Les mandats d’administrateur ne sont pas rémunérés.
Conformément 4 l'article 21 des statuts, la société est valablement représentée en justice et
dans les actes par deux administrateurs, agissant conjointement.

Vote
Ces décisions sont prises a l'unanimité des voix.
Cléture de la séance

L'ordre du jour étant épuisé, le président déclare la séance levée.

DONT PROCES-VERBAL
Fait et dressé 4 Bruxelles, date et lieu que dessus.
Lecture faite, les membres du bureau ont signé ce procés-verbal.

= [de f)- fe la«

6/7

 
w

Liste des présences / Aanwezigheidslijst

Qo nr. /

?

GDF SUEZ Belgium ELECTRABEL
représentée par / vertegenwoordigd door représentée par / vertegenwoordigd door
Michael Delmée Thierry van den Hove

    

CE.F.
épar / vertegenwoordigd door
Guido Vanhove

VT
