# CompanyDocumentParser

First launch setup file.

It installs Tesseract OCR, that will allow you to transform PDF into TXT files.
You can test it using OCRextract2.py program, giving as a parameter an input folder with PDF and an output folders that generates TXT. 
The TXT generated in our sample examples are already committed in PDFTXT.

Then install https://github.com/nvm-sh/nvm
We use LLM grammar to extract information about document, something not possible in GPT4.
For that, we use the tool https://github.com/synw/infergui, an hardcoder tool that takes the prompt templates as an input and is space sensitive.

Infergui interacts with koboldcpp and can test inference on many LLM models.
To have realistic inference time, you must compile koboldcpp with GPU option.
For french documents, we identified https://huggingface.co/TheBloke/Vigostral-7B-Chat-GGUF mastering french with good overall performance.

Infergui supports grammars, and an example tested grammar is 
```
interface Company {
         name: string;
         activity: string;
         revenue: string;
         registration_date: string;
         leaders: Array<string>;
         siret: string;
     }
```
or for french company documents:
```
interface Grammar {
  nom: string;
  capital: string;
  siege_social: string;
  forme_de_societe: string;
  denomination: string;
}
```
Whenever the PDF is too hard to analyze (complex tables), we favor the LayoutLM approach that has been trained with both document layout and content.
Read the paper: https://github.com/sjehan/CompanyDocumentParser/blob/main/PAPERS/LayoutML.pdf

A list of questions queried from the LayoutLM LLM is available in https://github.com/sjehan/CompanyDocumentParser/blob/main/LLMExtractor.sh
