import os
import sys
from chromadb import ChromaDB

def chunk_text(text, chunk_size):
    chunks = []
    for i in range(0, len(text), chunk_size):
        chunks.append(text[i:i+chunk_size])
    return chunks

def get_parent_folders(folder_path):
    folders = []
    while True:
        folder_path, folder = os.path.split(folder_path)
        if folder == "":
            break
        folders.append(folder)
    folders.reverse()
    return folders

def parse_folder(folder_path, chunk_size, db_path):
    db = ChromaDB(db_path)
    for root, dirs, files in os.walk(folder_path):
        parent_folders = get_parent_folders(root[len(folder_path):].lstrip(os.path.sep))
        for file in files:
            if file.endswith(".txt"):
                file_path = os.path.join(root, file)
                with open(file_path, "r") as f:
                    text = f.read()
                chunks = chunk_text(text, chunk_size)
                for chunk in chunks:
                    db.insert(chunk, metadata={"parent_folders": parent_folders})
    db.close()

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python script.py <folder_path> <chunk_size> <db_path>")
    else:
        folder_path = sys.argv[1]
        chunk_size = int(sys.argv[2])
        db_path = sys.argv[3]
        parse_folder(folder_path, chunk_size, db_path)