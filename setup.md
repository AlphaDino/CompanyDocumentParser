#!/bin/bash

# TESSERACT AND PYTHON SETUP
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt -y upgrade
python3 -V
curl -sSL https://install.python-poetry.org | python3 -
poetry --version
export PATH="/root/.local/bin:$PATH"
poetry --version
git clone https://github.com/VikParuchuri/marker.git
cd marker
sudo apt-get install apt-transport-https
echo "deb https://notesalexp.org/tesseract-ocr5/$(lsb_release -cs)/ $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/notesalexp.list > /dev/null
poetry install
poetry shell
apt install tesseract-ocr
pip install docquery
apt install python3-pip
apt install python3.11-venv

# DOCQUERY SETUP
python3 -m venv /root/pvenv
source /root/pvenv/bin/activate
pip install docquery
source /root/pvenv/bin/activate
pip install pydantic==1.10
sudo apt-get update
sudo apt-get install -y poppler-utils
pip install pdf2image
pip uninstall transformers
pip install transformers==4.23
docquery scan "quel est le capital de BLOMHOF" '/root/engie/CompanyDocumentParser/PDF/Blomhof_statuts 20221201.pdf'
docquery scan "ou est le siege de la societe" '/root/engie/CompanyDocumentParser/PDF/Blomhof_statuts 20221201.pdf'
docquery scan "nombre d'actions souscrites" '/root/engie/CompanyDocumentParser/PDF/72 - Liste des souscripteurs datée signée.pdf' 
docquery scan "capital" '/root/engie/CompanyDocumentParser/PDF/72 - Liste des souscripteurs datée signée.pdf' 

# OPEN SOURCE LLM SETUP
git clone https://github.com/LostRuins/koboldcpp
cd koboldcpp
make LLAMA_CUBLAS=1
# IF YOU DON'T HAVE GPU USE make
cd ..
mkdir MODELS
cd MODELS
# The best english model 7B
wget https://huggingface.co/TheBloke/Mistral-7B-Instruct-v0.1-GGUF/resolve/main/mistral-7b-instruct-v0.1.Q5_K_M.gguf
# The best  french model 7B
wget https://huggingface.co/TheBloke/Vigostral-7B-Chat-GGUF/resolve/main/vigostral-7b-chat.Q5_K_M.gguf


# SETUP THE CUDA KOBOLDCPP AND UTILITIES
cd koboldcpp
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.0-1_all.deb
sudo dpkg -i cuda-keyring_1.0-1_all.deb

wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.0-1_all.deb
sudo apt-get install cuda-12-3
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
wget https://developer.download.nvidia.com/compute/cuda/11.6.1/local_installers/cuda-repo-ubuntu2004-11-6-local_11.6.1-470.82.01-1_amd64.deb
sudo dpkg -i cuda-repo-ubuntu2004-11-6-local_11.6.1-470.82.01-1_amd64.deb
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
wget https://developer.download.nvidia.com/compute/cuda/11.6.1/local_installers/cuda-repo-ubuntu2004-11-6-local_11.6.1-470.82.01-1_amd64.deb
sudo apt-get update
sudo apt-get -y install cuda
lspci | grep VGA
sudo ubuntu-drivers devices
sudo ubuntu-drivers autoinstall

make LLAMA_CUBLAS=1 LLAMA_OPENBLAS=1 LLAMA_CLBLAST=1
sudo apt-get install libopenblas-dev
make LLAMA_CUBLAS=1 LLAMA_OPENBLAS=1 LLAMA_CLBLAST=1
sudo apt-get install opencl-headers
cd ..
git clone https://github.com/CNugteren/CLBlast.git
cd CLBlast
mkdir build
cd build
sudo snap install cmake
sudo snap install cmake --classic
sudo apt-get install opencl-headers ocl-icd-opencl-dev
cmake ..
make
cd ..
cd koboldcpp
make LLAMA_OPENBLAS=1
pip install backoff requests posthog opentelemetry-proto opentelemetry-exporter-otlp-proto-common wrapt deprecated zipp importlib-metadata opentelemetry-api opentelemetry-semantic-conventions opentelemetry-sdk grpcio googleapis-common-protos opentelemetry-exporter-otlp-proto-grpc pulsar-client click typer PyYAML bcrypt humanfriendly coloredlogs flatbuffers onnxruntime oauthlib requests-oauthlib cachetools pyasn1 pyasn1-modules rsa google-auth kubernetes tenacity opentelemetry-instrumentation opentelemetry-util-http asgiref opentelemetry-instrumentation-asgi opentelemetry-instrumentation-fastapi mmh3 pypika overrides chroma-hnswlib sniffio exceptiongroup anyio starlette fastapi importlib-resources h11 watchfiles httptools websockets python-dotenv uvloop uvicorn graphlib-backport chromadb
#INSTALL FRONT STUFF
curl http://get.nocodb.com/linux-x64 -o nocodb -L && chmod +x nocodb && ./nocodb


